<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\AnswerRecord;
use App\usertable;
use App\adminList;
use App\availableprizes;
use App\OurWinner;
use App\OurCampaign;
use App\SpecialPrize;
use App\CampaignQuestions;
use App\Clients;
use Session;
use DB;
use Carbon\Carbon;
use App\SessionKeeper;
use App\FormBuilder;
use App\BalanceHistorys;
use App\CashRequest;
use PHPMailer;

class ClientController extends Controller {

    //


  //  public function ClientPage() {
    //    return view('ClientPage');
  // 

    public function RegisterPage() {

        return view('RegisterPage');
    }

public function testshare()
{

return view('testshare');


}




	public function clientPage()
	{
	if(Session::get('clientLogin') == null)
    {
    
      return Redirect::to('clientLogin');
    }
	         
    $campaigns = DB::select("SELECT * from our_campaigns where ClientID = '".Session::get('clientLogin')."' and Status = 1 order by EndDate desc");
    $leads = array();
    $nonLeads = array();
    $clients = Clients::where('Status', '=', 1);
    foreach($campaigns as $eachCampaign){
    $leads[] = DB::select("SELECT distinct UserID from answer_records where CampaignID = ".$eachCampaign->id." and LeadStatus = 1 ");
    $nonLeads[] = DB::select("SELECT distinct UserID from answer_records where CampaignID = ".$eachCampaign->id." and LeadStatus = 0 ");
    }
      //  $campaigns = OurCampaign::where('ClientID', '=',  Session::get('clientLogin'))->where('Status', '=', 1)->get();
        $clients = Clients::where('Status', '=', 1);
        return view("ViewLeads")
                        ->with('campaigns', $campaigns)
                        ->with('clients', $clients)
                        ->with(compact('leads', 'leads'))
                        ->with(compact('nonLeads', 'nonLeads'));
                        
	
	}

  public function ViewLeads()
    {
    if(Session::get('clientLogin') == null)
    {
      return Redirect::to('clientLogin');
    }
    $campaigns = DB::select("SELECT * from our_campaigns where ClientID = '".Session::get('clientLogin')."' and Status = 1 order by EndDate desc");
    $leads = array();
    $nonLeads = array();
    foreach($campaigns as $eachCampaign){
    $leads[] = DB::select("SELECT distinct UserID from answer_records where CampaignID = ".$eachCampaign->id." and LeadStatus = 1 ");
    $nonLeads[] = DB::select("SELECT distinct UserID from answer_records where CampaignID = ".$eachCampaign->id." and LeadStatus = 0 ");
    }
      //  $campaigns = OurCampaign::where('ClientID', '=',  Session::get('clientLogin'))->where('Status', '=', 1)->get();
        $clients = Clients::where('Status', '=', 1);
        return view("ViewLeads")
                        ->with('campaigns', $campaigns)
                        ->with('clients', $clients)
                        ->with(compact('leads', 'leads'))
                        ->with(compact('nonLeads', 'nonLeads'));
    }     
   

public function clientAuth(Request $request)
{
  $checkClient = Clients::where('Email', '=', $request->username)
                ->where('Password', '=', md5($request->password))
                ->where('FirstTimeUser', '=', 0)
                ->first();
        if ($checkClient ) {
            Session::put('clientLogin', $checkClient ->id);
            return Redirect::to('/ClientDashBoard');
        }

        return Redirect::to('/clientLogin')->with('message', 'Error');
}


    public function clientLogout() {
        Session::flush();
        return Redirect::to('clientLogin');
    }

   
     public function HowToWin()
     {
         return view("HowToWin");
     }

 public function homeOption()
     {
 $NigerianStates = array(
            'Abuja',
            'Abia',
            'Adamawa',
            'Akwa Ibom',
            'Anambra',
            'Bauchi',
            'Bayelsa',
            'Benue',
            'Borno',
            'Cross River',
            'Delta',
            'Ebonyi',
            'Edo',
            'Ekiti',
            'Enugu',
            'Gombe',
            'Imo',
            'Jigawa',
            'Kaduna',
            'Kano',
            'Katsina',
            'Kebbi',
            'Kogi',
            'Kwara',
            'Lagos',
            'Nassarawa',
            'Niger',
            'Ogun',
            'Ondo',
            'Osun',
            'Oyo',
            'Plateau',
            'Rivers',
            'Sokoto',
            'Taraba',
            'Yobe',
            'Zamfara',
        );
        $stateArray = "<option value=''>Your state of residence</option>";
        foreach ($NigerianStates as $k) {
            $stateArray = $stateArray . "<option value='" . $k . "'>" . $k . "</option>";
        }

        $allPrize = availableprizes::select('PrizeName')->get();
        $allWInners = OurWinner::all();
        return view('homeOption')->with('allStates', $stateArray)->with(compact('allPrize', 'allPrize'))
                        ->with(compact('allWInners', 'allWInners'));
        
     }
     
     
     public function HowDoIWin()
     {
  /*   if(Session::get('logedIn') == null){
 return Redirect::to('');

}
            $checkInfo = usertable::where('facebook_id', '=', Session::get('logedIn'))->first();
            if ($checkInfo == null) {
                $checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
            }

            if ($checkInfo->UserImg != null && $checkInfo->UserImg != "") {
                Session::put('DP', $checkInfo->UserImg);
            } else if ($checkInfo->facebook_id != null && $checkInfo->facebook_id != "" && $checkInfo->facebook_id != "Direct Reg.") {
                Session::put('DP', $checkInfo->facebook_id);
            } else {
                Session::put('DP', 'noDP');
            }*/
       return view("HowDoIWin");
// ->with('DP', @Session::get('DP'));
     }
     
     public function PrivacyPolicy()
     {
/* if(Session::get('logedIn') == null){
 return Redirect::to('');

}
            $checkInfo = usertable::where('facebook_id', '=', Session::get('logedIn'))->first();
            if ($checkInfo == null) {
                $checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
            }

            if ($checkInfo->UserImg != null && $checkInfo->UserImg != "") {
                Session::put('DP', $checkInfo->UserImg);
            } else if ($checkInfo->facebook_id != null && $checkInfo->facebook_id != "" && $checkInfo->facebook_id != "Direct Reg.") {
                Session::put('DP', $checkInfo->facebook_id);
            } else {
                Session::put('DP', 'noDP');
            }*/
       return view("PrivacyPolicy");
      //  ->with('DP', @Session::get('DP'));
     }
     
     public function TermsAndCondition()
     {
      /*if(Session::get('logedIn') == null){
 return Redirect::to('');

}
            $checkInfo = usertable::where('facebook_id', '=', Session::get('logedIn'))->first();
            if ($checkInfo == null) {
                $checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
            }

            if ($checkInfo->UserImg != null && $checkInfo->UserImg != "") {
                Session::put('DP', $checkInfo->UserImg);
            } else if ($checkInfo->facebook_id != null && $checkInfo->facebook_id != "" && $checkInfo->facebook_id != "Direct Reg.") {
                Session::put('DP', $checkInfo->facebook_id);
            } else {
                Session::put('DP', 'noDP');
            }*/
       return view("TermsAndCondition");
       // ->with('DP', @Session::get('DP'));
     
     }
     
     public function GeneralRules()
     {
     /* if(Session::get('logedIn') == null){
 return Redirect::to('');

}
        $checkInfo = usertable::where('facebook_id', '=', Session::get('logedIn'))->first();
            if ($checkInfo == null) {
                $checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
            }

            if ($checkInfo->UserImg != null && $checkInfo->UserImg != "") {
                Session::put('DP', $checkInfo->UserImg);
            } else if ($checkInfo->facebook_id != null && $checkInfo->facebook_id != "" && $checkInfo->facebook_id != "Direct Reg.") {
                Session::put('DP', $checkInfo->facebook_id);
            } else {
                Session::put('DP', 'noDP');
            }*/
       return view("GeneralRules");
       // ->with('DP', @Session::get('DP'));
     
     }
     
     public function clientLogin($response = null)
     {
     
      if(!$response || $response == ""){
     return view("clientLogin");
     }
      if($response && $response != ""){
     $getClient = Clients::where('CompanyName', '=', $response)->first();
     if($getClient)
     {
      if($getClient->FirstTimeUser == 1){
     return view("clientLogin")->with('FirstTimer', 1);
}
return view("clientLogin");
     }
return view("clientLogin");
     }
}

    public function getParticipant(Request $request)
    {
    
    
 $getLeads = DB::select("SELECT distinct UserID from answer_records where CampaignID = ".$request->campId." and LeadStatus = 1 ");

 $allUsers = array();
 $leads = array();
 $aggregate = array();
 
     foreach ($getLeads as $eachUserID) {
             
            $leads[] = DB::select("SELECT * from usertables where id = ".$eachUserID->UserID);
           
        }
     
    return response()->json($leads);
    
    }
    
 public function getallquest(Request $request) {
        $getAllQuest = DB::select("SELECT * FROM campaign_questions WHERE CampaignID = '" . $request->campId . "'");
        $allQuest = " <table class='table table-striped table-bordered table-hover' id=''><thead><tr><th>Count</th><th colspan='2'>Question</th><tr></thead><tbody>";
        $count = 0;
        foreach ($getAllQuest as $eachQuest) {
            $count++;
            $allQuest = $allQuest . '<tr><td>' . $count . '</td><td colspan="2">' . $eachQuest->Question . '<td></tr>';
        }
        return response()->json($allQuest . '</tbody></table>');
    }

   
    
    public function WinMoney_PrivacyPolicy()
    {
      return view('WinMoney_PrivacyPolicy');
    }
    
    
    
     public function Winmoney_GeneralRules()
    {
      return view('Winmoney_GeneralRules');
    }
    
     public function Winmoney_HowToWin()
    {
      return view('Winmoney_HowToWin');
    }
    
    
     public function Winmoney_TermsAndCondition()
    {
      return view('Winmoney_TermsAndCondition');
    }

 
  public function ClientDashBoard()
  {
  
  
  if(Session::get('clientLogin') == null)
    {
    
      return Redirect::to('clientLogin');
    }
    
    //get campaign info to dashboard EndDate > " . Carbon::now()->format('Y-m-d')."
  $Campaigns = OurCampaign::where('ClientID', '=', Session::get('clientLogin'))->take(20)->get();
  
    //get campaign count dashboard
      
  //get campaign info to dashboard 
  $countOngoingCampaigns = count(OurCampaign::where('ClientID', '=', Session::get('clientLogin'))->where('EndDate', '>=',  Carbon::now()->format('Y-m-d'))->get());
  $countEndedCampaigns = count(OurCampaign::where('ClientID', '=', Session::get('clientLogin'))->where('EndDate', '<',  Carbon::now()->format('Y-m-d'))->get());
  
  $AllCampaigns = OurCampaign::where('ClientID', '=', Session::get('clientLogin'))->get();
  
  
  //get count winners
  $userArray = array();
  
  foreach($AllCampaigns as $eachOfCamp)
  {
  
  $leads = AnswerRecord::where('CampaignID', '=', $eachOfCamp->id)->where('LeadStatus', '=', 1)->first();
  if($leads)
  {
  
  $users = usertable::where('id', '=', $leads->UserID)->first();
  $userArray[] = $users;
  
  }
  
  }
  
 
  //get count winners
  $winnerArray = array();
  
  foreach($AllCampaigns as $eachOfCamp)
  {
  
  $winner = OurWinner::where('CampaignID', '=', $eachOfCamp->id)->first();
  
  if($winner)
  {
  
  //$getWinners = usertable::find($winner->UserID);
  $winnerArray[] = $winner;
  
  }
  
  }
   
   $specialPrizes = OurCampaign::where('ClientID', '=', Session::get('clientLogin'))->where('IfSpecial', '=', 1)->take(20)->get();
  
  return view('ClientDashBoard')
  ->with(compact('Campaigns', 'Campaigns'))
  ->with(compact('userArray', 'userArray'))
  ->with('ongoingCampSize', $countOngoingCampaigns)
  ->with('endedCampSize', $countEndedCampaigns)
  ->with('leadSize', count($userArray))
  ->with(compact('winnerArray', 'winnerArray'))
  ->with(compact('specialPrizes', 'specialPrizes'))
  ->with('winnerSize', count($winnerArray));
  
  }
  
  
   public function checkPrizeAvailable(Request $request)
  {
  
  $checkAvailable = AnswerRecord::where('UserID', '=', Session::get('logedIn'))->where('CampaignID', '=', $request->prizeId)->first();
  if(!$checkAvailable)
  {
  
  return response()->json('Available');
  }
  return response()->json('Not Available');
  
  }
  
  
  public function mailSender($subject, $body, $to, $from)
  {
  
  require_once('../kudipoll/PHPMailer_5.2.0/class.phpmailer.php');
 
                  $mail             = new PHPMailer();
                  $mail->IsSMTP();
                  $mail->SMTPAuth   = true;
                  $mail->Host       = "box1030.bluehost.com";
                  $mail->Port       = 465;
                  $mail->Username   = $from;
                  $mail->Password   = "Winmoney+";
                  $mail->SMTPSecure = 'ssl';
                  $mail->SetFrom($from, 'Kudipoll Nigeria');
                  $mail->AddReplyTo($from,"Kudipoll Nigeria");
                  $mail->Subject    = $subject;
                 // $mail->AltBody    = "Any message.";
                  $mail->MsgHTML($body);
                  $mail->AddAddress($to, 'You');
                  if(!$mail->Send()) {
                      return 0;
                  } else {
                        return 1;
                 }
    
  
  }
  
 
public function clientChangePassword()
{
if(Session::get('clientLogin') == null)
    {
    
      return Redirect::to('clientLogin');
    }
$getClient = Clients::find(Session::get('clientLogin'));

return view('clientChangePassword')
->with('CompanyName', $getClient->CompanyName)
->with('CompanyAddress', $getClient->CompanyAddress);

}

public function clientChangeInfo(Request $request)
{

$getClient = Clients::find(Session::get('clientLogin'));
$getClient->CompanyName = $request->companyName;
$getClient->CompanyAddress = $request->companyAdd;
$getClient->save();
return response()->json('Done');
}

public function clientPassword(Request $request)
{

$getClient = Clients::find(Session::get('clientLogin'));

if($getClient->Password != md5($request->currentPass))
{
return response()->json('Incorrect Old Password');
}


$getClient->Password = md5($request->newPass);
$getClient->save();
return response()->json('Done');
}

public function clientForgotPassword(Request $request)
{

$getClient = Clients::where('Email', '=', $request->getEmail)->first();
if(!$getClient){
return response()->json('Email Not Found');
}
$newPassword = rand(100000000, 999999999);
//coz it forgot password, i set firsttimeuser column to 2. If it were account activation i set it to 1
$getClient->FirstTimeUser = 2;
$getClient->Password = md5($newPassword);
$getClient->save();
if($this->mailSender("Kudipoll Client Reset Password", "Hello, <br> <br> Kindly use <span style='font-size: 20px;'>".$newPassword."</span> as a default password to reset your profile credentials. <br><br> Regards.", $request->getEmail, "support@kudipoll.com") == 1){
            return response()->json('Check Your Mail To Complete Process');
       }
            return response()->json('Soemething went wrong');


}

public function clientActivateAccount(Request $request)
{

$getClient = Clients::where('Password', '=', md5($request->defaultPassword))
->where('FirstTimeUser', '>', 0)
->first();

if(!$getClient)
{

return response()->json('Incorrect Credentials');

}

$getClient->Password = md5($request->getPassword);
$getClient->FirstTimeUser = 0;
$getClient->Status = 1;
$getClient->save();

return response()->json('Success');




}


}

