<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\AnswerRecord;
use App\usertable;
use App\adminList;
use App\availableprizes;
use App\OurWinner;
use App\OurCampaign;
use App\SpecialPrize;
use App\CampaignQuestions;
use App\Clients;
use Session;
use DB;
use Carbon\Carbon;
use App\SessionKeeper;
use App\FormBuilder;
use App\BalanceHistorys;
use App\CashRequest;
use PHPMailer;

class websiteController extends Controller {

   public function WinMoneyWebsite() {
        $NigerianStates = array(
            'Abuja',
            'Abia',
            'Adamawa',
            'Akwa Ibom',
            'Anambra',
            'Bauchi',
            'Bayelsa',
            'Benue',
            'Borno',
            'Cross River',
            'Delta',
            'Ebonyi',
            'Edo',
            'Ekiti',
            'Enugu',
            'Gombe',
            'Imo',
            'Jigawa',
            'Kaduna',
            'Kano',
            'Katsina',
            'Kebbi',
            'Kogi',
            'Kwara',
            'Lagos',
            'Nassarawa',
            'Niger',
            'Ogun',
            'Ondo',
            'Osun',
            'Oyo',
            'Plateau',
            'Rivers',
            'Sokoto',
            'Taraba',
            'Yobe',
            'Zamfara',
        );
        $stateArray = "<option value=''>Your state of residence</option>";
        foreach ($NigerianStates as $k) {
            $stateArray = $stateArray . "<option value='" . $k . "'>" . $k . "</option>";
        }

        $allPrize = availableprizes::select('PrizeName')->get();
        $allWInners = OurWinner::all();
        return view('website')->with('allStates', $stateArray)->with(compact('allPrize', 'allPrize'))
                        ->with(compact('allWInners', 'allWInners'));
    }
    
 public function userSignin(Request $request) {
        //lets validate

        session_start();
        $checkInfo = usertable::where('Email', '=', $request->username)->where('Password', '=', md5($request->password))->first();

        if ($checkInfo != null) {

            if ($checkInfo->UserStatus == 0) {
                return response()->json("Invalid Profile");
            }

            session()->put('logedIn', $checkInfo->id);
            Session::put('logedIn', $checkInfo->id);
            // Session::set('logedIn', 1);
            // $campaign_questions = new campaign_questions();
            $getQuest = "";
            $latest = DB::select("SELECT pm.* FROM campaign_questions pm Natural left join answer_records WHERE pm.CampaignID not in (SELECT pd.CampaignID FROM answer_records pd where UserID = " . $checkInfo->id . ")");
            //return view('Campaigns');
            Session::put('regStatus', 'Complete');
            $winners = DB::select("SELECT * FROM our_winners limit 5");
            return response()->json("Success");
//            return Redirect::to('/successful')->with('UserName', $checkInfo)->with(compact('latest', 'latest'))
//                            ->with(compact('winners', 'winners'))
//                            ->with('regStatus', 'Complete');
        } else {

            return response()->json("Incorrect Details");
        }
    }


public function newmemberReg(Request $request)
{

//do actual save to db
  $getUser = usertable::where('Email', '=', $request->Email)->first();
            if ($getUser) {
                return response()->json('Email Already Exists.');
            }
            $user = new usertable();
          
            $user->Surname = $request->Surname;
            $user->Other_Name = $request->OtherName;
            $user->Email = $request->Email;
            $user->State = $request->State;
            $user->City = $request->City;
            $user->Phone_Number = $request->Phone;
            $user->facebook_id = "Direct Reg.";

            $user->save();

            $checkEmail = usertable::where('Email', '=', $request->Email)->first();
          //  session()->put('logedIn', $checkEmail->id);
            Session::put('logedIn', $checkEmail->id);
            //Session::set('logedIn', 1);


            $getUser = usertable::where('Email', '=', $request->Email)->first();
            if (!$getUser) {
                return response()->json('Unsuccessful! Error Occured.');
            }

            $linkPassword = rand(100000000, 999999999);
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < 30; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            
//update sentcode and codetimestart column
            $getUser->SentCode = $randomString . $linkPassword;
            $getUser->CodeTimeStart = Carbon::now();
            $getUser->save();


//get first five available campaign to include in registration mail
             $getFiveAvailableCamp = DB::select("SELECT * from our_campaigns where EndDate > " . Carbon::now()->format('Y-m-d')." and StartDate >= " . Carbon::now()->format('Y-m-d')." and Status = 1 Limit 5");
$campStrings = "";
$countAvailable = 0;
                            foreach($getFiveAvailableCamp as $eachCampAvailable){            
$campStrings = $campStrings . "<div style='width: 17%; margin-right:1%; margin-left:2%; display: inline-block;'> <a href='http://kudipoll.com/successful'><img src='http://kudipoll.com/public/images/CampaignImages/".$eachCampAvailable->id."/".$eachCampAvailable->ImagePath."' width='100%'></a><div align='center' style='margin-top: 20px;'>".$eachCampAvailable->CampaignName."</div></div>";
            }
            
            @$headers = 'From: ' . "no-reply@kudipoll.ng" . "<br><br>";
           //mail($request->Email, "Kudipoll Account Confirmation", "Clink the link below to confirm your account <br><br> http://kudipoll.com/confirmAccount/" . $randomString.$linkPassword, @$headers);
            $this->mailSender("Kudipoll Account Confirmation", "Hello, <br><br> Welcome to Kudipoll. <br><br> Please <a href='http://kudipoll.com/confirmAccount/" . $randomString.$linkPassword."' title='Click here' alt='Kudipoll Account Confirmation Link'>Click here</a> to verify you account <br><br><br><br> Kudipoll is an online service where you can win lots of prizes for taken surveys. <br><br><br> <u>Available Prizes To Be Won</u> <br><br> <div style='padding-top: 30px; '>".$campStrings."</div> ", $request->Email, "info@kudipoll.com");

            return response()->json('Success');
         

}


 public function forgotPassword(Request $request) {
        try {
            //Find the user object from model if it exists
            //$user = user::findOrFail($id);
            $user = usertable::where('Email', '=', $request->confirmMail)->first();
            $user->Level = 1;
            if (!$user) {
                return response()->json('Invalid Email');
            }

            $linkPassword = rand(100000000, 999999999);
            $user->SentCode = $linkPassword;
            $user->CodeTimeStart = Carbon::now();
            $user->save();

            @$headers = 'From: ' . "no-reply@kudipoll.com" . "\r\n";

           // mail($request->confirmMail, "Kudipoll Profile Reset Password", "Clink the link below to change your password \r\n http://kudipoll.com/confirmation/" . $linkPassword, @$headers);

if($this->mailSender("Kudipoll Profile Reset Password", "Clink the link below to change your password <br><br> http://kudipoll.com/confirmation/" . $linkPassword, $request->confirmMail, "support@kudipoll.com") == 1){
            return response()->json('Check Your Mail To Complete Process');
       }
            return response()->json('Soemething went wrong');

            // return Redirect::to('/successful')->with('UserName', $id);
        } catch (ModelNotFoundException $err) {
            //Show error page
            return response()->json('Error');
        }
    }


 public function sendMail(Request $request)
    {
     @$headers = 'From: ' . $request->Email . "\r\n";
    mail('info@kudipoll.com', $request->Subject, $request->Message, @$headers);
    //$this->mailSender($request->Subject, $request->Message, "info@kudipoll.com", $request->Email);
    return response()->json('Success');
    }
 public function mailSender($subject, $body, $to, $from)
  {
  
  require_once('../kudipoll/PHPMailer_5.2.0/class.phpmailer.php');
 
                  $mail             = new PHPMailer();
                  $mail->IsSMTP();
                  $mail->SMTPAuth   = true;
                  $mail->Host       = "box1030.bluehost.com";
                  $mail->Port       = 465;
                  $mail->Username   = $from;
                  $mail->Password   = "Winmoney+";
                  $mail->SMTPSecure = 'ssl';
                  $mail->SetFrom($from, 'Kudipoll Nigeria');
                  $mail->AddReplyTo($from,"Kudipoll Nigeria");
                  $mail->Subject    = $subject;
                 // $mail->AltBody    = "Any message.";
                  $mail->MsgHTML($body);
                  $mail->AddAddress($to, 'You');
                  if(!$mail->Send()) {
                      return 0;
                  } else {
                        return 1;
                 }
    
  
  }
  
}