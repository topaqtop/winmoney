<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\AnswerRecord;
use App\usertable;
use App\adminList;
use App\availableprizes;
use App\OurWinner;
use App\OurCampaign;
use App\SpecialPrize;
use App\CampaignQuestions;
use App\Clients;
use Session;
use DB;
use Carbon\Carbon;
use App\SessionKeeper;
use App\FormBuilder;
use App\BalanceHistorys;
use App\CashRequest;
use PHPMailer;

class userController extends Controller {

  public function userSignout() {
        Session::flush();
        return Redirect::to('');
    }

 public function checkRegComplete()
      {
      
      $dataArray = array();
      
  $getTotWithdrawal = DB::table('cash_requests')->where('UserID', '=', Session::get('logedIn'))->where('Status', '=', 0)->sum('BookBalance');
      $getBookBalance = CashRequest::where('UserID', '=', Session::get('logedIn'))->where('Status', '=', 1)->get();
$unclaimedBalance = CashRequest::where('UserID', '=', Session::get('logedIn'))->where('Status', '=', 1)->first();
      if($getBookBalance  == null)
      {
      
      $dataArray[] = 'N/A';
      
      }
      else
      {
       $dataArray[] = $getBookBalance->sum('BookBalance');
      }
      $checkInfo = usertable::find(Session::get('logedIn'));
      if($checkInfo == null)
      {
      
       $checkInfo = usertable::where('facebook_id', '=', Session::get('logedIn'))->first();
      }
      
      if($checkInfo != null)
      {
      
      if($checkInfo->Password == null)
      {
      
       $dataArray[] = false;
       $dataArray[] = $getTotWithdrawal;
$getPerSurveyAmount = OurCampaign::where('id', '!=', 0)->first()->ToWin;
      $dataArray[] = $getPerSurveyAmount;

if($unclaimedBalance == null)
      {
      
      $dataArray[] = 'N/A';
      
      }
      else
      {
       $dataArray[] = $unclaimedBalance->BookBalance;
      }

      return response()->json($dataArray);


      
      }
      $dataArray[] = true;
      $dataArray[] = $getTotWithdrawal;
$getPerSurveyAmount = OurCampaign::where('id', '!=', 0)->first()->ToWin;
      $dataArray[] = $getPerSurveyAmount;

if($unclaimedBalance == null)
      {
      
      $dataArray[] = 'N/A';
      
      }
      else
      {
       $dataArray[] = $unclaimedBalance;
      }
      return response()->json($dataArray);


      
      }
      $dataArray[] = false;
      $dataArray[] = $getTotWithdrawal;

      $getPerSurveyAmount = OurCampaign::where('id', '!=', 0)->first()->ToWin;
      $dataArray[] = $getPerSurveyAmount;

if($unclaimedBalance == null)
      {
      
      $dataArray[] = 'N/A';
      
      }
      else
      {
       $dataArray[] = $unclaimedBalance;
      }
      return response()->json($dataArray);
      
      }

 public function UpdateUserInfo(Request $request) {
        try {
           
            
            $id = Session::get('logedIn');
            //$user = user::findOrFail($id);
            $user = usertable::where('facebook_id', $id)->first();
            if($user  == null)
            {
            
            $user = usertable::where('id', $id)->first(); 
            
            }
            //$request contain your post data sent from your edit from
            //$user is an object which contains the column names of your table
            //Set user object attributes
            $referrerCode = rand(100000000, 999999999);

            $user->Phone_number = $request->Phone;
           //  return response()->json($user->Phone_number);
            $user->Password = md5($request->Password);
            $user->Confirm_Password = md5($request->Password);
            $user->State = $request->State;
            $user->City = $request->City;
            $user->Age_Range = $request->Age;
           // $user->ReferrerCode = $referrerCode;
             $user->UserStatus= 1;

            session()->put('logedIn', $id);
            Session::put('logedIn', $id);
            if (@$request->ref != null && @$request->ref != "") {
                $checkReferrerCode = usertable::where('ReferrerCode', '=', @$request->ref)->first();
                if (!$checkReferrerCode) {
                    Session::put('regStatus', 'Incomplete');
                    return response()->json('Incorrect Referrer Code, Please Try Again.');
                } else {
                    $user->ReferrerBy = @$request->ref;
                }
            }
            Session::put('regStatus', 'Complete');
            // Save/update user. 
            // This will will update your the row in ur db.
            $user->save();

            // Session::set('logedIn', 1);w
            return response()->json("Success");

            // return Redirect::to('/successful')->with('UserName', $id);
        } catch (ModelNotFoundException $err) {
            //Show error page
             return response()->json('An Error Has Occured');

        }
    }

 public function saveAnswer(Request $request) {


        $getCampInfo = OurCampaign::where('id', '=', $request->campaignID)->first();
        $count = 0;
         $getUser = usertable::where('id', '=', Session::get('logedIn'))->first();
         $getUser->UserBalance = $getUser->UserBalance + $getCampInfo->ToWin;
         $getUser->save();
         
         //update balance history table
         
         $historyTable = new BalanceHistorys();
         $historyTable->UserID = Session::get('logedIn');
         $historyTable->DayTime = Carbon::now();
         $historyTable->ActionPerformed = 'Payment for taking  Survey "'.$getCampInfo->CampaignName;
         $historyTable->CurrentBalance = $getUser->UserBalance;
         $historyTable->CampaignID = $request->campaignID;
         $historyTable->Response= "Credited";
         $historyTable->Amount= $getCampInfo->ToWin;
         $historyTable->save();
         
        foreach (@$request->answer as $answer) {
            $answer_record = new AnswerRecord();
            $answer_record->UserID = Session::get('logedIn');
            $answer_record->QuestionID = $request->questionId[$count];
            $answer_record->Answer = $answer;
            $answer_record->CampaignID = $request->campaignID;
            $answer_record->save();

            
            
            
            //scoring
            //get input type, questionID and expected response from question table
            $questionInfo = CampaignQuestions::where('id', '=', $request->questionId[$count])->first();

            //get questionID and answer selected from answer records based on userid
            $answerInfo = AnswerRecord::where('QuestionID', '=', $request->questionId[$count])
                    ->where('UserID', '=', Session::get('logedIn'))
                    ->first();

            //select option
            if ($questionInfo->ResponseType == "mySelect" || $questionInfo->ResponseType == "myRadio") {
                if (str_replace('__&&__', '', $answerInfo->Answer) == $questionInfo->ExpectedResponse) {
                    $answerInfo->Score = 1;
                } else {
                    $answerInfo->Score = 0;
                }
            } else if ($questionInfo->ResponseType == "myRange") {
                if (str_replace('__&&__', '', $answerInfo->Answer) >= $questionInfo->ExpectedResponse) {
                    $answerInfo->Score = 1;
                } else {
                    $answerInfo->Score = 0;
                }
            } else if ($questionInfo->ResponseType == "myCheck") {
                //get answer logic
                $answerArray = explode('__&&__', $answerInfo->Answer);
                $questionArray = explode('__&&__', $questionInfo->ExpectedResponse);
                $logic = explode('__&&__', $questionInfo->ExpectedResponse)[count(explode('__&&__', $questionInfo->ExpectedResponse)) - 1];
                if ($logic == "AND") {
                    if (count(array_diff($answerArray, $questionArray)) >= 1) {
                        $answerInfo->Score = 0;
                    } else {
                        $answerInfo->Score = 1;
                    }
                } else if ($logic == "OR") {
                    if (count(array_intersect($answerArray, $questionArray)) >= 1) {
                        $answerInfo->Score = 1;
                    } else {
                        $answerInfo->Score = 0;
                    }
                }
            }

            $answerInfo->save();
            $count++;
        }
       
        $returnRecords = array();
        $returnRecords[] = $getUser->UserBalance;
        
        $getBookBalance = CashRequest::where('UserID', '=', Session::get('logedIn'))->where('Status', '=', 1)->first();
      if($getBookBalance  == null)
      {
      
      $returnRecords[] = 'N/A';
      
      }
      else
      {
       $returnRecords[] = $getBookBalance->BookBalance;
      }
      
      $getTotWithdrawal = DB::table('cash_requests')->where('UserID', '=', Session::get('logedIn'))->where('Status', '=', 0)->sum('BookBalance');
      if($getTotWithdrawal){
      $returnRecords[] = $getTotWithdrawal;
      }
      else{
       $returnRecords[] = 0;
      }
     $returnRecords[] =$getCampInfo ->IfSpecial;
     $returnRecords[] = $getCampInfo->PrizeName;
        return response()->json($returnRecords);
    }

public function getBalanceHistory()
{

$getHistory = BalanceHistorys::where('UserID', '=', Session::get('logedIn'))->get();
return response()->json($getHistory);

}


public function bookMyMoney(Request $request)
{

$dataArray = array();
$getUser = usertable::where('id', '=', Session::get('logedIn'))->first();

       $bookbalance = $getUser->UserBalance;

$addBooking = new CashRequest();
$addBooking->UserID = Session::get('logedIn');
$addBooking->UserBalance = 0;
$addBooking->BookBalance = $getUser->UserBalance;
$addBooking->Status = 1;
$addBooking->save();

$getUser->UserBalance = 0;
$getUser->save();

                   @$headers = 'From: ' . $getUser->Email . "\r\n";

          mail("info@kudipoll.com", "Request For Cash On Kudipoll", "Hello, <br><br>".$getUser->Surname." ".$getUser->Other_Name." Has Requested For Cash On Kudipoll. <br><br> Kindly respond.", @$headers);
           // $this->mailSender("Request For Cash On Kudipoll", "Hello, <br><br>".$getUser->Surname." ".$getUser->Other_Name." Has Requested For Cash On Kudipoll. <br><br> Kindly respond.", "info@kudipoll.com", $getUser->Email);

$dataArray[] = $bookbalance;
$dataArray[] = $getTotWithdrawal = DB::table('cash_requests')->where('UserID', '=', Session::get('logedIn'))->where('Status', '=', 0)->sum('BookBalance');

     return response()->json($dataArray);

}


public function getCampaign(Request $request) {
        $frames = "";
$unended = '';
$ended = '';
        if ($request->action == "done") {
        
        
        //tope start
        
        
        
             $countBoxes = 0;
                    $special = "";
            $test = "";
            //  $checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
            $checkInfo = usertable::where('facebook_id', '=', Session::get('logedIn'))->first();
            if ($checkInfo == null) {
                $checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
            }
            $latest = DB::select("SELECT pm.* FROM campaign_questions pm Natural left join answer_records WHERE pm.CampaignID in (SELECT pd.CampaignID FROM answer_records pd where UserID = " . $checkInfo->id . ")");
            $count = 0;
            $allId = array();
            $campCount = 0;
            foreach ($latest as $myCampaign) {

                //$tillEnd = (Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->diffInDays(Carbon::now()) + 1) == 1 ? 'Campaign Ends today' : (Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->diffInDays(Carbon::now()) + 1) . ' Day(s) to end';

$tillEnd = 'Prize Completed';
                $specialPrize = SpecialPrize::where('CampaignID', '=', $myCampaign->CampaignID)->first();
                if ($specialPrize) {
                    $special = '<h5>Special Prize: ' . $specialPrize->SpecialPrize . '</h5>';
                } else {
                    $special = '<h5>&emsp;</h5>';
                }
                if (!in_array($myCampaign->CampaignID, $allId)) {

                    $allId[] = $myCampaign->CampaignID;
                    $count++;

                    $eachQuest = "<ul class='questions'>";
                    csrf_field();
                    $countForm = 0;
                    foreach ($latest as $getQuest) {
                        if ($getQuest->CampaignID == $myCampaign->CampaignID) {

                            $countForm++;
                            //  $eachQuest = $eachQuest . "<label>" . $getQuest->Question . "</label><br>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $getQuest->ResponseData)) . "<br><br><input type='hidden' name='QuestID' class='Quest_" . $getQuest->id . "' value='" . $getQuest->id . "' />";
                            if ($getQuest->ResponseType == "mySelect") {
                                $inputPart = "";
                                // $getInput = DB::select("SELECT FormContent FROM form_builders where FormType = '" . $getQuest->ResponseType . "'");
                                $getInput = FormBuilder::where('FormType', '=', $getQuest->ResponseType)->first();
                                foreach (explode('__&&__', $getQuest->ResponseData) as $eachOpt) {
                                    if ($eachOpt != "" && $eachOpt != null)
                                        $inputPart = $inputPart . "<option value='" . $eachOpt . "'>" . $eachOpt . "</option>";
                                }
                                $finalInput = str_replace('eachopt', $inputPart, $getInput->FormContent);
                                //  $eachQuest = $eachQuest . "<label>" . $getQuest->Question . "</label><br>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $finalInput)) . "<br><br><input type='hidden' name='QuestID' class='Quest_" . $getQuest->id . "' value='" . $getQuest->id . "' />";
                                $eachQuest = $eachQuest . " <li class='question-list'>" .
                                        "<div class='wrapper'>" .
                                        "<div class='item'>" .
                                        "<span><i class='fa fa-arrow-right' style='color: gold;'></i></span>" .
                                        "</div>" .
                                        " <div class='question'><span>" . $getQuest->Question . "</span></div>" .
                                        "<div class='response'>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $finalInput)) . "</div><input type='hidden' name='QuestID' class='Quest_" . $getQuest->id . "' value='" . $getQuest->id . "' />" .
                                        "</div>" .
                                        " </li>";
                            }
                            else if ($getQuest->ResponseType == "myCheck") {
                                $inputPart = "<span class='checkBoxClass_" . $getQuest->id . "'><input type='hidden' name='checkBoxClass_" . $getQuest->id . "' value='checkBoxClass_" . $getQuest->id . "'>";

                                // $getInput = DB::select("SELECT FormContent FROM form_builders where FormType = '" . $getQuest->ResponseType . "'");
                                $getInput = FormBuilder::where('FormType', '=', $getQuest->ResponseType)->first();
                                foreach (explode('__&&__', $getQuest->ResponseData) as $eachOpt) {
                                    if ($eachOpt != "" && $eachOpt != null) {

                                        // $f = str_replace('eachopt', $eachOpt, $getInput->FormContent);
                                        $inputPart = $inputPart . "<input type='checkbox' name='checkbox' class='toDisable_subforcampid largeChecks' id='val_subforcount' value='" . $eachOpt . "' ><span style='position: relative; top:-6px; left: 5px;'>" . $eachOpt . "</span> &emsp;";
                                    }
                                }
                                $inputPart = $inputPart . "</span>";
                                // $eachQuest = $eachQuest . "<label>" . $getQuest->Question . "</label><br>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $inputPart)) . "<br><br>";
                                $eachQuest = $eachQuest . " <li class='question-list'>" .
                                        "<div class='wrapper'>" .
                                        "<div class='item'>" .
                                        "<span><i class='fa fa-arrow-right' style='color: gold;'></i></span>" .
                                        "</div>" .
                                        " <div class='question'><span>" . $getQuest->Question . "</span></div>" .
                                        "<div class='response'>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $inputPart)) . "</div>" .
                                        "</div>" .
                                        " </li>";
                            } else if ($getQuest->ResponseType == "myRadio") {
                                $inputPart = "<span class='radioClass_" . $getQuest->id . "'><input type='hidden' name='radioClass_" . $getQuest->id . "' value='radioClass_" . $getQuest->id . "'>";
                                // $getInput = DB::select("SELECT FormContent FROM form_builders where FormType = '" . $getQuest->ResponseType . "'");
                                $getInput = FormBuilder::where('FormType', '=', $getQuest->ResponseType)->first();
                                foreach (explode('__&&__', $getQuest->ResponseData) as $eachOpt) {
                                    if ($eachOpt != "" && $eachOpt != null) {
                                        // $f = str_replace('eachopt', $eachOpt, $getInput->FormContent);
                                        $inputPart = $inputPart . "<input type='radio' name='radioClass_" . $getQuest->id . "' class='toDisable_subforcampid largeChecks' id='val_subforcount' value='" . $eachOpt . "' ><span style='position: relative; top:-6px; left: 5px;'>" . $eachOpt . " </span>&emsp;";
                                    }
                                }
                                $inputPart = $inputPart . "</span>";
                                $eachQuest = $eachQuest . "<label>" . $getQuest->Question . "</label><br>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $inputPart)) . "<br><br>";
                            } else if ($getQuest->ResponseType == "myRange") {
                                $max = explode('-', explode('__&&__', $getQuest->ResponseData)[0])[0];
                                $min = explode('-', explode('__&&__', $getQuest->ResponseData)[0])[1];
                                //   $inputPart = "<input type='text' id='' name='rangeClass_' class='rangeSelect_".$getQuest->id."' value='' />";
                                $inputPart = "<input type='text' id='rangeSelect" . $getQuest->id . "' name='rangeClass_" . $getQuest->id . "' class='form-control rangeClass_" . $getQuest->id . "'  value='' /><div><span class='pull-left leastDesc rangeInfo' style='color: #333;   font-size: 12px; line-height: 1.333;text-shadow: none;top: 0;padding: 1px 5px; background: rgba(0,0,0,0.1); border-radius: 3px;'>" . explode('-', explode('__&&__', $getQuest->ResponseData)[1])[0] . "</span><span class='pull-right maxDesc rangeInfo' style='color: #333;   font-size: 12px; line-height: 1.333;text-shadow: none;top: 0;padding: 1px 5px; background: rgba(0,0,0,0.1); border-radius: 3px;'>" . explode('-', explode('__&&__', $getQuest->ResponseData)[1])[1] . "</span></div>";

                                $eachQuest = $eachQuest . " <li class='question-list'>" .
                                        "<div class='wrapper'>" .
                                        "<div class='item'>" .
                                        "<span><i class='fa fa-arrow-right' style='color: gold;'></i></span>" .
                                        "</div>" .
                                        " <div class='question'><span>" . $getQuest->Question . "</span></div>" .
                                        "<div class='response'>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $inputPart)) . "</div>" .
                                        "</div>" .
                                        " </li>" .
                                        "<script>" .
                                        "$('#rangeSelect" . $getQuest->id . "').ionRangeSlider({" .
                                        "hide_min_max: false," .
                                        " keyboard: true," .
                                        "min: '" . ($min) . "'," .
                                        "max: '" . ($max) . "'," .
                                        "type: 'single'," .
                                        "step: 1," .
                                        "grid: true" .
                                        "});" .
                                        " </script>";
                            }
                        }
                    }
                    $eachQuest = $eachQuest . "<input type='hidden' name='CampaigInfo' value='" . $myCampaign->CampaignID . "' /> </div>";
                    if (strlen(OurCampaign::find($myCampaign->CampaignID)->CampaignNotes) > 70) {
                        $det = '<span style=" display: inline;" class="popoverData" href="#" data-content="" rel="tooltip" data-placement="bottom" data-original-title="' . OurCampaign::find($myCampaign->CampaignID)->CampaignNotes . '" data-trigger="hover">' . substr(OurCampaign::find($myCampaign->CampaignID)->CampaignNotes, 0, 70) . '...</span>';
                    } else {
                        $det = OurCampaign::find($myCampaign->CampaignID)->CampaignNotes . '<br><br>';
                    }
                    if ($count > 0 && OurCampaign::find($myCampaign->CampaignID)->Status == 1) {
                    $IfCampaignSpecial = "";
                    
                                                             if(OurCampaign::find($myCampaign->CampaignID)->IfSpecial == 1){
                                                            $IfCampaignSpecial = '<div class="topTriangleSpecial"></div><div class="triangleTextSpecial">Special</div>';
                                                             }
if(OurCampaign::find($myCampaign->CampaignID)->IfSpecial == 2){
                                                            $IfCampaignSpecial = '<div class="topTriangleEnded"></div><div class="triangleTextEnded">Won</div>';
                                                             }
                                                         
                        $quotedRegStatus = "'" . @$request->regStatus . "'";
                        $quotedCampID = "'" . @$myCampaign->CampaignID . "'";
                        $img = "public/images/CampaignImages/" . @$myCampaign->CampaignID . "/" . OurCampaign::find($myCampaign->CampaignID)->ImagePath;
 $countBoxes++;
                        $frames = $frames . ' <div class="col-lg-4 campBoxes camp_' . $myCampaign->CampaignID . ' myClass boxCount_'.$countBoxes.'" align="center"><div class=" ">' .
                                ' <div class="box--sml text-center flush-pd-trailer col-lg-12">' .
                                $IfCampaignSpecial.
                                '<div class="block " href="#">' .
                                '<img src="' . $img . '" style="width: 100%;"><h5 class="box-title elltext">' . OurCampaign::find($myCampaign->CampaignID)->CampaignName . '</h5>' .
                                '<h5 class="box-title elltext" style="font-weight: 700;">' .
                                $special .
                                '</h5>' .
                                '<!--<h5>Cash Value: #30,000</h5>-->' .
                                '<h5 class="box-title elltext">' . $tillEnd . '</h5>' .
                                // '<h6><p>Details:' . $det . '</p></h6>' .
                                '<!--<p class="push-sbit"><button class="btn btn-prize btn-xs campaign_count_' . $count . ' QuestClass" disabled>COMPLETED <i class="fa fa-arrow-right"></i></button></p>-->' .
                                // '</h6><p class="push-sbit"><button class="btn btn-primary btn-xs campaign_count_' . $count . ' QuestClass" onclick="checkReg(' . $quotedRegStatus . ', ' . $quotedCampID . ')">TAKE QUIZ</button></p>' .
                                '</div>' .
                                '<input type="hidden" value="' . $eachQuest . '" class="getQuest_' . $myCampaign->CampaignID . '">' .
                                ' <input type="hidden" value="' . $count . '" class="getCount_' . $myCampaign->CampaignID . '">' .
                                ' </div>' .
                                '</div>' .
                                '</div>';
                    }
                }

                $campCount++;
            }
        
        //tope end
        
            
           
            //   return view("previousCampaign")->with(compact('latest', 'latest'));
        } else if ($request->action == "notdone") {

            $special = "";
            $test = "";
            $countBoxes = 0;
            //   $checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
            $checkInfo = usertable::where('facebook_id', '=', Session::get('logedIn'))->first();
            if ($checkInfo == null) {
                $checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
            }
            $latest = DB::select("SELECT pm.* FROM campaign_questions pm Natural left join answer_records WHERE pm.CampaignID not in (SELECT pd.CampaignID FROM answer_records pd where UserID = " . $checkInfo->id . ") order by pm.ID");
            $count = 0;
            $allId = array();
            $campCount = 0;
             
            foreach ($latest as $myCampaign) {

   

                $test = $test . "entered";
                //  $tillEnd = (Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->diffInDays(Carbon::now()->format('Y-m-d')) + 1) == 1 ? 'Campaign Ends today' : (Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->diffInDays(Carbon::now()->format('Y-m-d')) + 1) . ' Day(s) to end';
if((Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)) < Carbon::parse(Carbon::now()))
{
@$tillEnd = 'Prize Ended';
}else{
                @$tillEnd = (Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->diffInDays(Carbon::now()) + 1) == 1 ? 'Campaign Ends today' : (Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->diffInDays(Carbon::now()) + 1) . ' Day(s) to end';
}
                $specialPrize = SpecialPrize::where('CampaignID', '=', $myCampaign->CampaignID)->first();
                if ($specialPrize) {
                    $special = '<h5>Special Prize: ' . $specialPrize->SpecialPrize . '</h5>';
                } else {
                    $special = '<h5>&emsp;</h5>';
                }
                if (!in_array($myCampaign->CampaignID, $allId)) {

                    $allId[] = $myCampaign->CampaignID;
                    $count++;

                    $eachQuest = "<div class='questionWrapper'>";
                    csrf_field();
                    $countForm = 0;
                    foreach ($latest as $getQuest) {
                        if ($getQuest->CampaignID == $myCampaign->CampaignID) {

                            $countForm++;
                            // $eachQuest = $eachQuest . "<label>" . $getQuest->Question . "</label><br>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $getQuest->ResponseData)) . "<br><br><input type='hidden' name='QuestID' class='Quest_" . $getQuest->id . "' value='" . $getQuest->id . "' />";
                            if ($getQuest->ResponseType == "mySelect") {
                                $inputPart = "";
                                // $getInput = DB::select("SELECT FormContent FROM form_builders where FormType = '" . $getQuest->ResponseType . "'");
                                $getInput = FormBuilder::where('FormType', '=', $getQuest->ResponseType)->first();
                                foreach (explode('__&&__', $getQuest->ResponseData) as $eachOpt) {
                                    if ($eachOpt != "" && $eachOpt != null)
                                        $inputPart = $inputPart . "<option value='" . $eachOpt . "'>" . $eachOpt . "</option>";
                                }
                                $finalInput = str_replace('eachopt', $inputPart, $getInput->FormContent);
                                //   $eachQuest = $eachQuest . "<label>" . $getQuest->Question . "</label><br>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $finalInput)) . "<br><br><input type='hidden' name='QuestID' class='Quest_" . $getQuest->id . "' value='" . $getQuest->id . "' />";
                                $eachQuest = $eachQuest . " <li class='question-list'>" .
                                        "<div class='wrapper'>" .
                                        "<div class='item'>" .
                                        "<span><i class='fa fa-arrow-right' style='color: gold;'></i></span>" .
                                        "</div>" .
                                        " <div class='question'><span>" . $getQuest->Question . "</span></div>" .
                                        "<div class='response'>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $finalInput)) . "</div><input type='hidden' name='QuestID' class='Quest_" . $getQuest->id . "' value='" . $getQuest->id . "' />" .
                                        "</div>" .
                                        " </li>";
                            }
                            else if ($getQuest->ResponseType == "myCheck") {
                                $inputPart = "<span class='checkBoxClass_" . $getQuest->id . "'><input type='hidden' name='checkBoxClass_" . $getQuest->id . "' value='checkBoxClass_" . $getQuest->id . "'>";

                                // $getInput = DB::select("SELECT FormContent FROM form_builders where FormType = '" . $getQuest->ResponseType . "'");
                                $getInput = FormBuilder::where('FormType', '=', $getQuest->ResponseType)->first();
                                foreach (explode('__&&__', $getQuest->ResponseData) as $eachOpt) {
                                    if ($eachOpt != "" && $eachOpt != null) {

                                        // $f = str_replace('eachopt', $eachOpt, $getInput->FormContent);
                                        $inputPart = $inputPart . "<input type='checkbox' name='checkbox' class='toDisable_subforcampid largeChecks' id='val_subforcount' value='" . $eachOpt . "' ><span style='position: relative; top:-6px; left: 5px;'>" . $eachOpt . "</span> &emsp;";
                                    }
                                }
                                $inputPart = $inputPart . "</span>";
                                //  $eachQuest = $eachQuest . "<label>" . $getQuest->Question . "</label><br>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $inputPart)) . "<br><br>";
                                $eachQuest = $eachQuest . " <li class='question-list'>" .
                                        "<div class='wrapper'>" .
                                        "<div class='item'>" .
                                        "<span><i class='fa fa-arrow-right' style='color: gold;'></i></span>" .
                                        "</div>" .
                                        " <div class='question'><span>" . $getQuest->Question . "</span></div>" .
                                        "<div class='response'>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $inputPart)) . "</div>" .
                                        "</div>" .
                                        " </li>";
                            } else if ($getQuest->ResponseType == "myRadio") {
                                $inputPart = "<span class='radioClass_" . $getQuest->id . "'><input type='hidden' name='radioClass_" . $getQuest->id . "' value='radioClass_" . $getQuest->id . "'>";
                                // $getInput = DB::select("SELECT FormContent FROM form_builders where FormType = '" . $getQuest->ResponseType . "'");
                                $getInput = FormBuilder::where('FormType', '=', $getQuest->ResponseType)->first();
                                foreach (explode('__&&__', $getQuest->ResponseData) as $eachOpt) {
                                    if ($eachOpt != "" && $eachOpt != null) {
                                        // $f = str_replace('eachopt', $eachOpt, $getInput->FormContent);
                                        $inputPart = $inputPart . "<input type='radio' name='radioClass_" . $getQuest->id . "' class='toDisable_subforcampid largeChecks' id='val_subforcount' value='" . $eachOpt . "' ><span style='position: relative; top:-6px; left: 5px;'>" . $eachOpt . " </span>&emsp;";
                                    }
                                }
                                $inputPart = $inputPart . "</span>";
                                //   $eachQuest = $eachQuest . "<label>" . $getQuest->Question . "</label><br>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $inputPart)) . "<br><br>";

                                $eachQuest = $eachQuest . " <li class='question-list'>" .
                                        "<div class='wrapper'>" .
                                        "<div class='item'>" .
                                        "<span><i class='fa fa-arrow-right' style='color: gold;'></i></span>" .
                                        "</div>" .
                                        " <div class='question'><span>" . $getQuest->Question . "</span></div>" .
                                        "<div class='response'>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $inputPart)) . "</div>" .
                                        "</div>" .
                                        " </li>";
                            } else if ($getQuest->ResponseType == "myRange") {
                                $max = explode('-', explode('__&&__', $getQuest->ResponseData)[0])[0];
                                $min = explode('-', explode('__&&__', $getQuest->ResponseData)[0])[1];
                                //   $inputPart = "<input type='text' id='' name='rangeClass_' class='rangeSelect_".$getQuest->id."' value='' />";
                                $inputPart = "<input type='text' id='rangeSelect" . $getQuest->id . "' name='rangeClass_" . $getQuest->id . "' class='form-control rangeClass_" . $getQuest->id . "'  value='' /><div><span class='pull-left leastDesc rangeInfo' style='color: #333;   font-size: 12px; line-height: 1.333;text-shadow: none;top: 0;padding: 1px 5px; background: rgba(0,0,0,0.1); border-radius: 3px;'>" . explode('-', explode('__&&__', $getQuest->ResponseData)[1])[0] . "</span><span class='pull-right maxDesc rangeInfo' style='color: #333;   font-size: 12px; line-height: 1.333;text-shadow: none;top: 0;padding: 1px 5px; background: rgba(0,0,0,0.1); border-radius: 3px;'>" . explode('-', explode('__&&__', $getQuest->ResponseData)[1])[1] . "</span></div>";

                                // $eachQuest = $eachQuest . "<label>" . $getQuest->Question . "</label><br>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $inputPart)) . "<br><br>" .
                                $eachQuest = $eachQuest . " <li class='question-list'>" .
                                        "<div class='wrapper'>" .
                                        "<div class='item'>" .
                                        "<span><i class='fa fa-arrow-right' style='color: gold;'></i></span>" .
                                        "</div>" .
                                        " <div class='question'><span>" . $getQuest->Question . "</span></div>" .
                                        "<div class='response'>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $inputPart)) . "</div>" .
                                        "</div>" .
                                        " </li>" .
                                        "<script>" .
                                        "$('#rangeSelect" . $getQuest->id . "').ionRangeSlider({" .
                                        "hide_min_max: false," .
                                        " keyboard: true," .
                                        "min: '" . ($min) . "'," .
                                        "max: '" . ($max) . "'," .
                                        "type: 'single'," .
                                        "step: 1," .
                                        "grid: true" .
                                        "});" .
                                        " </script>";
                            }
                        }
                    }
 
                    @$eachQuest = $eachQuest . "<input type='hidden' name='CampaigInfo' value='" . $myCampaign->CampaignID . "' /> </div>";
                    if (strlen(@OurCampaign::find(@$myCampaign->CampaignID)->CampaignNotes) > 70) {
                        @$det = '<span style=" display: inline;" class="popoverData" href="#" data-content="" rel="tooltip" data-placement="bottom" data-original-title="' . @OurCampaign::find(@$myCampaign->CampaignID)->CampaignNotes . '" data-trigger="hover">' . substr(@OurCampaign::find(@$myCampaign->CampaignID)->CampaignNotes, 0, 70) . '...</span>';
                    } else {
                        @$det = OurCampaign::find(@$myCampaign->CampaignID)->CampaignNotes . '<br><br>';
                    }
                    if ($count > 0 && OurCampaign::find($myCampaign->CampaignID)->Status == 1) {
                    $IfCampaignSpecial = "";
                    
                                                             if(OurCampaign::find($myCampaign->CampaignID)->IfSpecial == 1){
                                                            $IfCampaignSpecial = '<div class="topTriangleSpecial"></div><div class="triangleTextSpecial">Special</div>';
                                                             }
if(OurCampaign::find($myCampaign->CampaignID)->IfSpecial == 2){
                                                            $IfCampaignSpecial = '<div class="topTriangleEnded"></div><div class="triangleTextEnded">Won</div>';
                                                             }
                    
                        $quotedRegStatus = "'" . @$request->regStatus . "'";
                        $quotedCampID = "'" . @$myCampaign->CampaignID . "'";
                        $img = "public/images/CampaignImages/" . @$myCampaign->CampaignID . "/" . OurCampaign::find($myCampaign->CampaignID)->ImagePath;

$ifGray = '';
$buttonType = '<button class="btn btn-prize btn-xs campaign_count_' . $count . ' QuestClass thisBox_'. $myCampaign->CampaignID.' trynowbtn " onclick="checkReg(\''.'unknown'.'\', ' . $quotedCampID . ', \''.$count.'\'); $(this).prop(\''.'disabled'.'\', true)">TRY NOW TO WIN '.OurCampaign::find($myCampaign->CampaignID)->ToWin.' <i class="fa fa-arrow-right"></i></button>';

if((Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)) < Carbon::parse(Carbon::now()))
{

$ifGray = 'grayout';
$buttonType = '<button class="btn btn-prize btn-xs QuestClass" disabled="">ENDED<i class="fa fa-arrow-right"></i></button>';

}


//now i want the unended prizes to show first so i would save the ended and unended to separate strings then concatenate it making the unended come before the ended.


if($ifGray == '' && (Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->StartDate)) < Carbon::parse(Carbon::now()))
{
$countBoxes++;
$unended = $unended . ' <div class=" col-lg-4 campBoxes camp_' . $myCampaign->CampaignID . ' myClass '.$ifGray.' boxCount_'.$countBoxes.'" align="center"><div class=" ">' .
                                ' <div class="box--sml text-center flush-pd-trailer col-lg-12">' .
                                $IfCampaignSpecial.
                                '<div class="block " href="#">' .
                                '<img src="' . $img . '" style="width: 100%; "><h5 class="box-title elltext">' . OurCampaign::find($myCampaign->CampaignID)->CampaignName . '</h5>' .
                                '<h5 class="box-title elltext" style="font-weight: 700;">' .
                                $special .
                                '</h5>' .
                                '<!--<h5>Cash Value: #30,000</h5>-->' .
                                '<h5 class="box-title elltext">' . $tillEnd . '</h5>' .
                                // '<h6><p>Details:' . $det . '</p></h6>' .
                                '<p class="push-sbit">'.
$buttonType.
'</p>'.
                                // '</h6><p class="push-sbit"><button class="btn btn-primary btn-xs campaign_count_' . $count . ' QuestClass" onclick="checkReg(' . $quotedRegStatus . ', ' . $quotedCampID . ')">TAKE QUIZ</button></p>' .
                                '</div>' .
                                '<input type="hidden" value="' . $eachQuest . '" class="getQuest_' . $myCampaign->CampaignID . '">' .
                                ' <input type="hidden" value="' . $count . '" class="getCount_' . $myCampaign->CampaignID . '">' .
                                ' </div>' .
                                '</div>' .
                                '</div>';
}
else if($ifGray == 'grayout' && (Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->StartDate)) < Carbon::parse(Carbon::now()))
{
$countBoxes++;
$ended = $ended . ' <div class=" col-lg-4 campBoxes camp_' . $myCampaign->CampaignID . ' myClass '.$ifGray.' boxCount_'.$countBoxes.'" align="center"><div class=" ">' .
                                ' <div class="box--sml text-center flush-pd-trailer col-lg-12">' .
                                $IfCampaignSpecial.
                                '<div class="block " href="#">' .
                                '<img src="' . $img . '" style="width: 100%; "><h5 class="box-title elltext">' . OurCampaign::find($myCampaign->CampaignID)->CampaignName . '</h5>' .
                                '<h5 class="box-title elltext" style="font-weight: 700;">' .
                                $special .
                                '</h5>' .
                                '<!--<h5>Cash Value: #30,000</h5>-->' .
                                '<h5 class="box-title elltext">' . $tillEnd . '</h5>' .
                                // '<h6><p>Details:' . $det . '</p></h6>' .
                                '<p class="push-sbit">'.
$buttonType.
'</p>'.
                                // '</h6><p class="push-sbit"><button class="btn btn-primary btn-xs campaign_count_' . $count . ' QuestClass" onclick="checkReg(' . $quotedRegStatus . ', ' . $quotedCampID . ')">TAKE QUIZ</button></p>' .
                                '</div>' .
                                '<input type="hidden" value="' . $eachQuest . '" class="getQuest_' . $myCampaign->CampaignID . '">' .
                                ' <input type="hidden" value="' . $count . '" class="getCount_' . $myCampaign->CampaignID . '">' .
                                ' </div>' .
                                '</div>' .
                                '</div>';

}


                        
                    }
                }

                $campCount++;
            }
            
           $frames = $unended . $ended;
                    $special = "";
            $test = "";
            //  $checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
            $checkInfo = usertable::where('facebook_id', '=', Session::get('logedIn'))->first();
            if ($checkInfo == null) {
                $checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
            }
            $latest = DB::select("SELECT pm.* FROM campaign_questions pm Natural left join answer_records WHERE pm.CampaignID in (SELECT pd.CampaignID FROM answer_records pd where UserID = " . $checkInfo->id . ")");
            $count = 0;
            $allId = array();
            $campCount = 0;
            foreach ($latest as $myCampaign) {

                $tillEnd = (Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->diffInDays(Carbon::now()) + 1) == 1 ? 'Campaign Ends today' : (Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->diffInDays(Carbon::now()) + 1) . ' Day(s) to end';
                $specialPrize = SpecialPrize::where('CampaignID', '=', $myCampaign->CampaignID)->first();
                if ($specialPrize) {
                    $special = '<h5>Special Prize: ' . $specialPrize->SpecialPrize . '</h5>';
                } else {
                    $special = '<h5>&emsp;</h5>';
                }
                if (!in_array($myCampaign->CampaignID, $allId)) {

                    $allId[] = $myCampaign->CampaignID;
                    $count++;

                    $eachQuest = "<ul class='questions'>";
                    csrf_field();
                    $countForm = 0;
                    foreach ($latest as $getQuest) {
                        if ($getQuest->CampaignID == $myCampaign->CampaignID) {

                            $countForm++;
                            //  $eachQuest = $eachQuest . "<label>" . $getQuest->Question . "</label><br>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $getQuest->ResponseData)) . "<br><br><input type='hidden' name='QuestID' class='Quest_" . $getQuest->id . "' value='" . $getQuest->id . "' />";
                            if ($getQuest->ResponseType == "mySelect") {
                                $inputPart = "";
                                // $getInput = DB::select("SELECT FormContent FROM form_builders where FormType = '" . $getQuest->ResponseType . "'");
                                $getInput = FormBuilder::where('FormType', '=', $getQuest->ResponseType)->first();
                                foreach (explode('__&&__', $getQuest->ResponseData) as $eachOpt) {
                                    if ($eachOpt != "" && $eachOpt != null)
                                        $inputPart = $inputPart . "<option value='" . $eachOpt . "'>" . $eachOpt . "</option>";
                                }
                                $finalInput = str_replace('eachopt', $inputPart, $getInput->FormContent);
                                //  $eachQuest = $eachQuest . "<label>" . $getQuest->Question . "</label><br>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $finalInput)) . "<br><br><input type='hidden' name='QuestID' class='Quest_" . $getQuest->id . "' value='" . $getQuest->id . "' />";
                                $eachQuest = $eachQuest . " <li class='question-list'>" .
                                        "<div class='wrapper'>" .
                                        "<div class='item'>" .
                                        "<span><i class='fa fa-arrow-right' style='color: gold;'></i></span>" .
                                        "</div>" .
                                        " <div class='question'><span>" . $getQuest->Question . "</span></div>" .
                                        "<div class='response'>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $finalInput)) . "</div><input type='hidden' name='QuestID' class='Quest_" . $getQuest->id . "' value='" . $getQuest->id . "' />" .
                                        "</div>" .
                                        " </li>";
                            }
                            else if ($getQuest->ResponseType == "myCheck") {
                                $inputPart = "<span class='checkBoxClass_" . $getQuest->id . "'><input type='hidden' name='checkBoxClass_" . $getQuest->id . "' value='checkBoxClass_" . $getQuest->id . "'>";

                                // $getInput = DB::select("SELECT FormContent FROM form_builders where FormType = '" . $getQuest->ResponseType . "'");
                                $getInput = FormBuilder::where('FormType', '=', $getQuest->ResponseType)->first();
                                foreach (explode('__&&__', $getQuest->ResponseData) as $eachOpt) {
                                    if ($eachOpt != "" && $eachOpt != null) {

                                        // $f = str_replace('eachopt', $eachOpt, $getInput->FormContent);
                                        $inputPart = $inputPart . "<input type='checkbox' name='checkbox' class='toDisable_subforcampid largeChecks' id='val_subforcount' value='" . $eachOpt . "' ><span style='position: relative; top:-6px; left: 5px;'>" . $eachOpt . "</span> &emsp;";
                                    }
                                }
                                $inputPart = $inputPart . "</span>";
                                // $eachQuest = $eachQuest . "<label>" . $getQuest->Question . "</label><br>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $inputPart)) . "<br><br>";
                                $eachQuest = $eachQuest . " <li class='question-list'>" .
                                        "<div class='wrapper'>" .
                                        "<div class='item'>" .
                                        "<span><i class='fa fa-arrow-right' style='color: gold;'></i></span>" .
                                        "</div>" .
                                        " <div class='question'><span>" . $getQuest->Question . "</span></div>" .
                                        "<div class='response'>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $inputPart)) . "</div>" .
                                        "</div>" .
                                        " </li>";
                            } else if ($getQuest->ResponseType == "myRadio") {
                                $inputPart = "<span class='radioClass_" . $getQuest->id . "'><input type='hidden' name='radioClass_" . $getQuest->id . "' value='radioClass_" . $getQuest->id . "'>";
                                // $getInput = DB::select("SELECT FormContent FROM form_builders where FormType = '" . $getQuest->ResponseType . "'");
                                $getInput = FormBuilder::where('FormType', '=', $getQuest->ResponseType)->first();
                                foreach (explode('__&&__', $getQuest->ResponseData) as $eachOpt) {
                                    if ($eachOpt != "" && $eachOpt != null) {
                                        // $f = str_replace('eachopt', $eachOpt, $getInput->FormContent);
                                        $inputPart = $inputPart . "<input type='radio' name='radioClass_" . $getQuest->id . "' class='toDisable_subforcampid largeChecks' id='val_subforcount' value='" . $eachOpt . "' ><span style='position: relative; top:-6px; left: 5px;'>" . $eachOpt . " </span>&emsp;";
                                    }
                                }
                                $inputPart = $inputPart . "</span>";
                                $eachQuest = $eachQuest . "<label>" . $getQuest->Question . "</label><br>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $inputPart)) . "<br><br>";
                            } else if ($getQuest->ResponseType == "myRange") {
                                $max = explode('-', explode('__&&__', $getQuest->ResponseData)[0])[0];
                                $min = explode('-', explode('__&&__', $getQuest->ResponseData)[0])[1];
                                //   $inputPart = "<input type='text' id='' name='rangeClass_' class='rangeSelect_".$getQuest->id."' value='' />";
                                $inputPart = "<input type='text' id='rangeSelect" . $getQuest->id . "' name='rangeClass_" . $getQuest->id . "' class='form-control rangeClass_" . $getQuest->id . "'  value='' /><div><span class='pull-left leastDesc rangeInfo' style='color: #333;   font-size: 12px; line-height: 1.333;text-shadow: none;top: 0;padding: 1px 5px; background: rgba(0,0,0,0.1); border-radius: 3px;'>" . explode('-', explode('__&&__', $getQuest->ResponseData)[1])[0] . "</span><span class='pull-right maxDesc rangeInfo' style='color: #333;   font-size: 12px; line-height: 1.333;text-shadow: none;top: 0;padding: 1px 5px; background: rgba(0,0,0,0.1); border-radius: 3px;'>" . explode('-', explode('__&&__', $getQuest->ResponseData)[1])[1] . "</span></div>";

                                $eachQuest = $eachQuest . " <li class='question-list'>" .
                                        "<div class='wrapper'>" .
                                        "<div class='item'>" .
                                        "<span><i class='fa fa-arrow-right' style='color: gold;'></i></span>" .
                                        "</div>" .
                                        " <div class='question'><span>" . $getQuest->Question . "</span></div>" .
                                        "<div class='response'>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $inputPart)) . "</div>" .
                                        "</div>" .
                                        " </li>" .
                                        "<script>" .
                                        "$('#rangeSelect" . $getQuest->id . "').ionRangeSlider({" .
                                        "hide_min_max: false," .
                                        " keyboard: true," .
                                        "min: '" . ($min) . "'," .
                                        "max: '" . ($max) . "'," .
                                        "type: 'single'," .
                                        "step: 1," .
                                        "grid: true" .
                                        "});" .
                                        " </script>";
                            }
                        }
                    }
                    $eachQuest = $eachQuest . "<input type='hidden' name='CampaigInfo' value='" . $myCampaign->CampaignID . "' /> </div>";
                    if (strlen(OurCampaign::find($myCampaign->CampaignID)->CampaignNotes) > 70) {
                        $det = '<span style=" display: inline;" class="popoverData" href="#" data-content="" rel="tooltip" data-placement="bottom" data-original-title="' . OurCampaign::find($myCampaign->CampaignID)->CampaignNotes . '" data-trigger="hover">' . substr(OurCampaign::find($myCampaign->CampaignID)->CampaignNotes, 0, 70) . '...</span>';
                    } else {
                        $det = OurCampaign::find($myCampaign->CampaignID)->CampaignNotes . '<br><br>';
                    }
                    if ($count > 0 && OurCampaign::find($myCampaign->CampaignID)->Status == 1) {
                    $IfCampaignSpecial = "";
                    
                                                             if(OurCampaign::find($myCampaign->CampaignID)->IfSpecial == 1){
                                                            $IfCampaignSpecial = '<div class="topTriangleSpecial"></div><div class="triangleTextSpecial">Special</div>';
                                                             }
if(OurCampaign::find($myCampaign->CampaignID)->IfSpecial == 2){
                                                            $IfCampaignSpecial = '<div class="topTriangleEnded"></div><div class="triangleTextEnded">Won</div>';
                                                             }
                    
                        $quotedRegStatus = "'" . @$request->regStatus . "'";
                        $quotedCampID = "'" . @$myCampaign->CampaignID . "'";
                        $img = "public/images/CampaignImages/" . @$myCampaign->CampaignID . "/" . OurCampaign::find($myCampaign->CampaignID)->ImagePath;
$countBoxes++;

                      //i removed this coz it adds the taken campaigns to the $frame variable, whereas i only want the available ones already implemented in the $ended and $unended var above this code.
                       /* $frames = $frames . ' <div class="grayout col-lg-4 campBoxes camp_' . $myCampaign->CampaignID . ' myClass boxCount_'.$countBoxes.'" align="center"><div class=" ">' .
                                ' <div class="box--sml text-center flush-pd-trailer col-lg-12">' .
                                $IfCampaignSpecial.
                                '<div class="block " href="#">' .
                                '<img src="' . $img . '" style="width: 100%;"><h5 class="box-title elltext">' . OurCampaign::find($myCampaign->CampaignID)->CampaignName . '</h5>' .
                                '<h5 class="box-title elltext" style="font-weight: 700;">' .
                                $special .
                                '</h5>' .
                                '<!--<h5>Cash Value: #30,000</h5>-->' .
                                '<h5 class="box-title elltext">' . $tillEnd . '</h5>' .
                                // '<h6><p>Details:' . $det . '</p></h6>' .
                                '<p class="push-sbit"><button class="btn btn-prize btn-xs campaign_count_' . $count . ' QuestClass" disabled>COMPLETED <i class="fa fa-arrow-right"></i></button></p>' .
                                // '</h6><p class="push-sbit"><button class="btn btn-primary btn-xs campaign_count_' . $count . ' QuestClass" onclick="checkReg(' . $quotedRegStatus . ', ' . $quotedCampID . ')">TAKE QUIZ</button></p>' .
                                '</div>' .
                                '<input type="hidden" value="' . $eachQuest . '" class="getQuest_' . $myCampaign->CampaignID . '">' .
                                ' <input type="hidden" value="' . $count . '" class="getCount_' . $myCampaign->CampaignID . '">' .
                                ' </div>' .
                                '</div>' .
                                '</div>';*/
                    }
                }

                $campCount++;
            }
            
            //   return view("previousCampaign")->with(compact('latest', 'latest'));
        }

        return response()->json(($frames));
    }



     public function RegisterUser(Request $request) {

        //lets validate
        $rules = array(
            'Surname' => 'required', // just a normal required validation
            'Other_Name' => 'required',
            'Email' => 'required|email', // required and must be unique in the ducks table
            'State' => 'required',
            'City' => 'required',
//            'Gender' => 'required',
//            'Age_Range' => 'required',
           'Phone_Number' => 'required|min:11|numeric', // required and has to match the password field
           // 'Password' => 'required|min:6',
          //  'Confirm_Password' => 'required|same:Password',
            'g-recaptcha-response' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        // check if the validator failed -----------------------
        if ($validator->fails()) {

            return Redirect::to('')
                            ->withErrors($validator)
                            ->withInput(Input::except('password1', 'password2'));
        } else {
            session_start();
            //get random referred code


           // $referrerCode = rand(100000000, 999999999);
//            $uniqueCode = "0";
//            while($checkIfCodeExist = usertable::where('ReferrerCode', '=', $referrerCode)->first())
//            {
//                $uniqueCode = (string)$referrerCode;
//            }
           $input = Input::only('Surname', 'Other_Name', 'Email', 'State', 'City', 'Phone_Number', 'Gender', 'Age_Range', 'ReferrerCode', 'Password', 'Confirm_Password');
           
           $getUser = usertable::where('Email', '=', $input['Email'])->first();
            if ($getUser) {
                return Redirect::to('')->with('message', 'Email Already Exists.');
            }
            $user = new usertable();
          
            $user->Surname = $input['Surname'];
            $user->Other_Name = $input['Other_Name'];
            $user->Email = $input['Email'];
            $user->State = $input['State'];
            $user->City = $input['City'];
//            $user->Gender = $input['Gender'];
//            $user->Age_Range = $input['Age_Range'];
            $user->Phone_Number = $input['Phone_Number'];
           // $user->Password = $input['Password'];
           // $user->Confirm_Password = $input['Confirm_Password'];
            $user->facebook_id = "Direct Reg.";
           // $user->ReferrerCode = $referrerCode;

            $user->save();

          // $checkEmail = usertable::where('Email', '=', $input['Email'])->where('Password', '=', md5($input['Password']))->first();
            $checkEmail = usertable::where('Email', '=', $input['Email'])->first();
            session()->put('logedIn', $checkEmail->id);
            Session::put('logedIn', $checkEmail->id);
            //Session::set('logedIn', 1);


            $getUser = usertable::where('Email', '=', $input['Email'])->first();
            if (!$getUser) {
                return Redirect::to('')->with('message', 'Unsuccessful! Error Occured.');
            }

            $linkPassword = rand(100000000, 999999999);
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < 30; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            ;

            $getUser->SentCode = $randomString . $linkPassword;
            $getUser->CodeTimeStart = Carbon::now();
            $getUser->save();

            @$headers = 'From: ' . "no-reply@kudipoll.com" . "\r\n";

         //  mail($input['Email'], "Kudipoll Account Confirmation", "Clink the link below to confirm your account \r\n http://kudipoll.com/confirmAccount/" . $randomString.$linkPassword, @$headers);
$this->mailSender("Kudipoll Account Confirmation", "Clink the link below to confirm your account <br><br> http://kudipoll.com/confirmAccount/" . $randomString.$linkPassword, $input['Email'], "info@kudipoll.com");

            return Redirect::to('')->with('message', 'Registeration Successful! \n We Have Sent A Link To Your Email For Account Confirmation.');
            // return redirect()->back()->with('message', 'SignUp Successful');
        }
    }

    public function confirmAccount($response) {
        $getUser = usertable::where('SentCode', '=', $response)->first();
        if (!$getUser) {
            return view("confirmpassword")->with('response', 'Link either expired or never existed');
        } else {
            $dateFromDatabase = strtotime((string) $getUser->CodeTimeStart);
            $dateTwelveHoursAgo = strtotime("-1440 minutes");

           if ($dateFromDatabase >= $dateTwelveHoursAgo) {
                // less than 5minutes ago
                //$getUser->UserStatus = 1;
               // $getUser->save();
                Session::put('logedIn', $getUser->id);
                  return Redirect::to('/completeRegistration');
             //   return Redirect::to('/successful')->with('regStatus', 'Complete');
          } else {
                // more than 5minutes ago
               return view("confirmpassword")->with('response', 'Link either expired or never existed');
           }
        }
    }

    public function completeRegistration()
    {
        
        return view("completeRegistration");
    }
    
    public function userComplete(Request $request)
    {
         $rules = array(
          
            'Gender' => 'required',
           'Age_Range' => 'required',
             'Password' => 'required|min:6',
            'Confirm_Password' => 'required|same:Password',
           // 'Phone_Number' => 'required|min:11|numeric', // required and has to match the password field
           
        );
        $validator = Validator::make(Input::all(), $rules);

        // check if the validator failed -----------------------
        if ($validator->fails()) {

            return Redirect::to('completeRegistration')
                            ->withErrors($validator);
                            //->withInput(Input::except('password1', 'password2'));
        } else {
            
          // $referrerCode = rand(100000000, 999999999);
             
          //  $input = Input::only('Gender', 'Age_Range', 'ReferrerCode', 'Password', 'Confirm_Password');
           $input = Input::only('Gender', 'Age_Range', 'Password', 'Confirm_Password');
            /* if (@$input['ReferrerCode'] != null && @$input['ReferrerCode'] != "") {
                $checkReferrerCode = usertable::where('ReferrerCode', '=', @$input['ReferrerCode'])->first();
                if (!$checkReferrerCode) {
                    return Redirect::to('/completeRegistration')->with('message', 'Incorrect Referrer Code');
                } else {
                    $user->ReferrerBy = @$input['ReferrerCode'];
                }
            }*/
            $user = usertable::find(Session::get('logedIn'));
         
             $user->Gender = $input['Gender'];
            $user->Age_Range = $input['Age_Range'];
            //$user->Phone_Number = $input['Phone_Number'];
            $user->Password = md5($input['Password']);
            $user->Confirm_Password = md5($input['Confirm_Password']);
           // $user->ReferrerCode = $referrerCode;
            $user->UserStatus = 1;
           
            $user->save();

            Session::put('logedIn', Session::get('logedIn'));
            Session::put('regStatus', 'Complete');
    
         return Redirect::to('/successful'); 
        }

    }

    public function CompleteRegPage() {
        $NigerianStates = array(
            'Abuja',
            'Abia',
            'Adamawa',
            'Akwa Ibom',
            'Anambra',
            'Bauchi',
            'Bayelsa',
            'Benue',
            'Borno',
            'Cross River',
            'Delta',
            'Ebonyi',
            'Edo',
            'Ekiti',
            'Enugu',
            'Gombe',
            'Imo',
            'Jigawa',
            'Kaduna',
            'Kano',
            'Katsina',
            'Kebbi',
            'Kogi',
            'Kwara',
            'Lagos',
            'Nassarawa',
            'Niger',
            'Ogun',
            'Ondo',
            'Osun',
            'Oyo',
            'Plateau',
            'Rivers',
            'Sokoto',
            'Taraba',
            'Yobe',
            'Zamfara',
        );
        $stateArray = "<option value=''>Your state Of residence</option>";
        foreach ($NigerianStates as $k) {
            $stateArray = $stateArray . "<option value='" . $k . "'>" . $k . "</option>";
        }
        return view('CompleteRegPage')->with('allStates', $stateArray);
    }

   
public function updateuserprofile(Request $request)
{

    try {
           
            
            $id = Session::get('logedIn');
            $user = usertable::where('facebook_id', $id)->first();
            if($user  == null)
            {
            
            $user = usertable::where('id', $id)->first(); 
            
            }
        
           // $referrerCode = rand(100000000, 999999999);

            $user->Phone_number = $request->Phone;
            $user->State = $request->State;
            $user->City = $request->City;
            $user->Surname= $request->SurName;
 $user->Other_Name= $request->Othername;

            if (@$request->ref != null && @$request->ref != "") {
                $checkReferrerCode = usertable::where('ReferrerCode', '=', @$request->ref)->first();
                if (!$checkReferrerCode) {
                    Session::put('regStatus', 'Incomplete');
                    return response()->json('Incorrect Referrer Code, Please Try Again.');
                } else {
                    $user->ReferrerBy = @$request->ref;
                }
            }
            Session::put('regStatus', 'Complete');
            // Save/update user. 
            // This will will update your the row in ur db.
            $user->save();

            // Session::set('logedIn', 1);w
            return response()->json("Success");

            // return Redirect::to('/successful')->with('UserName', $id);
        } catch (ModelNotFoundException $err) {
            //Show error page
             return response()->json('An Error Has Occured');

        }
}

     




    public function nowLoggedIn() {
        //   $logedInStatus =  session()->pull('logedIn');
        Session::put('camLimit', 20);
        if (Session::get('logedIn') != "" && Session::get('logedIn') != null) {
            // $id = session()->pull('logedIn');
            //save it to the session again so it wont forget
            Session::get('logedIn');

            $checkInfo = usertable::where('facebook_id', '=', Session::get('logedIn'))->first();
            if ($checkInfo == null) {
                $checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
            }




            if ($checkInfo->UserImg != null && $checkInfo->UserImg != "") {
                Session::put('DP', $checkInfo->UserImg);
            } else if ($checkInfo->facebook_id != null && $checkInfo->facebook_id != "" && $checkInfo->facebook_id != "Direct Reg.") {
                Session::put('DP', $checkInfo->facebook_id);
            } else {
                Session::put('DP', 'noDP');
            }



            // return view("Campaigns")->with('UserName', $checkInfo->Surname." ".$checkInfo->Other_Name);
            // $newQuest = DB::table('clientcampaigns')->whereNotIn('id', function($q){
            //$q->select('CampaignID')->from('answer_records');
//})->get();
            // $newQuest = DB::select("SELECT * FROM clientcampaigns WHERE id NOT IN ( SELECT CampaignID FROM answer_records )");
            // foreach ($newQuest as $eachRow) {
            //$getQuest = CampaignQuestions::all()->where('CampaignID', '=', $eachRow->id); 
            //DB::select("SELECT * FROM campaign_questions WHERE CampaignID = $eachRow->id");
            // }
            //  $latest = DB::table('campaign_questions')
            // ->select('*')->where('CampaignID', '=', DB::table('clientcampaigns')->select('id')->whereNotIn('CampaignID', '=', DB::table('answer_records')->select('CampaignID')))->get();
//return view("IndexPage")->with('regStatus', 'Incomplete')->with('allStates', $stateArray);
            $regStatus = Session::get('regStatus');
            $stateArray = Session::get('allStates');
            // $prizeTotal = DB::raw("SELECT sum(NumberAvailable) FROM availableprizes");

            $prizeTotal = DB::table('availableprizes')
                    ->sum('NumberAvailable');
            if ($regStatus == "Complete") {
                
                $forCompleted = DB::select("SELECT pm.* FROM campaign_questions pm Natural left join answer_records WHERE pm.CampaignID in (SELECT pd.CampaignID FROM answer_records pd where UserID = " . $checkInfo->id . ")");
                $latest = DB::select("SELECT pm.* FROM campaign_questions pm Natural left join answer_records WHERE pm.CampaignID NOT IN (SELECT pd.CampaignID FROM answer_records pd where UserID = " . $checkInfo->id . ") order by pm.ID");
                $winners = DB::select("SELECT * FROM our_winners");
                $allPrizes = DB::select("SELECT * FROM availableprizes limit 7");
                if ($checkInfo->ReferrerCode != null && $checkInfo->ReferrerCode != "") {
                    $referredUsers = DB::select("SELECT * FROM usertables where ReferrerBy = " . $checkInfo->ReferrerCode . "  limit 7");
                } else {
                    $referredUsers = array();
                }
                $countCamp = DB::select("SELECT count(*) from our_campaigns");
                $allQuestions = DB::select("SELECT * from our_campaigns where EndDate >= " . Carbon::now()->format('Y-m-d')." and StartDate >= " . Carbon::now()->format('Y-m-d')." and Status = 1 ");
$countEndedPrizes = count(OurCampaign::where('Status', '=', 1)->where('EndDate', '<', Carbon::now())->get());
                $countQuest = 0;
                $arrayCountCampaign = array();
                foreach ($allQuestions as $eachQuestion) {
                    $confirmQuest = AnswerRecord::where('CampaignID', '=', $eachQuestion->id)->where('UserID', '=', Session::get('logedIn'))->first();
                  // $confirmQuest = DB::select("SELECT Distinct UserID from answer_records where UserID = ".Session::get('logedIn')." and ");
                   
                    if ($confirmQuest != null && !in_array($confirmQuest->id, $arrayCountCampaign)) {
                        $countQuest++;
                         $arrayCountCampaign[] = $confirmQuest->id;
                    }
                }
                 foreach ($allQuestions as $eachQuestion) {
                    $confirmQuest = CampaignQuestions::where('CampaignID', '=', $eachQuestion->id)->first();
                    if ($confirmQuest == null) {
                        $countQuest++;
                    }
                }
                $allanswered = DB::select("SELECT DISTINCT CampaignID FROM answer_records where UserID = " . $checkInfo->id);
                return view("IndexPage")->with('UserName', @$checkInfo)
                                ->with(@compact('latest', 'latest'))
                                ->with(@compact('winners', 'winners'))
                                ->with(@compact('allPrizes', 'allPrizes'))
                                ->with(@compact('referredUsers', 'referredUsers'))
                                ->with(@compact('prizeTotal', $prizeTotal))
                                ->with('regStatus', 'Complete')
                                ->with('answered', @(count($allanswered)))
                                ->with('DP', @Session::get('DP'))
                                ->with(@compact('forCompleted', 'forCompleted'))
                                ->with('available', @(count($allQuestions)-$countQuest))
                                ->with('accountBalance', (string)$checkInfo->UserBalance);
            } else if ($regStatus == "Incomplete") {
                $NigerianStates = array(
                    'Abuja',
                    'Abia',
                    'Adamawa',
                    'Akwa Ibom',
                    'Anambra',
                    'Bauchi',
                    'Bayelsa',
                    'Benue',
                    'Borno',
                    'Cross River',
                    'Delta',
                    'Ebonyi',
                    'Edo',
                    'Ekiti',
                    'Enugu',
                    'Gombe',
                    'Imo',
                    'Jigawa',
                    'Kaduna',
                    'Kano',
                    'Katsina',
                    'Kebbi',
                    'Kogi',
                    'Kwara',
                    'Lagos',
                    'Nassarawa',
                    'Niger',
                    'Ogun',
                    'Ondo',
                    'Osun',
                    'Oyo',
                    'Plateau',
                    'Rivers',
                    'Sokoto',
                    'Taraba',
                    'Yobe',
                    'Zamfara',
                );
                $stateArray = "<option value=''>Your state of residence</option>";
                foreach ($NigerianStates as $k) {
                    $stateArray = $stateArray . "<option value='" . $k . "'>" . $k . "</option>";
                }
                 $forCompleted = DB::select("SELECT pm.* FROM campaign_questions pm Natural left join answer_records WHERE pm.CampaignID in (SELECT pd.CampaignID FROM answer_records pd where UserID = " . $checkInfo->id . ")");
                $latest = DB::select("SELECT pm.* FROM campaign_questions pm Natural left join answer_records WHERE pm.CampaignID NOT IN (SELECT pd.CampaignID FROM answer_records pd where UserID = " . $checkInfo->id . ") order by pm.ID");
                $winners = DB::select("SELECT * FROM our_winners");
                $allPrizes = DB::select("SELECT * FROM availableprizes limit 7");
                if ($checkInfo->ReferrerCode != null && $checkInfo->ReferrerCode != "") {
                    $referredUsers = DB::select("SELECT * FROM usertables where ReferrerBy = " . $checkInfo->ReferrerCode . "  limit 7");
                } else {
                    $referredUsers = array();
                }
                $countCamp = DB::select("SELECT count(*) from our_campaigns");
                $allQuestions = DB::select("SELECT * from our_campaigns where EndDate > " . Carbon::now()->format('Y-m-d')." and StartDate >= " . Carbon::now()->format('Y-m-d')." and Status = 1 ");
               $countEndedPrizes = count(OurCampaign::where('Status', '=', 1)->where('EndDate', '<', Carbon::now())->get());
                $countQuest = 0;
                $arrayCountCampaign = array();
                foreach ($allQuestions as $eachQuestion) {
                    $confirmQuest = AnswerRecord::where('CampaignID', '=', $eachQuestion->id)->where('UserID', '=', Session::get('logedIn'))->first();
                    if ($confirmQuest != null && !in_array($confirmQuest->id, $arrayCountCampaign)) {
                        $countQuest++;
                         $arrayCountCampaign[] = $confirmQuest->id;
                    }
                }
                 foreach ($allQuestions as $eachQuestion) {
                    $confirmQuest = CampaignQuestions::where('CampaignID', '=', $eachQuestion->id)->first();
                    if ($confirmQuest == null) {
                        $countQuest++;
                    }
                }
                $allanswered = DB::select("SELECT DISTINCT CampaignID FROM answer_records where UserID = " . $checkInfo->id);
                return view("IndexPage")->with('UserName', @$checkInfo)
                                ->with(@compact('latest', 'latest'))
                                ->with(@compact('winners', 'winners'))
                                ->with(@compact('allPrizes', 'allPrizes'))
                                ->with(@compact('referredUsers', 'referredUsers'))
                                ->with(@compact('prizeTotal', $prizeTotal))
                                ->with('regStatus', 'Incomplete')
                                ->with('answered', @(count($allanswered)))
                                ->with('DP', @Session::get('DP'))
                                 ->with(@compact('forCompleted', 'forCompleted'))
                                ->with('available',  @(count($allQuestions)-$countQuest))
                                ->with('allStates', @$stateArray)
                                ->with('accountBalance', (string)$checkInfo->UserBalance);
            } else {
                $NigerianStates = array(
                    'Abuja',
                    'Abia',
                    'Adamawa',
                    'Akwa Ibom',
                    'Anambra',
                    'Bauchi',
                    'Bayelsa',
                    'Benue',
                    'Borno',
                    'Cross River',
                    'Delta',
                    'Ebonyi',
                    'Edo',
                    'Ekiti',
                    'Enugu',
                    'Gombe',
                    'Imo',
                    'Jigawa',
                    'Kaduna',
                    'Kano',
                    'Katsina',
                    'Kebbi',
                    'Kogi',
                    'Kwara',
                    'Lagos',
                    'Nassarawa',
                    'Niger',
                    'Ogun',
                    'Ondo',
                    'Osun',
                    'Oyo',
                    'Plateau',
                    'Rivers',
                    'Sokoto',
                    'Taraba',
                    'Yobe',
                    'Zamfara',
                );
                $stateArray = "<option value=''>Your state of residence</option>";
                foreach ($NigerianStates as $k) {
                    $stateArray = $stateArray . "<option value='" . $k . "'>" . $k . "</option>";
                }
                $campignId = 0;
                 $forCompleted = DB::select("SELECT pm.* FROM campaign_questions pm Natural left join answer_records WHERE pm.CampaignID in (SELECT pd.CampaignID FROM answer_records pd where UserID = " . $checkInfo->id . ")");
                $latest = DB::select("SELECT pm.* FROM campaign_questions pm Natural left join answer_records WHERE pm.CampaignID NOT IN (SELECT pd.CampaignID FROM answer_records pd where UserID = " . $checkInfo->id . ") order by pm.ID");
                foreach ($latest as $d) {
                    $campignId = $d->CampaignID;
                }
                $winners = DB::select("SELECT * FROM our_winners");
                $allPrizes = DB::select("SELECT * FROM availableprizes limit 7");
                if ($checkInfo->ReferrerCode != null && $checkInfo->ReferrerCode != "") {
                    $referredUsers = DB::select("SELECT * FROM usertables where ReferrerBy = " . $checkInfo->ReferrerCode . "  limit 7");
                } else {
                    $referredUsers = array();
                }
                  $countCamp = DB::select("SELECT count(*) from our_campaigns");
                $allQuestions = DB::select("SELECT * from our_campaigns where EndDate > " . Carbon::now()->format('Y-m-d')." and StartDate >= " . Carbon::now()->format('Y-m-d')." and Status = 1 ");
$countEndedPrizes = count(OurCampaign::where('Status', '=', 1)->where('EndDate', '<', Carbon::now())->get());
                $countQuest = 0;
                $arrayCountCampaign = array();
                foreach ($allQuestions as $eachQuestion) {
                    $confirmQuest = AnswerRecord::where('CampaignID', '=', $eachQuestion->id)->where('UserID', '=', Session::get('logedIn'))->first();
                    if ($confirmQuest != null && !in_array($confirmQuest->id, $arrayCountCampaign)) {
                        $countQuest++;
                         $arrayCountCampaign[] = $confirmQuest->id;
                    }
                }
                foreach ($allQuestions as $eachQuestion) {
                    $confirmQuest = CampaignQuestions::where('CampaignID', '=', $eachQuestion->id)->first();
                    if ($confirmQuest == null) {
                        $countQuest++;
                    }
                }
                $allanswered = DB::select("SELECT DISTINCT CampaignID FROM answer_records where UserID = " . $checkInfo->id);

                return view("IndexPage")->with('UserName', @$checkInfo)
                                ->with(@compact('latest', 'latest'))
                                ->with(@compact('winners', 'winners'))
                                ->with(@compact('allPrizes', 'allPrizes'))
                                ->with(@compact('referredUsers', 'referredUsers'))
                                ->with('regStatus', 'unknown')
                                ->with('answered', @(count($allanswered)))
                                ->with(@compact('prizeTotal', $prizeTotal))
                                ->with('DP', @Session::get('DP'))
                                ->with('available',  @(count($allQuestions)-$countQuest))
                                ->with('answered', @(count($allanswered)))
                                ->with(@compact('forCompleted', 'forCompleted'))
                                ->with('allStates', @$stateArray)
                                ->with('accountBalance', (string)$checkInfo->UserBalance);
            }
        } else {
            return Redirect::to('');
        }
//        if ($request->session()->exists('logedIn')) {
//            return Redirect::to('/successful');
//        } else {
//            return Redirect::to('');
//        }
    }

  

    public function prevCamp() {
        if (Session::get('logedIn') != "" && Session::get('logedIn') != null) {
            // $id = session()->pull('logedIn');
            //save it to the session again so it wont forget
            Session::get('logedIn');
            //$checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
            $checkInfo = usertable::where('facebook_id', '=', Session::get('logedIn'))->first();
            if ($checkInfo == null) {
                $checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
            }
            $latest = DB::select("SELECT pm.* FROM campaign_questions pm Natural left join answer_records WHERE pm.CampaignID in (SELECT pd.CampaignID FROM answer_records pd where UserID = " . $checkInfo->id . ")");

            $winners = DB::select("SELECT * FROM our_winners limit 5");

            return view("previousCampaign")->with('UserName', $checkInfo)
                            ->with(compact('latest', 'latest'))
                            ->with(compact('winners', 'winners'));
        } else {
            return Redirect::to('');
        }
    }



    public function alreadyAnswered(Request $request) {
        $answer_record = AnswerRecord::where('UserID', '=', Session::get('logedIn'))->where('CampaignID', '=', $request->campaignID)->get();
        if ($answer_record) {
            return response()->json(var_dump($answer_record));
        }
    }

    

    public function getMore(Request $request) {
        //$campLimit = (int)Session::get('campLimit') + 1;

        Session::put('campLimit', (Session::get('campLimit') + 20));
        $frames = "";
        $test = "";

        if ($request->action == "done") {

            $special = "";

            // $checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
            $checkInfo = usertable::where('facebook_id', '=', Session::get('logedIn'))->first();
            if ($checkInfo == null) {
                $checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
            }
//            $latest = DB::select("SELECT pm.* FROM campaign_questions pm Natural left join answer_records WHERE pm.CampaignID in (SELECT pd.CampaignID FROM answer_records pd where UserID = " . $checkInfo->id . ") limit ". Session::get('campLimit'));
            $latest = DB::select("SELECT pm.* FROM campaign_questions pm Natural left join answer_records WHERE pm.CampaignID in (SELECT pd.CampaignID FROM answer_records pd where UserID = " . $checkInfo->id . ")");
            $count = 0;
            $allId = array();
            $campCount = 0;

            foreach ($latest as $myCampaign) {
                $test = $test . "entered";
                $tillEnd = (Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->diffInDays(Carbon::now()) + 1) == 1 ? 'Campaign Ends today' : (Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->diffInDays(Carbon::now()) + 1) . ' Day(s) to end';
                $specialPrize = SpecialPrize::where('CampaignID', '=', $myCampaign->CampaignID)->first();
                if ($specialPrize) {
                    $special = '<h6>Special Prize: ' . $specialPrize->SpecialPrize . '</h6>';
                } else {
                   $special = '<h5>&emsp;</h5>';
                }
                if (!in_array($myCampaign->CampaignID, $allId)) {

                    $allId[] = $myCampaign->CampaignID;
                    $count++;

                    $eachQuest = "<div class='questionWrapper'>";
                    csrf_field();
                    $countForm = 0;
                    foreach ($latest as $getQuest) {
                        if ($getQuest->CampaignID == $myCampaign->CampaignID) {

                            $countForm++;
                            // $eachQuest = $eachQuest . "<label>" . $getQuest->Question . "</label><br>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $getQuest->ResponseData)) . "<br><br><input type='hidden' name='QuestID' class='Quest_" . $getQuest->id . "' value='" . $getQuest->id . "' />";
                            if ($getQuest->ResponseType == "mySelect") {
                                $inputPart = "";
                                // $getInput = DB::select("SELECT FormContent FROM form_builders where FormType = '" . $getQuest->ResponseType . "'");
                                $getInput = FormBuilder::where('FormType', '=', $getQuest->ResponseType)->first();
                                foreach (explode('__&&__', $getQuest->ResponseData) as $eachOpt) {
                                    if ($eachOpt != "" && $eachOpt != null)
                                        $inputPart = $inputPart . "<option value='" . $eachOpt . "'>" . $eachOpt . "</option>";
                                }
                                $finalInput = str_replace('eachopt', $inputPart, $getInput->FormContent);
                                $eachQuest = $eachQuest . "<label>" . $getQuest->Question . "</label><br>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $finalInput)) . "<br><br><input type='hidden' name='QuestID' class='Quest_" . $getQuest->id . "' value='" . $getQuest->id . "' />";
                            }
                            else if ($getQuest->ResponseType == "myCheck") {
                                $inputPart = "<span class='checkBoxClass_" . $getQuest->id . "'><input type='hidden' name='checkBoxClass_" . $getQuest->id . "' value='checkBoxClass_" . $getQuest->id . "'>";

                                // $getInput = DB::select("SELECT FormContent FROM form_builders where FormType = '" . $getQuest->ResponseType . "'");
                                $getInput = FormBuilder::where('FormType', '=', $getQuest->ResponseType)->first();
                                foreach (explode('__&&__', $getQuest->ResponseData) as $eachOpt) {
                                    if ($eachOpt != "" && $eachOpt != null) {

                                        // $f = str_replace('eachopt', $eachOpt, $getInput->FormContent);
                                        $inputPart = $inputPart . "<input type='checkbox' name='checkbox' class='toDisable_subforcampid largeChecks' id='val_subforcount' value='" . $eachOpt . "' >" . $eachOpt . " </span>&emsp;";
                                    }
                                }
                                $inputPart = $inputPart . "</span>";
                                $eachQuest = $eachQuest . "<label>" . $getQuest->Question . "</label><br>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $inputPart)) . "<br><br>";
                            } else if ($getQuest->ResponseType == "myRadio") {
                                $inputPart = "<span class='radioClass_" . $getQuest->id . "'><input type='hidden' name='radioClass_" . $getQuest->id . "' value='radioClass_" . $getQuest->id . "'>";
                                // $getInput = DB::select("SELECT FormContent FROM form_builders where FormType = '" . $getQuest->ResponseType . "'");
                                $getInput = FormBuilder::where('FormType', '=', $getQuest->ResponseType)->first();
                                foreach (explode('__&&__', $getQuest->ResponseData) as $eachOpt) {
                                    if ($eachOpt != "" && $eachOpt != null) {
                                        // $f = str_replace('eachopt', $eachOpt, $getInput->FormContent);
                                        $inputPart = $inputPart . "<input type='radio' name='radioClass_" . $getQuest->id . "' class='toDisable_subforcampid largeChecks' id='val_subforcount' value='" . $eachOpt . "' ><span style='position: relative; top:-6px; left: 5px;'>" . $eachOpt . " </span>&emsp;";
                                    }
                                }
                                $inputPart = $inputPart . "</span>";
                                $eachQuest = $eachQuest . "<label>" . $getQuest->Question . "</label><br>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $inputPart)) . "<br><br>";
                            } else if ($getQuest->ResponseType == "myRange") {
                                $max = explode('-', explode('__&&__', $getQuest->ResponseData)[0])[0];
                                $min = explode('-', explode('__&&__', $getQuest->ResponseData)[0])[1];
                                //   $inputPart = "<input type='text' id='' name='rangeClass_' class='rangeSelect_".$getQuest->id."' value='' />";
                                $inputPart = "<input type='text' id='rangeSelect" . $getQuest->id . "' name='rangeClass_" . $getQuest->id . "' class='form-control rangeClass_" . $getQuest->id . "'  value='' /><div><span class='pull-left leastDesc rangeInfo' style='color: #333;   font-size: 12px; line-height: 1.333;text-shadow: none;top: 0;padding: 1px 5px; background: rgba(0,0,0,0.1); border-radius: 3px;'>" . explode('-', explode('__&&__', $getQuest->ResponseData)[1])[0] . "</span><span class='pull-right maxDesc rangeInfo' style='color: #333;   font-size: 12px; line-height: 1.333;text-shadow: none;top: 0;padding: 1px 5px; background: rgba(0,0,0,0.1); border-radius: 3px;'>" . explode('-', explode('__&&__', $getQuest->ResponseData)[1])[1] . "</span></div>";

                                $eachQuest = $eachQuest . "<label>" . $getQuest->Question . "</label><br>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $inputPart)) . "<br><br>" .
                                        "<script>" .
                                        "$('#rangeSelect" . $getQuest->id . "').ionRangeSlider({" .
                                        "hide_min_max: false," .
                                        " keyboard: true," .
                                        "min: '" . ($min) . "'," .
                                        "max: '" . ($max) . "'," .
                                        "type: 'single'," .
                                        "step: 1," .
                                        "grid: true" .
                                        "});" .
                                        " </script>";
                            }
                        }
                    }
                    $eachQuest = $eachQuest . "<input type='hidden' name='CampaigInfo' value='" . $myCampaign->CampaignID . "' /> </div>";
                    if ($count > 0 && Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->addDay() > Carbon::now()) {
                    $IfCampaignSpecial = "";
                    
                                                             if(OurCampaign::find($myCampaign->CampaignID)->IfSpecial == 1){
                                                            $IfCampaignSpecial = '<div class="topTriangleSpecial"></div><div class="triangleTextSpecial">Special</div>';
                                                             }
if(OurCampaign::find($myCampaign->CampaignID)->IfSpecial == 2){
                                                            $IfCampaignSpecial = '<div class="topTriangleEnded"></div><div class="triangleTextEnded">Won</div>';
                                                             }
                    
                        $quotedRegStatus = "'" . @$request->regStatus . "'";
                        $quotedCampID = "'" . @$myCampaign->CampaignID . "'";
                        $img = "public/images/CampaignImages/" . @$myCampaign->CampaignID . "/" . OurCampaign::find($myCampaign->CampaignID)->ImagePath;
                        $frames = $frames . ' <div class=" col-lg-4 campBoxes camp_' . $myCampaign->CampaignID . ' myClass" align="center"><div class=" ">' .
                                ' <div class="box--sml text-center flush-pd-trailer col-lg-12">' .
                                $IfCampaignSpecial.
                                '<div class="block " href="#">' .
                                '<img src="' . $img . '" width="100%"><h6 class="box-title elltext">' . OurCampaign::find($myCampaign->CampaignID)->CampaignName . '</h6>' .
                                '<h6 class="box-title elltext">' . $tillEnd . '</h6>' .
                                '<h6 class="box-title elltext" style="font-weight: 700;">' .
                                $special .
                                '<h6><p>Details:' . OurCampaign::find($myCampaign->CampaignID)->CampaignNotes . '</p></h6>' .
                                '</h6><p class="push-sbit"><button class="btn btn-primary btn-xs campaign_count_' . $count . ' QuestClass" onclick="checkReg(' . $quotedRegStatus . ', ' . $quotedCampID . ')">TAKE QUIZ</button></p>' .
                                '</div>' .
                                '<input type="hidden" value="' . $eachQuest . '" class="getQuest_' . $myCampaign->CampaignID . '">' .
                                ' <input type="hidden" value="' . $count . '" class="getCount_' . $myCampaign->CampaignID . '">' .
                                ' </div>' .
                                '</div>' .
                                '</div>';
                    }
                }

                $campCount++;
            }
            //   return view("previousCampaign")->with(compact('latest', 'latest'));
        } else if ($request->action == "notdone") {

            $special = "";
            $test = "";
            // $checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
            $checkInfo = usertable::where('facebook_id', '=', Session::get('logedIn'))->first();
            if ($checkInfo == null) {
                $checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
            }
            //  $latest = DB::select("SELECT pm.* FROM campaign_questions pm Natural left join answer_records WHERE pm.CampaignID not in (SELECT pd.CampaignID FROM answer_records pd where UserID = " . $checkInfo->id . ") limit ".Session::get('campLimit'));
            $latest = DB::select("SELECT pm.* FROM campaign_questions pm Natural left join answer_records WHERE pm.CampaignID not in (SELECT pd.CampaignID FROM answer_records pd where UserID = " . $checkInfo->id . ") order by pm.ID");
            $count = 0;
            $allId = array();
            $campCount = 0;
            foreach ($latest as $myCampaign) {
                $test = $test . "entered";
                //  $tillEnd = (Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->diffInDays(Carbon::now()->format('Y-m-d')) + 1) == 1 ? 'Campaign Ends today' : (Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->diffInDays(Carbon::now()->format('Y-m-d')) + 1) . ' Day(s) to end';
                $tillEnd = (Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->diffInDays(Carbon::now()) + 1) == 1 ? 'Campaign Ends today' : (Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->diffInDays(Carbon::now()) + 1) . ' Day(s) to end';
                $specialPrize = SpecialPrize::where('CampaignID', '=', $myCampaign->CampaignID)->first();
                if ($specialPrize) {
                    $special = '<h5>Special Prize: ' . $specialPrize->SpecialPrize . '</h5>';
                } else {
                    $special = '<h5>&emsp;</h5>';
                }
                if (!in_array($myCampaign->CampaignID, $allId)) {

                    $allId[] = $myCampaign->CampaignID;
                    $count++;

                    $eachQuest = "<div class='questionWrapper'>";
                    csrf_field();
                    $countForm = 0;
                    foreach ($latest as $getQuest) {
                        if ($getQuest->CampaignID == $myCampaign->CampaignID) {

                            $countForm++;
                            $eachQuest = $eachQuest . "<label>" . $getQuest->Question . "</label><br>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $getQuest->ResponseData)) . "<br><br><input type='hidden' name='QuestID' class='Quest_" . $getQuest->id . "' value='" . $getQuest->id . "' />";
                        }
                    }
                    $eachQuest = $eachQuest . "<input type='hidden' name='CampaigInfo' value='" . $myCampaign->CampaignID . "' /> </div>";

                    if ($count > 0 && Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->addDay() > Carbon::now()) {
                    $IfCampaignSpecial = "";
                    
                                                             if(OurCampaign::find($myCampaign->CampaignID)->IfSpecial == 1){
                                                            $IfCampaignSpecial = '<div class="topTriangleSpecial"></div><div class="triangleTextSpecial">Special</div>';
                                                             }
if(OurCampaign::find($myCampaign->CampaignID)->IfSpecial == 2){
                                                            $IfCampaignSpecial = '<div class="topTriangleEnded"></div><div class="triangleTextEnded">Won</div>';
                                                             }
                        $quotedRegStatus = "'" . @$request->regStatus . "'";
                        $quotedCampID = "'" . @$myCampaign->CampaignID . "'";
                        $img = "public/images/CampaignImages/" . @$myCampaign->CampaignID . "/" . OurCampaign::find($myCampaign->CampaignID)->ImagePath;
                        $frames = $frames . ' <div class=" col-lg-4 campBoxes camp_' . $myCampaign->CampaignID . ' myClass" align="center"><div class=" ">' .
                                ' <div class="box--sml text-center flush-pd-trailer col-lg-12">' .
                                $IfCampaignSpecial.
                                '<div class="block " href="#">' .
                                '<img src="' . $img . '" width="100%"><h6 class="box-title elltext">' . OurCampaign::find($myCampaign->CampaignID)->CampaignName . '</h6>' .
                                '<h6 class="box-title elltext">' . $tillEnd . '</h6>' .
                                '<h6 class="box-title elltext" style="font-weight: 700;">' .
                                $special .
                                '<h6><p>Details:' . OurCampaign::find($myCampaign->CampaignID)->CampaignNotes . '</p></h6>' .
                                '</h6><p class="push-sbit"><button class="btn btn-primary btn-xs campaign_count_' . $count . ' QuestClass" onclick="checkReg(' . $quotedRegStatus . ', ' . $quotedCampID . ')">TAKE QUIZ</button></p>' .
                                '</div>' .
                                '<input type="hidden" value="' . $eachQuest . '" class="getQuest_' . $myCampaign->CampaignID . '">' .
                                ' <input type="hidden" value="' . $count . '" class="getCount_' . $myCampaign->CampaignID . '">' .
                                ' </div>' .
                                '</div>' .
                                '</div>';
                    }
                }

                $campCount++;
            }
            //   return view("previousCampaign")->with(compact('latest', 'latest'));
        }
        //  return response()->json($latest);
        return response()->json($frames);
    }

    public function uploadDP() {

       $input = Input::only('fupload');
      
     $ext = explode('.', $input['fupload']->getClientOriginalName())[1];

     $input['fupload']->move(public_path('images'), $input['fupload']->getClientOriginalName());
 
    return response()->json($input['fupload']->getClientOriginalName());
//return response()->json('let start');
    }

    public function upDateDBColumn(Request $request) {
       $getUser = usertable::where('id', '=', Session::get('logedIn'))->first();
if(!$getUser)
{
  $getUser = usertable::where('facebook_id', '=', Session::get('logedIn'))->first();
}
        $ext = explode('.', $request->imgName)[1];
        $getUser->UserImg = Session::get('logedIn') . '.' . $ext;
        $getUser->save();

       copy('public/images/' . $request->imgName, 'public/images/' . Session::get('logedIn') . '.' . $ext);
        return response()->json(Session::get('logedIn') . '.' . $ext); 
      //  return response()->json(Session::get('logedIn'));
    }
    
    public function updateUserPassword()
    {
    
    $input = Input::only('password','password1', 'codeSent');            
    $user = usertable::where('SentCode', '=', $input['codeSent'])->first();
    $user->Password = md5($input['password']);
    $user->Confirm_Password= md5($input['password1']);
      $user->Level = 1;
    $user->save();
    return Redirect::to('');
    
    }

    public function confirmpassword($response) {
        $getUser = usertable::where('SentCode', '=', $response)->first();
        
        $getUser->save();
        if (!$getUser) {
            return Redirect::to('');
        } else {
            $dateFromDatabase = strtotime((string) $getUser->CodeTimeStart);
            $dateTwelveHoursAgo = strtotime("-5 minutes");

            if ($dateFromDatabase >= $dateTwelveHoursAgo && $getUser->Level == 1) {
                // less than 5minutes ago
               // $changePassword = '<div class="changePass"><input type="password" id="password" class="form-control" palaceholder="Enter Password"/><br><br><input type="password" id="password1" class="form-control" palaceholder="Retype Password"/><br><br><button type="button"class="btn btn-primary btn-xs">Save Changes</button></div>';
                //$myContent = '<input type="hidden" value="' . $changePassword . '" class="myPasswordChange" />';
                //return view("confirmpassword")->with('response', $myContent);
                return view("confirmpassword")->with('ForUser', $response);
            } else {
                // more than 5minutes ago
                return Redirect::to('');
            }
        }
    }



public function getMyProfile(Request $request)
{
$id = Session::get('logedIn');
            $user = usertable::where('facebook_id', $id)->first();
            if($user  == null)
            {
            
            $user = usertable::where('id', $id)->first(); 
            
            }

return response()->json($user);
}




}