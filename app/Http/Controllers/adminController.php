<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\AnswerRecord;
use App\usertable;
use App\adminList;
use App\availableprizes;
use App\OurWinner;
use App\OurCampaign;
use App\SpecialPrize;
use App\CampaignQuestions;
use App\Clients;
use Session;
use DB;
use Carbon\Carbon;
use App\SessionKeeper;
use App\FormBuilder;
use App\BalanceHistorys;
use App\CashRequest;
use PHPMailer;

class adminController extends Controller {
public function adminLogin() {
return view('adminLogin');
}
 public function adminAuth(Request $request)
    {
    
    $checkAdmin = adminList::where('Username','=',$request->username)
            ->where('Password','=',md5($request->password))
            ->first(); 
    if($checkAdmin)
    {
                             Session::put('adminLogin', $checkAdmin->id);
              return Redirect::to('/AdminPage');
    }
    
    return Redirect::to('/adminLogin')->with('message', 'Error');
    }

public function filterHistory(Request $request)
{

 $getFilteredHistory = DB::select("select * from balance_historys where UserID = ".$request->userId." and Date(DayTime) >= '".$request->fromDate."' and DATE(DayTime) < '".$request->toDate."'");
 
$totalHistory = array();
foreach($getFilteredHistory as $eachHistory)
{

 if($eachHistory->Response == "Credited"){
$allHistory = array();
$allHistory[] = $eachHistory->DayTime;
$allHistory[] = $eachHistory->Amount;
$allHistory[] = $eachHistory->CurrentBalance;
$allHistory[] = $eachHistory->Response;
$totalHistory[] = $allHistory;

}
    //$allHistory[] =  explode($eachHistory->Response, " ")[0].' Received '.$eachHistory->Amount.' Naira on '. $eachHistory->DayTime.' for taking survey and new balance is '.$eachHistory->CurrentBalance;
    else{

$allHistory = array();
$allHistory[] = $eachHistory->DayTime;
$allHistory[] = $eachHistory->Amount;
$allHistory[] = $eachHistory->CurrentBalance;
$allHistory[] = $eachHistory->Response;
$totalHistory[] = $allHistory;

    //$allHistory[] =  explode($eachHistory->Response, " ")[0].' was Debited of '.$eachHistory->Amount.' Naira on '. $eachHistory->DayTime.' after withdrawal and new balance is'.$eachHistory->CurrentBalance;
}
}
return response()->json($totalHistory);

}

public function getIndividualBalanceHistory(Request $request)
{

$getHistory = BalanceHistorys::where('UserID', '=', $request->userId)->get();
$totalHistory = array();
foreach($getHistory as $eachHistory)
{

 if($eachHistory->Response == "Credited"){
$allHistory = array();
$allHistory[] = $eachHistory->DayTime;
$allHistory[] = $eachHistory->Amount;
$allHistory[] = $eachHistory->CurrentBalance;
$allHistory[] = $eachHistory->Response;
$totalHistory[] = $allHistory;

}
    //$allHistory[] =  explode($eachHistory->Response, " ")[0].' Received '.$eachHistory->Amount.' Naira on '. $eachHistory->DayTime.' for taking survey and new balance is '.$eachHistory->CurrentBalance;
    else{

$allHistory = array();
$allHistory[] = $eachHistory->DayTime;
$allHistory[] = $eachHistory->Amount;
$allHistory[] = $eachHistory->CurrentBalance;
$allHistory[] = $eachHistory->Response;
$totalHistory[] = $allHistory;

    //$allHistory[] =  explode($eachHistory->Response, " ")[0].' was Debited of '.$eachHistory->Amount.' Naira on '. $eachHistory->DayTime.' after withdrawal and new balance is'.$eachHistory->CurrentBalance;
}
}
return response()->json($totalHistory);

}





   
    public function createCampaign() {
        $allCampaigns = DB::select("SELECT * FROM our_campaigns");
        $allClients = DB::select("SELECT * FROM clients where Status = 1");
        return view("CreateCampaign")
                        ->with(compact('allCampaigns', $allCampaigns))
                        ->with(compact('allClients', $allClients));
    }

    public function saveQuestion(Request $request) {
        //concatenate responsedata

        $checkCamp = OurCampaign::find($request->CampaignID);
        if ($checkCamp) {
            $allOpts = "";
            foreach ($request->ResponseType as $responseType) {
                $allOpts = $allOpts . $responseType . "__&&__";
            }

            $question = new CampaignQuestions();
            $question->Question = $request->Question;
            $question->ClientID = $request->ClientID;
            $question->CampaignID = $request->CampaignID;
            $question->ResponseData = $allOpts;
            $question->ResponseType = $request->myResponse;
            //$question->ExpectedResponse = $request->responseExpected;
            $question->save();
            return response()->json("Submit Successful");
        }
        return response()->json("Campaign Not Found");
    }

    public function newCampaign(Request $request) {

    
       $getDuplicate = OurCampaign::where('ClientID', '=', $request->allClients)
                ->where('CampaignName', '=', $request->newCampaign)
                ->where('CampaignNotes', '=', $request->campNotes)
                ->where('EndDate', '=', $request->campEnd)
                ->where('StartDate', '=', $request->campStart)
                ->where('PrizeName', '=', $request->prizeName)
                ->where('ImagePath', '=', str_replace(' ', '', $request->ImagePath))
                ->where('NonCampaign', '=', (integer)$request->prizeType)
                ->where('StartDate', '=', $request->campStart)
                ->first();

            if($getDuplicate)
            {

              return response()->json('Already Exists');

            }

         $newCamp = new OurCampaign();
        
        if($request->allClients == "N/A"){
        $newCamp->ClientID = 0;
        }
        else
        {
        $newCamp->ClientID = $request->allClients;
        }
        
        $newCamp->CampaignName = $request->newCampaign;
        $newCamp->CampaignNotes = $request->campNotes;
        $newCamp->EndDate = $request->campEnd;
        $newCamp->StartDate = $request->campStart;
        $newCamp->Status = 1;
        
        if($newCamp->IfSpecial == "N/A"){
         $newCamp->IfSpecial = 0;
        }
        else
        {
        $newCamp->IfSpecial = $request->campType;
        }
        
       
        $newCamp->PrizeName = $request->prizeName;
        $newCamp->ImagePath = str_replace(' ', '', $request->ImagePath);
        $newCamp->NonCampaign = (integer)$request->prizeType;
        
        $getTowinAmount = OurCampaign::first()->ToWin;
        $newCamp->ToWin = $getTowinAmount;
        $newCamp->save();

        $getId = OurCampaign::where('ClientID', '=', $request->allClients)
                ->where('CampaignName', '=', $request->newCampaign)
                ->where('CampaignNotes', '=', $request->campNotes)
                ->where('EndDate', '=', $request->campEnd)
                ->first();
        if (!file_exists('public/images/CampaignImages/' . $getId->id)) {
            mkdir('public/images/CampaignImages/' . $getId->id, 0777, true);
        }
        return response()->json($getId->id);
        
        
          
        
    }

    public function AdminPage() {
    if(Session::get('adminLogin') == null)
    {
    
      return Redirect::to('adminLogin');
    }


      //get campaign info to dashboard EndDate > " . Carbon::now()->format('Y-m-d')."
  $Campaigns = OurCampaign::take(20)->get();
  
    //get campaign count dashboard
      
  //get campaign info to dashboard 
  $countOngoingCampaigns = count(OurCampaign::where('EndDate', '>=',  Carbon::now()->format('Y-m-d'))->get());
  $countEndedCampaigns = count(OurCampaign::where('EndDate', '<',  Carbon::now()->format('Y-m-d'))->get());
  
  $AllCampaigns = OurCampaign::all();
  
  
  //get count winners
  $userArray = array();
  
  foreach($AllCampaigns as $eachOfCamp)
  {
  
  $leads = AnswerRecord::where('CampaignID', '=', $eachOfCamp->id)->where('LeadStatus', '=', 1)->first();
  if($leads)
  {
  
  $users = usertable::where('id', '=', $leads->UserID)->first();
  $userArray[] = $users;
  
  }
  
  }
  
 
  //get count winners
  $winnerArray = array();
  
  foreach($AllCampaigns as $eachOfCamp)
  {
  
  $winner = OurWinner::where('CampaignID', '=', $eachOfCamp->id)->first();
  
  if($winner)
  {
  
  //$getWinners = usertable::find($winner->UserID);
  $winnerArray[] = $winner;
  
  }
  
  }
   
   $specialPrizes = OurCampaign::where('IfSpecial', '=', 1)->take(20)->get();
  
  return view('AdminPage')
  ->with(compact('Campaigns', 'Campaigns'))
  ->with(compact('userArray', 'userArray'))
  ->with('ongoingCampSize', $countOngoingCampaigns)
  ->with('endedCampSize', $countEndedCampaigns)
  ->with('leadSize', count($userArray))
  ->with(compact('winnerArray', 'winnerArray'))
  ->with(compact('specialPrizes', 'specialPrizes'))
  ->with('winnerSize', count($winnerArray));
  
      //  return view("AdminPage");
    }



    public function CampaignCreate() {
    if(Session::get('adminLogin') == null)
    {
    
       return Redirect::to('adminLogin');
    }
      
$allCampaigns = DB::select("SELECT * FROM our_campaigns");
        $allClients = DB::select("SELECT * FROM clients where Status = 1");
       
        $campaigns = OurCampaign::all();
        $clients = Clients::where('Status', '=', 1);
        return view("CampaignList")
                        ->with('campaigns', $campaigns)
                        ->with('clients', $clients)
 ->with(compact('allCampaigns', $allCampaigns))
                        ->with(compact('allClients', $allClients));
    }

    public function AddQuestion(Request $request) {
    if(Session::get('adminLogin') == null)
    {
    
      return Redirect::to('adminLogin');
    }
        $campaignName = DB::select("SELECT * FROM our_campaigns where id = '" . $request . "'");
        $allCampaigns = DB::select("SELECT * FROM our_campaigns");
        $allClients = DB::select("SELECT * FROM clients where Status = 1");
        return view("AddQuestion")
                        ->with(compact('allCampaigns', $allCampaigns))
                        ->with(compact('allClients', $allClients))
                        ->with(compact('campaignName', $campaignName));
    }

    public function Report() {
    if(Session::get('adminLogin') == null)
    {
    
       return Redirect::to('adminLogin');
    }
        return view("Report");
    }

    public function CampaignList() {
    if(Session::get('adminLogin') == null)
    {
    
      return Redirect::to('adminLogin');
    }

 $allCampaigns = DB::select("SELECT * FROM our_campaigns where NonCampaign = 1");
        $allClients = DB::select("SELECT * FROM clients where Status = 1");
      // $count
        $campaigns = OurCampaign::all();
        $clients = Clients::where('Status', '=', 1);
        return view("CampaignList")
                        ->with('campaigns', $campaigns)
                        ->with('clients', $clients)
 ->with(compact('allCampaigns', $campaigns))
                        ->with(compact('allClients', $allClients));
    }

   

    public function delCamp(Request $request) {

$checkAnswer = AnswerRecord::where('CampaignID', '=', $request->campId)->first();
if($checkAnswer)
{
return response()->json("Campaign can not be deleted after user participation");
}
        $checkId = OurCampaign::find($request->campId);
$checkQuest = CampaignQuestions::where('CampaignID', '=', $request->campId)->get();
        if ($checkId) {
            DB::table('our_campaigns')->delete($request->campId);
if ($checkQuest) {
foreach($checkQuest as $eachId)
{
DB::table('campaign_questions')->delete($eachId->id);
}

}
            return response()->json("Delete Successful");
        }
        return response()->json("Campaign Does Not Exist");
    }
    
    public function delClient(Request $request) {
        $checkId = Clients::find($request->clientId);
        if ($checkId) {
            DB::table('clients')->delete($request->clientId);
            return response()->json("Delete Successful");
        }
        return response()->json("Client Does Not Exist");
    }
    

    function getCamp(Request $request) {
        $disCamp = OurCampaign::find($request->campId);

        return response()->json($disCamp);
    }

    public function uploadCampaignImage() {

        $input = Input::only('fupload', 'campId');

        $ext = explode('.', $input['fupload']->getClientOriginalName())[1];

        $input['fupload']->move(public_path('images/CampaignImages/' . $input['campId']), str_replace(' ', '', $input['fupload']->getClientOriginalName()));
        //  
        return response()->json($input['campId']);
        // return response()->json($dummyName.'.'.$ext);
    }

    public function updateCampaignImg() {
        $input = Input::only('updateUpload', 'campId');

        $ext = explode('.', $input['updateUpload']->getClientOriginalName())[1];
        $files = glob('images/CampaignImages/' . $input['campId'] . '/*'); // get all file names
        foreach ($files as $file) { // iterate files
            if (is_file($file))
                unlink($file); // delete file
        }
        $input['updateUpload']->move(public_path('images/CampaignImages/' . $input['campId']), $input['updateUpload']->getClientOriginalName());

        //now update image name on db
        $getCamp = OurCampaign::find($input['campId']);
        $getCamp->ImagePath = $input['updateUpload']->getClientOriginalName();
        $getCamp->save();

        return response()->json($input['updateUpload']->getClientOriginalName());
        // return response()->json($dummyName.'.'.$ext);
    }

function toggleSpecialPrize(Request $request)
{

$getCamp = OurCampaign::find($request->campId);
        if($getCamp->IfSpecial == 1)
        {
        $getCamp->IfSpecial = 2;
        }
        else{
        $getCamp->IfSpecial = 1;
        }
        $getCamp->save();
        return response()->json($getCamp->IfSpecial);

}

public function checkIfWinnerExists(Request $request)
{

$checkIfWinner = ourwinner::where('CampaignID', '=', $request->campId)->first();
if($checkIfWinner)
{
return response()->json('Winner Already Exists');

}
return response()->json('No Winner Yet');

}

    function updateCamp(Request $request) {
        $getCamp = OurCampaign::find($request->campId);
        $getCamp->CampaignName = $request->allVal[0];
        if($request->allVal[1] == "N/A")
        {
        //meaning it just a prize. Not campaign
        $getCamp->ClientID = 0;
        }
        else{
        $getCamp->ClientID = $request->allVal[1];
        }
        $getCamp->CampaignNotes = $request->allVal[2];
        
        $startDate = $request->allVal[3];
        if (strpos($startDate, '/') !== false) {
            $startDate = explode('/', $request->allVal[3])[2] . explode('/', $request->allVal[3])[0] . explode('/', $request->allVal[3])[1];
        }
        $getCamp->StartDate = $startDate;

        $endDate = $request->allVal[4];
        if (strpos($endDate, '/') !== false) {
            $endDate = explode('/', $request->allVal[4])[2] . explode('/', $request->allVal[4])[0] . explode('/', $request->allVal[4])[1];
        }
        $getCamp->EndDate = $endDate;
        
        if($request->allVal[5] == "N/A")
        {
        //meaning it just a prize. Not campaign
        $getCamp->IfSpecial = 0;
        }
        else
        {
        
        $getCamp->IfSpecial = $request->allVal[5];
        
        }
        
        $getCamp->PrizeName = $request->allVal[6];
        
        $getCamp->save();
        return response()->json("Update Successful");
    }

    /*public function CreateClient() {
    if(Session::get('adminLogin') == null)
    {
    
      return Redirect::to('adminLogin');
    }
        return view("CreateClient");
    }*/

    public function addNewClient(Request $request) {
    
         $getDuplicate = Clients::where('CompanyName', '=', $request->allVal[0])
                ->where('CompanyAddress', '=', $request->allVal[1])
                ->where('Email', '=', $request->allVal[2])
                ->where('Username', '=', $request->allVal[3])
                ->where('Password', '=', md5($request->allVal[4]))
                ->first();

            if($getDuplicate)
            {

              return response()->json('Already Exists');

            }

        $clientTable = new Clients();
        $clientTable->CompanyName = $request->allVal[0];
        $clientTable->CompanyAddress = $request->allVal[1];
        $clientTable->Email = $request->allVal[2];
        $clientTable->FirstTimeUser = 1;
        $clientTable->Username = $request->allVal[3];
        $clientTable->Password = md5($request->allVal[4]);
        $checkExist= Clients::where('CompanyName', '=', $request->allVal[0])->first();
        if($checkExist)
        {
        return response()->json("Company Already Exist");
        }
        
        if(!$clientTable->save()){
        
        return response()->json("Error Occured!");
        }else
        {
        //topehereonclient
        $this->mailSender("Kudipoll Client Account Activation", "Hello, <br><br> Welcome to Kudipoll. <br><br>Your Company Name is ".$request->allVal[0]."<br><br> Your Default Password is ".$request->allVal[4]." <br><br> Kindly follow the steps below to activate your account; <br><br> <ol><li>Visit <a href='http://kudipoll.com/clientLogin/".$request->allVal[0]."' alt='Kudipoll' title='Kudipoll Client Panel'> http://kudipoll.com/clientLogin/".$request->allVal[0]."</a></li><li>Complete the ACTIVATE ACCOUNT form with the default password above.</li></ol> <br><br>  Regards.", $request->allVal[2], "info@kudipoll.com");
        $getDetails = Clients::where('CompanyName', '=', $request->allVal[0])->first();
         return response()->json($getDetails->id);
     
        }
        
    }

    public function ViewClient() {
    if(Session::get('adminLogin') == null)
    {
    
       return Redirect::to('adminLogin');
    }
        $allClients = Clients::all();
        return view("ViewClient")
                        ->with('allClients', $allClients);
    }

    public function FilterCampaign() {
    
    if(Session::get('adminLogin') != null && Session::get('clientLogin') != null)
    {
    
    return Redirect::to('');
    
    }
    
    if(Session::get('adminLogin') != null)
    {
    
   
        $allCampaign = OurCampaign::where('NonCampaign', '=', 1)->get();
        Session::put('adminLogin', 1);
        //update session keeper
        $sessionKeeper = SessionKeeper::where('ForUser', '=', Session::get('adminLogin'))->first();
        if ($sessionKeeper != null) {
            $sessionKeeper->Status = 0;
            $sessionKeeper->save();
        } else {
            $sessionKeeper = new SessionKeeper();
            $sessionKeeper->Status = 0;
            $sessionKeeper->ForUser = Session::get('adminLogin');
            $sessionKeeper->Entity = "CamppaignFilter Table Record";
            $sessionKeeper->save();
        }

        return view("FilterCampaign")
                        ->with('allCampaign', $allCampaign)->with('UserRole', 'isAdmin');
                        }
                        
                         if(Session::get('clientLogin') != null)
    {
    
         
        $allCampaign = OurCampaign::where('ClientID', '=', Session::get('clientLogin'))->get();
        Session::put('clientLogin', 1);
        //update session keeper
        $sessionKeeper = SessionKeeper::where('ForUser', '=', Session::get('clientLogin'))->first();
        if ($sessionKeeper != null) {
            $sessionKeeper->Status = 0;
            $sessionKeeper->save();
        } else {
            $sessionKeeper = new SessionKeeper();
            $sessionKeeper->Status = 0;
            $sessionKeeper->ForUser = Session::get('clientLogin');
            $sessionKeeper->Entity = "CamppaignFilter Table Record";
            $sessionKeeper->save();
        }

        return view("FilterCampaign")
                        ->with('allCampaign', $allCampaign)->with('UserRole', 'isClient');
                        }
                         return Redirect::to('');
                        
    }

    public function getFilterQuestions(Request $request) {
        $allCampaign = CampaignQuestions::where('CampaignID', '=', $request->campId)->get();
        return response()
                        ->json($allCampaign);
    }

    public function getQuestOpt(Request $request) {
        $updateSessionKeeper = SessionKeeper::where('ForUser', '=', Session::get('adminLogin'))->first();
       if(!$updateSessionKeeper)
       {
       
       $updateSessionKeeper = SessionKeeper::where('ForUser', '=', Session::get('clientLogin'))->first();
       
       }
        
        $updateSessionKeeper->Status = 0;
        $updateSessionKeeper->save();

        $quest = CampaignQuestions::find($request->questId);
        $getResponse = $quest->ResponseType;
        $inputType = "";
        $countSteps = 0;
        if ($getResponse == "myCheck") {
            foreach (explode('__&&__', $quest->ResponseData) as $responseData) {
                if ($responseData != "") {
                    $countSteps++;
                    $inputType = $inputType . "<input type='checkbox' name='inputType' value='" . $responseData . "' id='checkFilter' class='largeInput'>" . $responseData . "&emsp;";
                }
            }
            $inputType = $inputType . "__myCheck_" . $countSteps;
        } else if ($getResponse == "myRadio") {
            foreach (explode('__&&__', $quest->ResponseData) as $responseData) {
                if ($responseData != "")
                    $inputType = $inputType . "<input type='radio' name='inputType' value='" . $responseData . "' class='largeInput'>" . $responseData . "&emsp;";
            }
            $inputType = $inputType . "__myRadio";
        } else if ($getResponse == "mySelect") {
            $inputType = "<select id='selectOpt' name='selectOpt' class='form-control'>";
            foreach (explode('__&&__', $quest->ResponseData) as $responseData) {
                if ($responseData != "")
                    $inputType = $inputType . "<option value='" . $responseData . "'>" . $responseData . "</option>";
            }

            $inputType = $inputType . "</select>__mySelect";
        } else if ($getResponse == "myRange") {

            $max = explode('-', explode('__&&__', $quest->ResponseData)[0])[0];
            $min = explode('-', explode('__&&__', $quest->ResponseData)[0])[1];
            //   $inputPart = "<input type='text' id='' name='rangeClass_' class='rangeSelect_".$getQuest->id."' value='' />";
            $inputType = "<input type='text' id='rangeSelect" . $quest->id . "' name='rangeClass_" . $quest->id . "' class='form-control rangeClass_" . $quest->id . "'  value='' /><div><span class='pull-left leastDesc rangeInfo' style='color: #333;   font-size: 12px; line-height: 1.333;text-shadow: none;top: 0;padding: 1px 5px; background: rgba(0,0,0,0.1); border-radius: 3px;'>" . explode('-', explode('__&&__', $quest->ResponseData)[1])[0] . "</span><span class='pull-right maxDesc rangeInfo' style='color: #333;   font-size: 12px; line-height: 1.333;text-shadow: none;top: 0;padding: 1px 5px; background: rgba(0,0,0,0.1); border-radius: 3px;'>" . explode('-', explode('__&&__', $quest->ResponseData)[1])[1] . "</span></div>__myRange__" . $min . "__" . $max;
        }
        return response()
                        ->json($inputType);
    }

public function getUserResponse(Request $request)
{
$test = "";
$eachData = "<div style='padding: 10px;' align='left'><br>";
$getAnswerRec = DB::select("SELECT * from answer_records where CampaignID = " . $request->campId." and UserID = ".$request->userId);
$countQuest = 1;
foreach($getAnswerRec as $eachQuest)
{

$getQuest = DB::select("select * from campaign_questions where id =".$eachQuest->QuestionID);
$test = $test.",".$getQuest[0]->Question ;
if($getQuest != null && $eachQuest != null){
//$eachData = $eachData. "<b>Question ".$countQuest.":</b> ".$getQuest->Question. "<br> &emsp; <b>Response:</b>".explode('__&&__', $eachQuest->Answer)[0];
$eachData = $eachData. "<br><b>Question ".$countQuest.":</b> ".$getQuest[0]->Question. "<br> &emsp;  <br>  <b>Response:</b> ".explode('__&&__', $eachQuest->Answer)[0]."<br><br>";
$countQuest++;
}
}

return response()->json($eachData."</div>");

}

public function filterAndAnswers(Request $request)
{

//get inputTypes
$countQuest = 0;

//now,i want to get total filter length, i.e how much was all before paging it to 50 per time. I will use the variable $fullRecord
$fullRecord = 0;

$questType = array();
$getCurrentPage = SessionKeeper::where('ForUser', '=', Session::get('adminLogin'))->first();
        if(!$getCurrentPage)
        {
        $getCurrentPage = SessionKeeper::where('ForUser', '=', Session::get('clientLogin'))->first();
        }
foreach($request->questType as $eachQuestType)
{

$questType[] = $eachQuestType;
}

$filters = array();
foreach($request->filterBy as $eachFilter)
{

$filters[] = $eachFilter;
}

$aggregateReport = [];
foreach($request->questId as $eachQuest)
{
$getUserId = [];
 $allUsers = [];
 $leadArray = [];
if($questType[$countQuest] == "mySelect" || $questType[$countQuest] == "myRadio"){

 
        $filterRecord = DB::select("SELECT distinct UserID from answer_records where CampaignID = " . $request->campId . " and QuestionID = " . $eachQuest . " and Answer = '" . $filters[$countQuest] . "__&&__'");
       $fullRecord  = sizeof($filterRecord);
       
        foreach ($filterRecord as $eachUserID) {
        $getUserId[] = $eachUserID->UserID;
            $allUsers[] = DB::select("SELECT * from usertables where id = " . $eachUserID->UserID);
      // $getLeadStatus = AnswerRecord::where('CampaignID', '=', $request->campId)->where('UserID', '=', $eachUserID->UserID)->first();
       //  $leadArray[] = $getLeadStatus->LeadStatus;
        }
        
}
else if($questType[$countQuest] == "myRange")
{



        $filterRecord = DB::select("SELECT distinct UserID from answer_records where CampaignID = " . $request->campId . " and QuestionID = " . $eachQuest  . " and Answer >= " . $filters[$countQuest]);
        
        $fullRecord  = sizeof($filterRecord);
        
        foreach ($filterRecord as $eachUserID) {
$getUserId[] = $eachUserID->UserID;
            $allUsers[] = DB::select("SELECT * from usertables where id = " . $eachUserID->UserID." and UserStatus = 1 and Password != ''");
     //  $getLeadStatus = AnswerRecord::where('CampaignID', '=', $request->campId)->where('UserID', '=', $eachUserID->UserID)->first();
        // $leadArray[] = $getLeadStatus->LeadStatus;
        }

   

}
else if($questType[$countQuest] == "myCheck")
{

        
        $queryPart = "";
        
        foreach ($filters[$countQuest] as $eachCheck) {
        
        $queryPart  = $queryPart."and Answer like '%" . $eachCheck . "%'";
        
        }
            $filterRecord = DB::select("SELECT distinct UserID from answer_records where CampaignID = " . $request->campId . " and QuestionID = " . $eachQuest ." ".$queryPart);
$fullRecord  = sizeof($filterRecord);
         
        foreach ($filterRecord as $eachUserID) {
$getUserId[] = $eachUserID->UserID;
            $allUsers[] = DB::select("SELECT * from usertables where id = " . $eachUserID->UserID." and UserStatus = 1 and Password != ''");
      // $getLeadStatus = AnswerRecord::where('CampaignID', '=', $request->campId)->where('UserID', '=', $eachUserID->UserID)->first();
       //  $leadArray[] = $getLeadStatus->LeadStatus;
        }
        


}

$aggregateReport[] = $getUserId;
$countQuest++;

}



//now find intersection of users

$increament = 1;
$innerArrayLevel = [];
$allArrays  = [];
$allUsersArray = [];
$countLeads = 0;

if($request->filterType == 'AND'){
foreach($aggregateReport as $eachOfAggregate){
if($increament == 1)
{
$allArrays[] = array_intersect($aggregateReport[0], $aggregateReport[$increament]);
}else{
if(isset($allArrays[$increament-2]))
$allArrays[] = array_intersect($allArrays[$increament-2], $aggregateReport[$increament-1]);

}
$increament++;
}
}
else if($request->filterType == 'OR')
{

foreach($aggregateReport as $eachOfAggregate){
if($increament == 1)
{
if(!isset($aggregateReport[1]))
{
$allArrays[] = $aggregateReport[0];
break;
}
else{
$allArrays[] = array_merge($aggregateReport[0], $aggregateReport[$increament]);
}
}else{
if(isset($allArrays[$increament-2]))
$allArrays[] = array_merge($allArrays[$increament-2], $aggregateReport[$increament-1]);

}
$increament++;
}
$allArrays[] = array_unique($allArrays[sizeof($allArrays)-1]);
}

//now get all users by id
$countUsers = 0;
foreach($allArrays[sizeof($allArrays)-1] as $usersHere)
{
    $allUsersArray[] = DB::select("SELECT * from usertables where id = " . $usersHere ." and UserStatus = 1 and Password != ''");
   $checkLeadStatus = DB::select("SELECT * from answer_records where UserID = ". $usersHere ." and CampaignID = ".$request->campId ." and LeadStatus = 1");
   $getLeadStatus = AnswerRecord::where('CampaignID', '=', $request->campId)->where('UserID', '=', $usersHere)->first();
   $leadArray[] = $getLeadStatus->LeadStatus;
    if($checkLeadStatus)
    {
    $countLeads++;
    }
    $countUsers++;
}


$pageArray = [];
        $countByPage = 0;
        $countActual = 0;
        $beginiFrom = 0;
        $endAt = 0;
        foreach ($allUsersArray as $eachUser) {
         if($request->actionType == "Normal")
        {
        	 $pageArray[] = $eachUser;
        	 $countActual++;
        }
           else if ($request->actionType == "Next") {
                if ($countByPage >= $getCurrentPage->Status && $countActual < 50) {
                    $pageArray[] = $eachUser;
                    $countActual++;
                    if ($countActual == 1) {
                        $beginiFrom = $countByPage + 1;
                    }
                }
                $countByPage++;
            } else if ($request->actionType == "Previous") {
                if ($countByPage >= $getCurrentPage->Status - 50 && $countActual < 50) {
                    $pageArray[] = $eachUser;

                    $countActual++;
                    if ($countActual == 1) {
                        $beginiFrom = $countByPage + 1;
                    }
                }
                $countByPage++;
            }
        }
        $endAt = $countActual + ($beginiFrom - 1);
        if (sizeof($pageArray) > 0) {
            $currentStatus = $getCurrentPage->Status;
            if ($request->actionType == "Next") {
                if ($currentStatus + $countActual >= 0)
                    $getCurrentPage->Status = $currentStatus + $countActual;
            } else if ($request->actionType == "Previous") {
                if ($currentStatus - $countActual >= 0)
                    $getCurrentPage->Status = $currentStatus - $countActual;
            } else if ($request->actionType == "Normal") {
                if ($countActual >= 0)
                    $getCurrentPage->Status = $countActual;
            }
            $getCurrentPage->save();
        }
        
         if($beginiFrom == 0)
       {
       $endAt = $endAt + 1;
       $beginiFrom = $beginiFrom + 1;
       }
        
        $getNumTotal = DB::select("SELECT * from usertables where UserStatus = 1 and Password != '' ");
        
        $FrontEndData = [];
        $FrontEndData[] = $pageArray;
        $FrontEndData[] = $beginiFrom;
        $FrontEndData[] = sizeof($allUsersArray);
        $FrontEndData[] = $endAt;
        $FrontEndData[] = $leadArray;
        
        //sends number of peaple in arrray
        $FrontEndData[] = $increament-1;
        
        //send total number of people
        $FrontEndData[] = sizeof($getNumTotal);
        
        //send number of leads
        $FrontEndData[] = $countLeads;
        
        //send total number of users filtered before 50 pertime pagination
        $FrontEndData[] = $fullRecord;
        
return response()->json($FrontEndData);
//return response()->json($allArrays);
//return response()->json($aggregateReport );

//return response()->json($allArrays[sizeof($allArrays)-1]);


}


    public function filterAnswers(Request $request) {
        // $filterRecord = AnswerRecord::where('CampaignID', '=', $request->campId)
        // ->where('QuestionID', '=', $request->questId)
        // ->where('Answer', '=', $request->filterBy)->get();
        $getCurrentPage = SessionKeeper::where('ForUser', '=', Session::get('adminLogin'))->first();
        if(!$getCurrentPage)
        {
        $getCurrentPage = SessionKeeper::where('ForUser', '=', Session::get('clientLogin'))->first();
        }
        $filterRecord = DB::select("SELECT distinct UserID from answer_records where CampaignID = " . $request->campId . " and QuestionID = " . $request->questId . " and Answer = '" . $request->filterBy . "__&&__'");
        $allUsers = [];
       $leadArray = [];
        foreach ($filterRecord as $eachUserID) {

            $allUsers[] = DB::select("SELECT * from usertables where id = " . $eachUserID->UserID);
       $getLeadStatus = AnswerRecord::where('CampaignID', '=', $request->campId)->where('UserID', '=', $eachUserID->UserID)->first();
         $leadArray[] = $getLeadStatus->LeadStatus;
        }
        //now pick by page
        $pageArray = [];
        $countByPage = 0;
        $countActual = 0;
        $beginiFrom = 0;
        $endAt = 0;
        foreach ($allUsers as $eachUser) {
        if($request->actionType == "Normal")
        {
        	 $pageArray[] = $eachUser;
        	 $countActual++;
        }
           else if ($request->actionType == "Next" ) {
           
                if ($countByPage >= $getCurrentPage->Status && $countActual < 50) {
                    $pageArray[] = $eachUser;
                    $countActual++;
                    if ($countActual == 1) {
                        $beginiFrom = $countByPage + 1;
                    }
                }
                $countByPage++;
            } else if ($request->actionType == "Previous") {
                if ($countByPage >= $getCurrentPage->Status - 50 && $countActual < 50) {
                    $pageArray[] = $eachUser;

                    $countActual++;
                    if ($countActual == 1) {
                        $beginiFrom = $countByPage + 1;
                    }
                }
                $countByPage++;
            }
        }
        $endAt = $countActual + ($beginiFrom - 1);
        if (sizeof($pageArray) > 0) {
            $currentStatus = $getCurrentPage->Status;
            if ($request->actionType == "Next") {
                if ($currentStatus + $countActual >= 0)
                    $getCurrentPage->Status = $currentStatus + $countActual;
            } else if ($request->actionType == "Previous") {
                if ($currentStatus - $countActual >= 0)
                    $getCurrentPage->Status = $currentStatus - $countActual;
            } else if ($request->actionType == "Normal") {
                if ($countActual >= 0)
                    $getCurrentPage->Status = $countActual;
            }
            $getCurrentPage->save();
        }
        
         if($beginiFrom == 0)
       {
       $endAt = $endAt + 1;
       $beginiFrom = $beginiFrom + 1;
       }
        
        $FrontEndData = [];
        $FrontEndData[] = $pageArray;
        $FrontEndData[] = $beginiFrom;
        $FrontEndData[] = sizeof($allUsers);
        $FrontEndData[] = $endAt;
        $FrontEndData[] = $leadArray;
        return response()->json($FrontEndData);
    }

    public function filterRangeAnswers(Request $request) {

        $getCurrentPage = SessionKeeper::where('ForUser', '=', Session::get('adminLogin'))->first();
        if(!$getCurrentPage)
        {
        $getCurrentPage = SessionKeeper::where('ForUser', '=', Session::get('clientLogin'))->first();
        }

        $filterRecord = DB::select("SELECT distinct UserID from answer_records where CampaignID = " . $request->campId . " and QuestionID = " . $request->questId . " and Answer >= " . $request->filterBy);
        $allUsers = [];
        
        $leadArray = [];
        foreach ($filterRecord as $eachUserID) {

            $allUsers[] = DB::select("SELECT * from usertables where id = " . $eachUserID->UserID);
       $getLeadStatus = AnswerRecord::where('CampaignID', '=', $request->campId)->where('UserID', '=', $eachUserID->UserID)->first();
         $leadArray[] = $getLeadStatus->LeadStatus;
        }

        //now pick by page
        $pageArray = [];
        $countByPage = 0;
        $countActual = 0;
        $beginiFrom = 0;
        $endAt = 0;
        foreach ($allUsers as $eachUser) {
   
        if($request->actionType == "Normal")
        {
      //  if ($countByPage >= $getCurrentPage->Status && $countActual < 50) {
                    $pageArray[] = $eachUser;
                    //$countActual++;
                   // if ($countActual == 1) {
                    //    $beginiFrom = $countByPage + 1;
                   // }
             //   }
        }
                  else  if ($request->actionType == "Next") {
                if ($countByPage >= $getCurrentPage->Status && $countActual < 50) {
                    $pageArray[] = $eachUser;
                    $countActual++;
                    if ($countActual == 1) {
                        $beginiFrom = $countByPage + 1;
                    }
                }
                $countByPage++;
            } else if ($request->actionType == "Previous") {
                if ($countByPage >= $getCurrentPage->Status - 50 && $countActual < 50) {
                    $pageArray[] = $eachUser;

                    $countActual++;
                    if ($countActual == 1) {
                        $beginiFrom = $countByPage + 1;
                    }
                }
                $countByPage++;
            }
        }
        $endAt = $countActual + ($beginiFrom - 1);
        if (sizeof($pageArray) > 0) {
            $currentStatus = $getCurrentPage->Status;
            if ($request->actionType == "Next") {
                if ($currentStatus + $countActual >= 0)
                    $getCurrentPage->Status = $currentStatus + $countActual;
            } else if ($request->actionType == "Previous") {
                if ($currentStatus - $countActual >= 0)
                    $getCurrentPage->Status = $currentStatus - $countActual;
            } else if ($request->actionType == "Normal") {
                if ($countActual >= 0)
                    $getCurrentPage->Status = $countActual;
            }
            $getCurrentPage->save();
        }
        $FrontEndData = [];
        $FrontEndData[] = $pageArray;
        $FrontEndData[] = $beginiFrom;
        $FrontEndData[] = sizeof($allUsers);
        $FrontEndData[] = $endAt;
         $FrontEndData[] = $leadArray;

        return response()->json($FrontEndData);
    }

    public function filterSelectAnswers(Request $request) {
       $getCurrentPage = SessionKeeper::where('ForUser', '=', Session::get('adminLogin'))->first();
        if(!$getCurrentPage)
        {
        $getCurrentPage = SessionKeeper::where('ForUser', '=', Session::get('clientLogin'))->first();
        }
        $allUsers = [];
        $queryPart = "";
        
        foreach ($request->filterBy as $eachCheck) {
        
        $queryPart  = $queryPart."and Answer like '%" . $eachCheck . "%'";
        
        }
            $filterRecord = DB::select("SELECT distinct UserID from answer_records where CampaignID = " . $request->campId . " and QuestionID = " . $request->questId ." ".$queryPart);

          $leadArray = [];
        foreach ($filterRecord as $eachUserID) {

            $allUsers[] = DB::select("SELECT * from usertables where id = " . $eachUserID->UserID);
       $getLeadStatus = AnswerRecord::where('CampaignID', '=', $request->campId)->where('UserID', '=', $eachUserID->UserID)->first();
         $leadArray[] = $getLeadStatus->LeadStatus;
        }
        

        $pageArray = [];
        $countByPage = 0;
        $countActual = 0;
        $beginiFrom = 0;
        $endAt = 0;
        foreach ($allUsers as $eachUser) {
         if($request->actionType == "Normal")
        {
        	 $pageArray[] = $eachUser;
        }
           else if ($request->actionType == "Next") {
                if ($countByPage >= $getCurrentPage->Status && $countActual < 50) {
                    $pageArray[] = $eachUser;
                    $countActual++;
                    if ($countActual == 1) {
                        $beginiFrom = $countByPage + 1;
                    }
                }
                $countByPage++;
            } else if ($request->actionType == "Previous") {
                if ($countByPage >= $getCurrentPage->Status - 50 && $countActual < 50) {
                    $pageArray[] = $eachUser;

                    $countActual++;
                    if ($countActual == 1) {
                        $beginiFrom = $countByPage + 1;
                    }
                }
                $countByPage++;
            }
        }
        $endAt = $countActual + ($beginiFrom - 1);
        if (sizeof($pageArray) > 0) {
            $currentStatus = $getCurrentPage->Status;
            if ($request->actionType == "Next") {
                if ($currentStatus + $countActual >= 0)
                    $getCurrentPage->Status = $currentStatus + $countActual;
            } else if ($request->actionType == "Previous") {
                if ($currentStatus - $countActual >= 0)
                    $getCurrentPage->Status = $currentStatus - $countActual;
            } else if ($request->actionType == "Normal") {
                if ($countActual >= 0)
                    $getCurrentPage->Status = $countActual;
            }
            $getCurrentPage->save();
        }
        $FrontEndData = [];
        $FrontEndData[] = $pageArray;
        $FrontEndData[] = $beginiFrom;
        $FrontEndData[] = sizeof($allUsers);
        $FrontEndData[] = $endAt;
        $FrontEndData[] = $leadArray;
        return response()->json($FrontEndData);
    }




    public function filterAll(Request $request) {
        $getCurrentPage = SessionKeeper::where('ForUser', '=', Session::get('adminLogin'))->first();
        if(!$getCurrentPage)
        {
        $getCurrentPage = SessionKeeper::where('ForUser', '=', Session::get('clientLogin'))->first();
        }
        $filterRecord = DB::select("SELECT distinct UserID from answer_records where CampaignID = " . $request->campId);
        $allUsers = [];
        $leadArray = [];
        foreach ($filterRecord as $eachUserID) {

            $allUsers[] = DB::select("SELECT * from usertables where id = " . $eachUserID->UserID);
       $getLeadStatus = AnswerRecord::where('CampaignID', '=', $request->campId)->where('UserID', '=', $eachUserID->UserID)->first();
         $leadArray[] = $getLeadStatus->LeadStatus;
        }
        $pageArray = [];
        $countByPage = 0;
        $countActual = 0;
        $beginiFrom = 0;
        $endAt = 0;
        foreach ($allUsers as $eachUser) {
         if($request->actionType == "Normal")
                 {
        	 $pageArray[] = $eachUser;
        	  $countActual++;
        }
          else if ($request->actionType == "Next") {
                if ($countByPage >= $getCurrentPage->Status && $countActual < 50) {
                    $pageArray[] = $eachUser;
                    $countActual++;
                    if ($countActual == 1) {
                        $beginiFrom = $countByPage + 1;
                    }
                }
                $countByPage++;
            } else if ($request->actionType == "Previous") {
                if ($countByPage >= $getCurrentPage->Status - 50 && $countActual < 50) {
                    $pageArray[] = $eachUser;

                    $countActual++;
                    if ($countActual == 1) {
                        $beginiFrom = $countByPage + 1;
                    }
                }
                $countByPage++;
            }
        }
        $endAt = $countActual + ($beginiFrom - 1);
        if (sizeof($pageArray) > 0) {
            $currentStatus = $getCurrentPage->Status;
            if ($request->actionType == "Next") {
                if ($currentStatus + $countActual >= 0)
                    $getCurrentPage->Status = $currentStatus + $countActual;
            } else if ($request->actionType == "Previous") {
                if ($currentStatus - $countActual >= 0)
                    $getCurrentPage->Status = $currentStatus - $countActual;
            } else if ($request->actionType == "Normal") {
                if ($countActual >= 0)
                    $getCurrentPage->Status = $countActual;
            }
            $getCurrentPage->save();
        }
        
       //note: to avoid showing starting from 0 on the front end datatable, we  add 1 if it d first group of 50
       if($beginiFrom == 0)
       {
       $endAt = $endAt + 1;
       $beginiFrom = $beginiFrom + 1;
       }
        
        $FrontEndData = [];
        $FrontEndData[] = $pageArray;
        $FrontEndData[] = $beginiFrom;
        $FrontEndData[] = sizeof($allUsers);
        $FrontEndData[] = $endAt;
        $FrontEndData[] = $leadArray;
        return response()->json($FrontEndData);
    }

    public function suspendCapaign(Request $request) {
        $thisCamp = OurCampaign::find($request->campId);
        $getStatus = 0;
        if ($thisCamp->Status == 1) {
            $thisCamp->Status = 0;
        } else {
            $thisCamp->Status = 1;
        }
        $thisCamp->save();

        return response()->json($thisCamp->Status);
    }
      public function suspendClient(Request $request) {
        $thisClient = Clients::find($request->clientId);
        $getStatus = 0;
        if ($thisClient->Status == 1) {
            $thisClient->Status = 0;
        } else {
            $thisClient->Status = 1;
        }
        $thisClient->save();

        return response()->json($thisClient->Status);
    }

    public function adminLogout() {
        Session::flush();
        return Redirect::to('adminLogin');
    }

 public function editClient(Request $request) {
        $findClient = Clients::find($request->clientID);
        $findClient->CompanyName = $request->client;
        $findClient->CompanyAddress = $request->clientAdd;
        $findClient->Username = $request->username;
        //$findClient->Password = md5($request->password);
        $findClient->Status = 1;
        $findClient->save();
        
    }
    
    public function viewusers()
    {
     if(Session::get('adminLogin') == null)
    {
    
    return Redirect::to('adminLogin');
    }
        $allUsers = usertable::all();
        return view("viewusers")
        ->with('allUsers', $allUsers);
    }

 public function cashRequest()
    {
     if(Session::get('adminLogin') == null)
    {
    
    return Redirect::to('adminLogin');
    }
    
     $eachArray = array();
     $amountToPay = array();
     $count = 0;
        $toPay = CashRequest::where('Status', '=', 1)->get();
        foreach($toPay as $eachPay){
        $count++;
        $allUsers = usertable::where('id', '=', $eachPay->UserID)->first();
        $eachArray[] = $allUsers;
        $amountToPay[] = $eachPay->BookBalance;
        }
        
        return view('cashRequest')->with('count', $count)->with(compact('eachArray', 'eachArray'))->with(compact('amountToPay', 'amountToPay'));
    }


public function sendUserMailForCashCollection(Request $request)
{


$getAmount = CashRequest::where('UserID', '=', $request->userId)->where('Status', '=', 1)->first();
$getUser = usertable::where('id', '=', $request->userId)->first();
@$headers = 'From: info@kudipoll.com'." <br><br>";
if($request->action == 1){
    //mail($getUser->Email, "Cash Collection From Kudipoll Nigeria", "Dear ".$getUser->Surname."<br><br>  We are pleased to inform you that you may now come and receive your Kudipoll Gift of ".$getAmount->BookBalance." Cash. <br><br> Visit us at hour office at so so so... <br><br> \r\n Congratulations! From Kudipoll Nigeria <br><br>  Regards.", @$headers);
    
    
if($this->mailSender("Cash Collection From Kudipoll Nigeria", "Dear ".$getUser->Surname." <br><br>  We are pleased to inform you that you may now come and receive your Kudipoll Gift of ".$getAmount->BookBalance." Cash. <br><br> Visit us at hour office at so so so... <br><br> Congratulations! From Kudipoll Nigeria <br><br>  Regards.", $getUser->Email, "info@kudipoll.com") == 1){
            //return response()->json('Check Your Mail To Complete Process');
       }
            //return response()->json('Soemething went wrong');
    
    $historyTable = new BalanceHistorys();
         $historyTable->UserID = $request->userId;
         $historyTable->DayTime = Carbon::now();
         $historyTable->ActionPerformed = 'Payment of '.$getAmount->BookBalance.' Naira made to '.$getUser->Surname.' '.$getUser->Other_Name;
         $historyTable->CurrentBalance = 0;
         $historyTable->CampaignID = 0;
         $historyTable->Response= "Credited";
         $historyTable->Amount= $getAmount->BookBalance;
         $historyTable->save();
         
         $getAmount->Status= 0;
$getAmount->save();
    }
    else{
    
$historyTable = new BalanceHistorys();
         $historyTable->UserID = $request->userId;
         $historyTable->DayTime = Carbon::now();
         $historyTable->ActionPerformed = 'Payment of '.$getAmount->BookBalance.' Naira Declined and no payment made to  '.$getUser->Surname.' '.$getUser->Other_Name;
         $historyTable->CurrentBalance = 0;
         $historyTable->CampaignID = 0;
         $historyTable->Response= "Debited";
         $historyTable->Amount= $getAmount->BookBalance;
         $historyTable->save();
         
         $getAmount->Status= 2;
$getAmount->save();
    }

return response()->json('');


}

   
    
    public function delUser(Request $request){
          $userId = usertable::find($request->userId);
        if ($userId) {
            DB::table('usertables')->delete($request->userId);
            return response()->json("Delete Successful");
        }
        return response()->json("User Does Not Exist");
    }
    
    public function suspendUser(Request $request)
    {
         $thisClient = usertable::find($request->userId);
        $getStatus = 0;
        if ($thisClient->UserStatus == 1) {
            $thisClient->UserStatus = 0;
        } else {
            $thisClient->UserStatus = 1;
        }
        $thisClient->save();

        return response()->json($thisClient->UserStatus);
    }
    
     public function suspendLead(Request $request)
     {
     $getStatus = 0;
     $thisUser = AnswerRecord::where('CampaignID', '=', $request->campId)->where('UserID', '=', $request->userId)->get();
         foreach($thisUser as $eachUser){
        
        if ($eachUser->LeadStatus == 1) {
            $eachUser->LeadStatus = 0;
        } else {
            $eachUser->LeadStatus = 1;
        }
        $eachUser->save();
       }
       $getStatus = $eachUser->LeadStatus;
        return response()->json($getStatus);
     }
     
     public function updateMultipleLead(Request $request)
     {
     
     $queryPart = "";
     $countIds = 0;
     //get all Ids for query part
     foreach($request->allIdArray as $eachId)
     {
     
     if($countIds == 0)
     {
     $queryPart = $queryPart.' UserID = '.$eachId;
     }
     else
     {
     $queryPart = $queryPart.' or UserID = '.$eachId;
     
     }
     $countIds++;
     
     }
     
     
      //get action
     $action = 0;
     if($request->toggleOption == 'add')
    {
     $action = 1;
    }
    else
    {
    $action = 0;
    }
     
     
     //now query the db
         
     $updateRecord = DB::statement("Update answer_records set LeadStatus = ".$action." where CampaignID = ".$request->campId." and (".$queryPart.")");
     return response()->json('completed');
     
     }
     
 public function showMiniRep(Request $request)
  {
    $getTotalUsers = DB::select("SELECT distinct UserID from answer_records where CampaignID = ".$request->campId);
    $getUserCount = DB::select("SELECT * from usertables where Password != '' and UserStatus = 1");
      $countLeads = 0;
        foreach($getUserCount as $eacLead)
        {
        $checkLeadStatus = DB::select("SELECT * from answer_records where UserID = ". $eacLead->id." and CampaignID = ".$request->campId ." and LeadStatus = 1");
    if($checkLeadStatus)
    {
    $countLeads++;
    }
        
        }
    $responseArray = array();
    $responseArray[] = count($getTotalUsers);
    $responseArray[] = count($getUserCount);
    $responseArray[] = $countLeads;
    $responseArray[] = sizeof($getTotalUsers);
    return response()->json($responseArray);
  }
  
  public function SpecialPrizes()
  {
  
  if(!Session::get('adminLogin'))
  {
  
   return Redirect::to('/adminLogin');
  
  }
  
  $allCampaigns = DB::select("SELECT * FROM our_campaigns where IfSpecial <> 0  or NonCampaign = 0");
        $allClients = DB::select("SELECT * FROM clients where Status = 1");
       
        $campaigns = DB::select("SELECT * FROM our_campaigns where IfSpecial <> 0 or NonCampaign = 0");
        $clients = Clients::where('Status', '=', 1);
        return view("SpecialPrizes")
                        ->with('campaigns', $campaigns)
                     ->with('clients', $clients)
 ->with(compact('allCampaigns', $allCampaigns))
                      ->with(compact('allClients', $allClients));
  
  }
  
  public function goGetWinner(Request $request)
  {
  
  $ifWon = OurCampaign::where('id', '=', $request->campId)->first();
  if($ifWon->IfSpecial == 2)
  {
  return response()->json('Ended');
  
  }
  $userArray = array();
  if($ifWon->NonCampaign != 0)
{

 $getUsers = DB::select("SELECT distinct UserID FROM answer_records where CampaignID = ".$request->campId);
 if(!$getUsers)
 {


  return response()->json("nil");
 
}

  foreach($getUsers as $eachUser)
  {
  
  $getPreviousWinner = OurWinner::where('UserID', '=', $eachUser->UserID)->where('CampaignID', '=', $request->campId)->first();
  if(!$getPreviousWinner){
  $userArray[] = $eachUser;
  }
  }
  
   if(sizeof($userArray) == 0)
  {
  
  return response()->json('Nomore');


}

$predictWinner = (integer)rand(0, sizeof($userArray)-1);
  
  $getUser = usertable::where('id', '=', $userArray[$predictWinner]->UserID)->first();

 $userRecord = array();
  $userRecord[] = $getUser->Surname.' '.$getUser->Other_Name;
  $userRecord[] = $getUser->Email;
  $userRecord[] = $getUser->Phone_Number;
  $userRecord[] = $getUser->City;
  $userRecord[] = $getUser->State;
  $userRecord[] = $getUser->id;
  return response()->json($userRecord);

}
else{

$getUsers = DB::select("SELECT id FROM usertables where UserStatus = 1 and Phone_Number <> '' and Age_Range <> ''");
 if(!$getUsers)
 {


  return response()->json("nil");
 
}
foreach($getUsers as $eachUser)
  {
  
  $getPreviousWinner = OurWinner::where('UserID', '=', $eachUser->id)->where('CampaignID', '=', $request->campId)->first();
  if(!$getPreviousWinner){
  $userArray[] = $eachUser;
  }
  }

 if(sizeof($userArray) == 0)
  {
  
  return response()->json('Nomore');


}
  
    $predictWinner = (integer)rand(0, sizeof($userArray)-1);
  
  $getUser = usertable::where('id', '=', $userArray[$predictWinner]->id)->first();
   $userRecord = array();
  $userRecord[] = $getUser->Surname.' '.$getUser->Other_Name;
  $userRecord[] = $getUser->Email;
  $userRecord[] = $getUser->Phone_Number;
  $userRecord[] = $getUser->City;
  $userRecord[] = $getUser->State;
  $userRecord[] = $getUser->id;
  return response()->json($userRecord);
}
  

}
  
 
  
  
  public function regWinner(Request $request)
  {
  
  
  
  $winnerTable = new OurWinner();
  $winnerTable->UserID= $request->userId;
  $winnerTable->CampaignID = $request->campId;
  $winnerTable->PrizeWon = OurCampaign::where('id', '=', $request->campId)->first()->PrizeName;
  $winnerTable->save();
$mailto = usertable::where('id', '=', $request->userId)->first();
if($this->mailSender("Reward Collection From Kudipoll Nigeria", "Dear ".$mailto->Surname."<br><br>  We are pleased to inform you that you may now come and receive your Kudipoll Reward of <br><br> Visit us at hour office at so so so... <br><br> Congratulations! From Kudipoll Nigeria <br><br>  Regards.", $mailto->Email, "info@kudipoll.com") == 1){
              return response()->json('Success');
       }
            return response()->json('Soemething went wrong');

  
  }
  
  
  public function viewPastWinner(Request $request)
  {
  
  $allWinners = array();
  $getWinners = OurWinner::where('CampaignID', '=', $request->campId)->get();
  foreach($getWinners as $eachWinner)
  {
    $allWinners[] = usertable::where('id', '=', $eachWinner->UserID)->first();
  }
  return response()->json($allWinners);
  }
  
  public function removeWinner(Request $request)
  {
  
  $getWinners = OurWinner::where('CampaignID', '=', $request->campId)->where('UserID', '=', $request->userId)->first();
  if($getWinners){
  DB::table('our_winners')->delete($getWinners->id);
  return response()->json("Delete Successful");
  }
  
  return response()->json("Winner Id Not Found");
  
  }
  
  public function updateAmountToWin(Request $request)
  {
  
  $forAllCampaigns = DB::table('our_campaigns')->update(['ToWin' => $request->newAmount]);

  return response()->json($request->newAmount);
  
  }
  
  public function getSpecialPrize(Request $request)
  {
  
  $allInfo = array();
  
  $checkPrizesTaken = DB::select('select distinct CampaignID from answer_records where UserID = '.Session::get('logedIn'));
  $prizeArray = array();
  foreach($checkPrizesTaken as $eachPrize)
  {
  $prizeArray[] = $eachPrize->CampaignID;
  }
  
   $specialPrizes = OurCampaign::where('NonCampaign', '=', 0)->get();
 
 
  
  $compareArrays = array();
  
  foreach($specialPrizes as $eachCamp)
  {
  
  if (!in_array($eachCamp->id, $prizeArray)) {
    $compareArrays[] = $eachCamp;
}
  
  }
  
  
  $prizeInfo = OurWinner::where('UserID', '=', Session::get('logedIn'))->get();
  
  $allInfo[] = $compareArrays;
  $allInfo[] = $prizeInfo; 
  
  return response()->json($allInfo);
  
  //return response()->json($prizeArray);
  }
  
 public function mailSender($subject, $body, $to, $from)
  {
  
  require_once('../kudipoll/PHPMailer_5.2.0/class.phpmailer.php');
 
                  $mail             = new PHPMailer();
                  $mail->IsSMTP();
                  $mail->SMTPAuth   = true;
                  $mail->Host       = "box1030.bluehost.com";
                  $mail->Port       = 465;
                  $mail->Username   = $from;
                  $mail->Password   = "Winmoney+";
                  $mail->SMTPSecure = 'ssl';
                  $mail->SetFrom($from, 'Kudipoll Nigeria');
                  $mail->AddReplyTo($from,"Kudipoll Nigeria");
                  $mail->Subject    = $subject;
                 // $mail->AltBody    = "Any message.";
                  $mail->MsgHTML($body);
                  $mail->AddAddress($to, 'You');
                  if(!$mail->Send()) {
                      return 0;
                  } else {
                        return 1;
                 }
    
  
  }
   public function getAllClient(Request $request)
    {
    $allClient = DB::select("SELECT * from clients where Status = 1");
    
    return response()->json($allClient );
    }

 public function getQuestionPreview(Request $request)
  {
  
  //topehere
  $getCampQuest = CampaignQuestions::where('CampaignID', '=', $request->campId)->get();
  $allQuestPreview = '';
$dataArray = array();
  $eachQuest = "<div class='questions mainPreviewBody'>";
  $countForm = 0;
  foreach($getCampQuest->reverse() as $getQuest)
  {
  
$countForm++;

if ($getQuest->ResponseType == "mySelect") {
                                $inputPart = "";
                                $getInput = FormBuilder::where('FormType', '=', $getQuest->ResponseType)->first();
                                foreach (explode('__&&__', $getQuest->ResponseData) as $eachOpt) {
                                    if ($eachOpt != "" && $eachOpt != null)
                                        $inputPart = $inputPart . "<option value='" . $eachOpt . "'>" . $eachOpt . "</option>";
                                }
                                $finalInput = str_replace('eachopt', $inputPart, $getInput->FormContent);
                                $eachQuest = $eachQuest . "<div class='quest_".$getQuest->id."'><div class='question-list'>" .
                                        "<span class='wrapper' style='width: 70%;'>" .
                                        "<div class='item'>" .
                                        "<span style='display: inline-block;margin-right: 15px;vertical-align: top;'><i class='fa fa-arrow-right' style='color: gold;'></i></span><span style='display: inline-block;width: 95%;text-align: justify;'><span>" . $getQuest->Question.
                                        "</span></span></div><br>" .
                                        "<div class='response' style='padding-left: 25px;'>" . str_replace('subforcampid', $request->campId, str_replace('subforcount', $countForm, $finalInput)) . "</div><input type='hidden' name='QuestID' class='Quest_" . $getQuest->id . "' value='" . $getQuest->id . "' />" .
                                        "</span><span class='actionBtns'><i class='fa fa-pencil' onclick='UpdateCampQuest(\"".$request->campId."\", \"".$getQuest->id."\")' style='color: blue; cursor: pointer;'></i>&emsp;<i class='fa fa-trash' style='color: red; cursor: pointer;' onclick='confirnCampDel(\"".$request->campId."\", \"".$getQuest->id."\");'></i></span>" .
                                        " </div><hr></div>";
                            }
                            else if ($getQuest->ResponseType == "myCheck") {
                                $inputPart = "<span class='checkBoxClass_" . $getQuest->id . "'><input type='hidden' name='checkBoxClass_" . $getQuest->id . "' value='checkBoxClass_" . $getQuest->id . "'>";

                                // $getInput = DB::select("SELECT FormContent FROM form_builders where FormType = '" . $getQuest->ResponseType . "'");
                                $getInput = FormBuilder::where('FormType', '=', $getQuest->ResponseType)->first();
                                foreach (explode('__&&__', $getQuest->ResponseData) as $eachOpt) {
                                    if ($eachOpt != "" && $eachOpt != null) {

                                        // $f = str_replace('eachopt', $eachOpt, $getInput->FormContent);
                                        $inputPart = $inputPart . "<input type='checkbox' name='checkbox' class='toDisable_subforcampid largeChecks' id='val_subforcount' value='" . $eachOpt . "' ><span style='position: relative; top:-6px; left: 5px;'>" . $eachOpt . " </span>&emsp;";
                                    }
                                }
                                $inputPart = $inputPart . "</span>";
                                // $eachQuest = $eachQuest . "<label>" . $getQuest->Question . "</label><br>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $inputPart)) . "<br><br>";
                                $eachQuest = $eachQuest . " <div class='quest_".$getQuest->id."'><div class='question-list'>" .
                                        "<span class='wrapper' style='width: 70%;'>" .
                                        "<div class='item'>" .
                                        "<span style='display: inline-block;margin-right: 15px;vertical-align: top;'><i class='fa fa-arrow-right' style='color: gold;'></i></span><span style='display: inline-block;width: 95%;text-align: justify;'><span>" . $getQuest->Question.
                                        "</span></span></div><br>" .
                                        "<div class='response' style='padding-left: 25px;'>" . str_replace('subforcampid', $request->campId, str_replace('subforcount', $countForm, $inputPart)) . "</div>" .
                                       "</span><span class='actionBtns'><i class='fa fa-pencil' onclick='UpdateCampQuest(\"".$request->campId."\", \"".$getQuest->id."\")' style='color: blue;cursor: pointer;'></i>&emsp;<i class='fa fa-trash' style='color: red;cursor: pointer;' onclick='confirnCampDel(\"".$request->campId."\", \"".$getQuest->id."\");'></i></span>" .
                                        " </div><hr></div>";
                            } else if ($getQuest->ResponseType == "myRadio") {
                                $inputPart = "<span class='radioClass_" . $getQuest->id . "'><input type='hidden' name='radioClass_" . $getQuest->id . "' value='radioClass_" . $getQuest->id . "'>";
                                $getInput = FormBuilder::where('FormType', '=', $getQuest->ResponseType)->first();
                                foreach (explode('__&&__', $getQuest->ResponseData) as $eachOpt) {
                                    if ($eachOpt != "" && $eachOpt != null) {
                                        $inputPart = $inputPart . "<input type='radio' name='radioClass_" . $getQuest->id . "' class='toDisable_subforcampid largeChecks' id='val_subforcount' value='" . $eachOpt . "' ><span style='position: relative; top:-6px; left: 5px;'>" . $eachOpt . "</span> &emsp;";
                                    }
                                }
                                $inputPart = $inputPart . "</span>";
                                $eachQuest = $eachQuest . "<div class='quest_".$getQuest->id."'><div class='question-list'>" .
                                       "<span class='wrapper' style='width: 70%;'>" .
                                        "<div class='item'>" .
                                        "<span style='display: inline-block;margin-right: 15px;vertical-align: top;'><i class='fa fa-arrow-right' style='color: gold;'></i></span><span style='display: inline-block;width: 95%;text-align: justify;'><span>" . $getQuest->Question.
                                        "</span></span></div><br><div class='response' style='padding-left: 25px;'>" . str_replace('subforcampid', $request->campId, str_replace('subforcount', $countForm, $inputPart)) ."</div> </span><span class='actionBtns'><i class='fa fa-pencil' onclick='UpdateCampQuest(\"".$request->campId."\", \"".$getQuest->id."\")' style='color: blue;cursor: pointer;'></i>&emsp;<i class='fa fa-trash' style='color: red;cursor: pointer;' onclick='confirnCampDel(\"".$request->campId."\", \"".$getQuest->id."\");'></i></span>" . 
                                        " </div><hr></div>";
                            } else if ($getQuest->ResponseType == "myRange") {
                                $max = explode('-', explode('__&&__', $getQuest->ResponseData)[0])[0];
                                $min = explode('-', explode('__&&__', $getQuest->ResponseData)[0])[1];
                                $inputPart = "<input type='text' id='rangeSelect" . $getQuest->id . "' name='rangeClass_" . $getQuest->id . "' class='form-control rangeClass_" . $getQuest->id . "'  value='' /><div><span class='pull-left leastDesc rangeInfo' style='color: #333;   font-size: 12px; line-height: 1.333;text-shadow: none;top: 0;padding: 1px 5px; background: rgba(0,0,0,0.1); border-radius: 3px;'>" . explode('-', explode('__&&__', $getQuest->ResponseData)[1])[0] . "</span><span class='pull-right maxDesc rangeInfo' style='color: #333;   font-size: 12px; line-height: 1.333;text-shadow: none;top: 0;padding: 1px 5px; background: rgba(0,0,0,0.1); border-radius: 3px;'>" . explode('-', explode('__&&__', $getQuest->ResponseData)[1])[1] . "</span></div>";

                                $eachQuest = $eachQuest . "<div class='quest_".$getQuest->id."'> <div class='question-list'>" .
                                        "<span class='wrapper' style='width: 70%;'>" .
                                        "<div class='item'>" .
                                        "<span style='display: inline-block;margin-right: 15px;vertical-align: top;'><i class='fa fa-arrow-right' style='color: gold;'></i></span><span style='display: inline-block;width: 95%;text-align: justify;'><span>" . $getQuest->Question.
                                        "</span></span></div><br>" .
                                        "<div class='response' style='padding-left: 25px;'>" . str_replace('subforcampid', $request->campId, str_replace('subforcount', $countForm, $inputPart)) . "</div>" .
                                          "</span><span class='actionBtns'><i class='fa fa-pencil' onclick='UpdateCampQuest(\"".$request->campId."\", \"".$getQuest->id."\")' style='color: blue;cursor: pointer;'></i>&emsp;<i class='fa fa-trash' style='color: red;cursor: pointer;' onclick='confirnCampDel(\"".$request->campId."\", \"".$getQuest->id."\");'></i></span>" .
                                        " </div><hr></div>" .
                                        "<script>" .
                                        "$('#rangeSelect" . $getQuest->id . "').ionRangeSlider({" .
                                        "hide_min_max: false," .
                                        " keyboard: true," .
                                        "min: '" . ($min) . "'," .
                                        "max: '" . ($max) . "'," .
                                        "type: 'single'," .
                                        "step: 1," .
                                        "grid: true" .
                                        "});" .
                                        " </script>";
                            }

  
  
  }
   $eachQuest = $eachQuest . "</div><input type='hidden' name='CampaigInfo' value='" . $request->campId . "' /> </div>";
   if($countForm == 0)
{

$dataArray[] = '<div style="font-weight: bolder;padding: 30px;" align="center">No Question Found</div>';

$getCampInfo= OurCampaign::find($request->campId);

$dataArray[] = $getCampInfo->CampaignName;
$dataArray[] = $getCampInfo->ClientID;
 return response()->json($dataArray);
}

$dataArray[] = $eachQuest;

$getCampInfo= OurCampaign::find($request->campId);

$dataArray[] = $getCampInfo->CampaignName;
$dataArray[] = $getCampInfo->ClientID;

  return response()->json($dataArray);
  
  }
  
  public function deleteCampQuest(Request $request)
  {
  //topedelete
  //check if user ardy participated in it

  $checkAnswerRec = AnswerRecord::where('CampaignID', '=', $request->campId)->first();
   if($checkAnswerRec)
{
return response()->json("Delete Unsuccessful! Some Users Already Took This Survey");
}
 
  $getQuest = CampaignQuestions::where('CampaignID', '=', $request->campId)->where('id', '=', $request->questId)->first();
  if($getQuest){
  DB::table('campaign_questions')->delete($getQuest->id);
  return response()->json("Delete Successful");
  }
  
  }

public function getCampAttribute(Request $request)
{

$infoArray = array();
 $getQuest = CampaignQuestions::where('CampaignID', '=', $request->campId)->where('id', '=', $request->questId)->first();
$infoArray[] = $getQuest->Question;
$infoArray[] = $getQuest->ResponseData;
$infoArray[] = $getQuest->ResponseType;
$infoArray[] = $getQuest->ExpectedResponse;
return response()->json($infoArray);


}

public function updateQuestionData(Request $request)
{

 $checkCamp = OurCampaign::find($request->CampaignID);
        if ($checkCamp) {
            $allOpts = "";
            foreach ($request->ResponseType as $responseType) {
                $allOpts = $allOpts . $responseType . "__&&__";
            }

 $question = CampaignQuestions::find($request->questId);
if (!$question) {
           return response()->json("Question Not Found");
            }
            //$question = new CampaignQuestions();
            $question->Question = $request->Question;
            $question->ClientID = $request->ClientID;
            $question->CampaignID = $request->CampaignID;
            $question->ResponseData = $allOpts;
            $question->ResponseType = $request->myResponse;
            //$question->ExpectedResponse = $request->responseExpected;
            $question->save();
            return response()->json("Update Successful");
        }
        return response()->json("Campaign Not Found");

}
 



public function AddCampaign()
{
    if(Session::get('adminLogin') == null)
    {
    
      return Redirect::to('adminLogin');
    }

 $allCampaigns = DB::select("SELECT * FROM our_campaigns where NonCampaign = 1");
        $allClients = DB::select("SELECT * FROM clients where Status = 1");
      // $count
        $campaigns = OurCampaign::all();
        $clients = Clients::where('Status', '=', 1);
        return view("AddCampaign")
                        ->with('campaigns', $allCampaigns)
                        ->with('clients', $clients)
 ->with(compact('allCampaigns', $allCampaigns))
                        ->with(compact('allClients', $allClients));
    

}

public function QuestionManager()
{
    if(Session::get('adminLogin') == null)
    {
    
      return Redirect::to('adminLogin');
    }

 $allCampaigns = DB::select("SELECT * FROM our_campaigns where NonCampaign = 1");
        $allClients = DB::select("SELECT * FROM clients where Status = 1");
      // $count
        $campaigns = OurCampaign::all();
        $clients = Clients::where('Status', '=', 1);
        return view("QuestionManager")
                        ->with('campaigns', $allCampaigns)
                        ->with('clients', $clients)
 ->with(compact('allCampaigns', $allCampaigns))
                        ->with(compact('allClients', $allClients));
    

}

public function CreateReward()
{

if(!Session::get('adminLogin'))
  {
  
   return Redirect::to('/adminLogin');
  
  }
  
  $allCampaigns = DB::select("SELECT * FROM our_campaigns where IfSpecial <> 0  or NonCampaign = 0");
        $allClients = DB::select("SELECT * FROM clients where Status = 1");
       
        $campaigns = DB::select("SELECT * FROM our_campaigns where IfSpecial <> 0 or NonCampaign = 0");
        $clients = Clients::where('Status', '=', 1);
        return view("CreateReward")
                        ->with('campaigns', $campaigns)
                     ->with('clients', $clients)
 ->with(compact('allCampaigns', $allCampaigns))
                      ->with(compact('allClients', $allClients));
  

}

public function CreateClient()
{
if(Session::get('adminLogin') == null)
    {
    
       return Redirect::to('adminLogin');
    }
        $allClients = Clients::all();
        return view("CreateClient")
                        ->with('allClients', $allClients);
}


public function sendMultipleMail(Request $request)
{

$getCamp = OurCampaign::find($request->campId);
//if(!$request->campId){
//echo '<script>alert('.$request->campId.')</script>';
//}
$notSent = array();
$getAllValidUsers = usertable::where('UserStatus', '=', 1)->get();
foreach($getAllValidUsers as $eachUsers)
{
//if($eachUsers->id == 158){
if($this->mailSender("New Kudipoll survey", "A new Kudipoll survey is now available. Click on the image below to try now <br><br> <div style='width: 20%; margin-left:116px;'> <a href='http://kudipoll.com/successful'><img src='http://kudipoll.com/public/images/CampaignImages/".$request->campId."/".$getCamp->ImagePath."' width='100%'></a><div align='center' style='margin-top: 20px;'>".$getCamp->CampaignName."</div></div>", $eachUsers->Email, "info@kudipoll.com") == 1){
            //return response()->json('Check Your Mail To Complete Process');
       }else{
            //return response()->json('Soemething went wrong');
$notSent[] = $eachUsers->id;
}
//}

}
return response()->json($notSent);
}
  


public function clientResendActivationMail(Request $request)
{

$newPassword = rand(10000, 99999);
$getClient = Clients::find($request->clientId);
$getClient->Password = md5($getClient->CompanyName.$newPassword);
$getClient->save();
if($this->mailSender("Kudipoll Client Account Activation", "Hello, <br><br> Welcome to Kudipoll. <br><br>Your Company Name is ".$getClient->CompanyName."<br><br> Your Default Password is ".$getClient->CompanyName.$newPassword." <br><br> Kindly follow the steps below to activate your account; <br><br> <ol><li>Visit <a href='http://kudipoll.com/clientLogin/".$getClient->CompanyName."' alt='Kudipoll' title='Kudipoll Client Panel'> http://kudipoll.com/clientLogin/".$getClient->CompanyName."</a></li><li>Complete the ACTIVATE ACCOUNT form with the default password above.</li></ol> <br><br>  Regards.", $getClient->Email, "info@kudipoll.com") == 1){
return response()->json('Mail Sent');
}
return response()->json('Something went wrong!');


}


}