<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOurusersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ourusers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('facebook_id');
            $table->string('Surname');
             $table->string('Other_Name');
            $table->string('Email');
            $table->string('State');
            $table->string('City');
            $table->string('Gender');
            $table->string('Age_Range');
            $table->string('Phone_Number');
            $table->string('Password');
            $table->string('Confirm_Password');
            $table->string('ReferrerCode');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ourusers');
    }
}
