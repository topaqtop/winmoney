<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['web']], function(){
    
    Route::get('/', function () {
   // return view('welcome');
     return Redirect::to('');   
});

Route::get('_oauth/facebook', 'Auth\RegisterController@redirectToProvider');
Route::get('_oauth/facebook/callback', 'Auth\RegisterController@handleProviderCallback');

Route::get('about', function(){
    $bitfumes = ['This', 'Is', 'Bitfumes'];
    return view('about', ['bitfumes' => $bitfumes]);
});

Route::get('UserSignUp', 'ClientController@ClientPage');
Route::get('Register', 'ClientController@RegisterPage');
Route::post('user', 'userController@RegisterUser');

Route::get('newmemberReg', 'websiteController@newmemberReg');
//Route::get('facebookGet', 'ClientController@fbPermission');
Route::get('facebookGet', 'ClientController@fbPermission');

Route::post('userComplete', 'userController@userComplete');

Route::get('successful', 'userController@nowLoggedIn');

Route::get('completeReg', 'userController@CompleteRegPage');

Route::get('updateuser', 'userController@UpdateUserInfo');

Route::get('updateuserprofile', 'userController@updateuserprofile');

Route::get('', 'websiteController@WinMoneyWebsite');

Route::post('SignIn', 'websiteController@userSignin');

Route::get('logout', 'userController@userSignout');

Route::get('previousCampaign', 'userController@prevCamp');

Route::get('saveAnswer', 'userController@saveAnswer');

Route::get('alreadyAnswered', 'userController@alreadyAnswered');

Route::get('getCampaign', 'userController@getCampaign');

Route::get('getMore', 'userController@getMore');

Route::post('uploadDP', 'userController@uploadDP');

Route::post('updateCampaignImg', 'adminController@updateCampaignImg');

Route::post('uploadCampaignImage', 'adminController@uploadCampaignImage');

Route::get('upDateDBColumn', 'userController@upDateDBColumn');

Route::get('checkRegComplete', 'userController@checkRegComplete');

Route::get('confirmation/{confirmcode}', 'userController@confirmpassword');

Route::get('confirmAccount/{confirmcode}', 'userController@confirmAccount');

Route::get('completeRegistration', 'userController@completeRegistration');

Route::get('forgotPassword', 'websiteController@forgotPassword');

Route::post('updateUserPassword', 'userController@updateUserPassword');

Route::get('createCampaign', 'adminController@createCampaign');

Route::get('saveQuestion', 'adminController@saveQuestion');

Route::get('newCampaign', 'adminController@newCampaign');

Route::get('AdminPage', 'adminController@AdminPage');

Route::get('adminLogin', 'adminController@adminLogin');

Route::get('CampaignCreate', 'adminController@CampaignCreate');

Route::get('AddQuestion', 'adminController@AddQuestion');

Route::get('Report', 'adminController@Report');

Route::post('adminAuth', 'adminController@adminAuth');

Route::post('clientAuth', 'ClientController@clientAuth');

Route::get('CampaignList', 'adminController@CampaignList');

Route::get('AddCampaign', 'adminController@AddCampaign');

Route::get('QuestionManager', 'adminController@QuestionManager');

Route::get('getCamp', 'adminController@getCamp');

Route::get('getallquest', 'ClientController@getallquest');

Route::get('delCamp', 'adminController@delCamp');

Route::get('updateCamp', 'adminController@updateCamp');

Route::get('addNewClient', 'adminController@addNewClient');

Route::get('CreateClient', 'adminController@CreateClient');

Route::get('ViewClient', 'adminController@ViewClient');

Route::get('FilterCampaign', 'adminController@FilterCampaign');

Route::get('getFilterQuestions', 'adminController@getFilterQuestions');

Route::get('getQuestOpt', 'adminController@getQuestOpt');

Route::get('filterAnswers', 'adminController@filterAnswers');

Route::get('filterRangeAnswers', 'adminController@filterRangeAnswers');

Route::get('filterSelectAnswers', 'adminController@filterSelectAnswers');

Route::get('filterAll', 'adminController@filterAll');

Route::get('adminLogout', 'adminController@adminLogout');

Route::get('clientLogout', 'ClientController@clientLogout');

Route::get('suspendCapaign', 'adminController@suspendCapaign');

Route::get('editClient', 'adminController@editClient');

Route::get('suspendClient', 'adminController@suspendClient');

Route::get('delClient', 'adminController@delClient');

Route::get('viewusers', 'adminController@viewusers');

Route::get('delUser', 'adminController@delUser');

Route::get('suspendUser', 'adminController@suspendUser');

Route::get('suspendLead', 'adminController@suspendLead');

Route::get('HowToWin', 'ClientController@HowToWin');

Route::get('homeOption', 'ClientController@homeOption');

Route::get('HowDoIWin', 'ClientController@HowDoIWin');

Route::get('PrivacyPolicy', 'ClientController@PrivacyPolicy');

Route::get('TermsAndCondition', 'ClientController@TermsAndCondition');

Route::get('GeneralRules', 'ClientController@GeneralRules');

Route::get('clientLogin/{companyName?}', 'ClientController@clientLogin');

Route::get('clientPage', 'ClientController@clientPage');

Route::get('ViewLeads', 'ClientController@ViewLeads');

Route::get('getParticipant', 'ClientController@getParticipant');

Route::get('getAllClient', 'adminController@getAllClient');

Route::get('WinMoney_PrivacyPolicy', 'ClientController@WinMoney_PrivacyPolicy');

Route::get('Winmoney_GeneralRules', 'ClientController@Winmoney_GeneralRules');

Route::get('Winmoney_HowToWin', 'ClientController@Winmoney_HowToWin');

Route::get('Winmoney_TermsAndCondition', 'ClientController@Winmoney_TermsAndCondition');

Route::get('sendMail', 'websiteController@sendMail');

Route::get('showMiniRep', 'adminController@showMiniRep');

Route::get('filterAndAnswers', 'adminController@filterAndAnswers');

Route::get('getUserResponse', 'adminController@getUserResponse');

Route::get('updateMultipleLead', 'adminController@updateMultipleLead');

Route::get('getBalanceHistory', 'userController@getBalanceHistory');

Route::get('getIndividualBalanceHistory', 'adminController@getIndividualBalanceHistory');

Route::get('filterHistory', 'adminController@filterHistory');

Route::get('bookMyMoney', 'userController@bookMyMoney');

Route::get('cashRequest', 'adminController@cashRequest');

Route::get('testshare', 'ClientController@testshare');

Route::get('toggleSpecialPrize', 'adminController@toggleSpecialPrize');

Route::get('sendUserMailForCashCollection', 'adminController@sendUserMailForCashCollection');

Route::get('goGetWinner', 'adminController@goGetWinner');

Route::get('SpecialPrizes', 'adminController@SpecialPrizes');

Route::get('regWinner', 'adminController@regWinner');

Route::get('viewPastWinner', 'adminController@viewPastWinner');

Route::get('checkIfWinnerExists', 'adminController@checkIfWinnerExists');

Route::get('removeWinner', 'adminController@removeWinner');

Route::get('updateAmountToWin', 'adminController@updateAmountToWin');

Route::get('CreateReward', 'adminController@CreateReward');

Route::get('getSpecialPrize', 'adminController@getSpecialPrize');

Route::get('ClientDashBoard', 'ClientController@ClientDashBoard');

Route::get('checkPrizeAvailable', 'ClientController@checkPrizeAvailable');

Route::get('getQuestionPreview', 'adminController@getQuestionPreview');

Route::get('deleteCampQuest', 'adminController@deleteCampQuest');

Route::get('CreateClient', 'adminController@CreateClient');

Route::get('getCampAttribute', 'adminController@getCampAttribute');

Route::get('updateQuestionData', 'adminController@updateQuestionData');

Route::get('getMyProfile', 'userController@getMyProfile');

Route::get('sendMultipleMail', 'adminController@sendMultipleMail');

Route::get('clientChangePassword', 'ClientController@clientChangePassword');

Route::get('clientPassword', 'ClientController@clientPassword');

Route::get('clientChangeInfo', 'ClientController@clientChangeInfo');

Route::get('clientForgotPassword', 'ClientController@clientForgotPassword');

Route::get('clientActivateAccount', 'ClientController@clientActivateAccount');

Route::get('clientResendActivationMail', 'adminController@clientResendActivationMail');
Route::resource('myCRUD', 'resourceController');

Auth::routes();

Route::get('/home', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index');

});
