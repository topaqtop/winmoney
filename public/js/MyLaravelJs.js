/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var allDone = [];
var questionCounter = 0;
localStorage.setItem('moreCamp', 'done');
 $('.nomore').hide();  
 
    var totAvailable = $('.myAvailable').html().trim();
    if(parseInt(totAvailable) == 0)
    {
     $('.nomore').show();
      $('.myloadmore').html("");
    }
    else{
        $('.myloadmore').html('<br><button class="btn btn-primary btn-xs loadMore" onclick="loadMore();">Load More</button>');
    }

function signOut()
{
    localStorage.clear();
    $.ajax({
        url: '/userSignout',
        type: 'POST',
        data: {},
        success: function (data) {
            location.href = '/WinMoneyIndex';
        },
        error: function (data) {
            alert("Error");
        }
    });

}


$(function () {

    //get regStatus if complete
    var regStatus = $('.regStatus').html().trim();
    if (regStatus != "unknown") {
        localStorage.setItem('regStatus', regStatus);
    }
    if (localStorage.getItem('regStatus') == "Incomplete")
    {
        $('.regUpdate').html("<span onclick='showModal();'><i class='fa fa-info'></i> Please click to update your account to increase your chances of winning");

        $('#myModal1').modal('show');
    }


    var retina = window.devicePixelRatio > 1 ? true : false;
    if (retina) {
        var retinaEl = jQuery("#logo img.logo-main");
        var retinaLogoW = retinaEl.width();
        var retinaLogoH = retinaEl.height();
        retinaEl.attr("src", "images/retina-goodfood.png").width(retinaLogoW).height(retinaLogoH);
        var stickyEl = jQuery("#logo img.logo-sticky");
        var stickyLogoW = stickyEl.width();
        var stickyLogoH = stickyEl.height();
        stickyEl.attr("src", "images/retina-goodfood.png").width(stickyLogoW).height(stickyLogoH);
        var mobileEl = jQuery("#logo img.logo-mobile");
        var mobileLogoW = mobileEl.width();
        var mobileLogoH = mobileEl.height();
        mobileEl.attr("src", "images/retina-goodfood.png").width(mobileLogoW).height(mobileLogoH);
    }
});
// $(document).ready(function () {
function takeThis(id) {

//            $('.campaign').removeClass('campaign-selected');
//            //get questions
//            $('.campaign_open_'+id).closest('.campaign').addClass('campaign-selected');
//           $('.campaign_open_'+id).closest('.one-third').addClass('opened');
//            $('.campaign_open_'+id).closest('.campaign').next().fadeIn();
    //var length = $('.campaign_wrapper .one-third').length;
    $('.campaign_wrapper .one-third').not('.opened').addClass('closed');
//            
    $('.myClass').hide();
    $('.camp_' + id).show();
    $('.camp_' + id).addClass('pull-left');

    setTimeout(function () {
        $('.question_container').removeClass('closed');
        var q = $('.getQuest_' + id).val();
        $('.myQuest').html(q);
        questionCounter = $('.getCount_' + id).val();


    }, (length * 0.001));
}

function closeThis() {
    // $('.close').on('click', function () {
    $('.question_container').addClass('closed');
    $('.campaign_wrapper .one-third').removeClass('closed');
    $('.campaign_wrapper .one-third').removeClass('opened');
    $('.campaign').removeClass('campaign-selected');
    $('.campaign_rules').hide();

    $('.myClass').show();
    $.each(allDone, function (count, value) {

        $('.camp_' + value).remove();

    });
    
   


}
// });

function showModal()
{
    $('#myModal1').modal('show');
}

function checkReg(regStatus, campId)
{
    if (regStatus == "unknown") {
        regStatus = localStorage.getItem('regStatus');
    }
    if (regStatus == "Incomplete")
    {
        $('#myModal1').modal('show');
    } else
    {
        takeThis(campId);
    }
}

function nextQuest()
{
    if ($('.campaign_count_' + (questionCounter)).length) {
        closeThis();
        $('.campaign_count_' + (questionCounter)).click();
        questionCounter++;
    }

}

function prevQuest(count)
{
    if (!$('.campaign_count_' + (parseInt(questionCounter))).length) {
        for (var i = 0; i <= count; i++)
        {

            if ($('.campaign_count_' + i).length && i != (parseInt(questionCounter))) {

                questionCounter = i;
                closeThis();
                $('.campaign_count_' + (i)).click();
                questionCounter--;
                break;

            }

        }
    } else {
        closeThis();
        questionCounter--;
        $('.campaign_count_' + (parseInt(questionCounter))).click();

    }

}


function subQuest()
{
    $('.questLoad').show();
    $('.myQuest').hide();
    var allVal = $('#form_' + questionCounter).serializeArray();
    var campaignID = allVal[allVal.length - 1].value;
    // var QuestionID = $('.getQuestID_'+campaignID).val();
    var questionId = [];
    var ifEmpty= [];
    
     $.each(allVal, function (count, val) {
        if (val.value == "") {
           ifEmpty.push(val.value);
        }
    });
    $.each(allVal, function (count, val) {
        if (count != (allVal.length - 1)) {
            if (count != 0 && count % 2 != 0) {
                questionId.push(val.value);
            }
        }
    });

    var validQuestCount = 0;
    if(ifEmpty.length > 0)
    {
          swal("Oops! Some Fields Were Empty");
            $('.questLoad').hide();
                    $('.myQuest').show();
            return false;
    }
    else{
    $.each(allVal, function (count, val) {
        if (count % 2 == 0 && count <= questionId.length && val.name.indexOf('val_') >= 0) {
            //alert('answer= '+ val.value +' campaignID= '+campaignID+' questionID= '+questionId[validQuestCount]);
            //submit
            $.ajax({
                url: '/saveAnswer',
                type: 'POST',
                data: {'_token': $('input[name=_token]').val(), 'answer': val.value, 'campaignID': campaignID, 'questionId': questionId[validQuestCount]},
                success: function (data) {

                    allDone.push(campaignID);
                    $('.questLoad').hide();
                    $('.myQuest').show();
                    nextQuest();
                    if (count == 0)
                    {

                        $('.myAvailable').html((parseInt($('.myAvailable').html()) - 1));
                        $('.myAnswered').html((parseInt($('.myAnswered').html()) + 1));
                 
                  
                    }
                    swal("Submit Successful!");
                   
                    totAvailable = $('.myAvailable').html().trim();
    if(parseInt(totAvailable) == 0 && $('.boxes').html() != "no more")
    {
     $('.nomore').show();
       $('.myloadmore').html("");
    }
     else{
        $('.myloadmore').html('<br><button class="btn btn-primary btn-xs loadMore" onclick="loadMore();">Load More</button>');
    }


                },
                error: function (data) {
                     $('.questLoad').hide();
                    $('.myQuest').show();
                     swal("Oops! Error Occured");
                }
            });

        }

    });
    }
}


function userLogin()
{
    /*! jQuery v2.1.4 | (c) 2005, 2015 jQuery Foundation, Inc. | jquery.org/license */


    var password = $('#loginpassword').val();
    var username = $('#LoginEmail').val();

    $.ajax({
        url: '/SignIn',
        type: 'POST',
        data: {username: username, password: password},
        success: function (data) {
            var response = $.parseJSON(data);
            if (response === "Successful")
            {
                location.href = "/successful";

            } else
            {
                $('#errorMsg').html("Wrong username or password");
            }
        },
        error: function (data) {
            alert(username + " " + password);
        }
    });
}

function GoTo(available)
{
    localStorage.setItem('moreCamp', available);
      $('.boxes').hide();
     $('.nomore').hide();
     $('.myloadmore').html("");
    $('.loadingCamp').show();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/getCampaign',
        type: 'get',
        data:
                {
                    _token: CSRF_TOKEN,
                    action: available,
                    regStatus: localStorage.getItem('regStatus')
                },

        success: function (data) {
                         $('.loadingCamp').hide();
                           $('.boxes').show();
            if(data == "")
            {
                $('.nomore').show();
                 $('.myloadmore').html("");
            }
             else{
        $('.myloadmore').html('<br><button class="btn btn-primary btn-xs loadMore" onclick="loadMore();">Load More</button>');
    }
            $('.boxes').html(data);
            $('.ProfileNav-item').removeClass('is-active');
            $('.available').addClass('is-active');
            
        },
        error: function (data) {
             $('.loadingCamp').hide();
            alert('Error');
        }
    });

}

function loadMore()
{
     $('.boxes').hide();
     $('.nomore').hide();
     $('.myloadmore').html("");
    $('.loadingCamp').show();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/getMore',
        type: 'get',
        data:
                {
                    _token: CSRF_TOKEN,
                    action: localStorage.getItem('moreCamp'),
                    regStatus: localStorage.getItem('regStatus')
                },

        success: function (data) {
            alert(data);
//                         $('.loadingCamp').hide();
//                           $('.boxes').show();
//            if(data == "")
//            {
//                $('.nomore').show();
//                 $('.myloadmore').html("");
//            }
//             else{
//        $('.myloadmore').html('<br><button class="btn btn-primary btn-xs loadMore" onclick="loadMore();">Load More</button>');
//         }
//            $('.boxes').html(data);
//            $('.ProfileNav-item').removeClass('is-active');
//            $('.available').addClass('is-active');
            
        },
        error: function (data) {
             $('.loadingCamp').hide();
            alert('Error');
        }
    });
}
