﻿<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7 "> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie10 lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie10 lt-ie9"> <![endif]-->
<!--[if IE 9]><html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
    <!--<![endif]-->

    <head>

        <!-- Basic Page Needs -->
        <meta charset="utf-8">
        <title>Be</title>
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Mobile Specific Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- Favicons -->
        <link rel="shortcut icon" href="{{ asset('images/favicon.ico')}}">
<link rel="stylesheet" href="{{ asset('bootstrap.min.css')}}">
        <!-- FONTS -->
        <link rel='stylesheet' id='Roboto-css' href='http://fonts.googleapis.com/css?family=Roboto:100,300,400,400italic,700'>
        <link rel='stylesheet' id='Patua+One-css' href='http://fonts.googleapis.com/css?family=Patua+One:100,300,400,400italic,700'>
        <link rel='stylesheet' id='Roboto+Slab-css' href='http://fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,400italic,500italic,700'>

        <!-- CSS -->
        <link href="{{ asset('css_laravel/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
        <link rel='stylesheet' id='global-css' href="{{ asset('css/global.css')}}">
        <link rel='stylesheet' id='structure-css' href="{{ asset('css/structure.css')}}">
        <link href="{{ asset('css/showcase.css')}}" rel="stylesheet" />
        <link rel='stylesheet' id='local-css' href="{{ asset('css/taxi.css')}}">
        <link rel='stylesheet' id='custom-css' href="{{ asset('css/custom.css')}}">

        <link href="{{ asset('css/font-awesome.min.css')}}" rel="stylesheet" />
        <style>
            a.btn-facebook{
                background-color: #4c70ba;
                color: #fff;
                border-color: #4c70ba;
            }
            a.btn-facebook:hover {
                background-color: #3b5998
            }
            .portfolio_slider_ul li{
                margin:0 20px !important;
            }
            .portfolio_slider .slider_nav{
                display:inline !important;
            }
            .image_wrapper div.details{
                background:#000;
                color:#fff;
                height:80px;
                padding:30px
            }
            .image_wrapper div.details span{
                display:block;
                margin:5px;
                line-height: 20px;
            }
            .icon_wrapper{
                display: inline-block;
                border-width: 3px;
                border-style: solid;
                width: 55px;
                height: 55px;
                line-height: 55px;
                font-size: 30px;
                -webkit-border-radius: 100%;
                border-radius: 100%;
            }
            .image_frame .image_wrapper .image_links{
                background:none;
                width: 100%;
                height: 80px;
                position: absolute;
                left: 0;
                bottom: -100px;
                z-index: 4;
                overflow: hidden;
            }
            .image_frame .image_wrapper .mask:after{
                background:rgba(0,0,0,.4);
            }
            .image_frame .image_wrapper .image_links a:hover {
                background: none; 
                color: #fff; 
            }
            .image_frame:hover .image_wrapper .image_links{
                bottom: 53%;
                left: 30%;
            }
            .video{
                position: absolute;
                top: 10px;
                right: 10px;
                color: #fff;
                background: red;
                padding: 10px;
                z-index: 2;
                font-weight: 700;
                font-size: 12px;
            }
            #Filters .filters_wrapper,#Filters .filters_wrapper ul{
                display:inline-block;
            }
            #Filters .filters_wrapper .categories{
                width: 100%;
                text-align: center;
                float: none;
            }
            #Filters .filters_wrapper ul li {
                float: none;
                width: auto;
                margin: 0.5%;
                display: inline-block;
            }
            #Filters .filters_wrapper ul li a {
                padding: 14px 20px;
            }

            .ui-tabs .ui-tabs-nav{
                background:#000;
            }
            .ui-tabs .ui-tabs-nav li a{
                background: #000;
                color: #fff;
            }
        </style>
    </head>

    <body class="home layout-full-width one-page mobile-tb-left no-content-padding header-transparent minimalist-header sticky-header sticky-white ab-hide subheader-title-left menuo-right menuo-no-borders">
        <div id="fb-root"></div>
        <script>
            (function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.9&appId=130518047495673";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>

        <!-- Main Theme Wrapper -->
        <div id="Wrapper">
            <!-- Header Wrapper -->
            <div id="Header_wrapper">
                <!-- Header -->
                <header id="Header">

                    <!-- Header -  Logo and Menu area -->
                    <div id="Top_bar">
                        <div class="container">
                            <div class="column one">
                                <div class="top_bar_left" style="width: 100%;">
                                    <!-- Logo-->
                                    <div class="logo">
                                        <a id="logo" href="website" title="BeTaxi - BeTheme"><img class="scale-with-grid" src="{{ asset('images/taxi.png')}}" alt="BeTaxi - BeTheme" />
                                        </a>
                                    </div>
                                    <!-- Main menu-->
                                    <div class="menu_wrapper">
                                        <nav id="menu" class="menu-main-menu-container">
                                            <ul id="menu-main-menu" class="menu">
                                                <li class="menu-item menu-item-type-custom menu-item-object-custom">
                                                    <a href="#Header_wrapper"><span>Past Winners</span></a>
                                                </li>
                                                <li class="menu-item menu-item-type-custom menu-item-object-custom">
                                                    <a href="#standards"><span>Prizes</span></a>
                                                </li>
                                                <li class="menu-item menu-item-type-custom menu-item-object-custom">
                                                    <a href="#localizations"><span>How it Works</span></a>
                                                </li>
                                                <li class="menu-item menu-item-type-custom menu-item-object-custom">
                                                    <a href="#prices"><span>Latest Works</span></a>
                                                </li>
                                                <li class="menu-item menu-item-type-custom menu-item-object-custom">
<!--                                                    <a href="#prices"><span>Login</span></a>-->
                                                    <a href="#" class="" data-toggle="modal" data-target="#myModal">Login</a>
                                                </li>

                                            </ul>
                                        </nav><a class="responsive-menu-toggle " href="#"><i class="icon-menu"></i></a>
                                    </div>
                                    <!-- Secondary menu area - only for certain pages -->
                                    <div class="secondary_menu_wrapper"></div>
                                    <!-- Banner area - only for certain pages-->
                                    <div class="banner_wrapper"></div>
                                    <!-- Header Searchform area-->
                                    <div class="search_wrapper">
                                        <form method="get" id="searchform" action="#">

                                            <input type="text" class="field" name="s" id="s" placeholder="Enter your search" />
                                            <input type="submit" class="submit flv_disp_none" value="" />
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>


            </div>

            <!-- Main Content -->
            <div id="Content">
                <div class="content_wrapper clearfix">
                    <div class="sections_group">
                        <div class="entry-content">
                                
                            <div class="section " id="intro" style="padding-top:220px; padding-bottom:110px; background-color:#05060d; background-image:url(images/home_taxi_header.jpg); background-repeat:no-repeat; background-position:center top; ">
                             @if(session()->has('message'))
                                <div align="center" style="margin-bottom: 50px; font-size: 30px;">  
<!--                                <div class="alert alert-success">-->
                                    {{ session()->get('message') }}

                                </div>
                             
                              @endif
                                <div class="section_wrapper clearfix">

                                    <div class="items_group clearfix">

                                        <!-- One Second (1/2) Column -->
                                        <div class="column one-second column_column ">

                                            <div class="column_attr" style=" padding:80px 0 0 0;">
                                                <h2 style="color: #fff;">SIGN UP TODAY AND 
                                                    <br />
                                                    INCREASE YOUR CHANCES OF WINNING</h2>
                                                <hr class="no_line" style="margin: 0 auto 50px;" />
                                                <h4 style="color: #fff;">Get In Touch</h4>
                                                <p class="phone">
                                                    info@winmoney.ng
                                                </p>
                                            </div>
                                        </div>
                                        <!-- One Second (1/2) Column -->
                                        <div class="column one-second column_column ">
                                            <div class="column_attr">
                                                <div class="order_form clearfix" style="background: url(images/home_taxi_form_bg.png) no-repeat right top; padding: 30px; border-radius: 20px 0 20px 20px;">
                                                    <h3 style="
                                                        text-align: center;
                                                        ">JOIN NOW</h3>

                                                    <!-- Trigger the modal with a button -->
                                                    <!-- Trigger the modal with a button -->


                                                    <!-- Modal -->
                                                    <div id="myModal" class="modal fade" role="dialog" align="center">
                                                        <div class="modal-dialog">

                                                            <!-- Modal content-->
                                                            <div class="modal-content" style="width:75%;">
                                                                <div class="modal-header" style="background-color: #D0D8DB">
                                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                    <h4 class="modal-title topic">Sign In</h4>
                                                                </div>
                                                                <div class="modal-body">
<!--                                                                    <p>Some text in the modal.</p>-->
                                                                    <div id="dialog-content" class="iris_modal-content">
                                                                        <div id="registration_forms" data-init-form="login" class="col transparent_overlay_box registration_forms flip_box_container js-flip_box">
                                                                            <div class="flip_box">

                                                                                <div class="flip_box_card js-toggles login_form front">
                                                                                     <form class="userLoginForm" action="/SignIn" method="post">
                                                                                        <div style="color: red" id="errorMsg"></div>
                                                                                        <div class="row input_wrapper @if ($errors->has('LoginEmail')) has-error @endif">
                                                                                            <input class="" type="email" id="LoginEmail" name="LoginEmail" placeholder="Email address" value="{{ Input::old('LoginEmail') }}" >
                                                                                            @if ($errors->has('LoginEmail')) <p class="help-block">{{ $errors->first('LoginEmail') }}</p> @endif
                                                                                        </div>
                                                                                        <div class="row input_wrapper @if ($errors->has('loginpassword')) has-error @endif">
                                                                                            <input class="" type="password" id="loginpassword" name="loginpassword" placeholder="Password" value="{{ Input::old('loginpassword') }}">
                                                                                            @if ($errors->has('loginpassword')) <p class="help-block">{{ $errors->first('loginpassword') }}</p> @endif
                                                                                            <div>
                                                                                                <a class="forgot_password iris_link iris_link--gray-2" tabindex="-1" href="#" onclick="showAnother();">Forgot your password?</a>
                                                                                            </div>
                                                                                        </div>


                                                                                        <div class="row input_wrapper">
                                                                                            <input type="submit" onclick="userLogin();" value="Log in with email">
                                                                                        </div>
                                                                                    </form>
                                                                                    <form class="userForgotPassword" action="/forgotpassword" method="post" style="display: none;">
                                                                                        <div id="ForgptPasswordMsg">   </div><br>
                                                                                        <div class="row input_wrapper @if ($errors->has('ConfirmEmail')) has-error @endif">
                                                                                            <input class="" type="email" id="ConfirmEmail" name="ConfirmEmail" placeholder="Email address" value="{{ Input::old('ConfirmEmail') }}" >
                                                                                            @if ($errors->has('ConfirmEmail')) <p class="help-block">{{ $errors->first('ConfirmEmail') }}</p> @endif
                                                                                         <div>
                                                                                                <a class="forgot_password iris_link iris_link--gray-2" tabindex="-1" href="#" onclick="showAnother();">Sign In?</a>
                                                                                            </div>
                                                                                        </div>
                                                                                       

                                                                                        <div class="row input_wrapper">
                                                                                            <input type="button" onclick="ForgotPasswordMail();" value="Send Mail">
                                                                                        </div>
                                                                                    </form>

                                                                                </div>

                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                                <!--                                                                <div class="modal-footer">
                                                                                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                                                                                </div>-->
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="column one column_column " style="text-align:center;margin-bottom:0px">
                                                        <a href="http://test.winmoney.ng/auth/facebook" class="button action_button btn-facebook" style="margin-top:20px"><i class="fa fa-facebook"></i> Sign Up/Sign In With Facebook</a>

                                                        <h1 style="
                                                            color: #6b552e;
                                                            font-weight: 700;
                                                            ">OR</h1>
                                                    </div>
                                                    <div class="column one column_column ">


                                                        <h4 style="color: #6b552e;text-align:center">
                                                            Fill the short form
                                                        </h4>
                                                        <div style="padding:0px 30px">
                                                            <div role="form" class="wpcf7" id="wpcf7-f9-p2-o1" lang="en-US" dir="ltr">
                                                                <div class="screen-reader-response"></div>
                                                                <div id="contactWrapper">
                                                                    <form action="/user" method="post">
                                                                        <!-- One Second (1/2) Column -->
                                                                        <div class="column one-second form-group @if ($errors->has('Surname')) has-error @endif">
                                                                            <input placeholder="Your surname" type="text" name="Surname" id="name" size="40" aria-required="true" aria-invalid="false" value="{{ Input::old('Surname') }}"/>
                                                                            @if ($errors->has('Surname')) <p class="help-block">{{ $errors->first('Surname') }}</p> @endif
                                                                        </div>
                                                                        <div class="column one-second form-group @if ($errors->has('Other_Name')) has-error @endif">
                                                                            <input placeholder="Your other name" type="text" name="Other_Name" id="name" size="40" aria-required="true" aria-invalid="false" value="{{ Input::old('Other_Name') }}"/>
                                                                            @if ($errors->has('Other_Name')) <p class="help-block">{{ $errors->first('Other_Name') }}</p> @endif
                                                                        </div>
                                                                        <!-- One Second (1/2) Column -->
                                                                        <div class="column one-second form-group @if ($errors->has('Email')) has-error @endif">
                                                                            <input placeholder="Your e-mail" type="email" name="Email" id="email" size="40" aria-required="true" aria-invalid="false" value="{{ Input::old('Email') }}"/>
                                                                            @if ($errors->has('Email')) <p class="help-block">{{ $errors->first('Email') }}</p> @endif
                                                                        </div>
                                                                        <div class="column one-second form-group @if ($errors->has('State')) has-error @endif">
                                                                            <select name="State" aria-required="true" aria-invalid="false" value="{{ Input::old('State') }}"><?php echo $allStates; ?></select>

                                                                            @if ($errors->has('State')) <p class="help-block">{{ $errors->first('State') }}</p> @endif
                                                                        </div>
                                                                        <div class="column one-second form-group @if ($errors->has('City')) has-error @endif">
                                                                            <input placeholder="Your City" type="text" name="City" id="email" size="40" aria-required="true" aria-invalid="false" value="{{ Input::old('City') }}"/>
                                                                            @if ($errors->has('City')) <p class="help-block">{{ $errors->first('City') }}</p> @endif
                                                                        </div>
                                                                        <div class="column one-second form-group @if ($errors->has('Age_Range')) has-error @endif">
                                                                            <select name="Age_Range" aria-required="true" aria-invalid="false" value="{{ Input::old('Age_Range') }}"><option value="">Your Age Range</option><option value="16-25" @if (old('Age_Range') == '16-25') selected="selected" @endif>16-25</option><option value="26-35" @if (old('Age_Range') == '26-35') selected="selected" @endif>26-35</option><option value="36-45" @if (old('Age_Range') == '36-35') selected="selected" @endif>36-45</option><option value="46-above" @if (old('Age_Range') == '46-above') selected="selected" @endif>46-above</option></select>
                                                                            @if ($errors->has('Age_Range')) <p class="help-block">{{ $errors->first('Age_Range') }}</p> @endif
                                                                        </div>
                                                                        <div class="column one-second form-group @if ($errors->has('Gender')) has-error @endif">
                                                                            <select name="Gender" id="gender" aria-required="true" aria-invalid="false" value="{{ Input::old('Gender') }}"><option value="">Your Gender</option><option value="Male" @if (old('Gender') == 'Male') selected="selected" @endif>Male</option><option value="Female" @if (old('Gender') == 'Female') selected="selected" @endif>Female</option></select>
                                                                            @if ($errors->has('Gender')) <p class="help-block">{{ $errors->first('Gender') }}</p> @endif
                                                                        </div>
                                                                        <div class="column one-second form-group @if ($errors->has('Phone_Number')) has-error @endif">
                                                                            <input placeholder="Your phone-number" type="text" name="Phone_Number" id="phone" size="40" aria-required="true" aria-invalid="false" value="{{ Input::old('Phone_Number') }}"/>
                                                                            @if ($errors->has('Phone_Number')) <p class="help-block">{{ $errors->first('Phone_Number') }}</p> @endif
                                                                        </div>
                                                                        <div class="column one-second form-group @if ($errors->has('Password')) has-error @endif">
                                                                            <input placeholder="Your Password" type="password" name="Password" id="subject" size="40" aria-invalid="false" value="{{ Input::old('Password') }}"/>
                                                                            @if ($errors->has('Password')) <p class="help-block">{{ $errors->first('Password') }}</p> @endif
                                                                        </div>
                                                                        <div class="column one-second form-group @if ($errors->has('Confirm_Password')) has-error @endif">
                                                                            <input placeholder="Confirm Password" type="password" name="Confirm_Password" id="subject" size="40" aria-invalid="false" value="{{ Input::old('Confirm_Password') }}"/>
                                                                            @if ($errors->has('Confirm_Password')) <p class="help-block">{{ $errors->first('Confirm_Password') }}</p> @endif
                                                                        </div>
                                                                         <div class="column one form-group @if ($errors->has('ReferrerCode')) has-error @endif">
                                                                            <input placeholder="Referrer Code (Optional)" type="text" name="ReferrerCode" id="subject" size="40" aria-invalid="false" value="{{ Input::old('ReferrerCode') }}"/>
                                                                            @if ($errors->has('ReferrerCode')) <p class="help-block">{{ $errors->first('ReferrerCode') }}</p> @endif
                                                                        </div>
                                                                        <div class="column one-second form-group @if ($errors->has('g-recaptcha-response')) has-error @endif">
                                                                            {!! app('captcha')->display(); !!}
                                                                            @if ($errors->has('g-recaptcha-response')) <p class="help-block">{{ $errors->first('g-recaptcha-response') }}</p> @endif
                                                                        </div>
                                                                        <!--  <div class="column one">
                                                                              <textarea placeholder="Message" name="body" id="body" style="width:100%;" rows="10" aria-invalid="false"></textarea>
                                                                          </div> -->
                                                                        <div class="column one">
                                                                            <button type="submit" class="regBtn">Send A Message</button>
                                                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                            
<!--                                                                            <input type="button" value="Send A Message" id="submit" onClick="return check_values();">-->
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="section mcb-section " style="padding-top:20px; padding-bottom:40px; background-color:#fff" id="winners">
                                <div class="section_wrapper mcb-section-inner">
                                    <div class="column mcb-column one no-margin" style="margin:0;text-align:center">

                                        <div id="Filters" class="isotope-filters">
                                            <div class="filters_wrapper" style="text-align:center;display: block;">
                                                <ul class="categories">
                                                    <li class="reset current-cat">
                                                        <a class="all" data-rel="*" href="#">All</a>
                                                    </li>
                                                    <li class="branding">
                                                        <a data-rel=".category-branding" href="content/showcase/category-branding.html">Monthly Winners</a>
                                                    </li>
                                                    <li class="design">
                                                        <a data-rel=".category-design" href="content/showcase/category-design.html">Consolation Prizes</a>
                                                    </li>
                                                    <li class="photos">
                                                        <a data-rel=".category-photos" href="content/showcase/category-photos.html">Referral Winners</a>
                                                    </li>
                                                    <li class="photos">
                                                        <a data-rel=".category-photos" href="content/showcase/category-photos.html">Other Winners</a>
                                                    </li>
                                                    <li class="photos">
                                                        <a data-rel=".category-photos" href="content/showcase/category-photos.html">Videos</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="portfolio_slider " style="margin-bottom:60px">
                                            <a class="slider_nav slider_prev themebg" href="#"><i class="fa fa-arrow-left"></i></a>
                                            <a class="slider_nav slider_next themebg" href="#"><i class="fa fa-arrow-right"></i></a>
                                            <ul class="portfolio_slider_ul isotope">
                                               @foreach($allWInners as $eachWinner)
                                                <li class="portfolio-item isotope-item category-branding category-design has-thumbnail">
                                                    <div class="image_frame scale-with-grid">
                                                        <div class="image_wrapper">
                                                            <a href="images/winner.jpg" class="zoom" rel="prettyphoto"><div class="mask"></div><img width="800" height="600" src="images/winner.jpg" class="scale-with-grid wp-post-image" alt="portfolio_1" itemprop="image"></a>
                                                            <div class="details">
                                                                <span>Prize: {{$eachWinner->PrizeWon}}</span>
                                                                  <span>Name: {{$eachWinner->Name}}</span>
                                                                <span>Date: {{$eachWinner->created_at}}</span>
                                                              
                                                                
                                                            </div>
                                                            <div class="image_links triple">
                                                                <div class="call_center"><a href="images/winner.jpg" class="zoom" rel="prettyphoto"><span class="icon_wrapper"><i class="fa fa-image"></i></span></a></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                @endforeach



                                            </ul>
                                        </div>

                                        <div class="column one" style="text-align:center">
                                            <a style="font-size:25px">View All Winners <i class="fa fa-arrow-right"></i></a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="section " id="standards" style="padding-top:90px; padding-bottom:40px; background-color:#ffda4b; background-image:url(images/home_taxi_section_bg1.png); background-repeat:no-repeat; background-position:center top; ">
                                <div class="section_wrapper clearfix">

                                    <div class="items_group clearfix">
                                        <!-- One full width row-->
                                        <div class="column one column_column ">
                                            <div class="column_attr align_center">
                                                <h2>
                                                    Stand A Chance of Winning the Following and Much More
                                                    <br />
                                                </h2>
                                                <hr class="no_line hrmargin_b_30" />
                                                <div class="image_frame image_item no_link scale-with-grid alignnone no_border">
                                                    <div class="image_wrapper">
                                                        <img class="scale-with-grid" src="images/home_taxi_heading_sep.png" alt="" width="234" height="16" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="portfolio_slider " style="margin-bottom:60px">
                                            <a class="slider_nav slider_prev themebg" href="#"><i class="fa fa-arrow-left"></i></a>
                                            <a class="slider_nav slider_next themebg" href="#"><i class="fa fa-arrow-right"></i></a>
                                            <ul class="portfolio_slider_ul isotope">
                                                
                                                @foreach($allPrize as $eachPrize)
                                                
                                                      <li class="portfolio-item isotope-item category-branding category-design has-thumbnail">
                                                    <div class="icon_box icon_position_top no_border">
                                                        <div class="image_wrapper">
                                                            <img src="images/home_taxi_iconbox_1.png" alt="Comfortable" class="scale-with-grid" width="148" height="151" />
                                                        </div>
                                                        <div class="desc_wrapper">
                                                            <h4>{{$eachPrize->PrizeName}}</h4>
                                                            <div class="desc">
                                                                Fusce scelerisque semper ligula, at accumsan leo ornare at. Curabitur sollicitudin nisi a turpis molestie imperdiet! Integer in interdum risus, vel cursus amet.
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                @endforeach

                                            </ul>


                                        </div>
                                        <div class="column one" style="text-align:center">
                                            <a style="font-size:25px">View All Prizes <i class="fa fa-arrow-right"></i></a>
                                        </div>

                                        <!-- One Third (1/3) Column -->
                                        <!--<div class="column one-third column_icon_box ">
                                            <div class="icon_box icon_position_top no_border">
                                                <div class="image_wrapper">
                                                    <img src="images/home_taxi_iconbox_1.png" alt="Comfortable" class="scale-with-grid" width="148" height="151" />
                                                </div>
                                                <div class="desc_wrapper">
                                                    <h4>Comfortable</h4>
                                                    <div class="desc">
                                                        Fusce scelerisque semper ligula, at accumsan leo ornare at. Curabitur sollicitudin nisi a turpis molestie imperdiet! Integer in interdum risus, vel cursus amet.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>-->
                                        <!-- One Third (1/3) Column -->
                                        <!--<div class="column one-third column_icon_box ">
                                            <div class="icon_box icon_position_top no_border">
                                                <div class="image_wrapper">
                                                    <img src="images/home_taxi_iconbox_2.png" alt="4 minutes taxi" class="scale-with-grid" width="148" height="151" />
                                                </div>
                                                <div class="desc_wrapper">
                                                    <h4>4 minutes taxi</h4>
                                                    <div class="desc">
                                                        Praesent a malesuada elit. Quisque scelerisque lacus id nunc malesuada, quis facilisis orci luctus. Nam non tortor quis lectus congue placerat at a velit metus.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>-->
                                        <!-- One Third (1/3) Column -->
                                        <!--<div class="column one-third column_icon_box ">
                                            <div class="icon_box icon_position_top no_border">
                                                <div class="image_wrapper">
                                                    <img src="images/home_taxi_iconbox_3.png" alt="Pleasant" class="scale-with-grid" width="148" height="151" />
                                                </div>
                                                <div class="desc_wrapper">
                                                    <h4>Pleasant</h4>
                                                    <div class="desc">
                                                        Praesent a malesuada elit. Quisque scelerisque lacus id nunc malesuada, quis facilisis orci luctus. Nam non tortor quis lectus congue placerat at a velit metus.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>-->
                                    </div>
                                </div>
                            </div>
                            <div class="section " id="localizations" style="padding-top:50px; padding-bottom:0px; background-color:#1c1611; background-image:url(images/home_taxi_section_bg2.png); background-repeat:no-repeat; background-position:center top; ">
                                <div class="section_wrapper clearfix">
                                    <div class="items_group clearfix">
                                        <!-- One full width row-->
                                        <div class="column one column_column ">
                                            <div class="column_attr align_center">
                                                <h2 style="color: #fff;">Follow these Easy Steps and Start Winning</h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="section sections_style_0 ">
                                <div class="section_wrapper clearfix">
                                    <div class="items_group clearfix">
                                        <!-- One full width row-->
                                        <div class="column two-third column_column ">
                                            <div class="column_attr ">
                                                <div class="showcase_highlight themecolor">
                                                    1
                                                </div><h4>Class aptent taciti sociosqu ad</h4>
                                                <p style="border-left: 1px solid #d9d9d9; padding-left: 35px;" class="big">
                                                    Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac, laoreet enim. Phasellus fermentum in, dolor. Pellentesque facilisis. Nulla imperdiet sit amet magna. Vestibulum dapibus, mauris nec malesuada fames ac turpis velit, rhoncus eu, luctus et interdum adipiscing wisi.
                                                </p>
                                            </div>
                                        </div>
                                        <!-- One Third (1/3) Column -->
                                        <div class="column one-third column_image ">
                                            <div class="image_frame no_link scale-with-grid aligncenter no_border" style="margin-top:130px">
                                                <div class="image_wrapper">
                                                    <img class="scale-with-grid" src="images/home_showcase_arrow.png" alt="" width="108" height="104" />
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Page devider -->
                                        <!-- One full width row-->
                                        <div class="column one column_divider ">
                                            <hr class="no_line" />
                                        </div>
                                        <!-- One Third (1/3) Column -->
                                        <div class="column one-third column_image ">
                                            <div class="image_frame no_link scale-with-grid aligncenter no_border" style="margin-top:130px">
                                                <div class="image_wrapper">
                                                    <img class="scale-with-grid" src="images/home_showcase_arrow2.png" alt="" width="108" height="104" />
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Two Third (2/3) Column -->
                                        <div class="column two-third column_column ">
                                            <div class="column_attr align_right">
                                                <div class="showcase_highlight themecolor">
                                                    2
                                                </div><h4>Proin condimentum fermentum nunc</h4>
                                                <p style="border-right: 1px solid #d9d9d9; padding-right: 35px;" class="big">
                                                    Aliquam erat ac ipsum. Integer aliquam purus. Quisque lorem tortor fringilla sed, vestibulum id, eleifend justo vel bibendum sapien massa ac turpis faucibus orci luctus non, consectetuer lobortis quis, varius in, purus. Integer ultrices posuere ipsum dolor lacus, suscipit adipiscing cubilia Curae, Nulla ipsum dolor lacus, suscipit adipiscing. Cum sociis natoque penatibus et ultrices volutpat.</h5>
                                            </div>
                                        </div>
                                        <!-- Page devider -->
                                        <!-- One full width row-->
                                        <div class="column one column_divider ">
                                            <hr class="no_line" />
                                        </div>
                                        <!-- Two Third (2/3) Column -->
                                        <div class="column two-third column_column ">
                                            <div class="column_attr ">
                                                <div class="showcase_highlight themecolor">
                                                    3
                                                </div><h4>Per inceptos himenaeos mauris</h4>
                                                <p style="border-left: 1px solid #d9d9d9; padding-left: 35px;" class="big">
                                                    Nullam wisi ultricies a, gravida vitae, dapibus risus ante sodales lectus blandit eu, tempor diam pede cursus vitae, ultricies eu, faucibus quis, porttitor eros cursus lectus, pellentesque eget, bibendum a, gravida ullamcorper quam. Nullam viverra consectetuer. Quisque cursus et, porttitor risus. Aliquam sem. In hendrerit nulla quam nunc, accumsan congue. Lorem ipsum primis in nibh vel risus. Sed vel lectus.</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="section " id="prices" style="padding-top:90px; padding-bottom:0px; background-color:#ffda4b">
                                <div class="section_wrapper clearfix">
                                    <div class="items_group clearfix">
                                        <div class="wrap mcb-wrap one-third column-margin-20px clearfix" style="padding:0 1%; ">

                                            <div class="column mcb-column one column_column column-margin-">
                                                <div class="column_attr">
                                                    <h5>Our Mission</h5>
                                                </div>
                                            </div>

                                            <div class="column mcb-column one column_column column-margin-">
                                                <div class="column_attr">
                                                    <div style="padding-left: 25px; background: url(images/home_billiard_list.png) no-repeat left 9px;">
                                                        <p class="big" style="color: #3d4547; margin-bottom: 3px;">
                                                            Periodic and Consolation Prizes
                                                        </p>
                                                        <p>
                                                            Neque porro quisquam est, quiem ipsum quia gravida sit amet, coniem eleifend rutrum.
                                                        </p>
                                                    </div>
                                                    <div style="padding-left: 25px; background: url(images/home_billiard_list.png) no-repeat left 9px;">
                                                        <p class="big" style="color: #3d4547; margin-bottom: 3px;">
                                                            Special Prizes
                                                        </p>
                                                        <p>
                                                            Neque porro quisquam est, quiem ipsum quia gravida sit amet, coniem eleifend rutrum.
                                                        </p>
                                                    </div>
                                                    <div style="padding-left: 25px; background: url(images/home_billiard_list.png) no-repeat left 9px;">
                                                        <p class="big" style="color: #3d4547; margin-bottom: 3px;">
                                                            Referral Bonuses
                                                        </p>
                                                        <p>
                                                            Fusce aliquet urna sed dolor finibus, sed imperdiet libero iaculis eu quam justo.
                                                        </p>
                                                    </div>
                                                    <div style="padding-left: 25px; background: url(images/home_billiard_list.png) no-repeat left 9px;">
                                                        <a class="big" style="color: #3d4547; margin-bottom: 3px;">
                                                            Terms and Conditions
                                                        </a>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- One Third (1/3) Column -->
                                        <div class="wrap mcb-wrap one-third column-margin-20px clearfix" style="padding:0 1%; ">
                                            <!-- One Full Row-->
                                            <div class="column mcb-column one column_column column-margin-">
                                                <div class="column_attr">
                                                    <h5>From Our Blog</h5>
                                                </div>
                                            </div>

                                            <!-- One Full Row-->
                                            <div class="column mcb-column one">
                                                <div class="Latest_news">
                                                    <ul class="ul-first">
                                                        <li class="post-407 post type-post status-publish format-image has-post-thumbnail hentry category-masonry tag-themeforest tag-zend post_format-post-format-image">
                                                            <div class="photo">
                                                                <a href="http://themes.muffingroup.com/be/theme/ut-in-laoreet-sapien-eu-amet/">
                                                                    <img width="960" height="750" src="http://betheme.muffingroupsc.netdna-cdn.com/be/theme/wp-content/uploads/2015/12/home_betheme_blog_masonry_1-960x750.jpg" class="scale-with-grid wp-post-image" alt="">
                                                                </a>
                                                            </div>
                                                            <div class="desc">
                                                                <h5>
                                                                    <a href="http://themes.muffingroup.com/be/theme/ut-in-laoreet-sapien-eu-amet/">Ut in laoreet sapien eu amet</a>
                                                                </h5>
                                                                <div class="desc_footer">
                                                                    <span class="date">
                                                                        <i class="icon-clock"></i> December 14, 2015
                                                                    </span>

                                                                    <i class="icon-comment-empty-fa"></i>
                                                                    <a href="http://themes.muffingroup.com/be/theme/ut-in-laoreet-sapien-eu-amet/#respond" class="post-comments">0</a>

                                                                    <div class="button-love">
                                                                        <a href="#" class="mfn-love " data-id="407">
                                                                            <span class="icons-wrapper"><i class="icon-heart-empty-fa"></i><i class="icon-heart-fa"></i></span>
                                                                            <span class="label">107</span>
                                                                        </a>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </li>

                                                        <li class="post-407 post type-post status-publish format-image has-post-thumbnail hentry category-masonry tag-themeforest tag-zend post_format-post-format-image">
                                                            <div class="photo">
                                                                <a href="http://themes.muffingroup.com/be/theme/ut-in-laoreet-sapien-eu-amet/">
                                                                    <img width="960" height="750" src="http://betheme.muffingroupsc.netdna-cdn.com/be/theme/wp-content/uploads/2015/12/home_betheme_blog_masonry_1-960x750.jpg" class="scale-with-grid wp-post-image" alt="">
                                                                </a>
                                                            </div>
                                                            <div class="desc">
                                                                <h5>
                                                                    <a href="http://themes.muffingroup.com/be/theme/ut-in-laoreet-sapien-eu-amet/">Ut in laoreet sapien eu amet</a>
                                                                </h5>
                                                                <div class="desc_footer">
                                                                    <span class="date">
                                                                        <i class="icon-clock"></i> December 14, 2015
                                                                    </span>

                                                                    <i class="icon-comment-empty-fa"></i>
                                                                    <a href="http://themes.muffingroup.com/be/theme/ut-in-laoreet-sapien-eu-amet/#respond" class="post-comments">0</a>

                                                                    <div class="button-love">
                                                                        <a href="#" class="mfn-love " data-id="407">
                                                                            <span class="icons-wrapper"><i class="icon-heart-empty-fa"></i><i class="icon-heart-fa"></i></span>
                                                                            <span class="label">107</span>
                                                                        </a>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>

                                                    <a class="button button_js kill_the_icon" href="#">

                                                        <span style="background: #000;
                                                              color: #fff;" class="button_label">Read more</span>
                                                    </a>

                                                </div>
                                            </div>
                                        </div>
                                        <!-- One Third (1/3) Column -->
                                        <div class="wrap mcb-wrap one-third column-margin-20px clearfix" style="padding:0 1%; ">
                                            <!-- One Full Row-->
                                            <div class="column mcb-column one column_column column-margin-">
                                                <div class="column_attr">
                                                    <h5>Catch Up on Social Media</h5>
                                                </div>
                                            </div>
                                            <!-- One Full Row-->
                                            <div class="column mcb-column one column_column column-margin-">
                                                <div class="column_attr">
                                                    <div class="jq-tabs tabs_wrapper tabs_horizontal">
                                                        <ul>
                                                            <li>
                                                                <a class="facebook" href="#-1v">FaceBook</a>
                                                            </li>
                                                            <li>
                                                                <a class="twitter" href="#-2v">Twitter</a>
                                                            </li>
                                                            <li>
                                                                <a class="linkedin" href="#-3v">LinkedIn</a>
                                                            </li>
                                                        </ul>
                                                        <div id="-1v">
                                                            <div class="fb-page" data-href="https://www.facebook.com/WinMoneyNG" data-tabs="timeline" data-height="300px" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/WinMoneyNG" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/WinMoneyNG">Win Money</a></blockquote></div>
                                                        </div>
                                                        <div id="-2v">
                                                            <a class="twitter-timeline" href="https://twitter.com/WinMoneyNigeria">Tweets by WinMoneyNigeria</a>
                                                            <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                                                        </div>
                                                        <div id="-3v">
                                                            
                                                            <script src="https://platform.linkedin.com/in.js" type="text/javascript"></script>
                                                            <script type="IN/CompanyProfile" data-id="16275281" data-format="inline" data-related="false">
                                                            </script>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--<div class="section column-margin-10px" id="about" style="padding-top:40px; padding-bottom:70px; background-color:#ffda4b">-->
                            <!--<div class="section_wrapper clearfix">
                                <div class="items_group clearfix">-->
                            <!-- One Second (1/2) Column -->
                            <!--<div class="column one-second column_column ">
                                <div class="column_attr">
                            <!-- One Second (1/2) Column -->
                            <!--<div class="column one-second">
                                <div class="image_frame image_item no_link scale-with-grid aligncenter has_border">
                                    <div class="image_wrapper"><img class="scale-with-grid" src="images/home_taxi_gallery1.jpg" alt="" width="800" height="600" />
                                    </div>
                                </div>
                            </div>-->
                            <!-- One Second (1/2) Column -->
                            <!--<div class="column one-second">
                                <div class="image_frame image_item no_link scale-with-grid aligncenter has_border">
                                    <div class="image_wrapper"><img class="scale-with-grid" src="images/home_taxi_gallery2.jpg" alt="" width="800" height="600" />
                                    </div>
                                </div>
                            </div>-->
                            <!--<hr class="no_line" />-->
                            <!-- One Second (1/2) Column -->
                            <!--<div class="column one-second">
                                <div class="image_frame image_item no_link scale-with-grid aligncenter has_border">
                                    <div class="image_wrapper"><img class="scale-with-grid" src="images/home_taxi_gallery3.jpg" alt="" width="800" height="600" />
                                    </div>
                                </div>
                            </div>-->
                            <!-- One Second (1/2) Column -->
                            <!--<div class="column one-second">
                                <div class="image_frame image_item no_link scale-with-grid aligncenter has_border">
                                    <div class="image_wrapper"><img class="scale-with-grid" src="images/home_taxi_gallery4.jpg" alt="" width="800" height="600" />
                                    </div>
                                </div>
                            </div>-->
                            <!--</div>
                        </div>-->
                            <!-- One Second (1/2) Column -->
                            <!--<div class="column one-second column_column ">
                                <div class="column_attr" style=" padding:0 20% 0 10px;">
                                    <hr class="no_line hrmargin_b_20" />
                                    <h3>About us</h3>
                                    <p class="big">
                                        Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac, laoreet enim. Phasellus fermentum in, dolor. Pellentesque facilisis. Nulla imperdiet sit amet magna. Vestibulum dapibus, mauris nec malesuada fames ac turpis velit, rhoncus eu, luctus.
                                    </p>
                                    <p>
                                        Phasellus fermentum in, dolor. Pellentesque facilisis. Nulla imperdiet sit amet magna. Vestibulum dapibus, mauris nec malesuada fames ac turpis velit, rhoncus eu, luctus.
                                    </p>
                                    <hr class="no_line hrmargin_b_30" />
                                    <a class="button scroll button_large button_theme button_js" href="#Header_wrapper"><span class="button_label">Order now</span></a>
                                </div>
                            </div>-->
                            <!--</div>
                        </div>
                    </div>-->


                            <div class="section " style="padding-top:50px; padding-bottom:0px; background-color:#e8a600; background-image:url(images/home_taxi_section_bg3.png); background-repeat:no-repeat; background-position:center top; ">
                                <div class="section_wrapper clearfix">
                                    <div class="items_group clearfix">
                                        <!-- One full width row-->
                                        <div class="column one column_column ">
                                            <div class="column_attr align_center">
                                                <div style="text-align: center; margin: 0 10%;">
                                                    <a>Terms and Conditions</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a>Rules</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a>Privacy Policy</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="section the_content no_content">
                                <div class="section_wrapper">
                                    <div class="the_content_wrapper"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer-->
            <footer id="Footer" class="clearfix">
                <!-- Footer copyright-->
                <div class="footer_copy">
                    <div class="container">
                        <div class="column one">
                            <a id="back_to_top" class="button button_left button_js " href="#"><span class="button_icon"><i class="icon-up-open-big"></i></span></a>
                            <div class="copyright">
                                &copy; 2017 WinMoney.ng 
                            </div>
                            <!--Social info area-->
                            <ul class="social"></ul>
                        </div>
                    </div>
                </div>
            </footer>
        </div>

        <!-- JS -->

      <script src="{{ asset('sweetalert-master/dist/sweetalert.min.js')}}"></script>
     <script src="{{ asset('jquery-1.12.4.js')}}"></script>
     <script src="{{ asset('bootstrap.min.js')}}"></script>
     <script src="{{ asset('js/MyLaravelJs.js')}}" type="text/javascript"></script>
          <script>
                                                                                                jQuery(window).load(function () {
                                                                                                    jQuery(".jq-tabs").tabs();

                                                                                                    var retina = window.devicePixelRatio > 1 ? true : false;
                                                                                                    if (retina) {
                                                                                                        var retinaEl = jQuery("#logo img.logo-main");
                                                                                                        var retinaLogoW = retinaEl.width();
                                                                                                        var retinaLogoH = retinaEl.height();
                                                                                                        retinaEl.attr("src", "{{ asset('images/retina-taxi.png')}}").width(retinaLogoW).height(retinaLogoH);
                                                                                                        var stickyEl = jQuery("#logo img.logo-sticky");
                                                                                                        var stickyLogoW = stickyEl.width();
                                                                                                        var stickyLogoH = stickyEl.height();
                                                                                                        stickyEl.attr("src", "{{ asset('images/retina-taxi.png')}}").width(stickyLogoW).height(stickyLogoH);
                                                                                                        var mobileEl = jQuery("#logo img.logo-mobile");
                                                                                                        var mobileLogoW = mobileEl.width();
                                                                                                        var mobileLogoH = mobileEl.height();
                                                                                                        mobileEl.attr("src", "{{ asset('images/retina-taxi.png')}}").width(mobileLogoW).height(mobileLogoH);
                                                                                                    }
                                                                                                });
        </script>
    </body>

</html>