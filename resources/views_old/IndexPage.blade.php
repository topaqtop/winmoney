<!DOCTYPE html>

<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <title>Frame</title>

        <!-- Latest compiled and minified CSS -->
        <!--  <link rel='stylesheet' id='Roboto-css' href='http://fonts.googleapis.com/css?family=Roboto:100,300,400,400italic,700'>
                <link rel='stylesheet' id='Patua+One-css' href='http://fonts.googleapis.com/css?family=Patua+One:100,300,400,400italic,700'>
                <link rel='stylesheet' id='Roboto+Slab-css' href='http://fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,400italic,500italic,700'>-->

        <!-- CSS -->
        <link href="{{ asset('css_laravel/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="{{ asset('bootstrap.min.css')}}">
            <link rel="stylesheet" type="text/css" href="{{ asset('sweetalert-master/dist/sweetalert.css')}}">

            <link href="{{ asset('css/font-awesome.min.css')}}" rel="stylesheet" />
            <style>

.questionFrom
{

    padding: 15px;
    display: inline-block;
    color: white;
    cursor: pointer;
    background-color: black;
    width: 127px;
    font-weight: 900;

}

.textarea.focus .wrapper {
    height: auto !important;
}
#typeform .form .questions>li .wrapper {
    padding: 30px 20px 40px 0;
}


.item {
    line-height: 28px;
    font-size: 15px;
}
.item {
    color: #428F8C;
    font-family: "Lato","Helvetica Neue",Helvetica,Arial,sans-serif;
    -webkit-font-smoothing: antialiased;
    position: absolute;
    padding: 0;
    text-align: right;
    width: 20px;
    line-height: 35px;
    margin-right: 30px;
    font-size: 16px;
}
.item .arrow .arrow-right {
    width: 4px;
    height: 4px;
    margin-top: -2px;
    margin-left: 3px;
    border-right: 2px solid #428F8C;
    border-bottom: 2px solid #428F8C;
    -webkit-transform: rotate(-45deg);
    -moz-transform: rotate(-45deg);
    -ms-transform: rotate(-45deg);
    -o-transform: rotate(-45deg);
}
.asterisk {
    font-family: 'Courier New',monospace;
    display: -moz-inline-stack;
    display: inline-block;
    vertical-align: top;
    zoom: 1;
    margin-top: -5px;
}

#typeform .form .questions>li.visible .content {
    display: block;
}
#typeform .form .questions>li .content {
    display: none;
    position: relative;
    padding: 27px 27px 0 50px;
}

.question {
    font-size: 20px;
}
.question {
    color: #40A843;
    font-family: "Lato","Helvetica Neue",Helvetica,Arial,sans-serif;
    position: relative;
    padding: 0;
    cursor: default;
    padding-left: 50px;
    line-height: 140%;
    text-align: left;
    font-size: 24px;
    -webkit-touch-callout: text;
    -webkit-user-select: text;
    -khtml-user-select: text;
    -moz-user-select: text;
    -ms-user-select: text;
    user-select: text;
}
 
                
                
        .campaign_img {
            cursor: pointer;
        }

        .campaign-selected {
            padding: 30px;
            box-shadow: 1px 1px 1px 1px #000;
        }

        .campaign_rules {
            box-shadow: 0 2px 0 #D7E4ED;
            margin-top: 30px;
            padding: 20px;
            background: #fff;
            border-radius: 4px;
            display: none;
        }

        .closed {
            opacity: 0;
            width: 0 !important;
            margin: 0;
            transition: all 0s;
            -webkit-transition: all 0s;
        }

        .question_wrapper {
            box-shadow: 0 2px 0 #D7E4ED;
            background-color: #FFF;
            border-radius: 3px;
            position: relative;
            color: #607081;
            position: relative;
            padding: 30px;
            margin-bottom: 1.14285714em;
            -moz-transition: all 100ms linear;
            -o-transition: all 100ms linear;
            -webkit-transition: all 100ms linear;
            transition: all 100ms linear;
        }

        .close {
            position: absolute;
           margin-top: -24px!important;
            right: 30px;
            top: 30px;
            padding: 10px;
            cursor: pointer;
            color: #000;
        }
                
                .headNote
                {
                    color: white !important;
                }

                .user-card-row .tbl-cell.tbl-cell-photo img {
                    display: block;
                    width: 32px;
                    height: 32px;
                    -webkit-border-radius: 50%;
                    border-radius: 50%;
                }

                :active, :hover {
                    outline: 0;
                }

                audio, canvas, img, video {
                    vertical-align: middle;
                }

                img {
                    border: none;
                }

                img {
                    border: 0;
                    -ms-interpolation-mode: bicubic;
                }

                .friends-list.stripped .friends-list-item:nth-child(odd) {
                    background-color: #fbfcfd;
                }
                .friends-list.stripped .friends-list-item {
                    padding-top: 10px;
                    padding-bottom: 10px;
                }
                .friends-list .friends-list-item {
                    padding: 0 15px 12px;
                }
                article, aside, details, figcaption, figure, footer, header, hgroup, main, nav, section, summary {
                    display: block;
                }
                .friends-list .friends-list-item .user-card-row {
                    font-size: .9375rem;
                }
                .user-card-row {
                    display: table;
                    width: 100%;
                    border-collapse: collapse;
                    font-size: .8125rem;
                }
                * {
                    outline: 0 !important;
                }
                * {
                    padding: 0;
                    margin: 0;
                }
                *, :after, :before {
                    -webkit-box-sizing: border-box;
                    -moz-box-sizing: border-box;
                    box-sizing: border-box;
                }

                .tbl-row {
                    display: table-row;
                }

                * {
                    outline: 0 !important;
                }

                * {
                    padding: 0;
                    margin: 0;
                }

                *, :after, :before {
                    -webkit-box-sizing: border-box;
                    -moz-box-sizing: border-box;
                    box-sizing: border-box;
                }

                .user-card-row .tbl-cell.tbl-cell-photo {
                    width: 42px;
                    padding-right: 10px;
                }

                .tbl-cell {
                    display: table-cell;
                    vertical-align: middle;
                }

                * {
                    outline: 0 !important;
                }

                * {
                    padding: 0;
                    margin: 0;
                }

                *, :after, :before {
                    -webkit-box-sizing: border-box;
                    -moz-box-sizing: border-box;
                    box-sizing: border-box;
                }



                .tbl-cell {
                    display: table-cell;
                    vertical-align: middle;
                }

                * {
                    outline: 0 !important;
                }

                * {
                    padding: 0;
                    margin: 0;
                }

                *, :after, :before {
                    -webkit-box-sizing: border-box;
                    -moz-box-sizing: border-box;
                    box-sizing: border-box;
                }

                .user-card-row .tbl-cell.tbl-cell-action {
                    width: 20px;
                    white-space: nowrap;
                    padding-left: 10px;
                    vertical-align: middle;
                }
                .tbl-cell {
                    display: table-cell;
                    vertical-align: middle;
                }
                * {
                    outline: 0 !important;
                }
                * {
                    padding: 0;
                    margin: 0;
                }
                *, :after, :before {
                    -webkit-box-sizing: border-box;
                    -moz-box-sizing: border-box;
                    box-sizing: border-box;
                }

                .user-card-row .user-card-row-name {
                    font-weight: 600;
                    color: #343434;
                }
                .user-card-row p {
                    margin: 0;
                }

                .user-card-row .user-card-row-status {
                    color: #6c7a86;
                }

                .user-card-row p {
                    margin: 0;
                }

                .user-card-row a {
                    -webkit-transition: none;
                    transition: none;
                }

                .plus-link-circle {
                    display: block;
                    width: 20px;
                    height: 20px;
                    border: solid 1px #00a8ff;
                    text-align: left;
                    line-height: 10px;
                    -webkit-border-radius: 50%;
                    border-radius: 50%;
                    color: #00a8ff;
                    font-size: 1.25rem;
                    font-weight: 700;
                    position: relative;
                }

                a, a:focus, a:hover {
                    color: #0082c6;
                    text-decoration: none;
                    cursor: pointer;
                    outline: 0 !important;
                }

                a, button {
                    -webkit-transition: all .2s ease-in-out;
                    transition: all .2s ease-in-out;
                }

                .friends-list.stripped {
                    padding: 0;
                }
                .friends-list {
                    padding: 0 0 6px;
                }
                * {
                    outline: 0 !important;
                }
                * {
                    padding: 0;
                    margin: 0;
                }
                *, :after, :before {
                    -webkit-box-sizing: border-box;
                    -moz-box-sizing: border-box;
                    box-sizing: border-box;
                }
                .box-typical .box-typical-header-sm {
                    padding: 12px 15px;
                    font-weight: 600;
                    position: relative;
                }

                article, aside, details, figcaption, figure, footer, header, hgroup, main, nav, section, summary {
                    display: block;
                }



                .ProfileNav-item.is-active, .ProfileNav-item.is-active:hover {
                    border-color: #657786;
                    color: #657786;
                    cursor: default;
                }

                .box-typical {
                    -webkit-border-radius: 4px;
                    border-radius: 4px;
                    background: #fff;
                    border: solid 1px #d8e2e7;
                    margin: 0 0 20px;
                }


                .ProfileNav-item {
                    display: inline-block;
                }

                li {
                    text-align: inherit;
                }


                ul {
                    margin: 0;
                    list-style: none;
                    padding: 0;
                }

                user agent stylesheet ul, menu, dir {
                    display: block;
                    list-style-type: disc;
                    -webkit-margin-before: 1em;
                    -webkit-margin-after: 1em;
                    -webkit-margin-start: 0px;
                    -webkit-margin-end: 0px;
                    -webkit-padding-start: 40px;
                }

                .ProfileNav-item.is-active .ProfileNav-stat, .ProfileNav-stat:hover, .ProfileNav-stat:focus {
                    border-bottom-width: 4px;
                }

                .u-borderUserColor, .u-borderUserColorHover:hover, .u-borderUserColorHover:focus {
                    border-color: gold !important;
                }

                .ProfileNav-stat {
                    height: 60px;
                    -moz-box-sizing: border-box;
                    box-sizing: border-box;
                    border-bottom: 0 solid;
                    display: block;
                    line-height: 1;
                    padding: 14px 15px 7px;
                    transition: all .15s ease-in-out;
                }

                .u-textCenter {
                    text-align: center !important;
                }

                .ProfileNav-item.is-active .ProfileNav-label, .ProfileNav-item.is-active:hover .ProfileNav-label {
                    color: #cdc6bd;
                }

                .ProfileNav-label {
                    color: #cdc6bd;
                    font-size: 11px;
                    letter-spacing: .02em;
                    text-transform: uppercase;
                    transition: color .15s ease-in-out;
                }

                .ProfileNav-label, .ProfileNav-value {
                    display: block;
                }

                .u-hiddenVisually {
                    border: 0 !important;
                    clip: rect(1px,1px,1px,1px) !important;
                    font-size: 1px !important;
                    height: 1px !important;
                    overflow: hidden !important;
                    padding: 0 !important;
                    position: absolute !important;
                    width: 1px !important;
                }

                .ProfileNav-item.is-active .ProfileNav-value, .ProfileNav-item.is-active:hover .ProfileNav-value {
                    color: inherit;
                }

                .MomentCapsuleCover-title, body:not(.edge-design) .ProfileNav-value {
                    font-weight: 600;
                }

                .ProfileNav-value {
                    color: #66757f;
                    font-size: 18px;
                    font-weight: bold;
                    padding-top: 3px;
                    transition: color .15s ease-in-out;
                }

                .ProfileNav-label, .ProfileNav-value {
                    display: block;
                }






                .campBoxes {
                    margin-top: 28px;
                }

                .rouded {
                    border-radius: 5px;
                    border-style: solid;
                    border-width: 2px;
                    height: 200px;
                    width: 200px;
                }

                .ourColor {
                    border-color: #000;
                    color: #ffda4b;
                }

                .top {
                    background-color: red;
                    color: white;
                    padding: 10px 10px 10px 10px;
                    margin-top: 30px;
                }

                .bottom {
                    padding: 10px 10px 10px 10px;
                }

                .bottom {
                    background-color: black;
                    color: white;
                    padding: 10px 10px 10px 10px;
                    border-style: solid;
                    border-width: 1px;
                }

                .leftBoxBorder {
                    border-style: solid;
                    border-width: 1px;
                    height: 100px;
                    background-color: white;
                }

                .campaignbox {
                    background-color: white;
                    height: 200px;
                    margin-top: 30px;
                }

                .leftBar {
                    background-color: rebeccapurple;
                }

                .rightSide {
                    background-color: lightsteelblue;
                    min-height: 510px;
                }


                .text-center {
                    text-align: center !important;
                }

                .flush-pd-trailer {
                    padding-bottom: 20px !important;
                }



                .box--sml {
                    background-color: #FFF;
                    border-radius: 7px !important;
                    padding-left: 0.9375em;
                    padding-right: 0.9375em;
                    color: #607081;
                    position: relative;
                    padding-top: 0.85714286em;
                    padding-bottom: 0.85714286em;
                    margin-bottom: 1.14285714em;
                    -moz-transition: all 100ms linear;
                    -o-transition: all 100ms linear;
                    /* -webkit-transition: all 100ms linear; */
                    /* transition: all 100ms linear; */
                    /* background-color: #fff; */
                    border: 2px solid #e6ecf0;
                    /* border-radius: 5px 5px 0 0; */
                }

                .box-title {
                    color: rgba(52,73,94,0.8);
                }

                row .row {
                    max-width: none;
                    width: auto;
                }

                .text-center {
                    text-align: center !important;
                }

                .row-slat {
                    background-color: #F4F7F7;
                    padding-top: 0.85714286em;
                    padding-bottom: 0.85714286em;
                    margin-top: 0.85714286em;
                    margin-bottom: 0.85714286em;
                }

                .push-sbit {
                    margin-bottom: 5px !important;
                }

                .block {
                    display: block !important;
                }

                .trailer--s {
                    margin-bottom: 0.57142857em;
                }

                .elltext {
                    white-space: nowrap;
                    overflow: hidden;
                    text-overflow: ellipsis;
                }

                .disabled {
                    color: #9fa9b3;
                }



                .cards-grid .card-typical {
                    margin: 0 15px 30px;
                }

                .card-typical .card-typical-section:first-child {
                    border-top: none;
                }

                .user-card-row {
                    display: table;
                    width: 100%;
                    border-collapse: collapse;
                    font-size: .8125rem;
                }

                .tbl-row {
                    display: table-row;
                }

                .inline {
                    display: inline;
                }

                .user-card-row .tbl-cell.tbl-cell-photo {
                    width: 42px;
                    padding-right: 10px;
                }

                .price-card {
                    width: 100%;
                    max-width: 225px;
                    text-align: center;
                    display: inline-block;
                    vertical-align: bottom;
                    margin: 20px 7px 0;
                }


                .price-card .price-card-header {
                    border: solid 1px #d8e2e7;
                    -webkit-border-radius: .25rem .25rem 0 0;
                    border-radius: .25rem .25rem 0 0;
                    background: #000;
                    padding: 15px;
                    font-size: 1.25rem;
                    color: #fff;
                }


                .price-card .price-card-body {
                    background: #fff;
                    border: solid 1px #d8e2e7;
                    border-top: none;
                    -webkit-border-radius: 0 0 .25rem .25rem;
                    border-radius: 0 0 .25rem .25rem;
                    padding: 25px 0 25px 0;
                    height: 100px;
                }
                
                .fontClass
                {
                    font-size: 13px;
                }


.medium-3 {
    width: 25%;
}
.column, .columns {
    padding-left: 0.9375em;
    padding-right: 0.9375em;
    width: 100%;
    float: left;
    position: relative;
}
      .ucard-graph-state {
    padding: 1em 0.75em;
    background-color: #F4F7F7;
    border-radius: 6px;
    margin-bottom: 0.42857143em;
    margin-top: 0.42857143em;
}


                    #fupload {
                        opacity: 0;
                        position: absolute;
                        z-index: -1;
                    }
                
            </style>
    </head>
    <body>
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#home1">Winmoney Logo</a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav navbar-right">

                    <!--    <li><a style="" href="#">Available Campaigns</a></li>
                        <li><a style="" href="#">Previous Campoaigns</a></li>
                       
                        <li><a style="" href="#">&emsp; &emsp; Welcome {{@$UserName->Surname}} {{@$UserName->Other_Name}}</a></li> -->

                              <li><a style="" href="/logout">Logout</a></li>

                    </ul>
                </div>
            </div>
        </nav>
        <div class="col-lg-12" align="center" style="margin-top: 30px;background: gold;padding: 40px;position: relative;min-height: 150px;">

            <div class="col-lg-3" style="
                 margin-left: 15px;
                 ">
                <div class="regStatus"><?php if (@$regStatus == null) {
    echo "Error Occured";
} else {
    echo @$regStatus;
} ?>
                
                </div>
                <div class="rouded ourColor" style=" position: absolute; background: #fff; top: 0px; z-index: 30; border: 4px solid #fff; border-radius: 10px; padding: 2px;">
                     <?php
                                if (@$DP == "noDP") {

                                    echo '<form class="pull-left" enctype="multipart/form-data" id="upload_form" role="form" method="POST" action=""> <input type="hidden" name="_token" value="{{ csrf_token()}}"><input type="file" id="fupload" name="fupload" onchange="uploadDP();" /><label for="fupload" class="pull-left" style="cursor: pointer; position: absolute; z-index: 1; color: black;"><i class="fa fa-upload"></i> Edit</label></form><span class="loadingDP" style="display: none;"><img src="public/images/sending.gif" style="border-radius: 10px; width: 50%; height: 50%; margin-top: 55px;"/></span><span class="DP"><img src="public/images/no_pics.jpg" style="border-radius: 10px; width: 100%; height: 100%;"/></span>
                        ';
                                } else {
                                    $supported_image = array(
                                        'gif',
                                        'jpg',
                                        'jpeg',
                                        'png'
                                    );

                                    $src_file_name = (string)$DP;
                                    $ext = strtolower(pathinfo($src_file_name, PATHINFO_EXTENSION)); // Using strtolower to overcome case sensitive
                                    if (in_array($ext, $supported_image)) {
                                        $myExt = explode('.', $DP)[1];
                                               echo '<form class="pull-left" enctype="multipart/form-data" id="upload_form" role="form" method="POST" action=""> <input type="hidden" name="_token" value="{{ csrf_token()}}"><input type="file" id="fupload" name="fupload" onchange="uploadDP();" /><label for="fupload" class="pull-left" style="cursor: pointer; position: absolute; z-index: 1; color: black;"><i class="fa fa-upload"></i> Edit</label></form><span class="loadingDP" style="display: none;"><img src="public/images/sending.gif" style="border-radius: 10px; width: 50%; height: 50%; margin-top: 55px;"/></span><span class="DP"> <img src="public/images/'.Session::get('logedIn').'.'.$myExt.'" style=" border-radius: 10px; width: 100%; height: 100%; "/></span>
                                         ';                 
                                    } else {
                                        
                                          echo '<form class="pull-left" enctype="multipart/form-data" id="upload_form" role="form" method="POST" action=""> <input type="hidden" name="_token" value="{{ csrf_token()}}"><input type="file" id="fupload" name="fupload" onchange="uploadDP();" /><label for="fupload" class="pull-left" style="cursor: pointer; position: absolute; z-index: 1; color: black;"><i class="fa fa-upload"></i> Edit</label></form><span class="loadingDP" style="display: none;"><img src="public/images/sending.gif" style="border-radius: 10px; width: 50%; height: 50%; margin-top: 55px;"/></span><span class="DP"> <img src="http://graph.facebook.com/'.$DP.'/picture?type=large" style=" border-radius: 10px; width: 100%; height: 100%; "/></span>
                                         ';
                                        
                                    }
                                    
     
                                }
                                ?>
                </div>

            </div>



        </div><div class="col-lg-12" style="
                   padding: 0px 0;
                   background: #000;
                   color: #fff;
                   ">
            <div class="">
                <div class="col-sm-9 col-sm-offset-3" style="padding-left:40px">

                    <ul class="ProfileNav-list">
                        <li class="ProfileNav-item ProfileNav-item--tweets is-active notdone " onclick="GoTo('notdone')">
                            <a class="ProfileNav-stat ProfileNav-stat--link u-borderUserColor u-textCenter js-tooltip js-nav" style="text-decoration: none;" data-nav="tweets" tabindex="0" data-original-title="8,433 Tweets">
                                <span class="ProfileNav-label" aria-hidden="true">Campaign Available</span>
                                <span class="u-hiddenVisually" style="color: white;">Campaign Available.</span>
                                <span class="ProfileNav-value headNote myAvailable" data-count="8433" data-is-compact="false">
                                    {{$available}}
                                </span>
                            </a>
                        </li>
                        <li class="ProfileNav-item ProfileNav-item--following done" onclick="GoTo('done')">
                            <a class="ProfileNav-stat ProfileNav-stat--link u-borderUserColor u-textCenter js-tooltip js-openSignupDialog js-nonNavigable u-textUserColor" style="text-decoration: none;" data-nav="following" href="#" data-original-title="431 Following">
                                <span class="ProfileNav-label" aria-hidden="true">Campaign Answered</span>
                                <span class="u-hiddenVisually " style="color: white;">Campaign Answered</span>
                                <span class="ProfileNav-value headNote myAnswered" data-count="431" data-is-compact="false">{{$answered}}</span>
                            </a>
                        </li>
                        <li class="ProfileNav-item ProfileNav-item--following ">
                            <a class="ProfileNav-stat ProfileNav-stat--link u-borderUserColor u-textCenter js-tooltip js-openSignupDialog js-nonNavigable u-textUserColor" style="text-decoration: none;" data-nav="following" href="#" data-original-title="431 Following">
                                <span class="ProfileNav-label" aria-hidden="true">Current Odds (Weekly Prize)</span>
                                <span class="u-hiddenVisually " style="color: white;">Current Odds</span>
                                <span class="ProfileNav-value headNote" data-count="431" data-is-compact="false">5/10</span>
                            </a>
                        </li>
                        <li class="ProfileNav-item ProfileNav-item--following ">
                            <a class="ProfileNav-stat ProfileNav-stat--link u-borderUserColor u-textCenter js-tooltip js-openSignupDialog js-nonNavigable u-textUserColor" style="text-decoration: none;" data-nav="following" href="#" data-original-title="431 Following">
                                <span class="ProfileNav-label" aria-hidden="true">Prizes Available</span>
                                <span class="u-hiddenVisually " style="color: white;">Current Odds</span>
                                <span class="ProfileNav-value headNote myPrizeTotal" data-count="431" data-is-compact="false">{{$prizeTotal}}</span>
                            </a>
                        </li>
                        <!-- <li class="ProfileNav-item ProfileNav-item--followers">
                     <a class="ProfileNav-stat ProfileNav-stat--link u-borderUserColor u-textCenter js-tooltip js-openSignupDialog js-nonNavigable u-textUserColor" style="text-decoration: none;" data-nav="followers" href="/Kenny_Dash/followers" data-original-title="557 Followers">
                         <span class="ProfileNav-label" aria-hidden="true">Followers</span>
                         <span class="u-hiddenVisually">Followers</span>
                         <span class="ProfileNav-value" data-count="557" data-is-compact="false">557</span>
                     </a>
                 </li>
                 <li class="ProfileNav-item ProfileNav-item--favorites" data-more-item=".ProfileNav-dropdownItem--favorites">
                     <a class="ProfileNav-stat ProfileNav-stat--link u-borderUserColor u-textCenter js-tooltip js-openSignupDialog js-nonNavigable u-textUserColor" style="text-decoration: none;" data-nav="favorites" href="/Kenny_Dash/likes" data-original-title="2 Likes">
                         <span class="ProfileNav-label" aria-hidden="true">Likes</span>
                         <span class="u-hiddenVisually">Likes</span>
                         <span class="ProfileNav-value" data-count="2" data-is-compact="false">2</span>
                     </a>
                 </li>-->

                    </ul>



                </div>
            </div>
        </div>

        <div class="col-lg-12" style="
             background: #E9F0F6;
             ">

            <div class="col-lg-3 " align="center">
                <div class="ourColor" style=" margin-top: 50px; color: #000; text-align: left; margin-left: 40px;">
                    <h2>{{@$UserName->Surname}} {{@$UserName->Other_Name}}</h2><a>{{@$UserName->Email}}</a><strong style=" display: block;">Referrer Code: {{@$UserName->ReferrerCode}}</strong><h6><i class="fa fa-map-marker"></i> {{@$UserName->State}}, Nigeria</h6><h6><i class="fa fa-calendar"></i> Joined {{Carbon\Carbon::parse(@$UserName->SurName)->format('M d Y')}}</h6>
                    <a class="regUpdate" style="color: red;"> 
                      </a><h4></h4>
                </div>

                <br><br>
                        <div class="col-lg-12 col-md-12 col-sm-12" style="margin-left: 24px;">
                            <section class="box-typical">
                                <header class="box-typical-header-sm" style="background-color: black; color: white; border-bottom-color: gold; border-bottom-style: solid;">Available Prizes</header>
                                <div class="friends-list stripped">
                                  <?php $countPrizes = 0; ?>
                                    @foreach(@$allPrizes as $eachPrize)
                                   <?php $countPrizes++; ?>
                                    <article class="friends-list-item">
                                        <div class="user-card-row">
                                            <div class="tbl-row">
                                                <div class="tbl-cell tbl-cell-photo">
                                                    <a href="#" class="fontClass">
                                                        <img src="img/photo-64-2.jpg" alt="">
                                                    </a>
                                                </div>
                                                <div class="tbl-cell" align="left">
                                                    <p class="user-card-row-name status-online"><a href="#" class="fontClass"><?php if(@$eachPrize->PrizeName == '30,000'){echo $eachPrize->PrizeName." Cash";}else{ echo $eachPrize->NumberAvailable." ".$eachPrize->PrizeName."s";} ?></a></p>
<!--                                                    <p class="user-card-row-status">Co-founder of <a href="#">Company</a></p>-->
                                                </div>

                                            </div>
                                        </div>
                                    </article>
                                    @endforeach
 @if($countPrizes < 1)
                                     <article class="friends-list-item">
                                        <div class="user-card-row">
                                            <div class="tbl-row">
                                                <div class="tbl-cell tbl-cell-photo">
                                                    <a href="#">
                                                        <img src="img/photo-64-2.jpg" alt="">
                                                    </a>
                                                </div>
                                                <div class="tbl-cell fontClass" align="left">
                                                    <p class="user-card-row-name status-online"><a href="#" class="fontClass">No PrizesYet</a></p>
<!--                                                    <p class="user-card-row-status">Co-founder of <a href="#">Company</a></p>-->
                                                </div>

                                            </div>
                                        </div>
                                    </article>
                                    @endif
                                   
                                </div>

                            </section><!--.box-typical-->


                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12" style="margin-left: 24px;">
                            <section class="box-typical">
                                <header class="box-typical-header-sm" style="background-color: black; color: white; border-bottom-color: gold; border-bottom-style: solid;">Recent Winners</header>
                                <div class="friends-list stripped">
                                  <?php $countWinners = 0; ?>
                                    @foreach(@$winners as $eachwinner)
                                 <?php $countWinners++; ?>
                                    <article class="friends-list-item">
                                        <div class="user-card-row">
                                            <div class="tbl-row">
                                                <div class="tbl-cell tbl-cell-photo">
                                                    <a href="#" class="fontClass">
                                                        <img src="img/photo-64-2.jpg" alt="">
                                                    </a>
                                                </div>
                                                <div class="tbl-cell" align="left">
                                                    <p class="user-card-row-name status-online"><a href="#" class="fontClass"><?php if($eachwinner->PrizeWon != "30,000"){ echo $eachwinner->Name."<br> won a ".$eachwinner->PrizeWon."<br> on ". Carbon\Carbon::parse($eachwinner->created_at)->format('M d Y');}else{ echo $eachwinner->Name."<br> won ".$eachwinner->PrizeWon."<br> on ". Carbon\Carbon::parse($eachwinner->created_at)->format('M d Y');}?></a></p>
<!--                                                    <p class="user-card-row-status">Co-founder of <a href="#">Company</a></p>-->
                                                </div>

                                            </div>
                                        </div>
                                    </article>
                                    @endforeach
                                    @if($countWinners< 1)
                                     <article class="friends-list-item">
                                        <div class="user-card-row">
                                            <div class="tbl-row">
                                                <div class="tbl-cell tbl-cell-photo">
                                                    <a href="#">
                                                        <img src="img/photo-64-2.jpg" alt="">
                                                    </a>
                                                </div>
                                                <div class="tbl-cell fontClass" align="left">
                                                    <p class="user-card-row-name status-online"><a href="#" class="fontClass">No Winners Yet</a></p>
<!--                                                    <p class="user-card-row-status">Co-founder of <a href="#">Company</a></p>-->
                                                </div>

                                            </div>
                                        </div>
                                    </article>
                                    @endif
                                </div>

                            </section><!--.box-typical-->


                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12" style="margin-left: 24px;">
                            <section class="box-typical">
                                <header class="box-typical-header-sm" style="background-color: black; color: white; border-bottom-color: gold; border-bottom-style: solid;">Referred Users</header>
                                <div class="friends-list stripped">
                                    <?php $count = 0; ?>
                                    @foreach(@$referredUsers as $eachreffered)
                                    <?php $count++; ?>
                                    <article class="friends-list-item">
                                        <div class="user-card-row">
                                            <div class="tbl-row">
                                                <div class="tbl-cell tbl-cell-photo">
                                                    <a href="#" class="fontClass">
                                                        <img src="img/photo-64-2.jpg" alt="">
                                                    </a>
                                                </div>
                                                <div class="tbl-cell" align="left">
                                                    <p class="user-card-row-name status-online"><a href="#" class="fontClass"><?php echo $eachreffered->Surname." ".$eachreffered->Other_Name?></a></p>
<!--                                                    <p class="user-card-row-status">Co-founder of <a href="#">Company</a></p>-->
                                                </div>

                                            </div>
                                        </div>
                                    </article>
                                    @endforeach
                                    @if($count < 1)
                                     <article class="friends-list-item">
                                        <div class="user-card-row">
                                            <div class="tbl-row">
                                                <div class="tbl-cell tbl-cell-photo">
                                                    <a href="#">
                                                        <img src="img/photo-64-2.jpg" alt="">
                                                    </a>
                                                </div>
                                                <div class="tbl-cell fontClass" align="left">
                                                    <p class="user-card-row-name status-online"><a href="#" class="fontClass">No User Referred</a></p>
<!--                                                    <p class="user-card-row-status">Co-founder of <a href="#">Company</a></p>-->
                                                </div>

                                            </div>
                                        </div>
                                    </article>
                                    @endif
                                    
                                </div>

                            </section><!--.box-typical-->


                        </div>


                        </div>
                        <div class="col-lg-9 " align="center">
                            <span class="nomore" style="display: none;"><article class="ucard-graph-state text-center col-lg-4" style=" margin-top: 130px; margin-left: 30%;">No Campaign</article></span>
                             <span style='line-height: 370px; display: none;' class="loadingCamp"><img src='{{ asset('images/load.gif')}}' width='100px;' />&emsp;Loading Campaign</span>
                           
 <div class="boxes">   
 <?php $allId = array();
                            $count = 0; ?>
                            @foreach($latest as $myCampaign)
                                @if($count < 20)
                            @if(!in_array($myCampaign -> CampaignID, $allId))
                            <?php
                            $allId[] = $myCampaign->CampaignID;
                            $count++;
                            ?>
                            <?php $eachQuest = "<form method='post' id='form_" . $count . "' align='left'>
                                                            ";
                            csrf_field(); ?>
<?php $countForm = 0; ?>
                            @foreach($latest as $getQuest)
                            @if($getQuest -> CampaignID == $myCampaign -> CampaignID)

<?php $countForm++;




$eachQuest = $eachQuest . "<label>" . $getQuest->Question . "</label><br><input type='text' name='val_" . $countForm . "' class='form-control toDisable_" . $myCampaign->CampaignID . "' id='val_" . $countForm . "' /><br><br><input type='hidden' name='QuestID' class='Quest_" . $getQuest->id . "' value='" . $getQuest->id . "' />"; ?>

                            @endif
                            @endforeach
<?php $eachQuest = $eachQuest . "<input type='hidden' name='CampaigInfo' value='" . $myCampaign->CampaignID . "' />
                                                        </form>"; ?>
                            @if($count > 0 && Carbon\Carbon::parse(App\OurCampaign::find($myCampaign->CampaignID) -> EndDate)->addDay() > Carbon\Carbon::now())
                           
                            <div class=" col-lg-4 campBoxes camp_{{$myCampaign -> CampaignID}} myClass" align="center">
                                <div class=" ">
                                    <div class="box--sml text-center flush-pd-trailer col-lg-12">
                                        <a class="block " href="#">
                                            <img src="winner.jpg" width="100%"><h6 class="box-title elltext">{{App\OurCampaign::find($myCampaign->CampaignID) -> CampaignName}}</h6>
                                                 <?php
                                                                $tillEnd = (Carbon\Carbon::parse(App\OurCampaign::find($myCampaign->CampaignID)->EndDate)->diffInDays(Carbon\Carbon::now()) + 1) == 1 ? 'Campaign Ends today' : (Carbon\Carbon::parse(App\OurCampaign::find($myCampaign->CampaignID)->EndDate)->diffInDays(Carbon\Carbon::now()) + 1) . ' Day(s) to end';
                                                                ?>
                                                <h5 class="box-title elltext">{{$tillEnd}}</h5>
                                                <h5 class="box-title elltext" style="font-weight: 700;">
                                                   
                                                    <?php
                                                    $specialPrize = App\SpecialPrize::where('CampignID', '=', $myCampaign->CampaignID)->first();
                                                         if($specialPrize) 
                                                         {
                                                             echo '<h5>Special Prize: '.$specialPrize->SpecialPrize.'</h5>';	
                                                         }
                                                         else
                                                         {
                                                             echo '<h5>Special Prize: NIL</h5>';
                                                         }
                                                    ?>
                                                    
                                                    <h5><p>Details: {{App\OurCampaign::find($myCampaign->CampaignID) -> CampaignNotes}}</p></h5>
                                                    
                                                    
                                                </h5><p class="push-sbit"><button class="btn btn-primary btn-xs campaign_count_{{$count}} QuestClass" onclick="checkReg('<?php echo @$regStatus; ?>', '<?php echo $myCampaign->CampaignID; ?>')">TAKE QUIZ</button></p>
                                        </a>
                                         <input type="hidden" value="<?php echo $eachQuest; ?>" class="getQuest_{{$myCampaign -> CampaignID}}">
                                        <input type="hidden" value="<?php echo $count; ?>" class="getCount_{{$myCampaign -> CampaignID}}">
                                    </div>
                                </div>
                            </div>
                        
                        
                                            @endif
                                            @endif
                                             @endif
                                            @endforeach
</div>

                                             <div class=" two-third closed question_container col-lg-8" id="myform" style="margin-top: 28px;">
                                                            <div class="question_wrapper">
                                                                <a class="close" onclick="closeThis();" >X</a>
                                                                <div class="question_header">
                                                                    <h4>
                                                                        Fill Now

                                                                    </h4>
                                                                </div>
                                                                <div class="question_body">
                                                                    <span class="questLoad" style="display: none; font-size: 30px; margin-top: 15px;">Submiting...</span>
                                                                    <div class="form-row myQuest">
                                                                        
                                                                    </div>
                                                                </div>

  <div class="question_footer" style="margin-top: 30px;">
<!--                                                                <a style="float: left; color: white; cursor: pointer;" class="footer-button prevQ" onclick="prevQuest('<?php echo $count; ?>');"><i class="fa fa-angle-left" style="color: black;"></i></a>-->
                                                                <button style="display:inline-block; color: white; cursor: pointer; background-color: black;" class="footer-button Sub questionFrom questionBtn" onclick="subQuest();">Submit</button>
<!--                                                                <a style="float: right; color: white; cursor: pointer;" class="footer-button nextQ" onclick="nextQuest('<?php echo $count; ?>');"><i class="fa fa-angle-right" style="color: black;"></i></a>-->
                                                            </div>
                                                            </div>
                                                          
                                                        </div>
                                            
                                              
                                            </div>
             <div class="pull-right myloadmore"></div>
          
                                            </div>
       
                                            <div id="myModal1" class="modal fade" role="dialog" align="center">
                                                <div class="modal-dialog">

                                                    <!-- Modal content-->
                                                    <div class="modal-content" style="width:75%;">
                                                        <div class="modal-header" style="background-color: #D0D8DB">
                                                            <a href="#" class="close" data-dismiss="modal">&times;</a>
                                                            <h4 class="modal-title">Please Complete Your Registeration</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                              @if(session()->has('message'))
                                 {{ session()->get('message') }}

                              @endif
<!--                                                                    <p>Some text in the modal.</p>-->
                                                            <form class="form-horizontal" role="form" method="POST" action="/updateuser" style="padding: 0px 20px 20px 20px;">
                                                                <br>
                                                                    <div class="column one-second form-group @if ($errors->has('Phone_Number')) has-error @endif">
                                                                        <input class="form-control" placeholder="Your phone-number" type="text" name="Phone_Number" id="phone" size="40" aria-required="true" aria-invalid="false" value="{{ Input::old('Phone_Number') }}"/>
                                                                        @if ($errors->has('Phone_Number')) <p class="help-block">{{ $errors->first('Phone_Number') }}</p> @endif
                                                                    </div>
                                                                    <div class="column one-second form-group @if ($errors->has('Password')) has-error @endif">
                                                                        <input class="form-control"  placeholder="Your Password" type="password" name="Password" id="subject" size="40" aria-invalid="false" value="{{ Input::old('Password') }}"/>
                                                                        @if ($errors->has('Password')) <p class="help-block">{{ $errors->first('Password') }}</p> @endif
                                                                    </div>

                                                                    <div class="column one-second form-group @if ($errors->has('Confirm_Password')) has-error @endif">
                                                                        <input class="form-control"  placeholder="Confirm Password" type="password" name="Confirm_Password" id="subject" size="40" aria-invalid="false" value="{{ Input::old('Confirm_Password') }}"/>
                                                                        @if ($errors->has('Confirm_Password')) <p class="help-block">{{ $errors->first('Confirm_Password') }}</p> @endif
                                                                    </div>
                                                                    <div class="column one-second form-group @if ($errors->has('Age_Range')) has-error @endif">
                                                                        <select class="form-control"  name="Age_Range" aria-required="true" aria-invalid="false" value="{{ Input::old('Age_Range') }}"><option value="">Your Age Range</option><option value="16-25" @if (old('Age_Range') == '16-25') selected="selected" @endif>16-25</option><option value="26-35" @if (old('Age_Range') == '26-35') selected="selected" @endif>26-35</option><option value="36-45" @if (old('Age_Range') == '36-35') selected="selected" @endif>36-45</option><option value="46-above" @if (old('Age_Range') == '46-above') selected="selected" @endif>46-above</option></select>
                                                                        @if ($errors->has('Age_Range')) <p class="help-block">{{ $errors->first('Age_Range') }}</p> @endif
                                                                    </div>
                                                                    <div class="column one-second form-group @if ($errors->has('State')) has-error @endif">
                                                                        <select class="form-control"  name="State" aria-required="true" aria-invalid="false" value="{{ Input::old('State') }}"><?php echo @$allStates; ?></select>

                                                                        @if ($errors->has('State')) <p class="help-block">{{ $errors->first('State') }}</p> @endif
                                                                    </div>
                                                                    <div class="column one-second form-group @if ($errors->has('City')) has-error @endif">
                                                                        <input class="form-control"  placeholder="Your City" type="text" name="City" id="email" size="40" aria-required="true" aria-invalid="false" value="{{ Input::old('City') }}"/>
                                                                        @if ($errors->has('City')) <p class="help-block">{{ $errors->first('City') }}</p> @endif
                                                                    </div>
                                                                    <div class="column one-second form-group @if ($errors->has('ReferrerCode')) has-error @endif">
                                                                        <input class="form-control"  placeholder="Referrer Code (Optional)" type="text" name="ReferrerCode" id="email" size="40" aria-required="true" aria-invalid="false" value="{{ Input::old('ReferrerCode') }}"/>
                                                                        @if ($errors->has('ReferrerCode')) <p class="help-block">{{ $errors->first('ReferrerCode') }}</p> @endif
                                                                    </div>
                                                                    <button type="submit" class="btn btn-warning">Update</button>

                                                            </form>
                                                        </div>
                                                        <!--                                                                <div class="modal-footer">
                                                                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                                                                        </div>-->
                                                    </div>

                                                </div>
                                            </div>
        <div class="modal1"><!-- Place at bottom of page --></div>
        <script src="{{ asset('sweetalert-master/dist/sweetalert.min.js')}}"></script>
                                            <script src="{{ asset('jquery-1.12.4.js')}}"></script>
                                            <script src="{{ asset('bootstrap.min.js')}}"></script>
                                            <script src="{{ asset('js/MyLaravelJs.js')}}" type="text/javascript"></script>

                                            </body>
                                            </html>