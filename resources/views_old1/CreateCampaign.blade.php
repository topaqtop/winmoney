
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <title>Frame</title>
    </head>
    <body>
        <link rel="stylesheet" href="{{ asset('bootstrap.min.css')}}">
            <div align="center">
                <br>
                    <span class="col-lg-5">
                         <h2>Create Campaign</h2>
                  <br><br>  <input type="text" name="newCamp" placeholder="Add New Campaign" id="newCamp"><br><br>
                                        <select class="clientNames" name="allClients" id="allClients">
                                            <option value="">Select Client</option>
                                            <?php
                                            foreach (@$allClients as $eachClient) {
                                                echo '<option value="' . $eachClient->id . '">' . $eachClient->CompanyName . '</option>';
                                            }
                                            ?>
                                        </select>
                                        <br><br>
                                                <textarea id="campNotes" name="campNotes" placeholder="Campaign Notes"></textarea>
                                                 <br><br>
                                                <input type="date" name="campEnd" placeholder="End Date" id="campEnd"><br><br>

                                                            <button type="button" onclick="addnewcamp();">Add</button>
                                                            </span>
                    <span class="col-lg-5">
                        <h2>Add Question To Campaign</h2>
                                                            <br>
                                                                <br>
                                                                    <form action="" method="post" class="addQuestForm">
                                                                        <select class="campName" name="allCamp" id="allCamp">
                                                                            <option value="">Select Campaign</option>
<?php
foreach (@$allCampaigns as $eachCamp) {
    echo '<option value="' . $eachCamp->id . '_' . $eachCamp->ClientID . '">' . $eachCamp->CampaignName . '</option>';
}
?>
                                                                        </select>
                                                                        <br>
                                                                            <br>

                                                                                Select Response Type<br>
                                                                                    <input type="text" name="campQuest" class="campQuest" placeholder="Enter Question" />
                                                                                    <br>
                                                                                        <span style="width:50%;">
                                                                                            <input type="radio" name="myResponse" class="textboxType myTextfield" onclick="getType('myTextfield');"> Text Field
                                                                                                <input type="radio" name="myResponse" class="textboxType myTextarea" onclick="getType('myTextarea');"> Text Area
                                                                                                    <input type="radio" name="myResponse" class="textboxType myRadio" onclick="getType('myRadio');"> Radio Button
                                                                                                        <input type="radio" name="myResponse" class="textboxType mySelect" onclick="getType('mySelect');"> Select Option
                                                                                                            <input type="radio" name="myResponse" class="textboxType myRange" onclick="getType('myRange');"> Range
                                                                                                                <input type="radio" name="myResponse" class="textboxType myCheck" onclick="getType('myCheck');"> Check Boxes

                                                                                                                    </span>
                                                                                                                    <span style="width:50%;" class="moreOptions">

                                                                                                                    </span>
                                                                                                                    <br><br>
                                                                                                                            <button type="button" onclick="saveCampQuest();">Save</button>
                                                                                                                            </form>
                                                                    </span>
                                                                                                                            </div>
                                                                                                                            <script src="{{ asset('sweetalert-master/dist/sweetalert.min.js')}}"></script>
                                                                                                                            <script src="{{ asset('jquery-1.12.4.js')}}"></script>
                                                                                                                            <script src="{{ asset('bootstrap.min.js')}}"></script>
                                                                                                                            <script src="{{ asset('js/MyLaravelJs.js')}}" type="text/javascript"></script>
                                                                                                                            </body>
                                                                                                                            </html>
