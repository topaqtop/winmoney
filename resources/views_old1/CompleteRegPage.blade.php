
Thank you for signing up.
<br>
Please complete your registration below:
<br>
<br>
<form class="form-horizontal" role="form" method="POST" action="/updateuser">
    <br>
    <div class="column one-second form-group @if ($errors->has('Phone_Number')) has-error @endif">
        <input placeholder="Your phone-number" type="text" name="Phone_Number" id="phone" size="40" aria-required="true" aria-invalid="false" value="{{ Input::old('Phone_Number') }}"/>
        @if ($errors->has('Phone_Number')) <p class="help-block">{{ $errors->first('Phone_Number') }}</p> @endif
    </div>
    <div class="column one-second form-group @if ($errors->has('Password')) has-error @endif">
        <input placeholder="Your Password" type="password" name="Password" id="subject" size="40" aria-invalid="false" value="{{ Input::old('Password') }}"/>
        @if ($errors->has('Password')) <p class="help-block">{{ $errors->first('Password') }}</p> @endif
    </div>

    <div class="column one-second form-group @if ($errors->has('Confirm_Password')) has-error @endif">
        <input placeholder="Confirm Password" type="password" name="Confirm_Password" id="subject" size="40" aria-invalid="false" value="{{ Input::old('Confirm_Password') }}"/>
        @if ($errors->has('Confirm_Password')) <p class="help-block">{{ $errors->first('Confirm_Password') }}</p> @endif
    </div>
    <div class="column one-second form-group @if ($errors->has('Age_Range')) has-error @endif">
        <select name="Age_Range" aria-required="true" aria-invalid="false" value="{{ Input::old('Age_Range') }}"><option value="">Your Age Range</option><option value="16-25" @if (old('Age_Range') == '16-25') selected="selected" @endif>16-25</option><option value="26-35" @if (old('Age_Range') == '26-35') selected="selected" @endif>26-35</option><option value="36-45" @if (old('Age_Range') == '36-35') selected="selected" @endif>36-45</option><option value="46-above" @if (old('Age_Range') == '46-above') selected="selected" @endif>46-above</option></select>
        @if ($errors->has('Age_Range')) <p class="help-block">{{ $errors->first('Age_Range') }}</p> @endif
    </div>
    <div class="column one-second form-group @if ($errors->has('State')) has-error @endif">
        <select name="State" aria-required="true" aria-invalid="false" value="{{ Input::old('State') }}"><?php echo $allStates; ?></select>

        @if ($errors->has('State')) <p class="help-block">{{ $errors->first('State') }}</p> @endif
    </div>
    <div class="column one-second form-group @if ($errors->has('City')) has-error @endif">
        <input placeholder="Your City" type="text" name="City" id="email" size="40" aria-required="true" aria-invalid="false" value="{{ Input::old('City') }}"/>
        @if ($errors->has('City')) <p class="help-block">{{ $errors->first('City') }}</p> @endif
    </div>
    <button type="submit" class="btn btn-warning">Update</button>

</form>

