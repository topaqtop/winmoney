<!DOCTYPE html>
<html>
<head lang="en">
    
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <title>WinMoney Campaign</title>
    <meta name="description">
    <meta name="author">

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Favicons -->
    <link rel="shortcut icon" href="{{ asset('images/favicon.ico')}}">

    <!-- FONTS -->
    <link rel='stylesheet' id='Roboto-css' href='http://fonts.googleapis.com/css?family=Roboto:100,300,400,400italic,700'>
    <link rel='stylesheet' id='Patua+One-css' href='http://fonts.googleapis.com/css?family=Patua+One:100,300,400,400italic,700'>
    <link rel='stylesheet' id='Libre+Baskerville-css' href='http://fonts.googleapis.com/css?family=Libre+Baskerville:100,300,400,400italic,700,700'>
    <link rel='stylesheet' id='Roboto+Slab-css' href='http://fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,400italic,700,700'>
    <link rel='stylesheet' id='Lato-css' href='http://fonts.googleapis.com/css?family=Lato:100,300,400'>

    <!-- CSS -->


   <link rel='stylesheet' id='global-css' href="{{ asset('css/global.css')}}">
    <link rel='stylesheet' id='structure-css' href="{{ asset('css/structure.css')}}">
    <link rel='stylesheet' id='local-css' href="{{ asset('css/goodfood.css')}}">
    <link rel='stylesheet' id='custom-css' href="{{ asset('css/custom.css')}}">
    <link href="{{ asset('css/font-awesome.min.css')}}" rel="stylesheet" />


    <link rel="stylesheet" href="{{ asset('css_external/separate/elements/cards.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css_external/lib/font-awesome/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css_external/lib/bootstrap/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css_external/main.css')}}">
    <style>
        
    .text-center {
    text-align: center !important;
}
.flush-pd-trailer {
    padding-bottom: 0 !important;
}
.box--sml {
    background-color: #FFF;
    border-radius: 3px;
    padding-left: 0.9375em;
    padding-right: 0.9375em;
    color: #607081;
    position: relative;
    padding-top: 0.85714286em;
    padding-bottom: 0.85714286em;
    margin-bottom: 1.14285714em;
    -moz-transition: all 100ms linear;
    -o-transition: all 100ms linear;
    -webkit-transition: all 100ms linear;
    transition: all 100ms linear;
}

.box-title {
    color: rgba(52,73,94,0.8);
}

row .row {
    margin-left: -0.9375em;
    margin-right: -0.9375em;
    max-width: none;
    width: auto;
}
.text-center {
    text-align: center !important;
}
.row-slat {
    background-color: #F4F7F7;
    padding-top: 0.85714286em;
    padding-bottom: 0.85714286em;
    margin-top: 0.85714286em;
    margin-bottom: 0.85714286em;
}
.push-sbit {
    margin-bottom: 5px !important;
}
.block {
    display: block !important;
}

.trailer--s {
    margin-bottom: 0.57142857em;
}

.elltext{
        white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
}

.disabled {
    color: #9fa9b3;
}
        #past_winners {
            padding: 20px;
            background: #fff;
            margin: 20px;
            border-radius: 4px;
            box-shadow: 0 2px 0 #D7E4ED;
        }

            #past_winners ul li {
                padding: 10px;
            }

        #Header_creative #Top_bar .menu > li > a span {
            text-align: left;
        }

        .campaign_img {
            cursor: pointer;
        }

        .campaign-selected {
            padding: 30px;
            box-shadow: 1px 1px 1px 1px #000;
        }

        .campaign_rules {
            box-shadow: 0 2px 0 #D7E4ED;
            margin-top: 30px;
            padding: 20px;
            background: #fff;
            border-radius: 4px;
            display: none;
        }

        .closed {
            opacity: 0;
            width: 0 !important;
            margin: 0;
            transition: all 1s;
            -webkit-transition: all 1s;
        }

        .question_wrapper {
            box-shadow: 0 2px 0 #D7E4ED;
            background-color: #FFF;
            border-radius: 3px;
            position: relative;
            color: #607081;
            position: relative;
            padding: 30px;
            margin-bottom: 1.14285714em;
            -moz-transition: all 100ms linear;
            -o-transition: all 100ms linear;
            -webkit-transition: all 100ms linear;
            transition: all 100ms linear;
        }

        .close {
            position: absolute;
            border: 1px solid #000;
            right: 30px;
            top: 30px;
            padding: 10px;
            cursor: pointer;
            color: #000;
        }

        .question_footer {
            margin-top: 20px;
            text-align: center;
        }

        .footer-button {
            background: #000;
            padding: 20px;
        }
        
        .adjusted
        {
            
        }
    </style>
</head>
<body class="with-side-menu">
    <header class="site-header">
        <div class="container-fluid">

            <a href="#" class="site-logo">
                <img class="hidden-md-down" src="{{ asset('images/taxi.png')}}" alt="">
                <img class="hidden-lg-up" src="{{ asset('images/taxi.png')}}" alt="">
            </a>

            <button id="show-hide-sidebar-toggle" class="show-hide-sidebar">
                <span>toggle menu</span>
            </button>

            <button class="hamburger hamburger--htla">
                <span>toggle menu</span>
            </button>
            <div class="site-header-content">
                <div class="site-header-content-in">
                    <div class="site-header-shown">


                       
                        <div class="dropdown user-menu">
                            <button class="dropdown-toggle" id="dd-user-menu" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="{{ asset('images/avatar-2-64.png')}}" alt="">
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dd-user-menu">
                                <a class="dropdown-item" href="#"><span class="font-icon glyphicon glyphicon-user"></span>Profile</a>
<!--                                <a class="dropdown-item" href="#"><span class="font-icon glyphicon glyphicon-cog"></span>Settings</a>
                                <a class="dropdown-item" href="#"><span class="font-icon glyphicon glyphicon-question-sign"></span>Help</a>-->
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="/logout"><span class="font-icon glyphicon glyphicon-log-out"></span>Logout</a>
                            </div>
                        </div>
                        
                        <button type="button" class="pull-right burger-right">
                            <i class="font-icon-menu-addl"></i>
                        </button>
                    </div><!--.site-header-shown-->
 <strong style="margin:0 20px">Welcome <?php echo $UserName ?></strong>|<strong style="margin:0 20px">Current Odds: 3/10</strong>


                    <div class="mobile-menu-right-overlay"></div>

                </div><!--site-header-content-in-->
            </div><!--.site-header-content-->
        </div><!--.container-fluid-->
    </header><!--.site-header-->
    <div class="mobile-menu-left-overlay"></div>
    <nav class="side-menu">
        <ul class="side-menu-list">
            <li class="brown with-sub">
                <span>
                    <i class="font-icon glyphicon glyphicon-tint"></i>
                    <span class="lbl"><a href="/successful"> <span><i class="fa fa-2x fa-question-circle"></i> Campaigns</span></a></span>
                </span>
            </li>
            <li class="brown with-sub">
                <span>
                    <i class="font-icon glyphicon glyphicon-tint"></i>
                    <span class="lbl"><a href="/previousCampaign"> <span><i class="fa fa-2x fa-question-circle"></i>Previous Campaigns</span></a></span>
                </span>
            </li>
            <!--<li class="brown with-sub">
                <span>
                    <i class="font-icon glyphicon glyphicon-tint"></i>
                    <span class="lbl">Skins</span>
                </span>
                <ul>
                    <li><a href="theme-side-ebony-clay.html"><span class="lbl">Ebony Clay</span></a></li>
                    <li><a href="theme-side-madison-caribbean.html"><span class="lbl">Madison Caribbean</span></a></li>
                    <li><a href="theme-side-caesium-dark-caribbean.html"><span class="lbl">Caesium Dark Caribbean</span></a></li>
                    <li><a href="theme-side-tin.html"><span class="lbl">Tin</span></a></li>
                    <li><a href="theme-side-litmus-blue.html"><span class="lbl">Litmus Blue</span></a></li>
                    <li><a href="theme-rebecca-purple.html"><span class="lbl">Rebecca Purple</span></a></li>
                    <li><a href="theme-picton-blue.html"><span class="lbl">Picton Blue</span></a></li>
                    <li><a href="theme-picton-blue-white-ebony.html"><span class="lbl">Picton Blue White Ebony</span></a></li>
                </ul>
            </li>-->

        </ul>

    </nav><!--.side-menu-->
    <div class="page-content col-lg-12">
        <div class="">
            <div>
                <div id="Content">
                    <div class="content_wrapper clearfix">
                        <div class="sections_group">
                            <div class="entry-content" itemprop="mainContentOfPage">
                                <div class="section mcb-section full-width  " style="padding-top:0px; padding-bottom:0px;">
                                    <div class="section_wrapper mcb-section-inner">
                                       
                                            <div class="mcb-wrap-inner">
                                                <div class="col-lg-9">
                                                    <div style="margin: 30px 0;" class="campaign_wrapper">


                                                        <!-- <div class=" one-third">
                                                            <div class="campaign">
                                                                <img class="campaign_img campaign_open" src="{{ asset('images/winner.jpg')}}" />
                                                                <div class="acton_wrapper">
                                                                    <a class="campaign_open">Take This Quiz</a>
                                                                </div>

                                                            </div>
                                                            <div class="campaign_rules">
                                                                <p> Answer and stand a chance of winning.</p>
                                                                <p> This campaign will last till 12 march 2012</p>
                                                                <p> View terms and conditions for rules</p>


                                                            </div>
                                                        </div>-->
                                                        <?php $allId = array();  $count= 0;  ?>
                                                        @foreach($latest as $myCampaign)

                                                        @if(!in_array($myCampaign -> CampaignID, $allId))
                                                        <?php
                                                        $allId[] = $myCampaign -> CampaignID;
                                                        $count++;
                                                        ?>
                                                        <?php $eachQuest = "<form method='post' id='form_".$count."'>
                                                            ";  csrf_field(); ?>
                                                            <?php $countForm = 0; ?>
                                                            @foreach($latest as $getQuest)
                                                            @if($getQuest -> CampaignID == $myCampaign -> CampaignID)

                                                            <?php  $countForm++; $eachQuest = $eachQuest."<label>".$getQuest->Question."</label><input type='text' name='val_".$countForm."' class='toDisable_".$myCampaign -> CampaignID."' id='val_".$countForm."' /><input type='hidden' name='QuestID' class='Quest_".$getQuest -> id."' value='".$getQuest -> id."' />"; ?>

                                                            @endif
                                                            @endforeach
                                                            <?php $eachQuest = $eachQuest."<input type='hidden' name='CampaigInfo' value='".$myCampaign -> CampaignID."' />
                                                        </form>"; ?>
                                                        @if($count > 0 && Carbon\Carbon::parse(App\OurCampaign::find($myCampaign->CampaignID) -> EndDate)->addDay() > Carbon\Carbon::now())
                                                        <div class="col-lg-4 camp_{{$myCampaign -> CampaignID}} myClass">
                                                            <div class="campaign">
                                                                <!--                                                            <img class="campaign_img campaign_open_{{$myCampaign -> CampaignID}} campaign_count_{{$count}}" onclick="takeThis({{$myCampaign -> CampaignID}})" src="{{ asset('images/winner.jpg')}}" />-->




                                                                <div class="card-grid-col campaign_img campaign_open_{{$myCampaign -> CampaignID}} campaign_count_{{$count}}" onclick="takeThis({{$myCampaign -> CampaignID}})">
                                                                    <article class="card-typical">
                                                                        <div class="card-typical-section">
                                                                            <div class="user-card-row">
                                                                                <div class="tbl-row">
                                                                                    <div class="tbl-cell tbl-cell-photo">
                                                                                        <a href="#">
                                                                                            <img src="{{ asset('images/photo-64-2.jpg')}}" alt="">
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="tbl-cell">
                                                                                        <p class="user-card-row-name"><a href="#">Tim Colins</a></p>
                                                                                        <?php
                                                                                        $tillEnd = (Carbon\Carbon::parse(App\OurCampaign::find($myCampaign->CampaignID) -> EndDate)->diffInDays(Carbon\Carbon::now())+1) == 1 ? 'Campaign Ends today' : (Carbon\Carbon::parse(App\OurCampaign::find($myCampaign->CampaignID) -> EndDate)->diffInDays(Carbon\Carbon::now())+1).' Day(s) to end';

                                                                                        ?>
                                                                                        <p class="color-blue-grey-lighter">{{$tillEnd}}</p>
                                                                                    </div>
                                                                                    <div class="tbl-cell tbl-cell-status">
                                                                                        <a href="#" class="font-icon font-icon-star active"></a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="card-typical-section card-typical-content">
                                                                            <div class="photo">
                                                                                <img src="{{ asset('images/winner.jpg')}}" alt="">
                                                                            </div>
                                                                            <header class="title"><a href="#">{{App\OurCampaign::find($myCampaign->CampaignID) -> CampaignName}}</a></header>
                                                                            <p>{{App\OurCampaign::find($myCampaign->CampaignID) -> CampaignNotes}}</p>
                                                                        </div>
                                                                        <div class="card-typical-section">
                                                                            <div class="card-typical-linked"><a href="#"><a class="campaign_open_{{$myCampaign -> CampaignID}} campaign_count_{{$count}}" onclick="takeThis({{$myCampaign -> CampaignID}})">Take Campaign {{App\OurCampaign::find($myCampaign->CampaignID) -> CampaignName}} </a></a></div>
                                                                           
                                                                        </div>
                                                                    </article><!--.card-typical-->
                                                                </div>



                                                                <!--                                                            <div class="acton_wrapper">
                                                                                                                    <a class="campaign_open_{{$myCampaign -> CampaignID}} campaign_count_{{$count}}" onclick="takeThis({{$myCampaign -> CampaignID}})">Take Campaign {{App\OurCampaign::find($myCampaign->CampaignID) -> CampaignName}} </a>
                                                                                                                </div>-->
                                                                <input type="hidden" value="<?php echo $eachQuest; ?>" class="getQuest_{{$myCampaign -> CampaignID}}">
                                                                <input type="hidden" value="<?php echo $count; ?>" class="getCount_{{$myCampaign -> CampaignID}}">


                                                            </div>
                                                            <!--                                                        <div class="campaign_rules">
                                                                                                            <p> Answer and stand a chance of winning.</p>
                                                                                                            <p> This campaign will last till 12 march 2012</p>
                                                                                                            <p> View terms and conditions for rules</p>


                                                                                                        </div>-->
                                                        </div>
                                                        @endif
                                                        @endif
                                                        @endforeach


                                                        <div class="box--sml text-center flush-pd-trailer col-lg-3"><a class="block " href="/en/prizes/30-amazon-gift-card-205"><img class="trailer--s" src="https://images.playfulbet.com/system/uploads/prize/prize_image/205/amazon.png" alt="Amazon"><h6 class="box-title elltext">$30 Amazon Gift Card</h6><p class="push-sbit"><strong class="coins">1,690,000&nbsp;Coins</strong></p></a></div>

                                                        <style>
                                                            .cards-grid .card-typical {
                                                        margin: 0 15px 30px;
                                                                }
                                                                
                                                                .card-typical .card-typical-section:first-child {
                                                                border-top: none;
                                                                    }
                                                                    
                                                                    .user-card-row {
    display: table;
    width: 100%;
    border-collapse: collapse;
    font-size: .8125rem;
}

.tbl-row {
    display: table-row;
}

.user-card-row .tbl-cell.tbl-cell-photo {
    width: 42px;
    padding-right: 10px;
}
                                                            </style>
                                                            
                                                        <div class="card-grid-col col-lg-4">
					<article class="card-typical">
						<div class="card-typical-section">
							<div class="user-card-row">
								<div class="tbl-row">
									<div class="tbl-cell tbl-cell-photo">
										<a href="#">
											<img src="img/avatar-1-64.png" alt="">
										</a>
									</div>
									<div class="tbl-cell">
										<p class="user-card-row-name"><a href="#">Tim Colins</a></p>
										<p class="color-blue-grey-lighter">3 days ago - 23 min read</p>
									</div>
									<div class="tbl-cell tbl-cell-status">
										<a href="#" class="font-icon font-icon-star"></a>
									</div>
								</div>
							</div>
						</div>
						<div class="card-typical-section card-typical-content">
							<header class="title"><a href="#">The Jacobs Ladder of coding</a></header>
							<p>That’s a great idea! I’m sure we could start this project as soon as possible. Let’s meet tomorow! That’s a great idea! I’m sure we could start this project as soon as possible. Let’s meet tomorow! </p>
						</div>
						<div class="card-typical-section">
							<div class="card-typical-linked">in <a href="#">Coders Life</a></div>
							<a href="#" class="card-typical-likes">
								<i class="font-icon font-icon-heart"></i>
								123
							</a>
						</div>
					</article><!--.card-typical-->
				</div>
                                                        
                                                        <div class=" two-third closed question_container col-lg-8">
                                                            <div class="question_wrapper">
                                                                <a class="close" onclick="closeThis();" >X</a>
                                                                <div class="question_header">
                                                                    <h4>
                                                                        Fill Now

                                                                    </h4>
                                                                </div>
                                                                <div class="question_body">
                                                                    <div class="form-row myQuest">

                                                                    </div>
                                                                </div>


                                                            </div>
                                                            <div class="question_footer">
                                                                <a style="float: left; color: white; cursor: pointer;" class="footer-button prevQ" onclick="prevQuest();"><i class="fa fa-angle-left"></i></a>
                                                                <a style="display:inline-block; color: white; cursor: pointer;" class="footer-button Sub" onclick="subQuest();">Submit</a>
                                                                <a style="float: right; color: white; cursor: pointer;" class="footer-button nextQ" onclick="nextQuest();"><i class="fa fa-angle-right"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                
                                                <div class="col-lg-3 col-md-6 col-sm-6" >
					<section class="box-typical">
						<header class="box-typical-header-sm">Past Winners</header>
						<div class="friends-list stripped" style="padding:0px 20px 0px 20px;">
                                                     @foreach($winners as $mywinners)
                                                                
							<article class="friends-list-item">
								<div class="user-card-row">
									<div class="tbl-row">
										<div class="tbl-cell tbl-cell-photo">
											<a href="#">
												<img src="{{ asset('images/photo-64-2.jpg')}}" alt="">
											</a>
										</div>
										<div class="tbl-cell">
											<p class="user-card-row-name status-online"><a href="#">{{$mywinners -> Name}}</a></p>
											<p class="user-card-row-status">Won <a href="#">{{$mywinners -> PrizeWon}}</a></p>
                                                                                        <p class="user-card-row-status">Date <a href="#">On {{Carbon\Carbon::parse($mywinners -> created_at)->format('m/d/Y')}}</a></p>
										</div>
										<div class="tbl-cell tbl-cell-action">
											<a href="#" class="plus-link-circle"><span>&plus;</span></a>
										</div>
									</div>
								</div>
							</article>
                                                                 @endforeach
                                                </div>
                                        </section>
                                                </div>
                                               
                                            </div>
                                       
                                    </div>
                                </div>

                            </div>
                        </div>



                    </div>
                </div>
            </div><!--.card-grid-->
            <div class="clear"></div>
        </div><!--.container-fluid-->
    </div><!--.page-content-->
    <!-- JS -->
    <script type="text/javascript" src="{{ asset('js/jquery-2.1.4.min.js')}}"></script>

    <script type="text/javascript" src="{{ asset('js/mfn.menu.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.plugins.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.jplayer.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/animations/animations.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/scripts.js')}}"></script>
    <script src="{{ asset('js_external/lib/tether/tether.min.js')}}"></script>
    <script src="{{ asset('js_external/lib/bootstrap/bootstrap.min.js')}}"></script>
    <script src="{{ asset('js_external/plugins.js')}}"></script>

    <script src="{{ asset('js_external/lib/salvattore/salvattore.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js_external/lib/match-height/jquery.matchHeight.min.js')}}"></script>
    <script>
        $(function() {
            $('.card-user').matchHeight();
        });
    </script>

    <script src="{{ asset('js_external/app.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/MyLaravelJS.js')}}"></script>
<script src="{{ asset('js/adminJs.js')}}" type="text/javascript"></script>

</body>
</html>
