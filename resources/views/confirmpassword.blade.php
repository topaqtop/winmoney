<html>
<head>
<title>SurveyMoney Password Reset</title>
   <link rel="stylesheet" type="text/css" href="{{ asset('sweetalert-master/dist/sweetalert.css')}}">
<style>
.centered {
  position: fixed;
  top: 50%;
  left: 50%;
  margin-top: -50px;
  margin-left: -100px;
}

.tableProp{
border:none
}
</style>
</head>
<body bgcolor="gold" >

<form action="/updateUserPassword" method="post" id="changePassword">
<div class="changePass centered">

<table class="centered tableProp">
<thead>
<tr><th colspan="2"><h4>Create New Kudipoll Password</h4></th></tr>
</thead>
<tbody>
<tr><td colspan ="2" id = "updateRegError" style="color: red; margin-bottom: 10px;"><td></tr>
<tr><td>New Password</td><td><input type="password" id="password" name="password" class="form-control" palaceholder="Enter Password"/></td></tr>
<tr><td>Retype Password</td><td><input type="password" id="password1" name="password1" class="form-control" palaceholder="Retype Password"/><input type="hidden" value="{{@$ForUser}}" id="codeSent" name="codeSent"></td></tr>
<tr><th colspan="2"><button type="button" onclick="changeUserPassword();" class="btn btn-primary btn-xs">Save Changes</button></th></tr>
</tbody>
</table>

</div>
</form>
<script src="{{ asset('sweetalert-master/dist/sweetalert.min.js')}}"></script>
<script src="{{ asset('ion/ion.rangeSlider-2.1.7/js/vendor/jquery-1.12.3.min.js')}}" type="text/javascript"></script>
 <script src="{{ asset('js/MyLaravelJs.js')}}" type="text/javascript"></script>
</body>
</html>