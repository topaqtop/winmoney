<!DOCTYPE html>

<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <meta name="csrf-token" content="{{ csrf_token() }}" /> 
<meta name="viewport" content="width=1024">
        <title>Kudipoll Website</title>
    <link rel="shortcut icon" href="public/images/surveymoney_logo.jpg">
        <!-- Latest compiled and minified CSS -->
        <!--  <link rel='stylesheet' id='Roboto-css' href='http://fonts.googleapis.com/css?family=Roboto:100,300,400,400italic,700'>
                <link rel='stylesheet' id='Patua+One-css' href='http://fonts.googleapis.com/css?family=Patua+One:100,300,400,400italic,700'>
                <link rel='stylesheet' id='Roboto+Slab-css' href='http://fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,400italic,500italic,700'>-->

        <!-- CSS -->
        <link href="" rel="stylesheet" />
        <link href="{{ asset('css/chosen.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('ion/ion.rangeSlider-2.1.7/css/ion.rangeSlider.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('ion/ion.rangeSlider-2.1.7/css/ion.rangeSlider.skinHTML5.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('css_laravel/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="{{ asset('bootstrap.min.css')}}">
            <link rel="stylesheet" type="text/css" href="{{ asset('sweetalert-master/dist/sweetalert.css')}}">
                <link href="{{ asset('css/font-awesome.min.css')}}" rel="stylesheet" />
                <link rel="stylesheet" href="{{ asset('css/rrssb.css')}}" />
                <style>



.belowLeftSyd{
width: 100%;
}

.campBoxForMedium
{
width: 33%!important;
}

@media (max-width: 1094px) and (min-width: 765px){
.campBoxes {
    width: 50%;
}
.recentWinner{
padding: 0px;
}
.belowLeftSyd {

padding: 0px;
}

.leftSyd{
padding-left: 0px
}

}
@media (min-width: 768px){
.col-sm-9 {
    width: 75%;
}
}
@media (min-width: 768px){
.col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9 {
    float: left;
}
}


                 .dpUpload
                    {
                       color: white;
                       background-color: black;
                       opacity: 0.5;
                    }
                    .grayout {
    opacity: 0.3; /* Real browsers */
    filter: alpha(opacity = 60); /* MSIE */
}
                      input.largeChecks
                    {
                        width: 20px;
                        height: 20px;
                    }
                  .navbar-default .navbar-collapse, .navbar-default .navbar-form{
                    border-color:#000;
                }
                .navbar-default .navbar-toggle{
                    border-color:#fff;
                }
                .navbar-default .navbar-toggle:focus, .navbar-default .navbar-toggle:hover{
                     border-color:gold;
                     background:none;
                }
                .navbar-default .navbar-toggle .icon-bar{
                    background:gold;
                }
                body{
                    background: #E9F0F6;
                }
                .filternavcontainer .navbar-default{
                    background:none !important;
                    border:none !important;
                    margin:0 !important;
                }
               .filternavcontainer .navbar-nav>li>a{
                   line-height:14px;
               }
               .filternavcontainer .navbar-collapse{
                    padding:0
                }
                 .useful-links {
                    padding:15px 8px;
                }
               

                 .useful-links li{
                   text-align: left;
    padding: 2px 20px;
                }
                .irs-slider{
                    border-color:#000;
                    background:#000;
                    box-shadow: none;
                }
                .irs-slider:hover{
                     background:#000;
                }
                .question_header h4{
                    width: 93%;
    line-height: 25px;
                }
                .share{display:none}
                .ul-help{
                    display: inline-block;float: right !important;
                }
                .ul-help a {
                    padding: 23px !important; color: #fff !important;text-transform: uppercase;font-size:11px;background-color: rgba(52,73,94,0.4);
                }
                .ul-help a:hover, .ul-help a:focus {
                   color: #fff !important;
                }
                .irs-from, .irs-to, .irs-single{
                    background:#000;
                }
                .irs-bar {
   
    border-top: 1px solid #000;
    border-bottom: 1px solid #000;
    background: gold;
}
                .irs-bar-edge {
    border: 1px solid #000;
    border-right:none;
    background: gold;
}
                .chosen-container .chosen-drop{
                    border-top:1px;
                    
                }
                
.chosen-container-single .chosen-single{
     padding: 10px;
    border: none;
    border-bottom: 1px solid rgba(59,94,84,0.4);
    border-radius: 0;
    background: none;
    box-shadow: none;
    height: 100%;
}
.chosen-container-active.chosen-with-drop .chosen-single{
    background:none;
}
                .chosen-container-single .chosen-single span{
                    font-size: 16px;
                }
                .response select,.response input[type=text]{
                    width: 100%;
    border: none;
    border-bottom: 1px solid rgba(59,94,84,0.4);
    padding: 10px 0;
                }
                .chosen-container-single .chosen-single div b {
                    background: url('img/chosen-sprite.png') no-repeat 0 2px;
                }
                .closeform{
                        position: absolute;
    margin-top: -22px!important;
    right: 10px;
    top: 12px;
    padding: 10px;
    cursor: pointer;
    color: red;
    float:right;
        font-size: 21px;
    font-weight: 700;
                }
                .question span{
                        letter-spacing: .5px;
    font-size: 18px;
    font-weight: normal;
                }
                .question-list{
                    padding:20px 0;
                }
                .response{
                    margin-left: 50px;
    margin-top: 20px;
                }

                .btn-prize{
                        background: #000;
    color: gold;
    padding: 10px;
                }
                              .btn-prize:hover,.btn-prize:active,.btn-prize:focus{

    color:#fff;
                }
.questionFrom
{

    padding: 15px;
    display: inline-block;
    color: white;
    cursor: pointer;
    background-color: black;
    width: 127px;
    font-weight: 900;

}

.textarea.focus .wrapper {
    height: auto !important;
}
#typeform .form .questions>li .wrapper {
    padding: 30px 20px 40px 0;
}


.item {
    line-height: 28px;
    font-size: 15px;
}
.item {
    color: gold;
    font-family: "Lato","Helvetica Neue",Helvetica,Arial,sans-serif;
    -webkit-font-smoothing: antialiased;
    position: absolute;
    padding: 0;
    text-align: right;
    width: 20px;
    line-height: 35px;
    margin-right: 30px;
    font-size: 16px;
}
.item .arrow .arrow-right {
    width: 4px;
    height: 4px;
    margin-top: -2px;
    margin-left: 3px;
    border-right: 2px solid #428F8C;
    border-bottom: 2px solid #428F8C;
    -webkit-transform: rotate(-45deg);
    -moz-transform: rotate(-45deg);
    -ms-transform: rotate(-45deg);
    -o-transform: rotate(-45deg);
}
.asterisk {
    font-family: 'Courier New',monospace;
    display: -moz-inline-stack;
    display: inline-block;
    vertical-align: top;
    zoom: 1;
    margin-top: -5px;
}

#typeform .form .questions>li.visible .content {
    display: block;
}
#typeform .form .questions>li .content {
    display: none;
    position: relative;
    padding: 27px 27px 0 50px;
}

.question {
    font-size: 20px;
}
.question {
    color: #0082c6;
    font-family: "Lato","Helvetica Neue",Helvetica,Arial,sans-serif;
    position: relative;
    padding: 0;
    cursor: default;
    padding-left: 50px;
    line-height: 140%;
    text-align: left;
    font-size: 24px;
    -webkit-touch-callout: text;
    -webkit-user-select: text;
    -khtml-user-select: text;
    -moz-user-select: text;
    -ms-user-select: text;
    user-select: text;
}
 
                
                
        .campaign_img {
            cursor: pointer;
        }

        .campaign-selected {
            padding: 30px;
            box-shadow: 1px 1px 1px 1px #000;
        }

        .campaign_rules {
            box-shadow: 0 2px 0 #D7E4ED;
            margin-top: 30px;
            padding: 20px;
            background: #fff;
            border-radius: 4px;
            display: none;
        }

        .closed {
            opacity: 0;
            width: 0 !important;
            margin: 0;
            transition: all 0s;
            -webkit-transition: all 0s;
        }

        .question_wrapper {
           padding:20px;
            border-radius: 3px;
            position: relative;
            color: #444;
            position: relative;
            padding: 30px;
            margin-bottom: 1.14285714em;
            -moz-transition: all 100ms linear;
            -o-transition: all 100ms linear;
            -webkit-transition: all 100ms linear;
            transition: all 100ms linear;
        }

        .close {
            position: absolute;
           margin-top: -24px!important;
            right: 30px;
            top: 30px;
            padding: 10px;
            cursor: pointer;
            color: #000;
        }
                
                .headNote
                {
                    color: white !important;
                }

                .user-card-row .tbl-cell.tbl-cell-photo img {
                    display: block;
                    width: 32px;
                    height: 32px;
                    -webkit-border-radius: 50%;
                    border-radius: 50%;
                }

                :active, :hover {
                    outline: 0;
                }

                audio, canvas, img, video {
                    vertical-align: middle;
                }

                img {
                    border: none;
                }

                img {
                    border: 0;
                    -ms-interpolation-mode: bicubic;
                }

                .friends-list.stripped .friends-list-item:nth-child(odd) {
                    background-color: #fbfcfd;
                }
                .friends-list.stripped .friends-list-item {
                    padding-top: 10px;
                    padding-bottom: 10px;
                }
                .friends-list .friends-list-item {
                    padding: 0 15px 12px;
                }
                article, aside, details, figcaption, figure, footer, header, hgroup, main, nav, section, summary {
                    display: block;
                }
                .friends-list .friends-list-item .user-card-row {
                    font-size: .9375rem;
                }
                .user-card-row {
                    display: table;
                    width: 100%;
                    border-collapse: collapse;
                    font-size: .8125rem;
                }
                * {
                    outline: 0 !important;
                }
                * {
                    padding: 0;
                    margin: 0;
                }
                *, :after, :before {
                    -webkit-box-sizing: border-box;
                    -moz-box-sizing: border-box;
                    box-sizing: border-box;
                }

                .tbl-row {
                    display: table-row;
                }

                * {
                    outline: 0 !important;
                }

                * {
                    padding: 0;
                    margin: 0;
                }

                *, :after, :before {
                    -webkit-box-sizing: border-box;
                    -moz-box-sizing: border-box;
                    box-sizing: border-box;
                }

                .user-card-row .tbl-cell.tbl-cell-photo {
                    width: 42px;
                    padding-right: 10px;
                }

                .tbl-cell {
                    display: table-cell;
                    vertical-align: middle;
                }

                * {
                    outline: 0 !important;
                }

                * {
                    padding: 0;
                    margin: 0;
                }

                *, :after, :before {
                    -webkit-box-sizing: border-box;
                    -moz-box-sizing: border-box;
                    box-sizing: border-box;
                }



                .tbl-cell {
                    display: table-cell;
                    vertical-align: middle;
                }

                * {
                    outline: 0 !important;
                }

                * {
                    padding: 0;
                    margin: 0;
                }

                *, :after, :before {
                    -webkit-box-sizing: border-box;
                    -moz-box-sizing: border-box;
                    box-sizing: border-box;
                }

                .user-card-row .tbl-cell.tbl-cell-action {
                    width: 20px;
                    white-space: nowrap;
                    padding-left: 10px;
                    vertical-align: middle;
                }
                .tbl-cell {
                    display: table-cell;
                    vertical-align: middle;
                }
                * {
                    outline: 0 !important;
                }
                * {
                    padding: 0;
                    margin: 0;
                }
                *, :after, :before {
                    -webkit-box-sizing: border-box;
                    -moz-box-sizing: border-box;
                    box-sizing: border-box;
                }

                .user-card-row .user-card-row-name {
                    font-weight: 600;
                    color: #343434;
                }
                .user-card-row p {
                    margin: 0;
                }

                .user-card-row .user-card-row-status {
                    color: #6c7a86;
                }

                .user-card-row p {
                    margin: 0;
                }

                .user-card-row a {
                    -webkit-transition: none;
                    transition: none;
                }

                .plus-link-circle {
                    display: block;
                    width: 20px;
                    height: 20px;
                    border: solid 1px #00a8ff;
                    text-align: left;
                    line-height: 10px;
                    -webkit-border-radius: 50%;
                    border-radius: 50%;
                    color: #00a8ff;
                    font-size: 1.25rem;
                    font-weight: 700;
                    position: relative;
                }

                a, a:focus, a:hover {
                    color: #0082c6;
                    text-decoration: none;
                    cursor: pointer;
                    outline: 0 !important;
                }

                a, button {
                    -webkit-transition: all .2s ease-in-out;
                    transition: all .2s ease-in-out;
                }

                .friends-list.stripped {
                    padding: 0;
                }
                .friends-list {
                    padding: 0 0 6px;
                }
                * {
                    outline: 0 !important;
                }
                * {
                    padding: 0;
                    margin: 0;
                }
                *, :after, :before {
                    -webkit-box-sizing: border-box;
                    -moz-box-sizing: border-box;
                    box-sizing: border-box;
                }
                .box-typical .box-typical-header-sm {
                    padding: 12px 15px;
                    font-weight: 600;
                    position: relative;
                }

                article, aside, details, figcaption, figure, footer, header, hgroup, main, nav, section, summary {
                    display: block;
                }

                .FilterNav-list{
                    display:inline;width:100%;
                }

                .FilterNav-item.is-active, .ProfileNav-item.is-active:hover {
                    border-color: #657786;
                    color: #657786;
                    cursor: default;
                }

                .box-typical {
                    -webkit-border-radius: 4px;
                    border-radius: 4px;
                    background: #fff;
                    border: solid 1px #d8e2e7;
                    margin: 0 0 20px;
                }


                .FilterNav-item {
                    display: inline-block;
                }

                li {
                    text-align: inherit;
                }


                ul {
                    margin: 0;
                    list-style: none;
                    padding: 0;
                }

                user agent stylesheet ul, menu, dir {
                    display: block;
                    list-style-type: disc;
                    -webkit-margin-before: 1em;
                    -webkit-margin-after: 1em;
                    -webkit-margin-start: 0px;
                    -webkit-margin-end: 0px;
                    -webkit-padding-start: 40px;
                }

                .FilterNav-item.is-active .FilterNav-stat, .FilterNav-stat:hover, .FilterNav-stat:focus {
                    border-bottom-width: 4px;
                }

             

                .FilterNav-stat {
                    height: 60px;
                    -moz-box-sizing: border-box;
                    box-sizing: border-box;
                    border-bottom: 0 solid;
                    display: block;
                    line-height: 1;
                    padding: 14px 15px 7px !important;
                    transition: all .15s ease-in-out;
                     text-align: center !important;
                      border-color: gold !important;
                }

                .FilterNav-stat:hover, .FilterNav-stat:focus {
                     border-color: gold !important;
                }

                .FilterNav-item.is-active .FilterNav-label, .FilterNav-item.is-active:hover .FilterNav-label {
                    color: #cdc6bd;
                }

                .FilterNav-label {
                    color: #cdc6bd;
                    font-size: 11px;
                    letter-spacing: .02em;
                    text-transform: uppercase;
                    transition: color .15s ease-in-out;
                }

                .FilterNav-label, .FilterNav-value {
                    display: block;
                }

             

                .FilterNav-item.is-active .FilterNav-value, .FilterNav-item.is-active:hover .FilterNav-value {
                    color: inherit;
                }

                .MomentCapsuleCover-title, body:not(.edge-design) .FilterNav-value {
                    font-weight: 600;
                }

                .FilterNav-value {
                    color: #66757f;
                    font-size: 18px;
                    font-weight: bold;
                    padding-top: 3px;
                    transition: color .15s ease-in-out;
                }

                .FilterNav-label, .FilterNav-value {
                    display: block;
                }






                .campBoxes {
                    margin-top: 28px;
                }

                .rouded {
                    border-radius: 5px;
                    border-style: solid;
                    border-width: 2px;
                    height: 200px;
                    width: 200px;
                }

                .ourColor {
                    border-color: #000;
                    color: #ffda4b;
                }

                .top {
                    background-color: red;
                    color: white;
                    padding: 10px 10px 10px 10px;
                    margin-top: 30px;
                }

                .bottom {
                    padding: 10px 10px 10px 10px;
                }

                .bottom {
                    background-color: black;
                    color: white;
                    padding: 10px 10px 10px 10px;
                    border-style: solid;
                    border-width: 1px;
                }

                .leftBoxBorder {
                    border-style: solid;
                    border-width: 1px;
                    height: 100px;
                    background-color: white;
                }

                .campaignbox {
                    background-color: white;
                    height: 200px;
                    margin-top: 30px;
                }

                .leftBar {
                    background-color: rebeccapurple;
                }

                .rightSide {
                    background-color: lightsteelblue;
                    min-height: 510px;
                }


                .text-center {
                    text-align: center !important;
                }

                .flush-pd-trailer {
                    padding-bottom: 20px !important;
                }



                .box--sml {
                    background-color: #FFF;
                    border-radius: 7px !important;
                    padding-left: 0.9375em;
                    padding-right: 0.9375em;
                    color: #607081;
                    position: relative;
                    padding-top: 0.85714286em;
                    padding-bottom: 0.85714286em;
                    margin-bottom: 1.14285714em;
                    -moz-transition: all 100ms linear;
                    -o-transition: all 100ms linear;
                    /* -webkit-transition: all 100ms linear; */
                    /* transition: all 100ms linear; */
                    /* background-color: #fff; */
                    border: 2px solid #e6ecf0;
                    /* border-radius: 5px 5px 0 0; */
                }

                .box-title {
                    color: rgba(52,73,94,0.8);
                }

                row .row {
                    max-width: none;
                    width: auto;
                }

                .text-center {
                    text-align: center !important;
                }

                .row-slat {
                    background-color: #F4F7F7;
                    padding-top: 0.85714286em;
                    padding-bottom: 0.85714286em;
                    margin-top: 0.85714286em;
                    margin-bottom: 0.85714286em;
                }

                .push-sbit {
                    margin-bottom: 5px !important;
                }

                .block {
                    display: block !important;
                }

                .trailer--s {
                    margin-bottom: 0.57142857em;
                }

                .elltext {
                    white-space: nowrap;
                    overflow: hidden;
                    text-overflow: ellipsis;
                }

                .disabled {
                    color: #9fa9b3;
                }



                .cards-grid .card-typical {
                    margin: 0 15px 30px;
                }

                .card-typical .card-typical-section:first-child {
                    border-top: none;
                }

                .user-card-row {
                    display: table;
                    width: 100%;
                    border-collapse: collapse;
                    font-size: .8125rem;
                }

                .tbl-row {
                    display: table-row;
                }

                .inline {
                    display: inline;
                }

                .user-card-row .tbl-cell.tbl-cell-photo {
                    width: 42px;
                    padding-right: 10px;
                }

                .price-card {
                    width: 100%;
                    max-width: 225px;
                    text-align: center;
                    display: inline-block;
                    vertical-align: bottom;
                    margin: 20px 7px 0;
                }


                .price-card .price-card-header {
                    border: solid 1px #d8e2e7;
                    -webkit-border-radius: .25rem .25rem 0 0;
                    border-radius: .25rem .25rem 0 0;
                    background: #000;
                    padding: 15px;
                    font-size: 1.25rem;
                    color: #fff;
                }


                .price-card .price-card-body {
                    background: #fff;
                    border: solid 1px #d8e2e7;
                    border-top: none;
                    -webkit-border-radius: 0 0 .25rem .25rem;
                    border-radius: 0 0 .25rem .25rem;
                    padding: 25px 0 25px 0;
                    height: 100px;
                }
                
                .fontClass
                {
                    font-size: 13px;
                }


.medium-3 {
    width: 25%;
}
.column, .columns {
    padding-left: 0.9375em;
    padding-right: 0.9375em;
    width: 100%;
    float: left;
    position: relative;
}
      .ucard-graph-state {
    padding: 1em 0.75em;
    background-color: #fff;
    border-radius: 6px;
    margin-bottom: 0.42857143em;
    margin-top: 0.42857143em;
}


                    #fupload {
                        opacity: 0;
                        position: absolute;
                        z-index: -1;
                    }
                .regStatus{
                    display:none;
                }
                .logo{float:right;padding-top: 15px;
    padding-bottom: 15px;    }
                .logo a{color: #777;}


                /*Media queries*/

                @media(max-width:1200px){
                    .ul-help{
                    float: none !important;display:inline-block !important;margin:10px 0
                }
                .ul-help a {
                    padding:15px 15px !important;
                }
                }

                 @media(max-width:946px){
                      .ul-help{
                    float: right !important;
                }
                 .FilterNav-list{padding-left: 100px;}  
                }
                  @media(max-width:897px){
                      .ul-help{
                    float: left !important;
                }
               
                }

                    @media(max-width:767px){
                      .ul-help{
                    display: block !important;
    text-align: center;
     float: none !important;
                }
               
                }


                </style>
                </head>
                <body>
                    <nav class="navbar navbar-default navbar-fixed-top" style="    margin-bottom: 0;
    padding: 10px 0;">
                        <div class="container">
                            <a class="navbar-brand" href="#home1"><img src="public/images/surveymoney_logo.jpg" width="100%" style="width: 90px;position:absolute;top: 7px;" /></a>
                           


                        </div>
                    </nav>
                    <div class="col-sm-12" align="center" style="margin-top: 70px;background: gold;padding: 40px;position: relative;min-height: 150px;">

                        <div class="col-sm-3" style="
                             margin-left: 15px;
                             ">
                         

             


                        </div>

                        <?php //echo $fbID;   ?>

                    </div>
                    <div class="col-sm-12" style="padding: 0px 0;
                         background: #000;
                         color: #fff;">
                        <div class="filternavcontainer">
                            <div class="col-sm-9 col-sm-offset-3" style="padding-left:40px">
                                <nav class="navbar navbar-default">
                                    <div class="">
                                        <div class="navbar-header">
                                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navfilter">
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                            </button>
                                            <!--<a class="navbar-brand" href="#home1">Surveymoney Logo</a>-->
                                        </div>
                                        <div class="collapse navbar-collapse" id="navfilter">
                                            <ul class="FilterNav-list nav navbar-nav">
                                                <!--<li class="FilterNav-item is-active notdone " /*onclick="GoTo('notdone')"*/>
                                                    <a class="FilterNav-stat" style="text-decoration: none;" href="javascript:void">
                                                        <span class="FilterNav-label" aria-hidden="true">Special Prizes Available</span>
                                                        <span class="FilterNav-value headNote myAvailable">{{@$available}}</span>
                                                    </a>
                                                </li>

                                                <li class="FilterNav-item done" onclick="GoTo('done')" >
                                                    <a class="FilterNav-stat" style="text-decoration: none;" href="javascript:void">
                                                        <span class="FilterNav-label" aria-hidden="true">Previous Prizes</span>
                                                        <span class="FilterNav-value headNote myAnswered">{{@$answered}}</span>
                                                    </a>
                                                </li>-->


                                                <li class="ul-help" /*onclick="goToWin();"*/ >
                                                    <a target="_blank" href="/HowDoIWin">
                                                     GENERAL RULES
                                                    </a>
                                                </li>

                                            </ul>

                                        </div>
                                    </div>
                                </nav>





                            </div>
                        </div>
                    </div>
                    
                    <div class="howtowinInfo" style="display: none; padding: 0px 30px 0px 30px;">
                    
                    
                  <div align="center">
                    <h3><p>
                    <b style="line-height: 100px; color: goldenrod;font-family: cursive;">HOW TO WIN </b>
                    </p></h3>
                    
                    <div alignn="center" style="text-align: justify; color: goldenrod; font-size: 20px;font-family: cursive;padding: 0px 40px 0px 40px;">
                    As a participant, simply log on to the Kudipoll website (www.kudipoll.ng) to register. Once you have registered, you can opt to participate in any of the prizes available and you stand a chance to win the prizes stated therein. Please ensure you carefully review the terms and conditions of the prizes, as well as our general rules and privacy policy.<br><br>
Good Luck!

                    </div>
                    </div>
                    
                    
                    </div>
                    <div class="wholebody">
                    <div class="col-sm-12" style="
                         background: #E9F0F6;
                         ">

                        <div class="col-sm-3 leftSyd" align="center">

                            <br><br>
 <div class="col-sm-3 belowLeftSyd leftSyd" align="center">
                                  
                             

                                    </div>
</div>

   <div class="howtowinInfo" style="padding: 0px 30px 0px 30px;">
                    
                    
                            <div class="col-lg-12 pagePart genRules" >
                                                                 <div align="center">
                    <h3><p>
                    <b style="line-height: 100px;">GENERAL RULES </b>
                    </p></h3>
                    
                    <div alignn="center" style="text-align: justify; font-size: 20px;padding: 0px 40px 0px 40px;">
                   Rules Pertaining to All Prizes:<BR><BR>
•	The winner will be notified via telephone or email by Kudipoll.<BR><BR>
•	No individual involved with the design or management of these prizes or companies or their staff members or their immediate family; nor members of the sponsors of any prize listed on this page, and/or any of their affiliated companies and/or agencies, and members of their immediate family, may enter the competition.<BR><BR>
•	No persons under 18 years of age may enter unless the prize specifically allows this and is stated in the rules of that prize.<BR><BR>
•	The prize must be claimed within 14 days after the end of the competition. Failure to do so equals forfeiture of the prize.  After which the prize would be awarded to another participant.<BR><BR>
•	In order to claim the prize, the winner will need to visit our office and is mandated to confirm receipt of the prize by posting on his/her social media page(s).
In the event that a winner cannot be physically present, he/she must then submit a photograph of themselves which will be posted together with their name on this website. Such winner must also confirm receipt of the prize by posting on his/her social media page(s).<BR><BR>
•	The winners agree to co-operate in participating in any public relations exposure.<BR><BR>
•	The winners’ details may be published at the discretion of the sponsor and Kudipoll.<BR><BR>
•	Any violation or attempt to violate any of the above rules will result in the immediate disqualification of the transgressor.<BR><BR>
•	The prize is not transferable and non-refundable. None of the prize components may be exchanged for cash in cases where the prize is not a cash prize.<BR><BR>
•	Kudipoll is not responsible for:<BR><BR>
•	Lost, late, delayed, misdirected, unintelligible or incomplete electronic mail.<BR><BR>
•	Telephone, electronic, hardware or software program, network, Internet or computer malfunctions, failures, or difficulties.<BR><BR>
•	Errors in transmission, any condition caused by events beyond the control of Kudipoll which may cause the promotion to be disrupted or corrupted.<BR><BR>
•	Kudipoll will not be held responsible for entries not received in the event of participants not successfully completing the entering mechanism supplied.<BR><BR>
•	By participating in this promotion, all entrants agree to these official rules, and understand that the prize will be forfeited if the winner is not able to claim the prize in the specified time period.<BR><BR>
•	Entries submitted using invalid or false email addresses will be disqualified.<BR><BR>
•	By participating in this promotion, all entrants agree to receive marketing communications from Kudipoll and their partners.<BR><BR>
•	Winners will be randomly selected via a draw that is to take place within 10 days of the closing date of the competition.<BR><BR>
•	Kudipoll reserves the right to terminate the competition at any stage.<BR><BR>
•	Kudipoll will not be liable for taxes or any other cost incurred by the participants in claiming any reward.<BR><BR>
•	The decision of Kudipoll in regard to all aspects of the promotion is final and no correspondence will be entered into.<BR><BR>
•	Kudipoll will assume no liability for any direct or indirect damage occurring as a result of participation in the promotion or a result of the use of any of the rewards.<BR><BR>
•	Kudipoll shall have the right to terminate the promotion without prior notice and without cost or liability of whatsoever nature. In such event the participants will waive all rights they may have had and acknowledge that they will have no right of recourse to the sponsor.<BR><BR>
•	Refer a Friend: If you provide us with information about another person, you confirm that they have appointed you to act for them, to consent to the processing of their personal data including sensitive personal data and that you have informed them of our identity and the purpose for which their personal data will be processed.<BR><BR>


                    </div>
                    </div></div>          
     
                                 
                                                                <div>&emsp;</div>
                                                                <?php
//                                                                $tope = 0;
//                                                                if ($count > 2) {
//                                                                    echo '<div class=" myloadmore" align="center"><br><a href="#" class="loadMore load-more button button-secondary black small" onclick="loadMore(); id="next-page-uid_30_5942bc1df4144" data-block_id="uid_30_5942bc1df4144">Load more<i class="icon icon-menu-down"></i></a></div>';
//                                                                }
                                                                ?>
                                                                </div>

                     <div id="myModal1" class="modal fade" role="dialog" align="center">
                                                                    <div class="modal-dialog">

                                                                        <!-- Modal content-->
                                                                        <div class="modal-content" style="width:75%;">
                                                                            <div class="modal-header" style="background-color: #D0D8DB">
                                                                                <a href="/succesful" class="close" data-dismiss="modal">&times;</a>
                                                                                <h4 class="modal-title">Please Complete Your Registeration</h4>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <div align = "center" id="updateRegError" style="color: red;"></div>
                                                  <!--                                                                    <p>Some text in the modal.</p>-->
                                                                                <form class="form-horizontal" role="form" method="POST" id="updateFbUserForm" style="padding: 0px 20px 20px 20px;">
                                                                                    <br>
                                                                                        <div class="column one-second form-group @if ($errors->has('Phone_Number')) has-error @endif">
                                                                                            <input class="form-control" placeholder="Your phone-number (0X000000000)" type="text" name="Phone_Number" id="phone" size="40" aria-required="true" aria-invalid="false" value="{{ Input::old('Phone_Number') }}"/>
                                                                                            @if ($errors->has('Phone_Number')) <p class="help-block">{{ $errors->first('Phone_Number') }}</p> @endif
                                                                                        </div>
                                                                                        <div class="column one-second form-group @if ($errors->has('Password')) has-error @endif">
                                                                                            <input class="form-control"  placeholder="Your Password" type="password" name="Password" id="subject" size="40" aria-invalid="false" value="{{ Input::old('Password') }}"/>
                                                                                            @if ($errors->has('Password')) <p class="help-block">{{ $errors->first('Password') }}</p> @endif
                                                                                        </div>

                                                                                        <div class="column one-second form-group @if ($errors->has('Confirm_Password')) has-error @endif">
                                                                                            <input class="form-control"  placeholder="Confirm Password" type="password" name="Confirm_Password" id="subject" size="40" aria-invalid="false" value="{{ Input::old('Confirm_Password') }}"/>
                                                                                            @if ($errors->has('Confirm_Password')) <p class="help-block">{{ $errors->first('Confirm_Password') }}</p> @endif
                                                                                        </div>
                                                                                        <div class="column one-second form-group @if ($errors->has('Age_Range')) has-error @endif">
                                                                                            <select class="form-control"  name="Age_Range" aria-required="true" aria-invalid="false" value="{{ Input::old('Age_Range') }}"><option value="">Your Age Range</option><option value="16-25" @if (old('Age_Range') == '16-25') selected="selected" @endif>16-25</option><option value="26-35" @if (old('Age_Range') == '26-35') selected="selected" @endif>26-35</option><option value="36-45" @if (old('Age_Range') == '36-35') selected="selected" @endif>36-45</option><option value="46-above" @if (old('Age_Range') == '46-above') selected="selected" @endif>46-above</option></select>
                                                                                            @if ($errors->has('Age_Range')) <p class="help-block">{{ $errors->first('Age_Range') }}</p> @endif
                                                                                        </div>
                                                                                        <div class="column one-second form-group @if ($errors->has('State')) has-error @endif">
                                                                                            <select class="form-control stateDropDown" onchange="getCities();" name="State" id="State" aria-required="true" aria-invalid="false" value="{{ Input::old('State') }}"><?php echo @$allStates; ?></select>

                                                                                            @if ($errors->has('State')) <p class="help-block">{{ $errors->first('State') }}</p> @endif
                                                                                        </div>
                                                                                        <div class="column one-second form-group @if ($errors->has('City')) has-error @endif">
<select class="form-control" id="City" name="City" aria-required="true" aria-invalid="false" value="{{ Input::old('City') }}"><option value=''>Your City</option></select>
                                                                                          <!--  <input class="form-control"  placeholder="Your City" type="text" name="City" id="City" size="40" aria-required="true" aria-invalid="false" value="{{ Input::old('City') }}"/>-->
                                                                                            @if ($errors->has('City')) <p class="help-block">{{ $errors->first('City') }}</p> @endif
                                                                                        </div>
                                                                                        <div class="column one-second form-group @if ($errors->has('ReferrerCode')) has-error @endif">
                                                                                            <input class="form-control"  placeholder="Referrer Code (Optional)" type="text" name="ReferrerCode" id="email" size="40" aria-required="true" aria-invalid="false" value="{{ Input::old('ReferrerCode') }}"/>
                                                                                            @if ($errors->has('ReferrerCode')) <p class="help-block">{{ $errors->first('ReferrerCode') }}</p> @endif
                                                                                        </div>
                                                                                        <button type="button" onclick="updateUserReg();" id="updateButton" class="btn btn-warning">Update</button>

                                                                                </form>
                                                                            </div>
                                                                            <!--                                                                <div class="modal-footer">
                                                                                                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                                                                                            </div>-->
                                                                        </div>

                                                                    </div>
                                                                </div>
                    </div>                
                                                               <div class="putLeftSideHere"></div>

															   <div class="modal1"><!-- Place at bottom of page --></div>
                                                                <script src="{{ asset('sweetalert-master/dist/sweetalert.min.js')}}"></script>
                                                             <!--   <script src="{{ asset('jquery-1.12.4.js')}}"></script>  -->
                                                                <script src="{{ asset('ion/ion.rangeSlider-2.1.7/js/vendor/jquery-1.12.3.min.js')}}" type="text/javascript"></script>                                              
                                                                <script src="{{ asset('bootstrap.min.js')}}"></script>
                                                                <script src="{{ asset('ion/ion.rangeSlider-2.1.7/js/ion-rangeSlider/ion.rangeSlider.min.js')}}" type="text/javascript"></script>
                                                                <script src="{{ asset('js/chosen.jquery.min.js')}}"></script>
                                                                <script src="{{ asset('js/MyLaravelJs.js')}}" type="text/javascript"></script>
                                                                <script src="{{ asset('js/rrssb.min.js')}}"></script>

                                                                <script>
                                                                                $('.popoverData').tooltip();
                                                                                $('.popoverOption').tooltip({trigger: "hover"});
                                                                </script>
<!--                                                                <script>

                                                                                $(function () {

                                                                                    $(".rangeSelect").ionRangeSlider({

                                                                                        min: 0,
                                                                                        max: 400,
                                                                                        from: 100,
                                                                                        to: 400,
                                                                                        type: 'double',
                                                                                        step: 1,
                                                                                        prefix: "$",
                                                                                        grid: true
                                                                                    });

                                                                                });
                                                                </script>-->
                                                                </body>
                                                                </html>