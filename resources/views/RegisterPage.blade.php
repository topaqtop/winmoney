<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="<?php echo csrf_token(); ?>" />
        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>.regBtn
            {
                cursor: pointer;
                width: 65%;
                height: 35px;
                background-color: aliceblue;
                border-radius: 25px;
                color: darkcyan;
                font-size: large;
            }
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref ">
            @if (Route::has('login'))
            <div class="top-right links">
                @if (Auth::check())
                <a href="{{ url('/home') }}">Home</a>
                @else
                <a href="{{ url('/login') }}">Login</a>
                <a href="{{ url('/register') }}">Register</a>
                @endif
            </div>
            @endif

            <div class="content">



                <div style="font-size: 30px; width: 600px; border-style: solid; border-width: 1px; padding: 10px; background-color: mediumaquamarine; color: white;" alidn="center">

                    <br>
                    @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                    @else
                    <div>
                        Registeeeer
                    </div> 
                    @endif
                    <form action="/user" method="post">
                        <br><br>
                        <div align="left" class="form-group @if ($errors->has('username')) has-error @endif">
                            <div>Name</div>
                            <div><input type="text" name="username" style="width:100%; height: 30px; font-size: 30px;" value="{{ Input::old('username') }}"/></div>
                            @if ($errors->has('username')) <p class="help-block">{{ $errors->first('username') }}</p> @endif
                        </div>

                        <br>
                        <!--                        <div align="left">
                                                    <div>Phone Number</div>
                                                    <div><input type="text" name="userfone" style="width:100%; height: 30px; font-size: 30px;"/></div>
                                                </div>
                                                <br>-->
                        <div align="left" class="form-group @if ($errors->has('useremail')) has-error @endif">
                            <div>Email</div>
                            <div><input type="text" name="useremail" style="width:100%; height: 30px; font-size: 30px;" value="{{ Input::old('useremail') }}"/></div>
                            @if ($errors->has('useremail')) <p class="help-block">{{ $errors->first('useremail') }}</p> @endif
                        </div>

                        <br>
                        <div align="left" class="form-group @if ($errors->has('usergender')) has-error @endif">
                            <div>Gender</div>
                            <div><select name="usergender" style="width:100%; height: 30px; font-size: 30px;" value="{{ Input::old('usergender') }}"><option value="">-</option><option value="Male">Male</option><option value="Female">Female</option></select></div>
                        @if ($errors->has('usergender')) <p class="help-block">{{ $errors->first('usergender') }}</p> @endif
                        </div>
                        <br>
                        <div align="left" class="form-group @if ($errors->has('phonenumber')) has-error @endif">
                            <div>Phone Number</div>
                            <div><input type="text" name="phonenumber" style="width:100%; height: 30px; font-size: 30px;"value="{{ Input::old('phonenumber') }}"/></div>
                            @if ($errors->has('phonenumber')) <p class="help-block">{{ $errors->first('phonenumber') }}</p> @endif
                        </div>
                        <br>
                        <div align="left" class="form-group @if ($errors->has('password1')) has-error @endif">
                            <div>Password</div>
                            <div><input type="password" name="password1" style="width:100%; height: 30px; font-size: 30px;"value="{{ Input::old('password1') }}"/></div>
                            @if ($errors->has('password1')) <p class="help-block">{{ $errors->first('password1') }}</p> @endif
                        </div>
                        <br>
                        <div align="left" class="form-group @if ($errors->has('password2')) has-error @endif">
                            <div>Retype Password</div>
                            <div><input type="password" name="password2" style="width:100%; height: 30px; font-size: 30px;"value="{{ Input::old('password2') }}"/></div>
                            @if ($errors->has('password2')) <p class="help-block">{{ $errors->first('password2') }}</p> @endif
                        </div>

                        <br>
                        <!--                        <div class="g-recaptcha" data-sitekey="6Lf6wyAUAAAAAK7ysXMUM1lXqNc8IcYBcUHyPlib"></div>-->
                        <div align="left" class="form-group @if ($errors->has('g-recaptcha-response')) has-error @endif">
                            {!! app('captcha')->display(); !!}
                             @if ($errors->has('g-recaptcha-response')) <p class="help-block">{{ $errors->first('g-recaptcha-response') }}</p> @endif
                        </div>

                        <!-- <div align="left">
                             <div>Age Bracket</div>
                             <div><input type="checkbox" name="userage" value="14-25">14-25 &emsp;<input type="checkbox" name="userage" value="26-40">26-40 &emsp;<input type="checkbox" name="userage" value="41-60">41-60 &emsp;<input type="checkbox" name="userage" value="Above 60">Above 60 &emsp;</div>
                         </div>-->
                        <br>
                        <button type="submit" class="regBtn">Register</button>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </form>
                </div>
                OR
                <div><a href="http://localhost:8000/auth/facebook">Register with facebook</a></div>
                <!--                <div class="links">
                                    <a href="https://laravel.com/docs">Documentation</a>
                                    <a href="https://laracasts.com">Laracasts</a>
                                    <a href="https://laravel-news.com">News</a>
                                    <a href="https://forge.laravel.com">Forge</a>
                                    <a href="https://github.com/laravel/laravel">GitHub</a>
                                </div>-->
            </div>
        </div>
        <script src='https://www.google.com/recaptcha/api.js'></script>
    </body>
</html>
