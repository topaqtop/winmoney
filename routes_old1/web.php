<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['web']], function(){
    
    Route::get('/', function () {
   // return view('welcome');
     return Redirect::to('');   
});

Route::get('auth/facebook', 'Auth\RegisterController@redirectToProvider');
Route::get('auth/facebook/callback', 'Auth\RegisterController@handleProviderCallback');

Route::get('about', function(){
    $bitfumes = ['This', 'Is', 'Bitfumes'];
    return view('about', ['bitfumes' => $bitfumes]);
});

Route::get('UserSignUp', 'ClientController@ClientPage');
Route::get('Register', 'ClientController@RegisterPage');
Route::post('user', 'ClientController@RegisterUser');

Route::get('newmemberReg', 'ClientController@newmemberReg');
//Route::get('facebookGet', 'ClientController@fbPermission');
Route::get('facebookGet', 'ClientController@fbPermission');

Route::post('userComplete', 'ClientController@userComplete');

Route::get('successful', 'ClientController@nowLoggedIn');

Route::get('completeReg', 'ClientController@CompleteRegPage');

Route::get('updateuser', 'ClientController@UpdateUserInfo');

Route::get('', 'ClientController@WinMoneyWebsite');

Route::post('SignIn', 'ClientController@userSignin');

Route::get('logout', 'ClientController@userSignout');

Route::get('previousCampaign', 'ClientController@prevCamp');

Route::get('saveAnswer', 'ClientController@saveAnswer');

Route::get('alreadyAnswered', 'ClientController@alreadyAnswered');

Route::get('getCampaign', 'ClientController@getCampaign');

Route::get('getMore', 'ClientController@getMore');

Route::post('uploadDP', 'ClientController@uploadDP');

Route::post('updateCampaignImg', 'ClientController@updateCampaignImg');

Route::post('uploadCampaignImage', 'ClientController@uploadCampaignImage');

Route::get('upDateDBColumn', 'ClientController@upDateDBColumn');

Route::get('checkRegComplete', 'ClientController@checkRegComplete');

Route::get('confirmation/{confirmcode}', 'ClientController@confirmpassword');

Route::get('confirmAccount/{confirmcode}', 'ClientController@confirmAccount');

Route::get('completeRegistration', 'ClientController@completeRegistration');

Route::get('forgotPassword', 'ClientController@forgotPassword');

Route::get('createCampaign', 'ClientController@createCampaign');

Route::get('saveQuestion', 'ClientController@saveQuestion');

Route::get('newCampaign', 'ClientController@newCampaign');

Route::get('AdminPage', 'ClientController@AdminPage');

Route::get('adminLogin', 'ClientController@adminLogin');

Route::get('CampaignCreate', 'ClientController@CampaignCreate');

Route::get('AddQuestion', 'ClientController@AddQuestion');

Route::get('Report', 'ClientController@Report');

Route::post('adminAuth', 'ClientController@adminAuth');

Route::post('clientAuth', 'ClientController@clientAuth');

Route::get('CampaignList', 'ClientController@CampaignList');

Route::get('getCamp', 'ClientController@getCamp');

Route::get('getallquest', 'ClientController@getallquest');

Route::get('delCamp', 'ClientController@delCamp');

Route::get('updateCamp', 'ClientController@updateCamp');

Route::get('addNewClient', 'ClientController@addNewClient');

Route::get('CreateClient', 'ClientController@CreateClient');

Route::get('ViewClient', 'ClientController@ViewClient');

Route::get('FilterCampaign', 'ClientController@FilterCampaign');

Route::get('getFilterQuestions', 'ClientController@getFilterQuestions');

Route::get('getQuestOpt', 'ClientController@getQuestOpt');

Route::get('filterAnswers', 'ClientController@filterAnswers');

Route::get('filterRangeAnswers', 'ClientController@filterRangeAnswers');

Route::get('filterSelectAnswers', 'ClientController@filterSelectAnswers');

Route::get('filterAll', 'ClientController@filterAll');

Route::get('adminLogout', 'ClientController@adminLogout');

Route::get('clientLogout', 'ClientController@clientLogout');

Route::get('suspendCapaign', 'ClientController@suspendCapaign');

Route::get('editClient', 'ClientController@editClient');

Route::get('suspendClient', 'ClientController@suspendClient');

Route::get('delClient', 'ClientController@delClient');

Route::get('viewusers', 'ClientController@viewusers');

Route::get('delUser', 'ClientController@delUser');

Route::get('suspendUser', 'ClientController@suspendUser');

Route::get('suspendLead', 'ClientController@suspendLead');

Route::get('HowToWin', 'ClientController@HowToWin');

Route::get('homeOption', 'ClientController@homeOption');

Route::get('HowDoIWin', 'ClientController@HowDoIWin');

Route::get('PrivacyPolicy', 'ClientController@PrivacyPolicy');

Route::get('TermsAndCondition', 'ClientController@TermsAndCondition');

Route::get('GeneralRules', 'ClientController@GeneralRules');

Route::get('clientLogin', 'ClientController@clientLogin');

Route::get('clientPage', 'ClientController@clientPage');

Route::get('ViewLeads', 'ClientController@ViewLeads');

Route::get('getParticipant', 'ClientController@getParticipant');

Route::get('getAllClient', 'ClientController@getAllClient');

Route::get('WinMoney_PrivacyPolicy', 'ClientController@WinMoney_PrivacyPolicy');

Route::get('Winmoney_GeneralRules', 'ClientController@Winmoney_GeneralRules');

Route::get('Winmoney_HowToWin', 'ClientController@Winmoney_HowToWin');

Route::get('Winmoney_TermsAndCondition', 'ClientController@Winmoney_TermsAndCondition');

Route::get('sendMail', 'ClientController@sendMail');

Route::get('showMiniRep', 'ClientController@showMiniRep');

Route::get('filterAndAnswers', 'ClientController@filterAndAnswers');

Route::resource('myCRUD', 'resourceController');


Auth::routes();

Route::get('/home', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index');

});
