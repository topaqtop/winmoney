/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//this function resets form to empty
$(".reset").click(function() {
    $(this).closest('form').find("input[type=password], input[type=text], textarea, select, input[type=file], input[type=number], input[type=date]").val("");
});


//this function show loading icon and fades background on a div with class 'portlet'
function showDivLoading()
{

//var getInitial = $('.portlet').html();
$('.portlet').prepend('<div align="center" class="divLoading" style="width: 100%;height: 100%;background: gray!important;opacity: 0.8;position: absolute!important;z-index: 30!important;font-size: 20px;color: white;line-height: 20;"><img src="public/images/adminLoading.gif" width="" style="width: 81px;">Loading...</div>');

}

//this function removes loading icon and background fade
function hideDivLoading()
{

$('.divLoading').remove();

}

//this function replaces fa icons with loading icon 
function showLoadingIcon(initialIcon, id)
{

    $('.'+initialIcon+'_'+id).removeClass(initialIcon);
    $('.'+initialIcon+'_'+id).addClass('fa-spinner fa-spin');

}


//this function replaces fa-loading icon with the initial icon prior to loading
function removeLoadingIcon(initialIcon, id)
{

    $('.'+initialIcon+'_'+id).removeClass('fa-spinner fa-spin');
    $('.'+initialIcon+'_'+id).addClass(initialIcon);

}


function reward_user( event ) {
    if ( event ) {
        // do something
        alert( 'Tweeted' );
    }
}


//this is my function to handle onload events
$(function(d, s, id) {


//set boxes to show
localStorage.setItem('BoxesFrom', 1);
localStorage.setItem('BoxesTo', 21);
var totBoxes = parseInt(localStorage.getItem('totalBoxes'));
var totalBoxLength = new Array(totBoxes);
//set boxes to show
$('.campBoxes').hide();
var boxMax = parseInt(localStorage.getItem('BoxesTo'));
var boxcounter = parseInt(localStorage.getItem('BoxesFrom'));
$.each(totalBoxLength, function(count, value){
if(count+1 >= parseInt(localStorage.getItem('BoxesFrom')) && count+1 <= parseInt(localStorage.getItem('BoxesTo'))){
$('.boxCount_'+(count+1)).show();
}
});
$('.boxPage').show();


//for(var i = 1; i<=2; i++){
//$('.boxCount_'+i).show();
//}

//side bars for admin panel
var sideBarContent =  '<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->'+
                            '<!-- BEGIN SIDEBAR TOGGLER BUTTON -->'+
                            '<li class="sidebar-toggler-wrapper hide">'+
                                '<div class="sidebar-toggler">'+
                                   ' <span></span>'+
                                '</div>'+
                            '</li>'+
                            '<!-- END SIDEBAR TOGGLER BUTTON -->'+
                            '<!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->'+
                            '<li class="sidebar-search-wrapper">'+
                                '<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->'+
                                '<!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->'+
                                '<!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->'+
                                '<form class="sidebar-search  " action="page_general_search_3.html" method="POST">'+
                                   ' <a href="javascript:;" class="remove">'+
                                        '<i class="icon-close"></i>'+
                                    '</a>'+
                                    '<div class="input-group">'+
                                       
                                    '</div>'+
                                '</form>'+
                               ' <!-- END RESPONSIVE QUICK SEARCH FORM -->'+
                            '</li>'+
                            '<li class="nav-item start active open">'+
                                '<a href="/AdminPage" class="nav-link nav-toggle">'+
                                    '<i class="icon-home"></i>'+
                                    '<span class="title">Dashboard</span>'+
                                   ' <span class="selected"></span>'+
                                 ' <!--  <span class="arrow open"></span> -->'+
                                '</a>'+
                               
                            '</li>'+
                           ' <li class="heading">'+
                               ' <h3 class="uppercase"></h3>'+
                            '</li>'+

                             '<li class="nav-item">'+
                             ' <a class="nav-link nav-toggle" onclick="openSubTopic(1)">'+
                                   ' <i class="icon-diamond"></i>'+
                                    '<span class="title">Campaigns</span>'+
                                    '<span class="arrow"></span>'+
                               ' </a>'+
                               '<ul style="display: none;" class="subTopic1">'+
                                ' <li class="nav-item" style="margin-bottom: 16px;margin-top: 8px;">'+
                               ' <a href="/CampaignList" class="nav-link nav-toggle" style="color: #b4bcc8;text-decoration: none;">'+
                                    '<!--<i class="icon-diamond"></i>-->'+
                                    '<span class="title">View Campaigns</span>'+
                                    '<span class="arrow"></span>'+
                               ' </a>'+
                                 '</li>'+
                                 '<li class="nav-item" style="margin-bottom: 16px;margin-top: 8px;">'+
                                '<a href="/AddCampaign" class="nav-link nav-toggle" style="color: #b4bcc8;text-decoration: none;">'+
                                    '<!--<i class="icon-diamond"></i>-->'+
                                   ' <span class="title">Create Campaign</span>'+
                                    '<span class="arrow"></span>'+
                                '</a>'+
                                 '</li>'+
                                 '</li>'+
                               ' <li class="nav-item" style="margin-bottom: 16px;margin-top: 8px;">'+
                                '<a href="/FilterCampaign" class="nav-link nav-toggle" style="color: #b4bcc8;text-decoration: none;">'+
                                    '<!--<i class="icon-diamond"></i>-->'+
                                   ' <span class="title">Filter Campaigns</span>'+
                                   ' <span class="arrow"></span>'+
                                '</a>'+
                                ' </li>'+
                                ' <li class="nav-item" style="margin-bottom: 16px;margin-top: 8px;">'+
                                '<a href="/QuestionManager" class="nav-link nav-toggle" style="color: #b4bcc8;text-decoration: none;">'+
                                   '<!--<i class="icon-diamond"></i>-->'+
                                   ' <span class="title">Question Manager</span>'+
                                  '  <span class="arrow"></span>'+
                                '</a>'+
                                ' </li>'+
                               ' </ul>'+

                            '</li>'+
                           
                            '<li class="nav-item  ">'+
                                '<a class="nav-link nav-toggle" onclick="openSubTopic(2)">'+
                                    '<i class="icon-diamond"></i>'+
                                    '<span class="title">Clients</span>'+
                                    '<span class="arrow"></span>'+
                                '</a>'+
                                  '<ul style="display: none;" class="subTopic2">'+
                                ' <li class="nav-item" style="margin-bottom: 16px;margin-top: 8px;">'+
                               ' <a href="/ViewClient" class="nav-link nav-toggle" style="color: #b4bcc8;text-decoration: none;">'+
                                    '<!--<i class="icon-diamond"></i>-->'+
                                    '<span class="title">View Client</span>'+
                                    '<span class="arrow"></span>'+
                               ' </a>'+
                                 '</li>'+
                                   ' <li class="nav-item" style="margin-bottom: 16px;margin-top: 8px;">'+
                               ' <a href="/CreateClient" class="nav-link nav-toggle" style="color: #b4bcc8;text-decoration: none;">'+
                                    '<!--<i class="icon-diamond"></i>-->'+
                                    '<span class="title">Create Client</span>'+
                                    '<span class="arrow"></span>'+
                               ' </a>'+
                                 '</li>'+
                                   '</ul>'+
                            '</li>'+
                           '<li class="nav-item  ">'+
                               ' <a href="/viewusers" class="nav-link nav-toggle">'+
                                   ' <i class="icon-diamond"></i>'+
                                   ' <span class="title">Users</span>'+
                                   ' <!--<span class="arrow"></span>-->'+
                                '</a>'+
                            '</li>'+
                            '<li class="nav-item  ">'+
                                '<a href="/cashRequest" class="nav-link nav-toggle">'+
                                    '<i class="icon-diamond"></i>'+
                                    '<span class="title">Cash Requests</span>'+
                                   '<!-- <span class="arrow"></span>-->'+
                               ' </a>'+
                            '</li>'+
                           
                            '<li class="nav-item  ">'+
                                '<a class="nav-link nav-toggle" onclick="openSubTopic(5)">'+
                                    '<i class="icon-diamond"></i>'+
                                    '<span class="title">Rewards</span>'+
                                    '<span class="arrow"></span>'+
                                '</a>'+
                                  '<ul style="display: none;" class="subTopic5">'+
                                ' <li class="nav-item" style="margin-bottom: 16px;margin-top: 8px;">'+
                               ' <a href="/SpecialPrizes" class="nav-link nav-toggle" style="color: #b4bcc8;text-decoration: none;">'+
                                    '<!--<i class="icon-diamond"></i>-->'+
                                    '<span class="title">View Rewards</span>'+
                                    '<span class="arrow"></span>'+
                               ' </a>'+
                                 '</li>'+
                                   ' <li class="nav-item" style="margin-bottom: 16px;margin-top: 8px;">'+
                               ' <a href="/CreateReward" class="nav-link nav-toggle" style="color: #b4bcc8;text-decoration: none;">'+
                                   '<!--<i class="icon-diamond"></i>-->'+
                                    '<span class="title">Create Reward</span>'+
                                    '<span class="arrow"></span>'+
                               ' </a>'+
                                 '</li>'+
                                   '</ul>'+
                            '</li>';
                        $('.menuBar').html(sideBarContent);

//this is the facebook share code
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id))
        return;
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/en_US/all.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
function showModal()
{
    $('#myModal1').modal('show');
}




$("#phone").keypress(function (e) {
    //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $("#updateRegError").html("Digits Only");
        return false;
    }
    else
    {
        $("#updateRegError").html("");
              

    }
});





function forgotPassword()
{

}


function checkImage(formId)
{

}





function openSubTopic(num){

$('.subTopic'+num).slideToggle('slow');

}

$('.clientUpdateInfo').click(function(){
showDivLoading();
var getCompanyAdd = $('.CompanyAddress').val();
var getCompanyName = $('.CompanyName').val();
$('.errorMsg').remove();
var error = 0;
if(getCompanyAdd == "")
{
error = 1;
hideDivLoading();
$('.CompanyAddress').after('<div class="errorMsg" style="color: red;">Company Address Is Required</div>');
}
if(getCompanyName == "")
{
error = 1;
hideDivLoading();
$('.CompanyName').after('<div class="errorMsg" style="color: red;">Company Name Is Required</div>');
}

if(error == 0){
//alert('done');

var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/clientChangeInfo',
        type: 'get',
        data: {'_token': CSRF_TOKEN, 'companyAdd': getCompanyAdd, 'companyName': getCompanyName},
        success: function (data) {
       hideDivLoading();
         swal('Update Successful');
        },
        error: function (data) {
            hideDivLoading();
            swal('Error occured');
        }
    });

}

});


$('.clientChangePassword').click(function(){
showDivLoading();
var getcurrentPass = $('.currentPass').val();
var getnewPass = $('.newPass').val();
var getretypeNewPass = $('.retypeNewPass').val();
$('.errorMsg').remove();
var error = 0;
if(getcurrentPass == "")
{
error = 1;
hideDivLoading();
$('.currentPass').after('<div class="errorMsg" style="color: red;">Current Password Is Required</div>');
}
if(getnewPass == "")
{
error = 1;
hideDivLoading();
$('.newPass').after('<div class="errorMsg" style="color: red;">New Password Is Required</div>');
}
if(getretypeNewPass == "")
{
error = 1;
hideDivLoading();
$('.retypeNewPass').after('<div class="errorMsg" style="color: red;">Retype Password Is Required</div>');
}

if(getnewPass.length < 5 )
{
error = 1;
hideDivLoading();
$('.newPass').after('<div class="errorMsg" style="color: red;">Password Character Must Be Above 4</div>');
}


if(getretypeNewPass != getnewPass )
{
error = 1;
hideDivLoading();
$('.retypeNewPass').after('<div class="errorMsg" style="color: red;">Password Does Not Match</div>');
}

if(error == 0){
//alert('done');
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/clientPassword',
        type: 'get',
        data: {'_token': CSRF_TOKEN, 'currentPass': getcurrentPass, 'newPass': getnewPass},
        success: function (data) {
         if(data == "Incorrect Old Password")
         {
hideDivLoading();
       $('.currentPass').after('<div class="errorMsg" style="color: red;">Incorrect Current Password</div>');

         }
else{
hideDivLoading();
swal('Update Successful');
}
        },
        error: function (data) {
hideDivLoading();
            swal('Error occured');
        }
    });
}

});


function showQuest2(campId, count)
{

    $('.viewQuestBtn_'+count).html('Processing...');
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/getallquest',
        type: 'get',
        data: {'_token': CSRF_TOKEN, campId: campId},
        success: function (data) {

            $('#viewCamp').modal();
            $('.QuestionList').html(data);
            
            //$('.viewQuestBtn2').addClass('fa-eye');
            //$('.viewQuestBtn2').removeClass('fa-spinner fa-spin');
         

            $('.viewQuestBtn_'+count).html('View Questions');

        },
        error: function (data) {
 $('.viewQuestBtn_'+count).html('View Questions');
            swal("Oops! Error Occured");
        }
    });
}


function showParticipants(campId, count)
{

    $('.actionBtn_'+count).html('Processing...');
    $('#participants').modal('show');
    var mytable = $('#sample_4').DataTable();
    mytable.clear();
    mytable.draw();

    $('#particpantList').html("<tr><th colspan='7'>Fetching Data...</th></tr>");
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/getParticipant',
        type: 'get',
        data: {'_token': CSRF_TOKEN, 'campId': campId},
        success: function (data) {

            if(data.length < 1)
            {

                var mytable = $('#sample_4').DataTable();
                mytable.clear();
                mytable.draw();
                $('.actionBtn_'+count).html('View Users');
                return false;

            }

            var rowArrays = [];
            var innerTr = "";
            var mytable = $('#sample_4').DataTable();
            $.each(data, function(count, val){
                rowArrays = [];
                rowArrays.push(val[count].Surname + " " + val[count].Other_Name);
                rowArrays.push(val[count].State + " " + val[count].City);
                rowArrays.push(val[count].Phone_Number);
                rowArrays.push(val[count].Email);
                rowArrays.push(val[count].Gender);
                rowArrays.push(val[count].Age_Range);
                     
                mytable.row.add(rowArrays);

             
            });


            mytable.draw();


            $('.actionBtn_'+count).html('View Users');
        },
        error: function (data) {
            $('.actionBtn_'+count).html('View Users');
            swal("Oops! Error Occured");
        }
    });

}







$('.clientForgotPassword').click(function(){

$('.errorMsg').remove();
var error = 0;
var getEmail = $('#clientEmail').val();
var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (!filter.test(getEmail)) {
error = 1;
$('#clientEmail').after('<div class="errorMsg" style="color: red;">Email Not In Correct Format</div>');
}else
if(getEmail == ""){
error = 1;
$('#clientEmail').after('<div class="errorMsg" style="color: red;">Email field can not be empty</div>');
}

if(error == 0){
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/clientForgotPassword',
        type: 'get',
        data: {'_token': CSRF_TOKEN, 'getEmail': getEmail},
        success: function (data) {
  if(data != 'Check Your Mail To Complete Process'){
$('#clientEmail').after('<div class="errorMsg" style="color: red;">'+data+'</div>');
}
else
{

$('.forget-form').hide();
$('.register-form').show();
$('.checkYourMailClass').show();
 $('.checkYourMailClass').effect("highlight", {}, 3000);



}
        },
        error: function (data) {
            $('#clientEmail').after('<div class="errorMsg" style="color: red;">Error Occured</div>');
        }
    });
}

});


$('.clientActivateAccount').click(function(){

$('.errorMsg').remove();
var error = 0;
var getDefaultPass= $('#defaultPassword').val();
var getPassword = $('#register_password').val();
var getrPassword = $('#rpassword').val();
if(getDefaultPass== "")
{
error = 1;
hideDivLoading();
$('#defaultPassword').after('<div class="errorMsg" style="color: red;">Default Password Is Required</div>');
}
if(getPassword == "")
{
error = 1;
hideDivLoading();
$('#register_password').after('<div class="errorMsg" style="color: red;">Password Is Required</div>');
}
if(getrPassword == "")
{
error = 1;
hideDivLoading();
$('#rpassword').after('<div class="errorMsg" style="color: red;">Retype Password Is Required</div>');
}

if(getPassword != getrPassword )
{
error = 1;
hideDivLoading();
$('.rpassword').after('<div class="errorMsg" style="color: red;">Password Does Not Match</div>');
}

if(error == 0){
//alert('done');
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/clientActivateAccount',
        type: 'get',
        data: {'_token': CSRF_TOKEN, 'defaultPassword': getDefaultPass, 'getPassword': getPassword},
        success: function (data) {
       
 if(data == "Incorrect Credentials")
         {
hideDivLoading();
       $('#defaultPassword').after('<div class="errorMsg" style="color: red;">Incorrect Credentials</div>');

         }
else{
hideDivLoading();
//swal('Update Successful');
$('.successMsg').show();
$('.checkYourMailClass').hide();

$('.successMsg').effect("highlight", {}, 3000);

//$(this).effect("highlight", {}, 3000);
//$('.blink_me').fadeOut(500).fadeIn(500, blink);  


}
        },
        error: function (data) {
hideDivLoading();
            swal('Error occured');
        }
    });
}

});


