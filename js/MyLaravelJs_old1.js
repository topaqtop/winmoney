/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var allDone = [];
var questionCounter = 0;
localStorage.setItem('moreCamp', 'done');
 $('.nomore').hide();  
 
    var totAvailable = $('.myAvailable').html().trim();
    if(parseInt(totAvailable) == 0)
    {
     $('.nomore').show();
      $('.myloadmore').html("");
    }
    else{
        $('.myloadmore').html('<br><button class="btn btn-primary btn-xs loadMore" onclick="loadMore();" style="display: none;">Load More</button>');
    }

function signOut()
{
    localStorage.clear();
    $.ajax({
        url: '/userSignout',
        type: 'POST',
        data: {},
        success: function (data) {
            location.href = '/WinMoneyIndex';
        },
        error: function (data) {
            alert("Error");
        }
    });

}


$(function () {

    //get regStatus if complete
    var regStatus = $('.regStatus').html().trim();
    if (regStatus != "unknown") {
        localStorage.setItem('regStatus', regStatus);
    }
    if (localStorage.getItem('regStatus') == "Incomplete")
    {
        $('.regUpdate').html("<span onclick='showModal();'><i class='fa fa-info'></i> Please click to update your account to increase your chances of winning");

        $('#myModal1').modal('show');
    }


    var retina = window.devicePixelRatio > 1 ? true : false;
    if (retina) {
        var retinaEl = jQuery("#logo img.logo-main");
        var retinaLogoW = retinaEl.width();
        var retinaLogoH = retinaEl.height();
        retinaEl.attr("src", "images/retina-goodfood.png").width(retinaLogoW).height(retinaLogoH);
        var stickyEl = jQuery("#logo img.logo-sticky");
        var stickyLogoW = stickyEl.width();
        var stickyLogoH = stickyEl.height();
        stickyEl.attr("src", "images/retina-goodfood.png").width(stickyLogoW).height(stickyLogoH);
        var mobileEl = jQuery("#logo img.logo-mobile");
        var mobileLogoW = mobileEl.width();
        var mobileLogoH = mobileEl.height();
        mobileEl.attr("src", "images/retina-goodfood.png").width(mobileLogoW).height(mobileLogoH);
    }
});
// $(document).ready(function () {
function takeThis(id) {

//            $('.campaign').removeClass('campaign-selected');
//            //get questions
//            $('.campaign_open_'+id).closest('.campaign').addClass('campaign-selected');
//           $('.campaign_open_'+id).closest('.one-third').addClass('opened');
//            $('.campaign_open_'+id).closest('.campaign').next().fadeIn();
    //var length = $('.campaign_wrapper .one-third').length;
    $('.campaign_wrapper .one-third').not('.opened').addClass('closed');
//            
    $('.myClass').hide();
    $('.camp_' + id).show();
    $('.camp_' + id).addClass('pull-left');

    setTimeout(function () {
        $('.question_container').removeClass('closed');
        var q = $('.getQuest_' + id).val();
        $('.myQuest').html(q);
        questionCounter = $('.getCount_' + id).val();


    }, (length * 0.001));
}

function closeThis() {
    // $('.close').on('click', function () {
    $('.question_container').addClass('closed');
    $('.campaign_wrapper .one-third').removeClass('closed');
    $('.campaign_wrapper .one-third').removeClass('opened');
    $('.campaign').removeClass('campaign-selected');
    $('.campaign_rules').hide();

    $('.myClass').show();
    $.each(allDone, function (count, value) {

        $('.camp_' + value).remove();

    });
    
   


}
// });

function showModal()
{
    $('#myModal1').modal('show');
}

function checkReg(regStatus, campId)
{
    if (regStatus == "unknown") {
        regStatus = localStorage.getItem('regStatus');
    }
    if (regStatus == "Incomplete")
    {
        $('#myModal1').modal('show');
    } else
    {
        takeThis(campId);
    }
}

function nextQuest()
{
    if ($('.campaign_count_' + (questionCounter)).length) {
        closeThis();
        $('.campaign_count_' + (questionCounter)).click();
        questionCounter++;
    }

}

function prevQuest(count)
{
    if (!$('.campaign_count_' + (parseInt(questionCounter))).length) {
        for (var i = 0; i <= count; i++)
        {

            if ($('.campaign_count_' + i).length && i != (parseInt(questionCounter))) {

                questionCounter = i;
                closeThis();
                $('.campaign_count_' + (i)).click();
                questionCounter--;
                break;

            }

        }
    } else {
        closeThis();
        questionCounter--;
        $('.campaign_count_' + (parseInt(questionCounter))).click();

    }

}


function subQuest()
{
 $('.questionBtn').attr('disabled', true);
    $('.questLoad').show();
   //$("#myform *").attr("disabled", "disabled").off('click');
    $('.myQuest').hide();
    var allVal = $('#form_' + questionCounter).serializeArray();
    var campaignID = allVal[allVal.length - 1].value;
    // var QuestionID = $('.getQuestID_'+campaignID).val();
    var questionId = [];
    var ifEmpty= [];
    
     $.each(allVal, function (count, val) {
        if (val.value == "") {
           ifEmpty.push(val.value);
        }
    });
    $.each(allVal, function (count, val) {
        if (count != (allVal.length - 1)) {
            if (count != 0 && count % 2 != 0) {
                questionId.push(val.value);
            }
        }
    });

    var validQuestCount = 0;
    if(ifEmpty.length > 0)
    {
          swal("Oops! Some Fields Were Empty");
            $('.questLoad').hide();
                    $('.myQuest').show();
 $('.questionBtn').attr('disabled', false);
            return false;
    }
    else{
    $.each(allVal, function (count, val) {
        if (count % 2 == 0 && count <= questionId.length && val.name.indexOf('val_') >= 0) {
            //alert('answer= '+ val.value +' campaignID= '+campaignID+' questionID= '+questionId[validQuestCount]);
            //submit
            $.ajax({
                url: '/saveAnswer',
                type: 'POST',
                data: {'_token': $('input[name=_token]').val(), 'answer': val.value, 'campaignID': campaignID, 'questionId': questionId[validQuestCount]},
                success: function (data) {

                    allDone.push(campaignID);
                   $('.questionBtn').attr('disabled', false);
                    $('.questLoad').hide();
                    $('.myQuest').show();
                    nextQuest();
                    if (count == 0)
                    {

                        $('.myAvailable').html((parseInt($('.myAvailable').html()) - 1));
                        $('.myAnswered').html((parseInt($('.myAnswered').html()) + 1));
                 
                  
                    }
                    swal("Submit Successful!");
                   
                    totAvailable = $('.myAvailable').html().trim();
    if(parseInt(totAvailable) == 0 && $('.boxes').html() != '<article class="ucard-graph-state text-center col-lg-4" style=" margin-top: 130px; margin-left: 30%;">No Campaign</article>')
    {
     $('.nomore').show();
       $('.myloadmore').html("");
    }
     else{
        $('.myloadmore').html('<br><button class="btn btn-primary btn-xs loadMore" onclick="loadMore();" style="display: none;">Load More</button>');
    }


                },
                error: function (data) {
                 
  $('.questionBtn').attr('disabled', false);
                     $('.questLoad').hide();
                    $('.myQuest').show();
                     swal("Oops! Error Occured");
                }
            });

        }

    });
    }
}


function userLogin()
{
    /*! jQuery v2.1.4 | (c) 2005, 2015 jQuery Foundation, Inc. | jquery.org/license */


    var password = $('#loginpassword').val();
    var username = $('#LoginEmail').val();

    $.ajax({
        url: '/SignIn',
        type: 'POST',
        data: {username: username, password: password},
        success: function (data) {
            var response = $.parseJSON(data);
            if (response === "Successful")
            {
                location.href = "/successful";

            } else
            {
                $('#errorMsg').html("Wrong username or password");
            }
        },
        error: function (data) {
            alert(username + " " + password);
        }
    });
}

function GoTo(available)
{
$('.question_container').addClass('closed');
    localStorage.setItem('moreCamp', available);
      $('.boxes').hide();
     $('.nomore').hide();
     $('.myloadmore').html("");
    $('.loadingCamp').show();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/getCampaign',
        type: 'get',
        data:
                {
                    _token: CSRF_TOKEN,
                    action: available,
                    regStatus: localStorage.getItem('regStatus')
                },

        success: function (data) {
                         $('.loadingCamp').hide();
                           $('.boxes').show();
            if(data == "")
            {
                $('.nomore').show();
                 $('.myloadmore').html("");
            }
             else{
        $('.myloadmore').html('<br><button class="btn btn-primary btn-xs loadMore" onclick="loadMore();" style="display: none;">Load More</button>');
    }
            $('.boxes').html(data);
            $('.ProfileNav-item').removeClass('is-active');
            $('.available').addClass('is-active');
            
        },
        error: function (data) {
             $('.loadingCamp').hide();
            alert('Error');
        }
    });

}

function loadMore()
{
     $('.boxes').hide();
     $('.nomore').hide();
     $('.myloadmore').html("");
    $('.loadingCamp').show();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/getMore',
        type: 'get',
        data:
                {
                    _token: CSRF_TOKEN,
                    action: localStorage.getItem('moreCamp'),
                    regStatus: localStorage.getItem('regStatus')
                },

        success: function (data) {
          //  alert(data);
           $('.loadingCamp').hide();
                           $('.boxes').show();
            if(data == "")
            {
                $('.nomore').show();
                 $('.myloadmore').html("");
            }
             else{
        $('.myloadmore').html('<br><button class="btn btn-primary btn-xs loadMore" onclick="loadMore();" style="display: none;">Load More</button>');
         }
            $('.boxes').html(data);
            $('.ProfileNav-item').removeClass('is-active');
            $('.available').addClass('is-active');
            
        },
        error: function (data) {
             $('.loadingCamp').hide();
            alert('Error');
        }
    });
}



function uploadDP()
{
    $('.DP').hide();
    $('.loadingDP').show();
    var ppName = $('#fupload').val();
    var ext = ppName.split('.')[1].toLowerCase();
    if (ext != "jpg" && ext != "jpeg" && ext != "png")
    {
        $('.DP').show();
        $('.loadingDP').hide();
        swal("Oops! File no supported.");
    } else
    {

//         var file_data = $('#fupload').prop('files')[0];   
//    var form_data = new FormData();                  
//    form_data.append('file', file_data);
//    alert(form_data);                             
        $.ajax({
            url: '/uploadDP', // point to server-side PHP script 
            dataType: 'json', // what to expect back from the PHP script, if anything
            aync: true,
            cache: false,
            contentType: false,
            processData: false,
            data: new FormData($("#upload_form")[0]),
            type: 'post',
            success: function (data) {
                // alert(data);
                var myP = ppName.split('\\')[2];
                  $('.DP').html('<img src="public/images/' + data + '" style="border-radius: 10px; width: 100%; height: 100%;"/>');
                  
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    url: '/upDateDBColumn',
                    type: 'get',
                    data: {
                        _token: CSRF_TOKEN,
                        imgName: myP,
                       
                    },
                    success: function (data1) {
                       
                        $('.DP').show();
                        $('.loadingDP').hide();
                        
                       
                    },
                    error: function (data1) {
                        swal("Oops! Error Occured");
                    }
                });


            },
            error: function (data) {
                $('.DP').show();
                $('.loadingDP').hide();
                swal("Oops! Error Occured.");
            }
        });



    }
}



function ForgotPasswordMail()
{
    var confirmMail = $('#ConfirmEmail').val();
    
     $('.forgot_password').attr('disabled', true);
     $('#ForgptPasswordMsg').html("");
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                     $.ajax({
                    url: '/forgotPassword',
                    type: 'get',
                    data: {
                        _token: CSRF_TOKEN,
                       confirmMail: confirmMail
                    },
                    success: function (data1) {
                       if(data1 != "Check Your Mail To Complete Process"){
                      $('#ForgptPasswordMsg').html("<span style='color: red;'>"+data1+"</span>");
                       }
                  else{
                        $('#ForgptPasswordMsg').html("<span style='color: green;'>"+data1+"</span>");
                    }
                     $('.forgot_password').attr('disabled', false);
                    },
                    error: function (data1) {
                         $('#ForgptPasswordMsg').html("<span style='color: red;'>Oops! Error Occured</span>");
                          $('.forgot_password').attr('disabled', false);
                    }
                    
                });

}

function showAnother()
{
    if($('.userLoginForm').is(":visible")){
    $('.userLoginForm').hide();
    $('.userForgotPassword').show();
    $('.topic').html("Forgot Password");
    }
    else
    {
    $('.userLoginForm').show();
    $('.userForgotPassword').hide();
    $('.topic').html("Sign In");
    }
    
}

function savenewpass()
{
var first = $('#password').val();
var second = $('#password1').val();
var sentCode = $('#sentCode').val();
if(first != "" && second != "" && first == second)
{
 var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                     $.ajax({
                    url: '/savePassword',
                    type: 'get',
                    data: {
                        _token: CSRF_TOKEN,
                       first: first,
                       second: second,
                       sentCode: sentCode 
                    },
                    success: function (data1) {
                     alert("Password Change Successful");
                     location.href="http://test.winmoney.ng";
                    },
                    error: function (data1) {
                       alert("Error Occured");
                    }
                    
                });
}
else
{
alert("Invalid Password");
}

}


function forgotPassword()
{
    
}



function inputBuilder(AnswerType, optionNum, responseType)
{
    //for textfield
    if (AnswerType == "myTextfield")
    {
        return "<input type='text' name='val_subforcount' class='form-control toDisable_subforcampid' id='val_subforcount' >";
    } else if (AnswerType == "myTextarea")
    {
        return "<textarea name='val_subforcount' class='form-control toDisable_subforcampid' id='val_subforcount'></textarea>";
    } else if (AnswerType == "myRadio")
    {
        var myInput = "";

        $.each(responseType, function (count, val) {

            myInput = myInput + "<input type='radio' name='val_subforcount' class='toDisable_subforcampid' id='val_subforcount' value='" + val + "' >" + val + "&emsp;";

        });

        return myInput;
    } else if (AnswerType == "mySelect")
    {
        var myInput = "<select name='val_subforcount' class='form-control toDisable_subforcampid' id='val_subforcount' >";

        $.each(responseType, function (count, val) {

            myInput = myInput + "<option value='" + val + "'>" + val + "</option>";

        });

        myInput = myInput + "</select>";

        return myInput;
    } else if (AnswerType == "myCheck")
    {
        var myInput = "";

        $.each(responseType, function (count, val) {

            myInput = myInput + "<input type='checkbox' name='val_subforcount' class=' toDisable_subforcampid' id='val_subforcount' value='" + val + "' >" + val + "&emsp;";

        });

        return myInput;
    } else if (AnswerType == "myRange")
    {

       // return "<input type='range' min='1' max='100' step='1' name='val_subforcount' class='form-control toDisable_subforcampid' id='val_subforcount'>&emsp;";
       return "<input id='range_1' type='text' name='val_subforcount' class='form-control toDisable_subforcampid' >&emsp;";
    }

}

function getType()
{
    var responseType = $('#myResponse').val();
    AnswerType = responseType;
    optionNum = 0;
    if (responseType == "myRadio" || responseType == "mySelect")
    {
        var opt = '<br><br><form method="post"><div class="optConfig"><div class="myOpt_0" style="margin-bottom: 20px;"><input type="text" class="opt_0 form-control " placeholder="Enter Options"/><span><input class="" type="radio" name="Expected" id="Expected_0" value="opt_0"/></span></div></div><br><div class=""><button type="button" class="addmore btn btn-success" onclick="addto(\'' + responseType + '\');">+</button>&emsp;<button class="remove btn btn-success" type="button" onclick="removeFrom();">-</button></div></form>';
        $('.moreOptions').html(opt);
    } else if (responseType == "myTextfield" || responseType == "myTextarea")
    {
        var opt = '<br><br><form method="post"><div class="optConfig"><div class="myOpt_0" style="margin-bottom: 20px;"> <input type="text" class="opt_0 form-control KeyTexts" placeholder="Enter Key-Words Separated With Comma(,)" data-role="tagsinput"></div></div><br></form>';
        $('.moreOptions').html(opt);
        $('.opt_0').tagsinput();
    } else if (responseType == "myCheck")
    {
        var opt = '<br><br><form method="post"><div class="optConfig"><div class="myOpt_0" style="margin-bottom: 20px;"> <input type="text" class="opt_0 form-control CheckBoxKeyTexts" placeholder="Enter Options"><span><input class="" type="checkbox" name="ExpectedBox" id="ExpectedBox_0" value="opt_0"/></span></div></div><br><br><div class=""><button type="button" class="addmore btn btn-success" onclick="addto(\'' + responseType + '\');">+</button>&emsp;<button class="remove btn btn-success" type="button" onclick="removeFrom();">-</button></div></form>';
        $('.moreOptions').html(opt);
//          $('.rightSide').after('<div class="form-group">'+
//                                           ' <label class="col-md-3 control-label"></label>'+
//                                            '<div class="col-md-4">'+
//                                                '<input type="text" class="checkBox_Response form-control" placeholder="Enter Key-Words Separated With Comma(,)" data-role="tagsinput">'+
//
//                                           ' </div>'+
//                                       ' </div>'
//                        );
//                $('.checkBox_Response').tagsinput();
       
    } else {
        $('.moreOptions').html("");

    }

}


function addto(responseType)
{
    if (responseType == "myRadio" || responseType == "mySelect") {
        optionNum++;
        $('.myOpt_' + (optionNum - 1)).after('<div class="myOpt_' + optionNum + '" style="margin-bottom: 20px;"><input type="text" class="opt_' + optionNum + ' form-control " placeholder="Enter Options"/><span><input class="" type="radio" name="Expected" id="Expected_' + optionNum + '" value="opt_' + optionNum + '"/></span></div>');

    } else if (responseType == "myTextfield" || responseType == "myTextarea")
    {
        optionNum++;
        $('.myOpt_' + (optionNum - 1)).after('<div class="myOpt_' + optionNum + '" style="margin-bottom: 20px;"><input type="text" class="opt_' + optionNum + ' form-control KeyTexts" placeholder="Enter Key-Words Separated With Comma(,)" data-role="tagsinput"></div>');
        $('.opt_' + (optionNum)).tagsinput();
    }
    else if (responseType == "myCheck")
    {
        optionNum++;
        $('.myOpt_' + (optionNum - 1)).after('<div class="myOpt_' + optionNum + '" style="margin-bottom: 20px;"><input type="text" class="opt_' + optionNum + ' form-control" placeholder="Enter Options"><span><input class="" type="checkbox" name="ExpectedBox" id="ExpectedBox_' + optionNum + '" value="opt_' + optionNum + '"/></span></div>');
      
    }


}

function removeFrom()
{
    if (optionNum > 0) {
        $('.myOpt_' + optionNum).remove();
        optionNum--;

    }

}




function saveCampQuest()
{
    var responseExpected = "";
    var ifEmpty = [];
    var checkRadios = [];
    var checkBoxes = [];
    var campAnswers = $('.addQuestForm').serializeArray();
    $.each(campAnswers, function (count, val) {
        if (val.value == "" && val.name != "myResponse") {
            ifEmpty.push(val.value);
        }

        if (val.name == "Expected")
        {
            checkRadios.push(val.value);
        }
        
        if(val.name == "ExpectedBox")
        {
            checkBoxes.push(val.value);
        }
    });

    if (ifEmpty.length > 0)
    {

        swal("Ensure to fill all fields");
    } else
    {
        var answerField = "";
        var responseType = [];
        var responseExpected = $('.KeyTexts').val();
        //  if($('.moreOptions').html() != "" )
        //  {
        if (AnswerType == "myRadio" || AnswerType == "mySelect") {
            for (var i = 0; i <= optionNum; i++)
            {

                if ($('.opt_' + i).val() == "" || $('.opt_' + i).val() == null)
                {
                    swal("Ensure to fill all fields");
                    return false;
                }
                responseType.push($('.opt_' + i).val());

            }
            answerField = inputBuilder(AnswerType, optionNum, responseType);
            if (checkRadios.length < 1)
            {
                swal("Oops! No Expected Response");
                return false;
            } else
            {
                responseExpected = $('.' + checkRadios[0]).val();
            }
        } else if (AnswerType == "myTextfield" || AnswerType == "myTextarea")
        {
            if ($('.KeyTexts').val() == "" || $('.KeyTexts').val() == null)
            {
                swal("Oops! No Key-Word Specified");
                return false;
            } else
            {
                responseExpected = $('.KeyTexts').val();
            }
            answerField = inputBuilder(AnswerType, optionNum, responseType);
            responseType = "NIL";
        }
         else if (AnswerType == "myCheck")
        {
           for (var i = 0; i <= optionNum; i++)
            {

                if ($('.opt_' + i).val() == "" || $('.opt_' + i).val() == null)
                {
                    swal("Ensure to fill all fields");
                    return false;
                }
                responseType.push($('.opt_' + i).val());

            }
            answerField = inputBuilder(AnswerType, optionNum, responseType);
            if (checkBoxes.length < 1)
            {
                swal("Oops! No Expected Response");
                return false;
            } else
            {
                responseExpected = "";
                $.each(checkBoxes, function(count, val){
                    if($('.'+val).val() != null)
                    responseExpected = responseExpected + $('.'+val).val() + ",";
                });
                
            }
           
        }
        else if (AnswerType == "myRange")
        {
            responseExpected = "30";
            answerField = inputBuilder(AnswerType, optionNum, responseType);
        }



        //}
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var CampaignID = $('.campName').val();
        var Question = $('.campQuest').val();





        $.ajax({
            url: '/saveQuestion',
            type: 'get',
            data: {'_token': CSRF_TOKEN, 'CampaignID': $('.campName').val().split('_')[0], 'ClientID': $('.campName').val().split('_')[1], 'Question': $('.campQuest').val(), 'myResponse': AnswerType, 'ResponseType': responseType, 'BuiltInput': answerField, 'responseExpected': responseExpected},
            success: function (data) {

                swal(data);

            },
            error: function (data) {

                swal("Oops! Error Occured");
            }
        });
    }
}



function addnewcamp()
{
    var newCamp = $('#newCamp').val();
    var allClients = $('#allClients').val();
    var campEnd = $('#campEnd').val();
    var campStart = $('#campStart').val();
    var campNotes = $('#campNotes').val();

    var responseArray = [newCamp, allClients, campEnd, campStart, campNotes];
    var checkEmpty = [];
    $.each(responseArray, function (count, val) {

        if (val == "" || val == null)
        {
            checkEmpty.push("1");
            swal("All Fields Must Have A Value");
            return false;
        }

    });

    if (checkEmpty.length > 0)
    {
        return false;
    }
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/newCampaign',
        type: 'get',
        data: {'_token': CSRF_TOKEN, 'newCampaign': newCamp, 'allClients': allClients, 'campEnd': campEnd, 'campStart': campStart, 'campNotes': campNotes},
        success: function (data) {

            swal("Submit Successful");
            $('.campName').append('<option value="' + data + '_' + allClients + '">' + newCamp + '</option>');

        },
        error: function (data) {

            swal("Oops! Error Occured");
        }
    });
}