/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var optionNum = 0;
var AnswerType = "";
var allDone = [];
var questionCounter = 0;
localStorage.setItem('moreCamp', 'done');
$('.nomore').hide();

var totAvailable = $('.myAvailable').html().trim();
if (parseInt(totAvailable) == 0)
{
    $('.nomore').show();
    //  $('.myloadmore').html("");
} else {
    //$('.myloadmore').html('<br><a href="#" class="loadMore load-more button button-secondary black small" onclick="loadMore(); id="next-page-uid_30_5942bc1df4144" data-block_id="uid_30_5942bc1df4144">Load more<i class="icon icon-menu-down"></i></a>');
    // $('.myloadmore').html('<br><button class="btn btn-primary btn-xs loadMore" onclick="loadMore();">Load More</button>');
}

function signOut()
{
    localStorage.clear();
    $.ajax({
        url: '/userSignout',
        type: 'POST',
        data: {},
        success: function (data) {
            location.href = '/WinMoneyIndex';
        },
        error: function (data) {
            alert("Error");
        }
    });

}

$(function () {


//$("#rangeSelect").ionRangeSlider();
    //get regStatus if complete
    var regStatus = $('.regStatus').html().trim();
    if (regStatus != "unknown") {
        localStorage.setItem('regStatus', regStatus);
    }
    if (localStorage.getItem('regStatus') == "Incomplete")
    {
        $('.regUpdate').html("<span onclick='showModal();'><i class='fa fa-info'></i> Please click to update your account to increase your chances of winning");

        $('#myModal1').modal('show');
    }


    var retina = window.devicePixelRatio > 1 ? true : false;
    if (retina) {
        var retinaEl = jQuery("#logo img.logo-main");
        var retinaLogoW = retinaEl.width();
        var retinaLogoH = retinaEl.height();
        retinaEl.attr("src", "images/retina-goodfood.png").width(retinaLogoW).height(retinaLogoH);
        var stickyEl = jQuery("#logo img.logo-sticky");
        var stickyLogoW = stickyEl.width();
        var stickyLogoH = stickyEl.height();
        stickyEl.attr("src", "images/retina-goodfood.png").width(stickyLogoW).height(stickyLogoH);
        var mobileEl = jQuery("#logo img.logo-mobile");
        var mobileLogoW = mobileEl.width();
        var mobileLogoH = mobileEl.height();
        mobileEl.attr("src", "images/retina-goodfood.png").width(mobileLogoW).height(mobileLogoH);
    }
});
// $(document).ready(function () {
function takeThis(id) {


//            $('.campaign').removeClass('campaign-selected');
//            //get questions
//            $('.campaign_open_'+id).closest('.campaign').addClass('campaign-selected');
//           $('.campaign_open_'+id).closest('.one-third').addClass('opened');
//            $('.campaign_open_'+id).closest('.campaign').next().fadeIn();
    //var length = $('.campaign_wrapper .one-third').length;
    $('.campaign_wrapper .one-third').not('.opened').addClass('closed');
//            
    $('.myClass').hide();
    $('.camp_' + id).show();
    $('.camp_' + id).addClass('pull-left');

    setTimeout(function () {
        $('.question_container').removeClass('closed');
        var q = $('.getQuest_' + id).val();
        $('.myQuest').html(q);
        $('.questLoad').hide();
        $('.myQuest').show();
        questionCounter = $('.getCount_' + id).val();


    }, (length * 0.0000001));
    // $(".rangeSelect").ionRangeSlider();

}

function closeThis() {
    // $('.close').on('click', function () {
     $('.share').hide();
    $('.question_container').addClass('closed');
    $('.campaign_wrapper .one-third').removeClass('closed');
    $('.campaign_wrapper .one-third').removeClass('opened');
    $('.campaign').removeClass('campaign-selected');
    $('.campaign_rules').hide();

    $('.myClass').show();
    $.each(allDone, function (count, value) {

        $('.camp_' + value).remove();

    });




}
// });

function showModal()
{
    $('#myModal1').modal('show');
}

function checkReg(regStatus, campId)
{
    if (regStatus == "unknown") {
        regStatus = localStorage.getItem('regStatus');
    }
    if (regStatus == "Incomplete")
    {
        $('#myModal1').modal('show');
    } else
    {
        takeThis(campId);
    }
}

function nextQuest()
{
    if ($('.campaign_count_' + (questionCounter)).length) {
        closeThis();
        $('.campaign_count_' + (questionCounter)).click();
        questionCounter++;
    }

}

function prevQuest(count)
{
    if (!$('.campaign_count_' + (parseInt(questionCounter))).length) {
        for (var i = 0; i <= count; i++)
        {

            if ($('.campaign_count_' + i).length && i != (parseInt(questionCounter))) {

                questionCounter = i;
                closeThis();
                $('.campaign_count_' + (i)).click();
                questionCounter--;
                break;

            }

        }
    } else {
        closeThis();
        questionCounter--;
        $('.campaign_count_' + (parseInt(questionCounter))).click();

    }

}



function subQuest()
{
    $('.mySubmitBtn').attr('disabled', true);
    $('.questLoad').show();
    $('.myQuest').hide();
    // alert(questionCounter);
    var allVal = $('#uniquesForm').serializeArray();
    console.log(allVal);
    var campaignID = allVal[allVal.length - 1].value;
    // var QuestionID = $('.getQuestID_'+campaignID).val();
    var questionId = [];
    var ifEmpty = [];


    $.each(allVal, function (count, val) {
        if (val.value == "" && val.name.indexOf('val_') >= 0) {
            ifEmpty.push(val.value);
        }

    });
    $.each(allVal, function (count, val) {
        if (val.name.indexOf('Quest') >= 0) {

            questionId.push(val.value);

        }
    });

    var validQuestCount = 0;
    if (ifEmpty.length > 0)
    {
        swal("Oops! Some Fields Were Empty");
        $('.questLoad').hide();
        $('.myQuest').show();
        $('.mySubmitBtn').attr('disabled', false);
        return false;
    } else {
        var answerArray = [];
        var questIdArray = [];
        $.each(allVal, function (count, val) {
            if (val.name.indexOf('val_') >= 0) {

                answerArray.push(val.value);

            }

        });

        //checkboxes
        var checkboxSpans = [];
        $.each(allVal, function (count, val) {
            if (val.name.indexOf('checkBoxClass_') >= 0) {
                checkboxSpans.push(val.value);
            }
        });
        //now get checkbox values
        if (checkboxSpans.length > 0) {
            var checkEmptyCheckBox = [];
            $.each(checkboxSpans, function (count, val) {

                var stringOfEach = "";
                $('.' + val + ' input:checked').each(function () {
                    stringOfEach = stringOfEach + ($(this).val()) + ",";
                });

                answerArray.push(stringOfEach);
                questionId.push(val.split('_')[1]);
                if (stringOfEach == "")
                {
                    checkEmptyCheckBox.push("Empty");
                }

            });

            if (checkEmptyCheckBox.length > 0)
            {
                swal("Oops! All Fields Are Required");
                $('.questLoad').hide();
                $('.myQuest').show();
                $('.mySubmitBtn').attr('disabled', false);
                return false;
            }

        }

        //radio buttons
        var radioSpans = [];
        var emptyRadio = [];
        $.each(allVal, function (count, val) {
            if (val.name.indexOf('radioClass_') >= 0) {
                radioSpans.push(val.value);
            }
        });
        //now get radio values
        if (radioSpans.length > 0) {
            var checkEmptyRadio = [];
            var returnStatement = true;
            $.each(radioSpans, function (count, val) {


                var radioVal = "";
                $('.' + val + ' input:checked').each(function () {
                    radioVal = radioVal + ($(this).val()) + ",";
                });


                if (radioVal != "")
                {
                    answerArray.push(radioVal);
                    questionId.push(val.split('_')[1]);
                    checkEmptyRadio.push("Success");
                }


                if (checkEmptyRadio.length < 1)
                {

                    emptyRadio.push("empty");
                }

            });

            if (emptyRadio.length > 0)
            {
                swal("Oops! All Fields Are Required");
                $('.questLoad').hide();
                $('.myQuest').show();
                $('.mySubmitBtn').attr('disabled', false);
                return false;
            }

        }

        //range select
        var rangeSpans = [];
        $.each(allVal, function (count, val) {
            if (val.name.indexOf('rangeClass_') >= 0) {
                rangeSpans.push(val.name);
            }
        });
        //now get radio values
        if (rangeSpans.length > 0) {
            var checkEmptyRange = [];
            $.each(rangeSpans, function (count, val) {

                answerArray.push($('.' + val).val());
                questionId.push(val.split('_')[1]);
                //checkEmptyRange.push("Success");

            });

//            if (checkEmptyRadio.length < 1)
//            {
//                swal("Oops! All Fields Are Required");
//                $('.questLoad').hide();
//                $('.myQuest').show();
//                $('.mySubmitBtn').attr('disabled', false);
//                return false;
//            }

        }






        $.ajax({
            url: '/saveAnswer',
            type: 'get',
            data: {'_token': $('input[name=_token]').val(), 'answer': answerArray, 'campaignID': campaignID, 'questionId': questionId},
            success: function (data) {

                allDone.push(campaignID);
                $('.mySubmitBtn').attr('disabled', false);
                $('.questLoad').hide();
                  $('.share').show(); 
                  $('.question_footer').hide();
                  
                    $('.question_header h4').html('Thank you for participating... Share now and increase your chances of being a winner');
                        $('.rrssb-buttons').rrssb({
                            // required:
                            title: 'Join Win Money and stand a chance of winning easy gifts and cash prizes monthly',
                            url: 'http://winmoney.ng/',

                            // optional:
                            description: 'Join Win Money and stand a chance of winning easy gifts and cash prizes monthly',
                            emailBody: 'Join Win Money and stand a chance of winning easy gifts and cash prizes monthly'
                        });
                        $(window).trigger('resize');
                  if (answerArray.length > 0)
                {
                    $('.myAvailable').html((parseInt($('.myAvailable').html()) - 1));
                    $('.myAnswered').html((parseInt($('.myAnswered').html()) + 1));
                }
               // swal("Submit Successful!");

                totAvailable = $('.myAvailable').html().trim();
            },
            error: function (data) {
                $('.mySubmitBtn').attr('disabled', false);
                $('.questLoad').hide();
                $('.myQuest').show();
                swal("Oops! Error Occured");
            }
        });
    }
}

function nextPrize()
{
    $('.share').hide();
    $('.question_footer').show();
     $('.myQuest').show();
                nextQuest();
               
                if (parseInt(totAvailable) == 0 && $('.boxes').html() != '<article class="ucard-graph-state text-center col-lg-4" style=" margin-top: 130px; margin-left: 30%;">No More Campaign </article>')
                {
                    $('.nomore').show();
                    //  $('.myloadmore').html("");
                } 
}

function userLogin()
{
    /*! jQuery v2.1.4 | (c) 2005, 2015 jQuery Foundation, Inc. | jquery.org/license */

    $('#errorMsg').html("");
    var password = $('#loginpassword').val();
    var username = $('#LoginEmail').val();
    if (password == "" || username == "")
    {
        $('#errorMsg').html('<span style="color: red;">Incorrect Username Or Password</span>');
        return false;
    }

    $.ajax({
        url: '/SignIn',
        type: 'POST',
        data: {username: username, password: password},
        success: function (data) {
            if (data == "Invalid Profile")
            {
                $('#errorMsg').html('<span style="color: red; margin-bottom: 20px;">Profile Is Inactive</span>');
                return false;

            } else if (data == "Incorrect Details")
            {
                $('#errorMsg').html('<span style="color: red; margin-bottom: 20px;">Wrong username or password</span>');
                return false;
            } else
            {
                location.href = "/successful";
            }
        },
        error: function (data) {

            $('#errorMsg').html('<span style="color: red; margin-bottom: 20px;">Error Occured</span>');
            return false;
        }
    });
}

function GoTo(available)
{
    $('.question_container').addClass('closed');
    localStorage.setItem('moreCamp', available);
    $('.boxes').hide();
    $('.nomore').hide();
    // $('.myloadmore').html("");
    $('.loadingCamp').show();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/getCampaign',
        type: 'get',
        data:
                {
                    _token: CSRF_TOKEN,
                    action: available,
                    regStatus: localStorage.getItem('regStatus')
                },

        success: function (data) {
            $('.loadingCamp').hide();
            $('.boxes').show();
            if (data == "")
            {
                $('.nomore').show();
                // $('.myloadmore').html("");
            } else {
                // $('.myloadmore').html('<br><a href="#" class="loadMore load-more button button-secondary black small" onclick="loadMore(); id="next-page-uid_30_5942bc1df4144" data-block_id="uid_30_5942bc1df4144">Load more<i class="icon icon-menu-down"></i></a>');
                // $('.myloadmore').html('<br><button class="btn btn-primary btn-xs loadMore" onclick="loadMore();">Load More</button>');
            }
            $('.boxes').html(data);
            $('.ProfileNav-item').removeClass('is-active');
            $('.available').addClass('is-active');
            $('.popoverData').tooltip();
            $('.popoverOption').tooltip({trigger: "hover"});
        },
        error: function (data) {
            $('.loadingCamp').hide();
            alert('Error');
        }
    });

}

function loadMore()
{
    $('.boxes').hide();
    $('.nomore').hide();
    //$('.myloadmore').html("");
    $('.loadingCamp').show();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/getMore',
        type: 'get',
        data:
                {
                    _token: CSRF_TOKEN,
                    action: localStorage.getItem('moreCamp'),
                    regStatus: localStorage.getItem('regStatus')
                },

        success: function (data) {
            // alert(data);
            $('.loadingCamp').hide();
            $('.boxes').show();
            if (data == "")
            {
                $('.nomore').show();
                // $('.myloadmore').html("");
            } else {
                // $('.myloadmore').html('<br><a href="#" class="loadMore load-more button button-secondary black small" onclick="loadMore(); id="next-page-uid_30_5942bc1df4144" data-block_id="uid_30_5942bc1df4144">Load more<i class="icon icon-menu-down"></i></a>');
                // $('.myloadmore').html('<br><button class="btn btn-primary btn-xs loadMore" onclick="loadMore();">Load More</button>');
            }
            $('.boxes').html(data);
            $('.ProfileNav-item').removeClass('is-active');
            $('.available').addClass('is-active');

        },
        error: function (data) {
            $('.loadingCamp').hide();
            alert('Error');
        }
    });
}


function uploadDP()
{
    $('.DP').hide();
    $('.loadingDP').show();
    var ppName = $('#fupload').val();
    var ext = ppName.split('.')[1].toLowerCase();
    if (ext != "jpg" && ext != "jpeg" && ext != "png")
    {
        $('.DP').show();
        $('.loadingDP').hide();
        swal("Oops! File no supported.");
    } else
    {

//         var file_data = $('#fupload').prop('files')[0];   
//    var form_data = new FormData();                  
//    form_data.append('file', file_data);
//    alert(form_data);                             
        $.ajax({
            url: '/uploadDP', // point to server-side PHP script 
            dataType: 'json', // what to expect back from the PHP script, if anything
            aync: true,
            cache: false,
            contentType: false,
            processData: false,
            data: new FormData($("#upload_form")[0]),
            type: 'post',
            success: function (data) {
                // alert(data);
                var myP = ppName.split('\\')[2];
                  $('.DP').html('<img src="public/images/' + data + '" style="border-radius: 10px; width: 100%; height: 100%;"/>');
                  
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    url: '/upDateDBColumn',
                    type: 'get',
                    data: {
                        _token: CSRF_TOKEN,
                        imgName: myP,
                       
                    },
                    success: function (data1) {
                       
                        $('.DP').show();
                        $('.loadingDP').hide();
                        
                       
                    },
                    error: function (data1) {
                        swal("Oops! Error Occured");
                    }
                });


            },
            error: function (data) {
                $('.DP').show();
                $('.loadingDP').hide();
                swal("Oops! Error Occured.");
            }
        });



    }
}


function uploadCampaignImage()
{
    var ppName = $('#fupload').val();
    var ext = ppName.split('.')[1].toLowerCase();
    if (ext != "jpg" && ext != "jpeg" && ext != "png")
    {
        $('.imgError').html("Oops! File no supported.");
    } else
    {

        $.ajax({
            url: '/uploadCampaignImage', // point to server-side PHP script 
            dataType: 'json', // what to expect back from the PHP script, if anything
            aync: true,
            cache: false,
            contentType: false,
            processData: false,
            data: new FormData($("#upload_form")[0]),
            type: 'post',
            success: function (data) {
                // alert(data);
                //  var myP = ppName.split('\\')[2];
                // $('.pic').html('<img src="images/CampaignImages/'+campId+'/'+myP+'" width="70%" />');

            },
            error: function (data) {
                swal("Oops! Error Occured.");
            }
        });



    }
}

function updateCampaignImg()
{
    var campId = $('.editCampId').val();
    var initialImg = $('.pic').html();
    $('.pic').html("Uploading...");
    var ppName = $('#updateUpload').val();
    var ext = ppName.split('.')[1].toLowerCase();
    if (ext != "jpg" && ext != "jpeg" && ext != "png")
    {
        $('.pic').html(initialImg);
        $('.imgError').html("Oops! File no supported.");
    } else
    {
        var myFile = new FormData($("#updateCampForm")[0]);
        myFile.append('campId', campId);
        $.ajax({
            url: '/updateCampaignImg', // point to server-side PHP script 
            dataType: 'json', // what to expect back from the PHP script, if anything
            aync: true,
            cache: false,
            contentType: false,
            processData: false,
            data: myFile,
            type: 'post',
            success: function (data) {
                // alert(data);
                var myP = ppName.split('\\')[2];
                $('.ImagePath_' + campId).html(myP);
                $('.pic').html('<img src="public/images/CampaignImages/' + campId + '/' + myP + '" width="70%" />');

            },
            error: function (data) {
                $('.pic').html(initialImg);
                swal("Oops! Error Occured.");
            }
        });



    }
}

function ForgotPasswordMail()
{
    var confirmMail = $('#ConfirmEmail').val();
    $('.forgot_password').attr('disabled', true);
    $('#ForgptPasswordMsg').html("");
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/forgotPassword',
        type: 'get',
        data: {
            _token: CSRF_TOKEN,
            confirmMail: confirmMail
        },
        success: function (data1) {
            if (data1 != "Check Your Mail To Complete Process") {
                $('#ForgptPasswordMsg').html("<span style='color: red;'>" + data1 + "</span>");
            } else {
                $('#ForgptPasswordMsg').html("<span style='color: green;'>" + data1 + "</span>");
            }
            $('.forgot_password').attr('disabled', false);
        },
        error: function (data1) {
            $('#ForgptPasswordMsg').html("<span style='color: red;'>Oops! Error Occured</span>");
            $('.forgot_password').attr('disabled', false);
        }

    });

}

function showAnother()
{
    if ($('.userLoginForm').is(":visible")) {
        $('.userLoginForm').hide();
        $('.userForgotPassword').show();
        $('.topic').html("Forgot Password");
    } else
    {
        $('.userLoginForm').show();
        $('.userForgotPassword').hide();
        $('.topic').html("Sign In");
    }

}

function forgotPassword()
{

}

function inputBuilder(AnswerType, optionNum, responseType)
{
    //for textfield
    if (AnswerType == "myTextfield")
    {
        return "<input type='text' name='val_subforcount' class='form-control toDisable_subforcampid' id='val_subforcount' >";
    } else if (AnswerType == "myTextarea")
    {
        return "<textarea name='val_subforcount' class='form-control toDisable_subforcampid' id='val_subforcount'></textarea>";
    } else if (AnswerType == "myRadio")
    {
        var myInput = "";

        $.each(responseType, function (count, val) {

            myInput = myInput + "<input type='radio' name='val_subforcount' class='toDisable_subforcampid' id='val_subforcount' value='" + val + "' >" + val + "&emsp;";

        });

        return myInput;
    } else if (AnswerType == "mySelect")
    {
        var myInput = "<select name='val_subforcount' class='form-control toDisable_subforcampid' id='val_subforcount' >";

        $.each(responseType, function (count, val) {

            myInput = myInput + "<option value='" + val + "'>" + val + "</option>";

        });

        myInput = myInput + "</select>";

        return myInput;
    } else if (AnswerType == "myCheck")
    {
        var myInput = "";

        $.each(responseType, function (count, val) {

            myInput = myInput + "<input type='checkbox' name='val_subforcount' class=' toDisable_subforcampid' id='val_subforcount' value='" + val + "' >" + val + "&emsp;";

            // $('.checkBox_Response').tagsinput();
        });

        return myInput;
    } else if (AnswerType == "myRange")
    {

        // return "<input type='range' min='1' max='100' step='1' name='val_subforcount' class='form-control toDisable_subforcampid' id='val_subforcount'>&emsp;";
        return "<input id='range_1' type='text' name='val_subforcount' class='form-control toDisable_subforcampid' >&emsp;";
    }

}

function getType()
{
    $('.checks').remove();
    var responseType = $('#myResponse').val();
    AnswerType = responseType;
    optionNum = 0;
    if (responseType == "myRadio" || responseType == "mySelect")
    {
        var opt = '<br><br><form method="post"><div class="optConfig"><div class="myOpt_0" style="margin-bottom: 20px;"><input type="text" class="opt_0 form-control " placeholder="Enter Options"/><span><input class="" type="radio" name="Expected" id="Expected_0" value="opt_0"/></span></div></div><br><div class=""><button type="button" class="addmore btn btn-success" onclick="addto(\'' + responseType + '\');">+</button>&emsp;<button class="remove btn btn-success" type="button" onclick="removeFrom();">-</button></div></form>';
        $('.moreOptions').html(opt);
    } else if (responseType == "myTextfield" || responseType == "myTextarea")
    {
        var opt = '<br><br><form method="post"><div class="optConfig"><div class="myOpt_0" style="margin-bottom: 20px;"> <input type="text" class="opt_0 form-control KeyTexts" placeholder="Enter Key-Words Separated With Comma(,)" data-role="tagsinput"></div></div><br></form>';
        $('.moreOptions').html(opt);
        $('.opt_0').tagsinput();
    } else if (responseType == "myCheck")
    {
        var opt = '<br><br><form method="post"><div class="optConfig"><div class="myOpt_0" style="margin-bottom: 20px;"> <input type="text" class="opt_0 form-control CheckBoxKeyTexts" placeholder="Enter Options"><span><input class="" type="checkbox" name="ExpectedBox" id="ExpectedBox_0" value="opt_0"/></span></div></div><br><br><div class=""><button type="button" class="addmore btn btn-success" onclick="addto(\'' + responseType + '\');">+</button>&emsp;<button class="remove btn btn-success" type="button" onclick="removeFrom();">-</button></div></form>';

        $('.rightSide').after('<div class="form-group checks">' +
                ' <label class="col-md-3 control-label">Select Response Logic</label>' +
                '<div class="col-md-4">' +
                '<input type="radio" name="logic" class="andLogic" value="AND"> AND &emsp; <input type="radio" name="logic" class="orLogic" value="OR"> OR &emsp;' +
                ' </div>' +
                ' </div>'
                );
        $('.moreOptions').html(opt);
//          $('.rightSide').after('<div class="form-group">'+
//                                           ' <label class="col-md-3 control-label"></label>'+
//                                            '<div class="col-md-4">'+
//                                                '<input type="text" class="checkBox_Response form-control" placeholder="Enter Key-Words Separated With Comma(,)" data-role="tagsinput">'+
//
//                                           ' </div>'+
//                                       ' </div>'
//                        );
//                $('.checkBox_Response').tagsinput();

    } else {

        var opt = '<br><br>';

        $('.rightSide').after('<div class="form-group checks">' +
                ' <label class="col-md-3 control-label"> Range Max</label>' +
                '<div class="col-md-4">' +
                '<input type="text" name="rangemax" class="form-control rangemax" value="" placeholder="Enter Range Max">' +
                ' </div>' +
                ' </div>' +
                '<div class="form-group checks">' +
                ' <label class="col-md-3 control-label"> Range Min</label>' +
                '<div class="col-md-4">' +
                '<input type="text" name="rangemin" class="form-control rangemin" value="" placeholder="Enter Range Min">' +
                ' </div>' +
                ' </div>' +
                '<div class="form-group checks">' +
                ' <label class="col-md-3 control-label"> Least Value</label>' +
                '<div class="col-md-4">' +
                '<input type="text" name="leastval" class="form-control leastval" value="" placeholder="Enter Least Value">' +
                ' </div>' +
                ' </div>' +
                '<div class="form-group checks">' +
                ' <label class="col-md-3 control-label"> Min-Value Label</label>' +
                '<div class="col-md-4">' +
                '<input type="text" name="minvaldesc" class="form-control minvaldesc " value="" placeholder="Enter Minimum Value Label">' +
                ' </div>' +
                ' </div>' +
                '<div class="form-group checks">' +
                ' <label class="col-md-3 control-label"> Max-Value Label</label>' +
                '<div class="col-md-4">' +
                '<input type="text" name="maxvaldesc" class="form-control maxvaldesc" value="" placeholder="Enter Maximum Value Label">' +
                ' </div>' +
                ' </div>'
                );
        $('.moreOptions').html(opt);

    }

}


function addto(responseType)
{
    if (responseType == "myRadio" || responseType == "mySelect") {
        optionNum++;
        $('.myOpt_' + (optionNum - 1)).after('<div class="myOpt_' + optionNum + '" style="margin-bottom: 20px;"><input type="text" class="opt_' + optionNum + ' form-control " placeholder="Enter Options"/><span><input class="" type="radio" name="Expected" id="Expected_' + optionNum + '" value="opt_' + optionNum + '"/></span></div>');

    } else if (responseType == "myTextfield" || responseType == "myTextarea")
    {
        optionNum++;
        $('.myOpt_' + (optionNum - 1)).after('<div class="myOpt_' + optionNum + '" style="margin-bottom: 20px;"><input type="text" class="opt_' + optionNum + ' form-control KeyTexts" placeholder="Enter Key-Words Separated With Comma(,)" data-role="tagsinput"></div>');
        $('.opt_' + (optionNum)).tagsinput();
    } else if (responseType == "myCheck")
    {
        optionNum++;
        $('.myOpt_' + (optionNum - 1)).after('<div class="myOpt_' + optionNum + '" style="margin-bottom: 20px;"><input type="text" class="opt_' + optionNum + ' form-control" placeholder="Enter Options"><span><input class="" type="checkbox" name="ExpectedBox" id="ExpectedBox_' + optionNum + '" value="opt_' + optionNum + '"/></span></div>');

    }


}

function removeFrom()
{
    if (optionNum > 0) {
        $('.myOpt_' + optionNum).remove();
        optionNum--;

    }

}




function saveCampQuest()
{
    var responseExpected = "";
    var ifEmpty = [];
    var checkRadios = [];
    var checkBoxes = [];
    var campAnswers = $('.addQuestForm').serializeArray();
    $.each(campAnswers, function (count, val) {
        if (val.value == "" && val.name != "myResponse") {
            ifEmpty.push(val.value);
        }

        if (val.name == "Expected")
        {
            checkRadios.push(val.value);
        }

        if (val.name == "ExpectedBox")
        {
            checkBoxes.push(val.value);
        }
    });

    if (ifEmpty.length > 0)
    {
        swal("Ensure to fill all fields");
        return false;
    } else
    {
        var answerField = "";
        var responseType = [];
        var responseExpected = $('.KeyTexts').val();
        //  if($('.moreOptions').html() != "" )
        //  {
        if (AnswerType == "myRadio" || AnswerType == "mySelect") {
            for (var i = 0; i <= optionNum; i++)
            {

                if ($('.opt_' + i).val() == "" || $('.opt_' + i).val() == null)
                {
                    swal("Ensure to fill all fields");
                    return false;
                } else if ($('.opt_' + i).val().indexOf(',') >= 0)
                {
                    swal("Remove Commas(,) From All Response Inputs");
                    return false;
                }
                responseType.push($('.opt_' + i).val());

            }
            answerField = inputBuilder(AnswerType, optionNum, responseType);
            if (checkRadios.length < 1)
            {
                swal("Oops! No Expected Response");
                return false;
            } else
            {
                responseExpected = $('.' + checkRadios[0]).val();
            }
        } else if (AnswerType == "myTextfield" || AnswerType == "myTextarea")
        {
            if ($('.KeyTexts').val() == "" || $('.KeyTexts').val() == null)
            {
                swal("Oops! No Key-Word Specified");
                return false;
            } else if ($('.KeyTexts').val().indexOf(',') >= 0)
            {
                swal("Remove Commas(,) From All Response Inputs");
                return false;
            } else
            {
                responseExpected = $('.KeyTexts').val();
            }
            answerField = inputBuilder(AnswerType, optionNum, responseType);
            responseType = "NIL";
        } else if (AnswerType == "myCheck")
        {
            for (var i = 0; i <= optionNum; i++)
            {

                if ($('.opt_' + i).val() == "" || $('.opt_' + i).val() == null)
                {
                    swal("Ensure to fill all fields");
                    return false;
                } else if ($('.opt_' + i).val().indexOf(',') >= 0)
                {
                    swal("Remove Commas(,) From All Response Inputs");
                    return false;
                }
                responseType.push($('.opt_' + i).val());

            }
            answerField = inputBuilder(AnswerType, optionNum, responseType);
            var logic = "";
            $('input[name=radioName]:checked', '#myForm').val()
            if ($('input[name=logic]:checked').val() == "" || $('input[name=logic]:checked').val() == null)
            {
                swal("Oops! No Response Logic Selected");
                return false;
            } else
            {
                logic = $('input[name=logic]:checked').val();
            }
            if (checkBoxes.length < 1)
            {
                swal("Oops! No Expected Response");
                return false;
            } else
            {
                responseExpected = "";
                $.each(checkBoxes, function (count, val) {
                    if ($('.' + val).val() != null)
                        responseExpected = responseExpected + $('.' + val).val() + ",";
                });
                responseExpected = responseExpected + "," + logic;
            }


        } else if (AnswerType == "myRange")
        {


            var validationArray = [];
            $.each(campAnswers, function (count, val) {
                if (val.name.indexOf('range') >= 0 || val.name == "leastval") {
                    if (!val.value.match(/^\d+$/)) {

                        validationArray.push("Data Error");
                    }

                }
            });
            $.each(campAnswers, function (count, val) {
                if (val.name.indexOf('valdesc') >= 0) {
                    if (val.value.indexOf(',') >= 0) {

                        validationArray.push("Comma Error");
                    }

                }
            });

            if (validationArray.length > 0)
            {
                if (validationArray[0] == "Data Error") {
                    swal("Oops! Range values must contain only digits");
                } else if (validationArray[0] == "Comma Error") {
                    swal("Oops! Remove Comma From Range Descriptions");
                }
                return false;
            }

            responseType.push(campAnswers[3].value + "-" + campAnswers[4].value);
            responseType.push(campAnswers[6].value + "-" + campAnswers[7].value)
            responseExpected = campAnswers[5].value;

        }
        //}
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var CampaignID = $('.campName').val();
        var Question = $('.campQuest').val();
        $('#newCamp').val($('.campName_' + CampaignID.split('_')[0]).html());

        $.ajax({
            url: '/saveQuestion',
            type: 'get',
            data: {'_token': CSRF_TOKEN, 'CampaignID': $('#campName').val().split('_')[0], 'ClientID': $('#campName').val().split('_')[1], 'Question': Question, 'ResponseType': responseType, 'responseExpected': responseExpected, 'myResponse': AnswerType, 'ResponseData': responseType},
            success: function (data) {

                swal("Submit Successful");
                location.href = "/CampaignList";
            },
            error: function (data) {

                swal("Oops! Error Occured");
            }
        });

    }
}



function addnewcamp()
{
    var newCamp = $('#newCamp').val();
    var allClients = $('#allClients').val();
    var campEnd = $('#campEnd').val();
    campEnd = campEnd.split('/')[2] + "-" + campEnd.split('/')[0] + "-" + campEnd.split('/')[1];
    var campStart = $('#campStart').val();
    campStart = campStart.split('/')[2] + "-" + campStart.split('/')[0] + "-" + campStart.split('/')[1];
    var campNotes = $('#campNotes').val();
    var ImagePath = $('#fupload').val().split('\\')[2];

    var responseArray = [newCamp, allClients, campEnd, campStart, campNotes, ImagePath];
    var checkEmpty = [];
    $.each(responseArray, function (count, val) {

        if (val == "" || val == null)
        {
            checkEmpty.push("1");
            swal("All Fields Must Have A Value");
            return false;
        }

    });

    if (checkEmpty.length > 0)
    {
        return false;
    }

    var ppName = $('#fupload').val();
    var ext = ppName.split('.')[1].toLowerCase();
    if (ext != "jpg" && ext != "jpeg" && ext != "png")
    {
        swal("Oops! File no supported.");
    } else
    {
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: '/newCampaign',
            type: 'get',
            data: {'_token': CSRF_TOKEN, 'newCampaign': newCamp, 'allClients': allClients, 'campEnd': campEnd, 'campStart': campStart, 'campNotes': campNotes, 'ImagePath': ImagePath},
            success: function (data) {
                var myFile = new FormData($("#upload_form")[0]);
                myFile.append('campId', data);
                $.ajax({
                    url: '/uploadCampaignImage', // point to server-side PHP script 
                    dataType: 'json', // what to expect back from the PHP script, if anything
                    aync: true,
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: myFile,
                    type: 'post',
                    success: function (data) {
                        swal("Submit Successful");
                    },
                    error: function (data) {
                        swal("Oops! Error Occured.");
                    }
                });


                //    $('.campName').append('<option value="' + data + '_' + allClients + '">' + newCamp + '</option>');

            },
            error: function (data) {

                swal("Oops! Error Occured");
            }
        });
    }
}


function showQuest(campId)
{
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/getallquest',
        type: 'get',
        data: {'_token': CSRF_TOKEN, campId: campId},
        success: function (data) {

            $('#viewCamp').modal();
            $('.QuestionList').html(data);


        },
        error: function (data) {

            swal("Oops! Error Occured");
        }
    });
}

function delCamp(campId)
{
    $('#confirmdelete').modal();
    $('.myId').val(campId);
}

function deleteCamp()
{
    $('#confirmdelete').modal('toggle');
    var campId = $('.myId').val();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/delCamp',
        type: 'get',
        data: {'_token': CSRF_TOKEN, campId: campId},
        success: function (data) {

            $('.row_' + campId).hide();
        },
        error: function (data) {

            swal("Oops! Error Occured");
        }
    });
}


function editCamp(campId)
{
    $('#editCamp').modal();
    $('.editCampId').val(campId);
    $('#newCamp').val($('.campName_' + campId).html());
    $('#campStart').val($('.StartDate_' + campId).html());
    $('#campEnd').val($('.EndDate_' + campId).html());
    $('#campNotes').val($('.campDet_' + campId).html());
    $('.pic').html('<img src="public/images/CampaignImages/' + campId + '/' + $.trim($('.ImagePath_' + campId).html()) + '" width="70%" />');

}

function updateCamp()
{
    var campId = $('.editCampId').val();
    $('#showMsg').html("");
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var formData = $('#updateCampForm').serializeArray();
    var checkEmpty = [];
    var formVal = [];
    $.each(formData, function (count, val) {
        if (val.value == "")
        {
            checkEmpty.push(val.name);
        }
        formVal.push(val.value);
    });
    if (checkEmpty.length > 0)
    {
        $('#showMsg').html('<span style="color: red;">The ' + checkEmpty[0] + ' Field Is Required</span><div>&emsp;</div>');
        return false;
    }

    $.ajax({
        url: '/updateCamp',
        type: 'get',
        data: {'_token': CSRF_TOKEN, campId: campId, 'allVal': formVal},
        success: function (data) {
            $('#editCamp').modal('toggle');
            $('.campName_' + campId).html($('#newCamp').val());
            $('.StartDate_' + campId).html($('#campStart').val());
            $('.EndDate_' + campId).html($('#campEnd').val());
            $('.campDet_' + campId).html($('#campNotes').val());
            $('.ClientName_' + campId).html($('.clientParam').val());

        },
        error: function (data) {

            swal("Oops! Error Occured");
        }
    });





}

function getClients(sel)
{
    $('.clientParam').val(sel.options[sel.selectedIndex].text);
}

function addnewclient()
{
    var allData = $('#addClientForm').serializeArray();
    var checkEmpty = [];
    var formVal = [];
    $.each(allData, function (count, val) {
        if (val.value == null || val.value == "")
        {
            checkEmpty.push(val.name);
        }
        formVal.push(val.value);
    });

    if (checkEmpty.length > 0)
    {
        swal(checkEmpty[0] + "is a required field");
        return false;
    }

    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/addNewClient',
        type: 'get',
        data: {'_token': CSRF_TOKEN, 'allVal': formVal},
        success: function (data) {

            swal("Client Successfully Created");

        },
        error: function (data) {

            swal("Oops! Error Occured");
        }
    });
}

function deactivateCamp(campId)
{

}

function getResponseType()
{
    $('#actualQuest').html("");
    localStorage.setItem('saveFilter', null);
    localStorage.setItem('currentLimit', null);
    var mytable = $('#sample_3').DataTable();
    mytable.clear();
    mytable.draw();
    var campId = $('#camp').val();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/getFilterQuestions',
        type: 'get',
        data: {'_token': CSRF_TOKEN, 'campId': campId},
        success: function (data) {
            var response = "<div>&emsp;</div><div>&emsp;</div><b>List Of Questions Selected Campaigh</b><div>&emsp;</div><input type='radio' value='all' name='responseOpt' onclick='thisQuestion(\"" + "Not Applicable" + "\", \"" + campId + "\")'/>View All<div>&emsp;</div>";
            $.each(data, function (count, val) {
                response = response + "<input type='radio' value='" + val.id + "' name='responseOpt' onclick='thisQuestion(\"" + val.id + "\", \"" + campId + "\")'/>" + val.Question + "<div>&emsp;</div>";
            });

            $('#questList').html(response);

        },
        error: function (data) {

            swal("Oops! Error Occured");
        }
    });
}

function thisQuestion(questId, campId)
{
    if (questId == "Not Applicable")
    {
        $('#actualQuest').html("<div>&emsp;</div><div align='center'><input type='button' class='btn btn-primary btn-sm' value='Filter' onclick='filterBy(\"" + campId + "\", \"" + "getAll" + "\", \"" + "Not Applicable" + "\", \"" + "Normal" + "\")'></div>");
        $('#myPagination').html("<button type='button' class='btn btn-primary btn-sm' onclick='filterBy(\"" + campId + "\", \"" + "getAll" + "\", \"" + "Not Applicable" + "\", \"" + "Previous" + "\");'>Previous</button>&emsp;<button type='button' class='btn btn-primary btn-sm' onclick='filterBy(\"" + campId + "\", \"" + "getAll" + "\", \"" + "Not Applicable" + "\", \"" + "Next" + "\");'>Load More</button>");
    } else {
        localStorage.setItem('saveFilter', null);
        localStorage.setItem('currentLimit', null);
        var mytable = $('#sample_3').DataTable();
        mytable.clear();
        mytable.draw();

        $('#actualQuest').html("Loading");
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: '/getQuestOpt',
            type: 'get',
            data: {'_token': CSRF_TOKEN, 'questId': questId},
            success: function (data) {

                if (data != "") {
                    $('#actualQuest').html(data.split('__')[0] + "<div>&emsp;</div><div align='center'><input type='button' class='btn btn-primary btn-sm' value='Filter' onclick='filterBy(\"" + campId + "\", \"" + questId + "\", \"" + data.split('__')[1] + "\", \"" + "Normal" + "\")'></div>");
                    $('#myPagination').html("<button type='button' class='btn btn-primary btn-sm' onclick='filterBy(\"" + campId + "\", \"" + questId + "\", \"" + data.split('__')[1] + "\", \"" + "Previous" + "\")'>Previous</button>&emsp;<button type='button' class='btn btn-primary btn-sm' onclick='filterBy(\"" + campId + "\", \"" + questId + "\", \"" + data.split('__')[1] + "\", \"" + "Next" + "\")'>Load More</button>");

                } else
                {
                    $('#actualQuest').html("Campaign Has No Questions");
                }

                if (data.split('__')[1] == "myRange") {
                    $('#rangeSelect' + questId).ionRangeSlider({
                        hide_min_max: false,
                        keyboard: true,
                        min: data.split('__')[2],
                        max: data.split('__')[3],
                        type: 'single',
                        step: 1,
                        grid: true
                    });
                }
            },
            error: function (data) {
                $('#actualQuest').html("Error Occured");
                swal("Oops! Error Occured");
            }
        });
    }
}


function filterBy(campId, questId, questType, actionType)
{
    //$('#filterBody').html("");
    if ((actionType == "Next" || actionType == "Normal") && questId != "getAll") {
//        var mytable = $('#sample_3').DataTable();
//        mytable.clear();
//        mytable.draw();
        if (questType == "mySelect" || questType == "myRadio")
        {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var formResult = $('#filterForm').serializeArray();
            var filterOptionArray = [];
            $.each(formResult, function (count, val) {
                filterOptionArray.push(val.value);
            });
            var filterBy = filterOptionArray[0];
            var rowArays = [];
            $.ajax({
                url: '/filterAnswers',
                type: 'get',
                data: {'_token': CSRF_TOKEN, 'questId': questId, 'campId': campId, 'questType': questType, 'filterBy': filterBy, 'actionType': actionType},
                success: function (data) {
                   
                    if(data[0].length > 0){
                         var mytable = $('#sample_3').DataTable();
                    mytable.clear();
                    mytable.draw();
                    $.each(data[0], function (count1, val1) {

                        $.each(val1, function (count, val) {
                            rowArays = [];
                            rowArays.push(val.Surname + " " + val.Other_Name);
                            rowArays.push(val.State + " " + val.City);
                            rowArays.push(val.Phone_Number);
                            rowArays.push(val.Email);
                            rowArays.push(val.Gender);
                            rowArays.push(val.Age_Range);

                            if (rowArays.length > 0)
                            {
                                var mytable = $('#sample_3').DataTable();
                                mytable.row.add(rowArays);
                                mytable.draw();
                            } else {
                                // $('#filterBody').html("<tr><td colspan='6'>No Result Available</td></tr>");
                                var mytable = $('#sample_3').DataTable();
                                // mytable.row.remove();
                                mytable.clear();
                                mytable.draw();
                            }

                        });


                    });
                     $('#currentPage').html('Showing '+ data[1] + ' to ' +(parseInt(data[3])) + ' of '+data[2]+' entries');
            }
            
                },
                error: function (data) {

                    swal("Oops! Error Occured");
                }
            });
        } else if (questType == "myRange")
        {

            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var formResult = $('#filterForm').serializeArray();
            var filterOptionArray = [];
            $.each(formResult, function (count, val) {
                filterOptionArray.push(val.value);
            });
            var filterBy = filterOptionArray[0];
            $.ajax({
                url: '/filterRangeAnswers',
                type: 'get',
                data: {'_token': CSRF_TOKEN, 'questId': questId, 'campId': campId, 'questType': questType, 'filterBy': filterBy, 'actionType': actionType},
                success: function (data) {
                  
                    if (data[0].length > 0) {
                          var mytable = $('#sample_3').DataTable();
                        // mytable.row.remove();
                        mytable.clear();
                        mytable.draw();
                        $.each(data[0], function (count1, val1) {

                            $.each(val1, function (count, val) {
                                rowArays = [];
                                rowArays.push(val.Surname + " " + val.Other_Name);
                                rowArays.push(val.State + " " + val.City);
                                rowArays.push(val.Phone_Number);
                                rowArays.push(val.Email);
                                rowArays.push(val.Gender);
                                rowArays.push(val.Age_Range);

                                if (rowArays.length > 0)
                                {
                                    var mytable = $('#sample_3').DataTable();
                                    mytable.row.add(rowArays);
                                    mytable.draw();
                                } else {
                                    // $('#filterBody').html("<tr><td colspan='6'>No Result Available</td></tr>");
                                    var mytable = $('#sample_3').DataTable();
                                    // mytable.row.remove();
                                    mytable.clear();
                                    mytable.draw();
                                }

                            });


                        });
                        $('#currentPage').html('Showing '+ data[1] + ' to ' +(parseInt(data[3])) + ' of '+data[2]+' entries');
                    }
                    
                        
                },
                error: function (data) {

                    swal("Oops! Error Occured");
                }
            });
        } else if (questType.indexOf("myCheck") >= 0)
        {
//            var mytable = $('#sample_3').DataTable();
//            //   mytable.remove();
//            mytable.clear();
//            mytable.draw();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var formResult = $('#filterForm').serializeArray();
            var filterOptionArray = [];
            $.each(formResult, function (count, val) {
                filterOptionArray.push(val.value);
            });

            $.ajax({
                url: '/filterSelectAnswers',
                type: 'get',
                data: {'_token': CSRF_TOKEN, 'questId': questId, 'campId': campId, 'questType': questType, 'filterBy': filterOptionArray, 'actionType' : actionType},
                success: function (data) {

                  
                    if(data[0].length > 0){
                          var mytable = $('#sample_3').DataTable();
                    mytable.clear();
                    mytable.draw();
                    $.each(data[0], function (count1, val1) {

                        $.each(val1, function (count, val) {
                            rowArays = [];
                            rowArays.push(val.Surname + " " + val.Other_Name);
                            rowArays.push(val.State + " " + val.City);
                            rowArays.push(val.Phone_Number);
                            rowArays.push(val.Email);
                            rowArays.push(val.Gender);
                            rowArays.push(val.Age_Range);

                            if (rowArays.length > 0)
                            {
                                var mytable = $('#sample_3').DataTable();
                                mytable.row.add(rowArays);
                                mytable.draw();
                            } else {
                                // $('#filterBody').html("<tr><td colspan='6'>No Result Available</td></tr>");
                                var mytable = $('#sample_3').DataTable();
                                // mytable.row.remove();
                                mytable.clear();
                                mytable.draw();
                            }

                        });


                    });
                    $('#currentPage').html('Showing '+ data[1] + ' to ' +(parseInt(data[3])) + ' of '+data[2]+' entries');
            }
               

                },
                error: function (data) {

                    swal("Oops! Error Occured");
                }
            });
        }
    } else if (actionType == "Previous" && questId != "getAll") {

        if (questType == "mySelect" || questType == "myRadio")
        {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var formResult = $('#filterForm').serializeArray();
            var filterOptionArray = [];
            $.each(formResult, function (count, val) {
                filterOptionArray.push(val.value);
            });
            var filterBy = filterOptionArray[0];
            var rowArays = [];
            $.ajax({
                url: '/filterAnswers',
                type: 'get',
                data: {'_token': CSRF_TOKEN, 'questId': questId, 'campId': campId, 'questType': questType, 'filterBy': filterBy, 'actionType': actionType},
                success: function (data) {

                   
                    if(data[0].length > 0){
                          var mytable = $('#sample_3').DataTable();
                    mytable.clear();
                    mytable.draw();
                    $.each(data[0], function (count1, val1) {

                        $.each(val1, function (count, val) {
                            rowArays = [];
                            rowArays.push(val.Surname + " " + val.Other_Name);
                            rowArays.push(val.State + " " + val.City);
                            rowArays.push(val.Phone_Number);
                            rowArays.push(val.Email);
                            rowArays.push(val.Gender);
                            rowArays.push(val.Age_Range);

                            if (rowArays.length > 0)
                            {
                                var mytable = $('#sample_3').DataTable();
                                mytable.row.add(rowArays);
                                mytable.draw();
                            } else {
                                // $('#filterBody').html("<tr><td colspan='6'>No Result Available</td></tr>");
                                var mytable = $('#sample_3').DataTable();
                                // mytable.row.remove();
                                mytable.clear();
                                mytable.draw();
                            }

                        });


                    });
                     $('#currentPage').html('Showing '+ data[1] + ' to ' +(parseInt(data[3])) + ' of '+data[2]+' entries');
            }
                

                },
                error: function (data) {

                    swal("Oops! Error Occured");
                }
            });
        } else if (questType == "myRange")
        {
        
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var formResult = $('#filterForm').serializeArray();
            var filterOptionArray = [];
            $.each(formResult, function (count, val) {
                filterOptionArray.push(val.value);
            });
            var filterBy = filterOptionArray[0];
            $.ajax({
                url: '/filterRangeAnswers',
                type: 'get',
                data: {'_token': CSRF_TOKEN, 'questId': questId, 'campId': campId, 'questType': questType, 'filterBy': filterBy, 'actionType': actionType},
                success: function (data) {
                      
                    if (data[0].length > 0) {
                      var mytable = $('#sample_3').DataTable();
                        mytable.clear();
                        mytable.draw();
                        $.each(data[0], function (count1, val1) {
                            $.each(val1, function (count, val) {
                                rowArays = [];
                                rowArays.push(val.Surname + " " + val.Other_Name);
                                rowArays.push(val.State + " " + val.City);
                                rowArays.push(val.Phone_Number);
                                rowArays.push(val.Email);
                                rowArays.push(val.Gender);
                                rowArays.push(val.Age_Range);

                                if (rowArays.length > 0)
                                {
                                    var mytable = $('#sample_3').DataTable();
                                    mytable.row.add(rowArays);
                                    mytable.draw();
                                } else {
                                    // $('#filterBody').html("<tr><td colspan='6'>No Result Available</td></tr>");
                                    var mytable = $('#sample_3').DataTable();
                                    // mytable.row.remove();
                                    mytable.clear();
                                    mytable.draw();
                                }

                            });


                        });
                          $('#currentPage').html('Showing '+ data[1] + ' to ' +(parseInt(data[3])) + ' of '+data[2]+' entries');
                    }
                     
                },
                error: function (data) {

                    swal("Oops! Error Occured");
                }
            });
        } else if (questType.indexOf("myCheck") >= 0)
        {
//            var mytable = $('#sample_3').DataTable();
//            //   mytable.remove();
//            mytable.clear();
//            mytable.draw();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var formResult = $('#filterForm').serializeArray();
            var filterOptionArray = [];
            $.each(formResult, function (count, val) {
                filterOptionArray.push(val.value);
            });

            $.ajax({
                url: '/filterSelectAnswers',
                type: 'get',
                data: {'_token': CSRF_TOKEN, 'questId': questId, 'campId': campId, 'questType': questType, 'filterBy': filterOptionArray, 'actionType': actionType},
                success: function (data) {

                    
                     
                    if(data[0].length > 0){
                            var mytable = $('#sample_3').DataTable();
                    mytable.clear();
                    mytable.draw();
                    $.each(data[0], function (count1, val1) {

                        $.each(val1, function (count, val) {
                            rowArays = [];
                            rowArays.push(val.Surname + " " + val.Other_Name);
                            rowArays.push(val.State + " " + val.City);
                            rowArays.push(val.Phone_Number);
                            rowArays.push(val.Email);
                            rowArays.push(val.Gender);
                            rowArays.push(val.Age_Range);

                            if (rowArays.length > 0)
                            {
                                var mytable = $('#sample_3').DataTable();
                                mytable.row.add(rowArays);
                                mytable.draw();
                            } else {
                                // $('#filterBody').html("<tr><td colspan='6'>No Result Available</td></tr>");
                                var mytable = $('#sample_3').DataTable();
                                // mytable.row.remove();
                                mytable.clear();
                                mytable.draw();
                            }

                        });


                    });
                     $('#currentPage').html('Showing '+ data[1] + ' to ' +(parseInt(data[3])) + ' of '+data[2]+' entries');
            }
               


                },
                error: function (data) {

                    swal("Oops! Error Occured");
                }
            });
        }
    } else if (questId == "getAll")
    {
        if (actionType == "Normal" || actionType == "Next") {
          //  $('#actualQuest').html("Loading");
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var rowArays = [];
//            var mytable = $('#sample_3').DataTable();
//            mytable.clear();
//            mytable.draw();
            $.ajax({
                url: '/filterAll',
                type: 'get',
                data: {'_token': CSRF_TOKEN, 'campId': campId, 'actionType': actionType},
                success: function (data) {

                     
                    if(data[0].length > 0){
                         var mytable = $('#sample_3').DataTable();
                    mytable.clear();
                    mytable.draw();
                    $.each(data[0], function (count1, val1) {

                        $.each(val1, function (count, val) {
                            rowArays = [];
                            rowArays.push(val.Surname + " " + val.Other_Name);
                            rowArays.push(val.State + " " + val.City);
                            rowArays.push(val.Phone_Number);
                            rowArays.push(val.Email);
                            rowArays.push(val.Gender);
                            rowArays.push(val.Age_Range);

                            if (rowArays.length > 0)
                            {
                                var mytable = $('#sample_3').DataTable();
                                mytable.row.add(rowArays);
                                mytable.draw();
                            } else {
                                // $('#filterBody').html("<tr><td colspan='6'>No Result Available</td></tr>");
                                var mytable = $('#sample_3').DataTable();
                                // mytable.row.remove();
                                mytable.clear();
                                mytable.draw();
                            }

                        });


                    });
                     $('#currentPage').html('Showing '+ data[1] + ' to ' +(parseInt(data[3])) + ' of '+data[2]+' entries');
            }
                
                },
                error: function (data) {
                    $('#actualQuest').html("Error Occured");
                    swal("Oops! Error Occured");
                }
            });
        } else if (actionType == "Previous")
        {
//            $('#actualQuest').html("Loading");
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var rowArays = [];
//            var mytable = $('#sample_3').DataTable();
//            mytable.clear();
//            mytable.draw();
            $.ajax({
                url: '/filterAll',
                type: 'get',
                data: {'_token': CSRF_TOKEN, 'campId': campId, 'actionType': actionType},
                success: function (data) {


                   
                    if(data[0].length > 0){
                          var mytable = $('#sample_3').DataTable();
                    mytable.clear();
                    mytable.draw();
                    $.each(data[0], function (count1, val1) {

                        $.each(val1, function (count, val) {
                            rowArays = [];
                            rowArays.push(val.Surname + " " + val.Other_Name);
                            rowArays.push(val.State + " " + val.City);
                            rowArays.push(val.Phone_Number);
                            rowArays.push(val.Email);
                            rowArays.push(val.Gender);
                            rowArays.push(val.Age_Range);

                            if (rowArays.length > 0)
                            {
                                var mytable = $('#sample_3').DataTable();
                                mytable.row.add(rowArays);
                                mytable.draw();
                            } else {
                                // $('#filterBody').html("<tr><td colspan='6'>No Result Available</td></tr>");
                                var mytable = $('#sample_3').DataTable();
                                // mytable.row.remove();
                                mytable.clear();
                                mytable.draw();
                            }

                        });


                    });
                     $('#currentPage').html('Showing '+ data[1] + ' to ' +(parseInt(data[3])) + ' of '+data[2]+' entries');
            }
              

                },
                error: function (data) {
                    $('#actualQuest').html("Error Occured");
                    swal("Oops! Error Occured");
                }
            });
        }
    }
}


function deactivateCamp(campId)
{
       var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: '/suspendCapaign',
            type: 'get',
            data: {'_token': CSRF_TOKEN, 'campId': campId},
            success: function (data) {
                var Status = "";
                if(data == 1)
                {
                    Status = "green";
                }
                else
                {
                    Status = "red";
                }
                $('.toggleCamp_'+campId).html('<a onclick="deactivateCamp('+campId+')" style="color: '+Status+';" class="fa fa-power-off" ></a>');
            },
            error: function (data) {
                $('#actualQuest').html("Error Occured");
                swal("Oops! Error Occured");
            }
        });
}

function editClientModal(clientID)
{
     $('#editClient').modal();
    $('.clientID').val(clientID);
    $('#newClient').val($('.CompanyName_' + clientID).html());
    $('#ClientAdd').val($('.CompanyAddress_' + clientID).html());
    $('#clientName').val($('.UserName_' + clientID).html());
    $('#clientPassword').val($('.Password_' + clientID).html());
}

function editClient()
{
     $('#clientError').html('');
    var serialize = $('#updateClientForm').serializeArray();
    var checkEmpty = [];
    $.each(serialize, function(count, val)
    {
        if(val.value == "" || val.value == null)
        {
            checkEmpty.push(val.name);
        }
    });
    
    if(checkEmpty.length > 0)
    {
        $('#clientError').html('<span style="red">The '+checkEmpty[0]+' Field is Required</span>');
        return false;
    }
    var client = $('#newClient').val();
    var clientAdd = $('#ClientAdd').val();
   var username = $('#clientName').val();
   var password = $('#clientPassword').val();
    var clientID = $('.clientID').val();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: '/editClient',
            type: 'get',
            data: {'_token': CSRF_TOKEN, 'clientID': clientID, 'client': client, 'clientAdd': clientAdd, 'username': username, 'password': password},
            success: function (data) {
             $('.CompanyName_' + clientID).html(client);
    $('.CompanyAddress_' + clientID).html(clientAdd);
    $('.UserName_' + clientID).html(username);
    $('.Password_' + clientID).html(password);
            $('#editClient').modal('toggle');
                
            },
            error: function (data) {
                $('#actualQuest').html("Error Occured");
                swal("Oops! Error Occured");
            }
        });
}

function suspendClient(clientId)
{
       var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: '/suspendClient',
            type: 'get',
            data: {'_token': CSRF_TOKEN, 'clientId': clientId},
            success: function (data) {
                var Status = "";
                if(data == 1)
                {
                    Status = "green";
                }
                else
                {
                    Status = "red";
                }
                $('.toggleClient_'+clientId).html('<a onclick="suspendClient('+clientId+')" style="color: '+Status+';" class="fa fa-power-off" ></a>');
            },
            error: function (data) {
                $('#actualQuest').html("Error Occured");
                swal("Oops! Error Occured");
            }
        });
}

function delClient(clientId)
{
    $('#confirmdelete').modal();
    $('.myId').val(clientId);
}

function deleteClient()
{
 $('#confirmdelete').modal('toggle');
    var clientId = $('.myId').val();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/delClient',
        type: 'get',
        data: {'_token': CSRF_TOKEN, clientId: clientId},
        success: function (data) {
   
            $('.row_' + clientId).hide();
        },
        error: function (data) {

            swal("Oops! Error Occured");
        }
    });
}

function delUser(userID)
{
    $('#confirmdelete').modal();
    $('.myId').val(userID);
}


function deleteUser()
{
     $('#confirmdelete').modal('toggle');
    var userId = $('.myId').val();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/delUser',
        type: 'get',
        data: {'_token': CSRF_TOKEN, userId: userId},
        success: function (data) {
   
            $('.row_' + userId).hide();
        },
        error: function (data) {

            swal("Oops! Error Occured");
        }
    });
}

function suspendUser(userId)
{
       var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: '/suspendUser',
            type: 'get',
            data: {'_token': CSRF_TOKEN, 'userId': userId},
            success: function (data) {
                var Status = "";
                if(data == 1)
                {
                    Status = "green";
                }
                else
                {
                    Status = "red";
                }
                $('.toggleUser_'+userId).html('<a onclick="suspendUser('+userId+')" style="color: '+Status+';" class="fa fa-power-off" ></a>');
            },
            error: function (data) {
                $('#actualQuest').html("Error Occured");
                swal("Oops! Error Occured");
            }
        });
}

function suspendLead(userId)
{
       var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: '/suspendLead',
            type: 'get',
            data: {'_token': CSRF_TOKEN, 'userId': userId},
            success: function (data) {
                var Level = "";
                if(data == 1)
                {
                    Level = "green";
                }
                else
                {
                    Level = "red";
                }
               // <i onclick="suspendLead('<?php echo $eachUser->id; ?>')" style="cursor: pointer;" class="<?php if($eachUser->Level == 1){ echo 'fa fa-times'; }else{echo 'fa fa-check';} ?>" ></i>
              if(Level == "green")
              {
                   $('.toggleLead_'+userId).html('<a onclick="suspendLead('+userId+')" style="cursor: pointer;" class="fa fa-check" ></a>');
              }
              else
              {
                  $('.toggleLead_'+userId).html('<a onclick="suspendLead('+userId+')" style="cursor: pointer;" class="fa fa-times" ></a>');
              }
               
            },
            error: function (data) {
                $('#actualQuest').html("Error Occured");
                swal("Oops! Error Occured");
            }
        });
}

function allUsers(campId)
{

}