function userLogin()
{
    /*! jQuery v2.1.4 | (c) 2005, 2015 jQuery Foundation, Inc. | jquery.org/license */
    jQuery('.loginBtn').prop('disabled', true);
    jQuery('.loginBtn').html('Please Wait...');
    jQuery('#errorMsg').html("");
    var password = jQuery('#loginpassword').val();
    var username = jQuery('#LoginEmail').val();
    if (password == "" || username == "")
    {
        jQuery('#errorMsg').html('<span style="color: red;">Incorrect Username Or Password</span>');
jQuery('.loginBtn').prop('disabled', false);
    jQuery('.loginBtn').html('Log in with email');
        return false;
    }

    jQuery.ajax({
        url: '/SignIn',
        type: 'POST',
        data: {username: username, password: password},
        success: function (data) {
            if (data == "Invalid Profile")
            {
                jQuery('#errorMsg').html('<span style="color: red; margin-bottom: 20px;">Profile Is Inactive</span>');
jQuery('.loginBtn').prop('disabled', false);
    jQuery('.loginBtn').html('Log in with email');
                return false;

            } else if (data == "Incorrect Details")
            {
                jQuery('#errorMsg').html('<span style="color: red; margin-bottom: 20px;">Wrong username or password</span>');
jQuery('.loginBtn').prop('disabled', false);
    jQuery('.loginBtn').html('Log in with email');
                return false;
            } else
            {
                location.href = "/successful";
            }
        },
        error: function (data) {

            jQuery('#errorMsg').html('<span style="color: red; margin-bottom: 20px;">Error Occured</span>');
jQuery('.loginBtn').prop('disabled', false);
    jQuery('.loginBtn').html('Log in with email');
            return false;
        }
    });
}


function regNewMember()
{
jQuery('#errorHere').html('');

jQuery('.regBtn').prop('disable', true);
jQuery('.regBtn').html('Processing...');
 var CSRF_TOKEN = jQuery('meta[name="csrf-token"]').attr('content');
var allData = jQuery('#newmemberReg').serializeArray();
var errorArray = [];

jQuery.each(allData, function(count, val){


if(val.value == "")
{
errorArray.push(val.name);
}

});


if(errorArray.length > 0)
{
 jQuery('#errorHere').html(errorArray[0]+ ' Is Required');
jQuery('.regBtn').prop('disable', false);
jQuery('.regBtn').html('Register Now');
return false;
}


if(jQuery('#phone').val().length != 11)
{
 jQuery('#errorHere').html('Phone Number Must Be 11 Characters');
jQuery('.regBtn').prop('disable', false);
jQuery('.regBtn').html('Register Now');
return false;
}

 var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (!filter.test(jQuery('#email').val())) {
       jQuery('#errorHere').html('Email Not In Correct Format');
jQuery('.regBtn').prop('disable', false);
jQuery('.regBtn').html('Register Now');
return false;
    }
   

 
        jQuery.ajax({
            url: '/newmemberReg',
            type: 'get',
            data: {'_token': CSRF_TOKEN, 'Surname': allData[0].value, 'OtherName': allData[1].value, 'State': allData[2].value, 'City': allData[3].value, 'Phone': allData[4].value, 'Email': allData[5].value},
            success: function (data) {
         
            if(data == "Success")
{
jQuery('.regBtn').prop('disable', false);
jQuery('.regBtn').html('Register Now');
 //swal('Registeration Successful! \n We Have Sent A Link To Your Email For Account Confirmation.'); 
jQuery('.messageHere').html('Registeration Successful! \n We Have Sent A Link To Your Email For Account Confirmation.');
  jQuery('#regSuccess').modal();
}
else
{

jQuery('.messageHere').html(data);
jQuery('.regBtn').prop('disable', false);
jQuery('.regBtn').html('Register Now');
  jQuery('#regSuccess').modal();  
}
            },
            error: function (data) {
jQuery('.regBtn').prop('disable', false);
jQuery('.regBtn').html('Register Now');
             // jQuery('#errorHere').html("Oops! Error Occured");
jQuery('.messageHere').html('Oops! Error Occured');
  jQuery('#regSuccess').modal();
            }
        });

}


 jQuery("#phone").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        jQuery("#errorHere").html("Digits Only");
               return false;
    }
else
{
 jQuery("#errorHere").html("");
              

}
   });



function ForgotPasswordMail()
{
    var confirmMail = jQuery('#ConfirmEmail').val();
    jQuery('.forgot_password').attr('disabled', true);
    jQuery('#ForgptPasswordMsg').html("");
    var CSRF_TOKEN = jQuery('meta[name="csrf-token"]').attr('content');
    jQuery.ajax({
        url: '/forgotPassword',
        type: 'get',
        data: {
            _token: CSRF_TOKEN,
            confirmMail: confirmMail
        },
        success: function (data1) {
            if (data1 != "Check Your Mail To Complete Process") {
                jQuery('#ForgptPasswordMsg').html("<span style='color: red;'>" + data1 + "</span>");
            } else {
                jQuery('#ForgptPasswordMsg').html("<span style='color: green;'>" + data1 + "</span>");
            }
            jQuery('.forgot_password').attr('disabled', false);
        },
        error: function (data1) {
            jQuery('#ForgptPasswordMsg').html("<span style='color: red;'>Oops! Error Occured</span>");
            jQuery('.forgot_password').attr('disabled', false);
        }

    });

}


function showAnother()
{
    if (jQuery('.userLoginForm').is(":visible")) {
        jQuery('.userLoginForm').hide();
        jQuery('.userForgotPassword').show();
        jQuery('.topic').html("Forgot Password");
    } else
    {
        jQuery('.userLoginForm').show();
        jQuery('.userForgotPassword').hide();
        jQuery('.topic').html("Sign In");
    }

}
function contactSendMail(){

jQuery('#contactFormError').html('');

jQuery('#template-contactform-submit').prop('disable', true);

jQuery('#template-contactform-submit').html('Sending...');
 var CSRF_TOKEN = jQuery('meta[name="csrf-token"]').attr('content');
var allData = jQuery('#template-contactform').serializeArray();
var errorArray = [];
var passwordMatch = [];
var phone = [];
var state = "";
var city = "";
var passwordLength = [];
jQuery.each(allData, function(count, val){

  if(val.value == "")
{
errorArray.push(val.name+' Feild Is Required');
}

if(val.name == 'Email')
if(!isValidEmailAddress(jQuery('#template-contactform-email').val()))
{
errorArray.push('Email Not Valid');
}

});


if(errorArray.length > 0)
{
jQuery('#contactFormError').html('<span style="color: red;">'+errorArray[0]+'</span>');
jQuery('#template-contactform-submit').prop('disabled', false);
jQuery('#template-contactform-submit').html('Send Message');
return false;
}
  jQuery.ajax({
            url: '/sendMail',
            type: 'get',
            data: {'_token': CSRF_TOKEN, 'Name': allData[0].value, 'Email': allData[1].value, 'Subject': allData[2].value, 'Message': allData[3].value},
            success: function (data) {
         
if(data == "Success")
{
jQuery('#contactFormError').html('<span style="color: green;">Thank You. Message Sent.</span>');
jQuery('#template-contactform-submit').prop('disabled', false);
jQuery('#template-contactform-submit').html('Send Message');
}
else
{
jQuery('#contactFormError').html('<span style="color: red;">Oops! Something Went Wrong.</span>');
jQuery('#template-contactform-submit').prop('disabled', false);
jQuery('#template-contactform-submit').html('Send Message');

}
},
            error: function (data) {
jQuery('#contactFormError').html('<span style="color: red;">Oops! Error Occured.</span>');
jQuery('#template-contactform-submit').prop('disabled', false);
jQuery('#template-contactform-submit').html('Send Message');
            }
        });

}

function isValidEmailAddress(emailAddress) {
    var pattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return pattern.test(emailAddress);
};

function getCities()
{
    jQuery('#City').html("<option>Loading Cities...</option>");
    var selectedState = jQuery('#State').val();

    var cityObjects = {

        'Abuja': [
            'Gwagwalada',
            'Kuje',
            'Abaji',
            'Abuja Municipal',
            'Bwari',
            'Kwali'
        ],
        'Abia': [
            'Aba North',
            'Aba South',
            'Arochukwu',
            'Bende',
            'Ikwuano',
            'Isiala-Ngwa North',
            'Isiala-Ngwa South',
            'Isuikwato',
            'Obi Nwa',
            'Ohafia',
            'Osisioma',
            'Ngwa',
            'Ugwunagbo',
            'Ukwa East',
            'Ukwa West',
            'Umuahia North',
            'Umuahia South',
            'Umu-Neochi'
        ],
        'Adamawa': [
            'Demsa',
            'Fufore',
            'Ganaye',
            'Gireri',
            'Gombi',
            'Guyuk',
            'Hong',
            'Jada',
            'Lamurde',
            'Madagali',
            'Maiha',
            'Mayo-Belwa',
            'Michika',
            'Mubi North',
            'Mubi South',
            'Numan',
            'Shelleng',
            'Song',
            'Toungo',
            'Yola North',
            'Yola SouthDemsa',
            'Fufore',
            'Ganaye',
            'Gireri',
            'Gombi',
            'Guyuk',
            'Hong',
            'Jada',
            'Lamurde',
            'Madagali',
            'Maiha',
            'Mayo-Belwa',
            'Michika',
            'Mubi North',
            'Mubi South',
            'Numan',
            'Shelleng',
            'Song',
            'Toungo',
            'Yola North',
            'Yola South'
        ],
        'Akwa Ibom': [
            'Abak',
            'Eastern Obolo',
            'Eket',
            'Esit Eket',
            'Essien Udim',
            'Etim Ekpo',
            'Etinan',
            'Ibeno',
            'Ibesikpo Asutan',
            'Ibiono Ibom',
            'Ika',
            'Ikono',
            'Ikot Abasi',
            'Ikot Ekpene',
            'Ini',
            'Itu',
            'Mbo',
            'Mkpat Enin',
            'Nsit Atai',
            'Nsit Ibom',
            'Nsit Ubium',
            'Obot Akara',
            'Okobo',
            'Onna',
            'Oron',
            'Oruk Anam',
            'Udung Uko',
            'Ukanafun',
            'Uruan',
            'Urue-Offong/Oruko',
            'Uyo'
        ],
        'Anambra': [
            'Aguata',
            'Anambra East',
            'Anambra West',
            'Anaocha',
            'Awka North',
            'Awka South',
            'Ayamelum',
            'Dunukofia',
            'Ekwusigo',
            'Idemili North',
            'Idemili south',
            'Ihiala',
            'Njikoka',
            'Nnewi North',
            'Nnewi South',
            'Ogbaru',
            'Onitsha North',
            'Onitsha South',
            'Orumba North',
            'Orumba South',
            'Oyi'
        ],
        'Bauchi': [
            'Alkaleri',
            'Bauchi',
            'Bogoro',
            'Damban',
            'Darazo',
            'Dass',
            'Ganjuwa',
            'Giade',
            'Itas/Gadau',
            'Jamaare',
            'Katagum',
            'Kirfi',
            'Misau',
            'Ningi',
            'Shira',
            'Tafawa-Balewa',
            'Toro',
            'Warji',
            'Zaki'
        ],
        'Bayelsa': [
            'Brass',
            'Ekeremor',
            'Kolokuma/Opokuma',
            'Nembe',
            'Ogbia',
            'Sagbama',
            'Southern Jaw',
            'Yenegoa'
        ],
        'Benue': [
            'Ado',
            'Agatu',
            'Apa',
            'Buruku',
            'Gboko',
            'Guma',
            'Gwer East',
            'Gwer West',
            'Katsina-Ala',
            'Konshisha',
            'Kwande',
            'Logo',
            'Makurdi',
            'Obi',
            'Ogbadibo',
            'Oju',
            'Okpokwu',
            'Ohimini',
            'Oturkpo',
            'Tarka',
            'Ukum',
            'Ushongo',
            'Vandeikya',
        ],
        'Borno': [
            'Abadam',
            'Askira/Uba',
            'Bama',
            'Bayo',
            'Biu',
            'Chibok',
            'Damboa',
            'Dikwa',
            'Gubio',
            'Guzamala',
            'Gwoza',
            'Hawul',
            'Jere',
            'Kaga',
            'Kala/Balge',
            'Konduga',
            'Kukawa',
            'Kwaya Kusar',
            'Mafa',
            'Magumeri',
            'Maiduguri',
            'Marte',
            'Mobbar',
            'Monguno',
            'Ngala',
            'Nganzai',
            'Shani'
        ],
        'Cross River': [
            'Akpabuyo',
            'Odukpani',
            'Akamkpa',
            'Biase',
            'Abi',
            'Ikom',
            'Yarkur',
            'Odubra',
            'Boki',
            'Ogoja',
            'Yala',
            'Obanliku',
            'Obudu',
            'Calabar South',
            'Etung',
            'Bekwara',
            'Bakassi',
            'Calabar Municipality'
        ],
        'Delta': [
            'Oshimili',
            'Aniocha',
            'Aniocha South',
            'Ika South',
            'Ika North-East',
            'Ndokwa West',
            'Ndokwa East',
            'Isoko south',
            'Isoko North',
            'Bomadi',
            'Burutu',
            'Ughelli South',
            'Ughelli North',
            'Ethiope West',
            'Ethiope East',
            'Sapele',
            'Okpe',
            'Warri North',
            'Warri South',
            'Uvwie',
            'Udu',
            'Warri Central',
            'Ukwani',
            'Oshimili North',
            'Patani'
        ],
        'Ebonyi': [
            'Afikpo South',
            'Afikpo North',
            'Onicha',
            'Ohaozara',
            'Abakaliki',
            'Ishielu',
            'lkwo',
            'Ezza',
            'Ezza South',
            'Ohaukwu',
            'Ebonyi',
            'Ivo'
        ],
        'Edo': [
            'Esan North-East',
            'Esan Central',
            'Esan West',
            'Egor',
            'Ukpoba',
            'Central',
            'Etsako Central',
            'Igueben',
            'Oredo',
            'Ovia SouthWest',
            'Ovia South-East',
            'Orhionwon',
            'Uhunmwonde',
            'Etsako East',
            'Esan South-East'
        ],
        'Ekiti': [
            'Ado',
            'Ekiti-East',
            'Ekiti-West',
            'Emure/Ise/Orun',
            'Ekiti South-West',
            'Ikare',
            'Irepodun',
            'Ijero',
            'Ido/Osi',
            'Oye',
            'Ikole',
            'Moba',
            'Gbonyin',
            'Efon',
            'Ise/Orun',
            'Ilejemeje'
        ],
        'Enugu': [
            'Enugu South',
            'Igbo-Eze South',
            'Enugu North',
            'Nkanu',
            'Udi Agwu',
            'Oji-River',
            'Ezeagu',
            'IgboEze North',
            'Isi-Uzo',
            'Nsukka',
            'Igbo-Ekiti',
            'Uzo-Uwani',
            'Enugu Eas',
            'Aninri',
            'Nkanu East',
            'Udenu'
        ],
        'Gombe': [
            'Akko',
            'Balanga',
            'Billiri',
            'Dukku',
            'Kaltungo',
            'Kwami',
            'Shomgom',
            'Funakaye',
            'Gombe',
            'Nafada/Bajoga',
            'Yamaltu/Delta'
        ],
        'Imo': [
            'Aboh-Mbaise',
            'Ahiazu-Mbaise',
            'Ehime-Mbano',
            'Ezinihitte',
            'Ideato North',
            'Ideato South',
            'Ihitte/Uboma',
            'Ikeduru',
            'Isiala Mbano',
            'Isu',
            'Mbaitoli',
            'Mbaitoli',
            'Ngor-Okpala',
            'Njaba',
            'Nwangele',
            'Nkwerre',
            'Obowo',
            'Oguta',
            'Ohaji/Egbema',
            'Okigwe',
            'Orlu',
            'Orsu',
            'Oru East',
            'Oru West',
            'Owerri-Municipal',
            'Owerri North',
            'Owerri West'
        ],
        'Jigawa': [
            'Auyo',
            'Babura',
            'Birni Kudu',
            'Biriniwa',
            'Buji',
            'Dutse',
            'Gagarawa',
            'Garki',
            'Gumel',
            'Guri',
            'Gwaram',
            'Gwiwa',
            'Hadejia',
            'Jahun',
            'Kafin Hausa',
            'Kaugama Kazaure',
            'Kiri Kasamma',
            'Kiyawa',
            'Maigatari',
            'Malam Madori',
            'Miga',
            'Ringim',
            'Roni',
            'Sule-Tankarkar',
            'Taura',
            'Yankwashi'
        ],
        'Kaduna': [
            'Birni-Gwari',
            'Chikun',
            'Giwa',
            'Igabi',
            'Ikara',
            'jaba',
            'Jemaa',
            'Kachia',
            'Kaduna North',
            'Kaduna South',
            'Kagarko',
            'Kajuru',
            'Kaura',
            'Kauru',
            'Kubau',
            'Kudan',
            'Lere',
            'Makarfi',
            'Sabon-Gari',
            'Sanga',
            'Soba',
            'Zango-Kataf',
            'Zaria'
        ],
        'Kano': [
            'Ajingi',
            'Albasu',
            'Bagwai',
            'Bebeji',
            'Bichi',
            'Bunkure',
            'Dala',
            'Dambatta',
            'Dawakin Kudu',
            'Dawakin Tofa',
            'Doguwa',
            'Fagge',
            'Gabasawa',
            'Garko',
            'Garum',
            'Mallam',
            'Gaya',
            'Gezawa',
            'Gwale',
            'Gwarzo',
            'Kabo',
            'Kano Municipal',
            'Karaye',
            'Kibiya',
            'Kiru',
            'kumbotso',
            'Kunchi',
            'Kura',
            'Madobi',
            'Makoda',
            'Minjibir',
            'Nasarawa',
            'Rano',
            'Rimin Gado',
            'Rogo',
            'Shanono',
            'Sumaila',
            'Takali',
            'Tarauni',
            'Tofa',
            'Tsanyawa',
            'Tudun Wada',
            'Ungogo',
            'Warawa',
            'Wudil'
        ],
        'Katsina': [
            'Bakori',
            'Batagarawa',
            'Batsari',
            'Baure',
            'Bindawa',
            'Charanchi',
            'Dandume',
            'Danja',
            'Dan Musa',
            'Daura',
            'Dutsi',
            'Dutsin-Ma',
            'Faskari',
            'Funtua',
            'Ingawa',
            'Jibia',
            'Kafur',
            'Kaita',
            'Kankara',
            'Kankia',
            'Katsina',
            'Kurfi',
            'Kusada',
            'Mai-Adua',
            'Malumfashi',
            'Mani',
            'Mashi',
            'Matazuu',
            'Musawa',
            'Rimi',
            'Sabuwa',
            'Safana',
            'Sandamu',
            'Zango'
        ],
        'Kebbi': [
            'Aleiro',
            'Arewa-Dandi',
            'Argungu',
            'Augie',
            'Bagudo',
            'Birnin Kebbi',
            'Bunza',
            'Dandi',
            'Fakai',
            'Gwandu',
            'Jega',
            'Kalgo',
            'Koko/Besse',
            'Maiyama',
            'Ngaski',
            'Sakaba',
            'Shanga',
            'Suru',
            'Wasagu/Danko',
            'Yauri',
            'Zuru'
        ],
        'Kogi': [
            'Adavi',
            'Ajaokuta',
            'Ankpa',
            'Bassa',
            'Dekina',
            'Ibaji',
            'Idah',
            'Igalamela-Odolu',
            'Ijumu',
            'Kabba/Bunu',
            'Kogi',
            'Lokoja',
            'Mopa-Muro',
            'Ofu',
            'Ogori/Mangongo',
            'Okehi',
            'Okene',
            'Olamabolo',
            'Omala',
            'Yagba East',
            'Yagba West'
        ],
        'Kwara': [
            'Asa',
            'Baruten',
            'Edu',
            'Ekiti',
            'Ifelodun',
            'Ilorin East',
            'Ilorin West',
            'Irepodun',
            'Isin',
            'Kaiama',
            'Moro',
            'Offa',
            'Oke-Ero',
            'Oyun',
            'Pategi'
        ],
        'Lagos': [
            'Agege',
            'Ajeromi-Ifelodun',
            'Alimosho',
            'Amuwo-Odofin',
            'Apapa',
            'Badagry',
            'Epe',
            'Eti-Osa',
            'Ibeju/Lekki',
            'Ifako-Ijaye',
            'Ikeja',
            'Ikorodu',
            'Kosofe',
            'Lagos Island',
            'Lagos Mainland',
            'Mushin',
            'Ojo',
            'Oshodi-Isolo',
            'Shomolu',
            'Surulere'
        ],
        'Nassarawa': [
            'Akwanga',
            'Awe',
            'Doma',
            'Karu',
            'Keana',
            'Keffi',
            'Kokona',
            'Lafia',
            'Nasarawa',
            'Nasarawa-Eggon',
            'Obi',
            'Toto',
            'Wamba'
        ],
        'Niger': [
            'Agaie',
            'Agwara',
            'Bida',
            'Borgu',
            'Bosso',
            'Chanchaga',
            'Edati',
            'Gbako',
            'Gurara',
            'Katcha',
            'Kontagora',
            'Lapai',
            'Lavun',
            'Magama',
            'Mariga',
            'Mashegu',
            'Mokwa',
            'Muya',
            'Pailoro',
            'Rafi',
            'Rijau',
            'Shiroro',
            'Suleja',
            'Tafa',
            'Wushishi'
        ],
        'Ogun': [
            'Abeokuta North',
            'Abeokuta South',
            'Ado-Odo/Ota',
            'Egbado North',
            'Egbado South',
            'Ewekoro',
            'Ifo',
            'Ijebu East',
            'Ijebu North',
            'Ijebu North East',
            'Ijebu Ode',
            'Ikenne',
            'Imeko-Afon',
            'Ipokia',
            'Obafemi-Owode',
            'Ogun Waterside',
            'Odeda',
            'Odogbolu',
            'Remo North',
            'Shagamu'
        ],
        'Ondo': [
            'Akoko North East',
            'Akoko North West',
            'Akoko South Akure East',
            'Akoko South West',
            'Akure North',
            'Akure South',
            'Ese-Odo',
            'Idanre',
            'Ifedore',
            'Ilaje',
            'Ile-Oluji',
            'Okeigbo',
            'Irele',
            'Odigbo',
            'Okitipupa',
            'Ondo East',
            'Ondo West',
            'Ose',
            'Owo',
        ],
        'Osun': [
            'Aiyedade',
            'Aiyedire',
            'Atakumosa East',
            'Atakumosa West',
            'Boluwaduro',
            'Boripe',
            'Ede North',
            'Ede South',
            'Egbedore',
            'Ejigbo',
            'Ife Central',
            'Ife East',
            'Ife North',
            'Ife South',
            'Ifedayo',
            'Ifelodun',
            'Ila',
            'Ilesha East',
            'Ilesha West',
            'Irepodun',
            'Irewole',
            'Isokan',
            'Iwo',
            'Obokun',
            'Odo-Otin',
            'Ola-Oluwa',
            'Olorunda',
            'Oriade',
            'Orolu',
            'Osogbo'
        ],
        'Oyo': [
            'Afijio',
            'Akinyele',
            'Atiba',
            'Atigbo',
            'Egbeda',
            'IbadanCentral',
            'Ibadan North',
            'Ibadan North West',
            'Ibadan South East',
            'Ibadan South West',
            'Ibarapa Central',
            'Ibarapa East',
            'Ibarapa North',
            'Ido',
            'Irepo',
            'Iseyin',
            'Itesiwaju',
            'Iwajowa',
            'Kajola',
            'Lagelu Ogbomosho North',
            'Ogbmosho South',
            'Ogo Oluwa',
            'Olorunsogo',
            'Oluyole',
            'Ona-Ara',
            'Orelope',
            'Ori Ire',
            'Oyo East',
            'Oyo West',
            'Saki East',
            'Saki West',
            'Surulere'
        ],
        'Plateau': [
            'Barikin Ladi',
            'Bassa',
            'Bokkos',
            'Jos East',
            'Jos North',
            'Jos South',
            'Kanam',
            'Kanke',
            'Langtang North',
            'Langtang South',
            'Mangu',
            'Mikang',
            'Pankshin',
            'Qua-an Pan',
            'Riyom',
            'Shendam',
            'Wase'
        ],
        'Rivers': [
            'Abua/Odual',
            'Ahoada East',
            'Ahoada West',
            'Akuku Toru',
            'Andoni',
            'Asari-Toru',
            'Bonny',
            'Degema',
            'Emohua',
            'Eleme',
            'Etche',
            'Gokana',
            'Ikwerre',
            'Khana',
            'Obia/Akpor',
            'Ogba/Egbema/Ndoni',
            'Ogu/Bolo',
            'Okrika',
            'Omumma',
            'Opobo/Nkoro',
            'Oyigbo',
            'Port-Harcourt',
            'Tai'
        ],
        'Sokoto': [
            'Binji',
            'Bodinga',
            'Dange-shnsi',
            'Gada',
            'Goronyo',
            'Gudu',
            'Gawabawa',
            'Illela',
            'Isa',
            'Kware',
            'kebbe',
            'Rabah',
            'Sabon birni',
            'Shagari',
            'Silame',
            'Sokoto North',
            'Sokoto South',
            'Tambuwal',
            'Tqngaza',
            'Tureta',
            'Wamako',
            'Wurno',
            'Yabo'
        ],
        'Taraba': [
            'Ardo-kola',
            'Bali',
            'Donga',
            'Gashaka',
            'Cassol',
            'Ibi',
            'Jalingo',
            'Karin-Lamido',
            'Kurmi',
            'Lau',
            'Sardauna',
            'Takum',
            'Ussa',
            'Wukari',
            'Yorro',
            'Zing'
        ],
        'Yobe': [
            'Bade',
            'Bursari',
            'Damaturu',
            'Fika',
            'Fune',
            'Geidam',
            'Gujba',
            'Gulani',
            'Jakusko',
            'Karasuwa',
            'Karawa'
        ],
        'Zamfara': [
            'Anka',
            'Bakura',
            'Birnin Magaji/Kiyaw',
            'Bukkuyum',
            'Bungudu',
            'Chafe',
            'Gummi',
            'Gusau',
            'Kaura Namoda',
            'Maradun',
            'Maru',
            'Shinkafi',
            'Talata Mafara',
            'Zurmi'
        ]
    };

    var eachCity = "<option value=''>Your City</option>";
    if (selectedState != "") {
        jQuery.each(cityObjects[selectedState], function (count, val) {
            eachCity = eachCity + "<option value'" + val + "'>" + val + "</option>";
        }); 
    }
    jQuery('#City').html(eachCity);
}



function onIt(IconBox, fa){

jQuery('.'+IconBox).addClass('newIconBoxColor');

}

function nowOut(IconBox, fa){

jQuery('.'+IconBox).removeClass('newIconBoxColor');

}