//this is my function to handle onload events
$(function(d, s, id) {


//set boxes to show
localStorage.setItem('BoxesFrom', 1);
localStorage.setItem('BoxesTo', 21);
//set boxes to show
$('.campBoxes').hide();


//for(var i = 1; i<=2; i++){
//$('.boxCount_'+i).show();
//}

//side bars for admin panel
var sideBarContent =  '<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->'+
                            '<!-- BEGIN SIDEBAR TOGGLER BUTTON -->'+
                            '<li class="sidebar-toggler-wrapper hide">'+
                                '<div class="sidebar-toggler">'+
                                   ' <span></span>'+
                                '</div>'+
                            '</li>'+
                            '<!-- END SIDEBAR TOGGLER BUTTON -->'+
                            '<!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->'+
                            '<li class="sidebar-search-wrapper">'+
                                '<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->'+
                                '<!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->'+
                                '<!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->'+
                                '<form class="sidebar-search  " action="page_general_search_3.html" method="POST">'+
                                   ' <a href="javascript:;" class="remove">'+
                                        '<i class="icon-close"></i>'+
                                    '</a>'+
                                    '<div class="input-group">'+
                                       
                                    '</div>'+
                                '</form>'+
                               ' <!-- END RESPONSIVE QUICK SEARCH FORM -->'+
                            '</li>'+
                            '<li class="nav-item start active open">'+
                                '<a href="/AdminPage" class="nav-link nav-toggle">'+
                                    '<i class="icon-home"></i>'+
                                    '<span class="title">Dashboard</span>'+
                                   ' <span class="selected"></span>'+
                                 ' <!--  <span class="arrow open"></span> -->'+
                                '</a>'+
                               
                            '</li>'+
                           ' <li class="heading">'+
                               ' <h3 class="uppercase"></h3>'+
                            '</li>'+

                             '<li class="nav-item">'+
                             ' <a class="nav-link nav-toggle" onclick="openSubTopic(1)">'+
                                   ' <i class="icon-diamond"></i>'+
                                    '<span class="title">Campaigns</span>'+
                                    '<span class="arrow"></span>'+
                               ' </a>'+
                               '<ul style="display: none;" class="subTopic1">'+
                                ' <li class="nav-item" style="margin-bottom: 16px;margin-top: 8px;">'+
                               ' <a href="/CampaignList" class="nav-link nav-toggle" style="color: #b4bcc8;text-decoration: none;">'+
                                    '<!--<i class="icon-diamond"></i>-->'+
                                    '<span class="title">View Campaigns</span>'+
                                    '<span class="arrow"></span>'+
                               ' </a>'+
                                 '</li>'+
                                 '<li class="nav-item" style="margin-bottom: 16px;margin-top: 8px;">'+
                                '<a href="/AddCampaign" class="nav-link nav-toggle" style="color: #b4bcc8;text-decoration: none;">'+
                                    '<!--<i class="icon-diamond"></i>-->'+
                                   ' <span class="title">Create Campaign</span>'+
                                    '<span class="arrow"></span>'+
                                '</a>'+
                                 '</li>'+
                                 '</li>'+
                               ' <li class="nav-item" style="margin-bottom: 16px;margin-top: 8px;">'+
                                '<a href="/FilterCampaign" class="nav-link nav-toggle" style="color: #b4bcc8;text-decoration: none;">'+
                                    '<!--<i class="icon-diamond"></i>-->'+
                                   ' <span class="title">Filter Campaigns</span>'+
                                   ' <span class="arrow"></span>'+
                                '</a>'+
                                ' </li>'+
                                ' <li class="nav-item" style="margin-bottom: 16px;margin-top: 8px;">'+
                                '<a href="/QuestionManager" class="nav-link nav-toggle" style="color: #b4bcc8;text-decoration: none;">'+
                                   '<!--<i class="icon-diamond"></i>-->'+
                                   ' <span class="title">Question Manager</span>'+
                                  '  <span class="arrow"></span>'+
                                '</a>'+
                                ' </li>'+
                               ' </ul>'+

                            '</li>'+
                           
                            '<li class="nav-item  ">'+
                                '<a class="nav-link nav-toggle" onclick="openSubTopic(2)">'+
                                    '<i class="icon-diamond"></i>'+
                                    '<span class="title">Clients</span>'+
                                    '<span class="arrow"></span>'+
                                '</a>'+
                                  '<ul style="display: none;" class="subTopic2">'+
                                ' <li class="nav-item" style="margin-bottom: 16px;margin-top: 8px;">'+
                               ' <a href="/ViewClient" class="nav-link nav-toggle" style="color: #b4bcc8;text-decoration: none;">'+
                                    '<!--<i class="icon-diamond"></i>-->'+
                                    '<span class="title">View Client</span>'+
                                    '<span class="arrow"></span>'+
                               ' </a>'+
                                 '</li>'+
                                   ' <li class="nav-item" style="margin-bottom: 16px;margin-top: 8px;">'+
                               ' <a href="/CreateClient" class="nav-link nav-toggle" style="color: #b4bcc8;text-decoration: none;">'+
                                    '<!--<i class="icon-diamond"></i>-->'+
                                    '<span class="title">Create Client</span>'+
                                    '<span class="arrow"></span>'+
                               ' </a>'+
                                 '</li>'+
                                   '</ul>'+
                            '</li>'+
                           '<li class="nav-item  ">'+
                               ' <a href="/viewusers" class="nav-link nav-toggle">'+
                                   ' <i class="icon-diamond"></i>'+
                                   ' <span class="title">Users</span>'+
                                   ' <!--<span class="arrow"></span>-->'+
                                '</a>'+
                            '</li>'+
                            '<li class="nav-item  ">'+
                                '<a href="/cashRequest" class="nav-link nav-toggle">'+
                                    '<i class="icon-diamond"></i>'+
                                    '<span class="title">Cash Requests</span>'+
                                   '<!-- <span class="arrow"></span>-->'+
                               ' </a>'+
                            '</li>'+
                           
                            '<li class="nav-item  ">'+
                                '<a class="nav-link nav-toggle" onclick="openSubTopic(5)">'+
                                    '<i class="icon-diamond"></i>'+
                                    '<span class="title">Rewards</span>'+
                                    '<span class="arrow"></span>'+
                                '</a>'+
                                  '<ul style="display: none;" class="subTopic5">'+
                                ' <li class="nav-item" style="margin-bottom: 16px;margin-top: 8px;">'+
                               ' <a href="/SpecialPrizes" class="nav-link nav-toggle" style="color: #b4bcc8;text-decoration: none;">'+
                                    '<!--<i class="icon-diamond"></i>-->'+
                                    '<span class="title">View Rewards</span>'+
                                    '<span class="arrow"></span>'+
                               ' </a>'+
                                 '</li>'+
                                   ' <li class="nav-item" style="margin-bottom: 16px;margin-top: 8px;">'+
                               ' <a href="/CreateReward" class="nav-link nav-toggle" style="color: #b4bcc8;text-decoration: none;">'+
                                   '<!--<i class="icon-diamond"></i>-->'+
                                    '<span class="title">Create Reward</span>'+
                                    '<span class="arrow"></span>'+
                               ' </a>'+
                                 '</li>'+
                                   '</ul>'+
                            '</li>';
                        $('.menuBar').html(sideBarContent);

//this is the facebook share code
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id))
        return;
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/en_US/all.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function showModal()
{
    $('#myModal1').modal('show');
}




$("#phone").keypress(function (e) {
    //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $("#updateRegError").html("Digits Only");
        return false;
    }
    else
    {
        $("#updateRegError").html("");
              

    }
});



//this code is user to filter user activity history on the viewuser page of admin panel
function filterHistory()
{

    var fromDate = $('.fromDate').val();
    fromDate = fromDate.split('/')[2]+'-'+fromDate.split('/')[0]+'-'+fromDate.split('/')[1];
    var toDate = $('.toDate').val();
    toDate = toDate.split('/')[2]+'-'+toDate.split('/')[0]+'-'+toDate.split('/')[1];
    var userId = $('#userId').val();
    var userName = $('.FullName_'+userId).html();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
   $('#userNameHere').html(userName);

$('#historyBody').html('<tr><td colspan="5">Loading...</td></tr>');
    $.ajax({
        url: '/filterHistory',
        type: 'get',
        data:{ _token: CSRF_TOKEN, fromDate: fromDate, toDate: toDate, userId: userId },
        success: function (data) {
            var dataToShow ='';
                    var myTable = $('#individualBalanceHistory').DataTable();
                 myTable.clear();
myTable.draw();

var rowArray = [];
            $.each(data.reverse(), function(count, value){
              rowArray = [];
              rowArray.push(userName);
                   rowArray.push(value[0]);
                     rowArray.push(value[1]);
                    rowArray.push(value[2]);
                    rowArray.push(value[3]);
                
              myTable.row.add(rowArray);
              myTable.draw();
            });
            if(data.length < 1)
            {
                var myTable = $('#individualBalanceHistory').DataTable();
                 myTable.clear();
myTable.draw();
            }
           // $('#fillHistory').html(dataToShow+'</tbody>' );
            $('#individualBalanceHistory').DataTable();
            $('.toDate').datepicker();
            $('.fromDate').datepicker();
        },
        error: function (data) {
            swal('Error Occured');
        }
    });

}

//this function blinks the money icon when user has reached a point to request for withdrawal
(function blink() { 
    $('.blink_me').fadeOut(500).fadeIn(500, blink); 
})();




//show modal to confirm if cash request is accepted
function makePayment(rowId, id)
{
    $('#confirmPayment').modal();
    $('.myId').val(id);
    $('.rowId').val(rowId);
}


//confirm cash request acceptance
function paymentConfirmed()
{
    var userId = $('.myId').val();
    var rowId = $('.rowId').val();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/sendUserMailForCashCollection',
        type: 'get',
        data:{ _token: CSRF_TOKEN, userId: userId, action: 1 },
        success: function (data) {
            $('#confirmPayment').modal('toggle');
            $('.row_'+rowId).remove();
            swal('A mail has been sent to the user for cash collection');
        },
        error: function (data) {
            swal('Error Occured');
        }
    });

}


//show modal to decline cash request
function declinePayment(rowId, id)
{
    $('#declinePayment').modal();
    $('.myId').val(id);
    $('.rowId').val(rowId);
}

//decline cash request
function paymentDeclined()
{

    var userId = $('.myId').val();
    var rowId = $('.rowId').val();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/sendUserMailForCashCollection',
        type: 'get',
        data:{ _token: CSRF_TOKEN, userId: userId, action: 0 },
        success: function (data) {
            $('#declinePayment').modal('toggle');
            $('.row_'+rowId).remove();
            swal('Payment Declined');
        },
        error: function (data) {
            swal('Error Occured');
        }
    });

}


//clear fliter history on the user history filter modal
function clearFilterHistory()
{

    $('.toDate').val('');
    $('.fromDate').val('');
    var userId = $('#userId').val();
    var userName = $('.FullName_'+userId).html();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
 $('#userNameHere').html(userName);
    $.ajax({
        url: '/getIndividualBalanceHistory',
        type: 'get',
        data:{ _token: CSRF_TOKEN, userId: userId },
        success: function (data) {
           var dataToShow ='';
                    var myTable = $('#individualBalanceHistory').DataTable();
                 myTable.clear();
myTable.draw();
$('#historyBody').html('<tr><td colspan="5">Loading...</td></tr>');
var rowArray = [];
            $.each(data.reverse(), function(count, value){
              rowArray = [];
              rowArray.push(userName);
                   rowArray.push(value[0]);
                     rowArray.push(value[1]);
                    rowArray.push(value[2]);
                    rowArray.push(value[3]);
                
              myTable.row.add(rowArray);
              myTable.draw();
            });
            if(data.length < 1)
            {
                var myTable = $('#individualBalanceHistory').DataTable();
                 myTable.clear();
myTable.draw();
            }
           // $('#fillHistory').html(dataToShow+'</tbody>' );
            $('#individualBalanceHistory').DataTable();
            $('.toDate').datepicker();
            $('.fromDate').datepicker();
            $('#userBalanceHistory').modal();
            //set userId to hidden input
            $('#userId').val(userId);
        },
        error: function (data) {
            swal('Error Occured');
        }
    });
}

//get balance history of individual on the admin's viewuser page
function viewBalanceHistory(userId)
{

    var userName = $('.FullName_'+userId).html();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
$('.fromDate').val('');
$('.toDate').val('');
 $('#userNameHere').html(userName);
    $.ajax({
        url: '/getIndividualBalanceHistory',
        type: 'get',
        data:{ _token: CSRF_TOKEN, userId: userId },
        success: function (data) {

          var dataToShow ='';
                    var myTable = $('#individualBalanceHistory').DataTable();
                 myTable.clear();
myTable.draw();
$('#historyBody').html('<tr><td colspan="5">Loading...</td></tr>');
var rowArray = [];
            $.each(data.reverse(), function(count, value){
              rowArray = [];
              rowArray.push(userName);
                   rowArray.push(value[0]);
                     rowArray.push(value[1]);
                    rowArray.push(value[2]);
                    rowArray.push(value[3]);
                
              myTable.row.add(rowArray);
              myTable.draw();
            });
            if(data.length < 1)
            {
                var myTable = $('#individualBalanceHistory').DataTable();
                 myTable.clear();
myTable.draw();
            }
           // $('#fillHistory').html(dataToShow+'</tbody>' );
            $('#individualBalanceHistory').DataTable();
            $('.toDate').datepicker();
            $('.fromDate').datepicker();
            $('#userBalanceHistory').modal();
            //set userId to hidden input
            $('#userId').val(userId);
        },
        error: function (data) {
            swal('Error Occured');
        }
    });

}



//i used this to toggle button which opens and folds up forms, but now i didnt use anymore because each forms are now separated to their own //pages and made as sub-menus for each module on the admin panel
function myToggleBtn() {
    if ($('.togglerBtn').html() == "Add New") {
        $('.togglerBtn').html("Fold Up");
        //$('.togglerBtn').prop('disabled', true);
    }
    else {
        $('.togglerBtn').html("Add New");
        //$('.togglerBtn').prop('disabled', false);
    }
}




function uploadCampaignImage()
{
    var ppName = $('#fupload').val();
    var ext = ppName.split('.')[1].toLowerCase();
    if (ext != "jpg" && ext != "jpeg" && ext != "png")
    {
        $('.imgError').html("Oops! File no supported.");
    } else
    {

        $.ajax({
            url: '/uploadCampaignImage', // point to server-side PHP script 
            dataType: 'json', // what to expect back from the PHP script, if anything
            aync: true,
            cache: false,
            contentType: false,
            processData: false,
            data: new FormData($("#upload_form")[0]),
            type: 'post',
            success: function (data) {
                // alert(data);
                //  var myP = ppName.split('\\')[2];
                // $('.pic').html('<img src="public/images/CampaignImages/'+campId+'/'+myP+'" width="70%" />');

            },
            error: function (data) {
                swal("Oops! Error Occured.");
            }
        });



    }
}

function updateCampaignImg()
{
    var campId = $('.editCampId').val();
    var initialImg = $('.pic').html();
    $('.pic').html("Uploading...");
    var ppName = $('#updateUpload').val();
    var ext = ppName.split('.')[1].toLowerCase();
    if (ext != "jpg" && ext != "jpeg" && ext != "png")
    {
        $('.pic').html(initialImg);
        $('.imgError').html("Oops! File no supported.");
    } else
    {
        var myFile = new FormData($("#updateCampForm")[0]);
        myFile.append('campId', campId);
        $.ajax({
            url: '/updateCampaignImg', // point to server-side PHP script 
            dataType: 'json', // what to expect back from the PHP script, if anything
            aync: true,
            cache: false,
            contentType: false,
            processData: false,
            data: myFile,
            type: 'post',
            success: function (data) {
                // alert(data);
                var myP = ppName.split('\\')[2];
                $('.ImagePath_' + campId).html(myP);
                $('.pic').html('<img src="public/images/CampaignImages/' + campId + '/' + myP + '" width="70%" />');

            },
            error: function (data) {
                $('.pic').html(initialImg);
                swal("Oops! Error Occured.");
            }
        });



    }
}



function inputBuilder(AnswerType, optionNum, responseType)
{
    //for textfield
    if (AnswerType == "myTextfield")
    {
        return "<input type='text' name='val_subforcount' class='form-control toDisable_subforcampid' id='val_subforcount' >";
    } else if (AnswerType == "myTextarea")
    {
        return "<textarea name='val_subforcount' class='form-control toDisable_subforcampid' id='val_subforcount'></textarea>";
    } else if (AnswerType == "myRadio")
    {
        var myInput = "";

        $.each(responseType, function (count, val) {

            myInput = myInput + "<input type='radio' name='val_subforcount' class='toDisable_subforcampid' id='val_subforcount' value='" + val + "' >" + val + "&emsp;";

        });

        return myInput;
    } else if (AnswerType == "mySelect")
    {
        var myInput = "<select name='val_subforcount' class='form-control toDisable_subforcampid' id='val_subforcount' >";

        $.each(responseType, function (count, val) {

            myInput = myInput + "<option value='" + val + "'>" + val + "</option>";

        });

        myInput = myInput + "</select>";

        return myInput;
    } else if (AnswerType == "myCheck")
    {
        var myInput = "";

        $.each(responseType, function (count, val) {

            myInput = myInput + "<input type='checkbox' name='val_subforcount' class=' toDisable_subforcampid' id='val_subforcount' value='" + val + "' >" + val + "&emsp;";

            // $('.checkBox_Response').tagsinput();
        });

        return myInput;
    } else if (AnswerType == "myRange")
    {

        // return "<input type='range' min='1' max='100' step='1' name='val_subforcount' class='form-control toDisable_subforcampid' id='val_subforcount'>&emsp;";
        return "<input id='range_1' type='text' name='val_subforcount' class='form-control toDisable_subforcampid' >&emsp;";
    }

}

function getType()
{
    $('.checks').remove();
    var responseType = $('#myResponse').val();
    AnswerType = responseType;
    optionNum = 0;
    if(responseType == "")
{
$('.moreOptions').html('');
}
 else if (responseType == "myRadio" || responseType == "mySelect")
    {
        var opt = '<br><br><form method="post"><div class="optConfig"><div class="myOpt_0" style="margin-bottom: 20px;"><input type="text" class="opt_0 form-control " placeholder="Enter Options"/><!--<span><input class="" type="radio" name="Expected" id="Expected_0" value="opt_0"/></span>--></div></div><br><div class=""><button type="button" class="addmore btn btn-success" onclick="addto(\'' + responseType + '\');">+</button>&emsp;<button class="remove btn btn-success" type="button" onclick="removeFrom();">-</button></div></form>';
        $('.moreOptions').html(opt);
    } else if (responseType == "myTextfield" || responseType == "myTextarea")
    {
        var opt = '<br><br><form method="post"><div class="optConfig"><div class="myOpt_0" style="margin-bottom: 20px;"> <input type="text" class="opt_0 form-control KeyTexts" placeholder="Enter Key-Words Separated With Comma(,)" data-role="tagsinput"></div></div><br></form>';
        $('.moreOptions').html(opt);
        $('.opt_0').tagsinput();
    } else if (responseType == "myCheck")
    {
        var opt = '<br><br><form method="post"><div class="optConfig"><div class="myOpt_0" style="margin-bottom: 20px;"> <input type="text" class="opt_0 form-control CheckBoxKeyTexts" placeholder="Enter Options"><!--<span><input class="" type="checkbox" name="ExpectedBox" id="ExpectedBox_0" value="opt_0"/></span>--></div></div><br><br><div class=""><button type="button" class="addmore btn btn-success" onclick="addto(\'' + responseType + '\');">+</button>&emsp;<button class="remove btn btn-success" type="button" onclick="removeFrom();">-</button></div></form>';

        //$('.rightSide').after('<div class="form-group checks">' +
            //    ' <label class="col-md-3 control-label">Select Response Logic</label>' +
               // '<div class="col-md-4">' +
             //   '<input type="radio" name="logic" class="andLogic" value="AND"> AND &emsp; <input type="radio" name="logic" class="orLogic" value="OR"> OR &emsp;' +
               // ' </div>' +
               // ' </div>'
              //  );


        $('.moreOptions').html(opt);
        //          $('.rightSide').after('<div class="form-group">'+
        //                                           ' <label class="col-md-3 control-label"></label>'+
        //                                            '<div class="col-md-4">'+
        //                                                '<input type="text" class="checkBox_Response form-control" placeholder="Enter Key-Words Separated With Comma(,)" data-role="tagsinput">'+
        //
        //                                           ' </div>'+
        //                                       ' </div>'
        //                        );
        //                $('.checkBox_Response').tagsinput();

    } else {

        var opt = '<br><br>';

        $('.rightSide').after('<div class="form-group checks">' +
                ' <label class="col-md-3 control-label"> Range Max</label>' +
                '<div class="col-md-4">' +
                '<input type="text" name="rangemax" class="form-control rangemax" value="" placeholder="Enter Range Max">' +
                ' </div>' +
                ' </div>' +
                '<div class="form-group checks">' +
                ' <label class="col-md-3 control-label"> Range Min</label>' +
                '<div class="col-md-4">' +
                '<input type="text" name="rangemin" class="form-control rangemin" value="" placeholder="Enter Range Min">' +
                ' </div>' +
                ' </div>' +
                '<!--<div class="form-group checks">' +
                ' <label class="col-md-3 control-label"> Least Value</label>' +
                '<div class="col-md-4">' +
                '<input type="text" name="leastval" class="form-control leastval" value="" placeholder="Enter Least Value">' +
                ' </div>' +
                ' </div> -->' +
                '<div class="form-group checks">' +
                ' <label class="col-md-3 control-label"> Min-Value Label</label>' +
                '<div class="col-md-4">' +
                '<input type="text" name="minvaldesc" class="form-control minvaldesc " value="" placeholder="Enter Minimum Value Label">' +
                ' </div>' +
                ' </div>' +
                '<div class="form-group checks">' +
                ' <label class="col-md-3 control-label"> Max-Value Label</label>' +
                '<div class="col-md-4">' +
                '<input type="text" name="maxvaldesc" class="form-control maxvaldesc" value="" placeholder="Enter Maximum Value Label">' +
                ' </div>' +
                ' </div>'
                );
        $('.moreOptions').html(opt);

    }

}


function addto(responseType)
{
    if (responseType == "myRadio" || responseType == "mySelect") {
        optionNum++;
        $('.myOpt_' + (optionNum - 1)).after('<div class="myOpt_' + optionNum + '" style="margin-bottom: 20px;"><input type="text" class="opt_' + optionNum + ' form-control " placeholder="Enter Options"/><!--<span><input class="" type="radio" name="Expected" id="Expected_' + optionNum + '" value="opt_' + optionNum + '"/></span>--></div>');

    } else if (responseType == "myTextfield" || responseType == "myTextarea")
    {
        optionNum++;
        $('.myOpt_' + (optionNum - 1)).after('<div class="myOpt_' + optionNum + '" style="margin-bottom: 20px;"><input type="text" class="opt_' + optionNum + ' form-control KeyTexts" placeholder="Enter Key-Words Separated With Comma(,)" data-role="tagsinput"></div>');
        $('.opt_' + (optionNum)).tagsinput();
    } else if (responseType == "myCheck")
    {
        optionNum++;
        $('.myOpt_' + (optionNum - 1)).after('<div class="myOpt_' + optionNum + '" style="margin-bottom: 20px;"><input type="text" class="opt_' + optionNum + ' form-control" placeholder="Enter Options"><!--<span><input class="" type="checkbox" name="ExpectedBox" id="ExpectedBox_' + optionNum + '" value="opt_' + optionNum + '"/></span>--></div>');

    }


}

function removeFrom()
{
    if (optionNum > 0) {
        $('.myOpt_' + optionNum).remove();
        optionNum--;

    }

}


function clearCamp()
{

$('#myResponse').val("");
$('.moreOptions').html('');
$('.nicEdit-main').html('');
$('.checks').html('');

}

function saveCampQuest()
{
   var actionType = $('.actionType').val();
    if(actionType == "Add")
    {

        var responseExpected = "";
        var ifEmpty = [];
        var checkRadios = [];
        var checkBoxes = [];
        var campAnswers = $('.addQuestForm').serializeArray();
        $.each(campAnswers, function (count, val) {
           
 //i just added leastval so it wont be mandatory
            if (val.value == "" && val.name != "myResponse" && val.name != "campQuest" && val.name != "leastval") {
                ifEmpty.push(val.value);
            }


           /* if (val.name == "Expected")
            {
               
               checkRadios.push(val.value);
            }

            if (val.name == "ExpectedBox")
            {
                checkBoxes.push(val.value);
            }

*/
        });

checkRadios.push('We just add this to make the field not mandatory');
 checkBoxes.push('We just add this to make the field not mandatory');
        var campQuestion = $('.nicEdit-main').html();
        if (ifEmpty.length > 0 || campQuestion == "")
        {
            swal("Ensure to fill all fields");
            return false;
        } else
        {
            var answerField = "";
            var responseType = [];
            var responseExpected = $('.KeyTexts').val();
            //  if($('.moreOptions').html() != "" )
            //  {
            if (AnswerType == "myRadio" || AnswerType == "mySelect") {
                for (var i = 0; i <= optionNum; i++)
                {

                    if ($('.opt_' + i).val() == "" || $('.opt_' + i).val() == null)
                    {
                        swal("Ensure to fill all fields");
                        return false;
                    } else if ($('.opt_' + i).val().indexOf('__&&__') >= 0)
                    {
                        swal("Remove All __&&__ Characters From All Response Inputs");
                        return false;
                    }
                    responseType.push($('.opt_' + i).val());

                }
                answerField = inputBuilder(AnswerType, optionNum, responseType);
                if (checkRadios.length < 1)
                {
                    swal("Oops! No Expected Response");
                    return false;
                } else
                {
                    responseExpected = $('.' + checkRadios[0]).val();
                }
            } else if (AnswerType == "myTextfield" || AnswerType == "myTextarea")
            {
                if ($('.KeyTexts').val() == "" || $('.KeyTexts').val() == null)
                {
                    swal("Oops! No Key-Word Specified");
                    return false;
                } else if ($('.KeyTexts').val().indexOf('__&&__') >= 0)
                {
                    swal("Remove All __&&__ Characters From All Response Inputs");
                    return false;
                } else
                {
                    responseExpected = $('.KeyTexts').val();
                }
                answerField = inputBuilder(AnswerType, optionNum, responseType);
                responseType = "NIL";
            } else if (AnswerType == "myCheck")
            {
                for (var i = 0; i <= optionNum; i++)
                {

                    if ($('.opt_' + i).val() == "" || $('.opt_' + i).val() == null)
                    {
                        swal("Ensure to fill all fields");
                        return false;
                    } else if ($('.opt_' + i).val().indexOf('__&&__') >= 0)
                    {
                        swal("Remove All __&&__ Characters From All Response Inputs");
                        return false;
                    }
                    responseType.push($('.opt_' + i).val());

                }
                answerField = inputBuilder(AnswerType, optionNum, responseType);
                var logic = "";
                $('input[name=radioName]:checked', '#myForm').val()
                if ($('input[name=logic]:checked').val() == "" || $('input[name=logic]:checked').val() == null)
                {
                    //swal("Oops! No Response Logic Selected");
                    //return false;
                } else
                {
                    logic = $('input[name=logic]:checked').val();
                }
                if (checkBoxes.length < 1)
                {
                    swal("Oops! No Expected Response");
                    return false;
                } else
                {
                    responseExpected = "";
                    $.each(checkBoxes, function (count, val) {
                        if ($('.' + val).val() != null)
                            responseExpected = responseExpected + $('.' + val).val() + "__&&__";
                    });
                    responseExpected = responseExpected + "__&&__" + logic;
                }


            } else if (AnswerType == "myRange")
            {


                var validationArray = [];
                $.each(campAnswers, function (count, val) {
                    if (val.name.indexOf('range') >= 0 || val.name == "leastval") {
                        if (!val.value.match(/^\d+$/)) {
                          if(val.name != "leastval")
                                  {
                                validationArray.push("Data Error");
                                   }
                           // validationArray.push("Data Error");
                        }

                    }
                });
                $.each(campAnswers, function (count, val) {
                    if (val.name.indexOf('valdesc') >= 0) {
                        if (val.value.indexOf('__&&__') >= 0) {

                            validationArray.push("Comma Error");
                        }

                    }
                });

                if (validationArray.length > 0)
                {
                    if (validationArray[0] == "Data Error") {
                        swal("Oops! Range values must contain only digits");
                    } else if (validationArray[0] == "Comma Error") {
                        swal("Oops! Remove All __&&__ Characters From Range Descriptions");
                    }
                    return false;
                }
                

              if(parseInt($('.leastval').val()) > parseInt($('.rangemax').val()) || parseInt($('.leastval').val()) < parseInt($('.rangemin').val()))
               {

                  swal("Least Value Expected Must Fall Between Max And Min Value");
                 return false;

                } 


  if(parseInt($('.rangemax').val()) <= parseInt($('.rangemin').val()))
               {

                  swal("Maximum Value Must Be Greater Than Minimum Value");
                 return false;

                } 

                responseType.push(campAnswers[3].value + "-" + campAnswers[4].value);
                responseType.push(campAnswers[5].value + "-" + campAnswers[6].value)
               // responseExpected = campAnswers[5].value;

            }
            //}
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var CampaignID = $('.campName').val();

            var Question = campQuestion;
            $('#newCamp').val($('.campName_' + CampaignID.split('_')[0]).html());

            $.ajax({
                url: '/saveQuestion',
                type: 'get',
                data: {'_token': CSRF_TOKEN, 'CampaignID': $('#campName').val().split('_')[0], 'ClientID': $('#campName').val().split('_')[1], 'Question': Question, 'ResponseType': responseType, 'responseExpected': responseExpected, 'myResponse': AnswerType, 'ResponseData': responseType},
                success: function (data) {

                    swal(data);
                    //location.href = "/CampaignList";
var previewInfo = localStorage.getItem('previewInfo');
showAddQuestion(previewInfo.split('__&&__')[0], previewInfo.split('__&&__')[1], previewInfo.split('__&&__')[2], 'fa-plus');
                     //showFormArea();
clearCamp();
 $('html,body').animate({
        scrollTop: $(".formPreview").offset().top},
           'slow');
//increament quest count column
$('.QuestCount_'+campId).html(($('.QuestCount_'+campId).html())+1);
                     
                },
                error: function (data) {

                    swal("Oops! Error Occured");
                }
            });

        }
    }
    else if(actionType == "update")
    {

     var responseExpected = "";
        var ifEmpty = [];
        var checkRadios = [];
        var checkBoxes = [];
        var campAnswers = $('.addQuestForm').serializeArray();
        $.each(campAnswers, function (count, val) {

           //i just added leastval so it wont be mandatory
            if (val.value == "" && val.name != "myResponse" && val.name != "campQuest" && val.name != "leastval") {
                ifEmpty.push(val.value);
            }

           /* if (val.name == "Expected")
            {
                checkRadios.push(val.value);
            }

            if (val.name == "ExpectedBox")
            {
                checkBoxes.push(val.value);
            }*/
        });
checkRadios.push('We just add this to make the field not mandatory');
 checkBoxes.push('We just add this to make the field not mandatory');
        var campQuestion = $('.nicEdit-main').html();
        if (ifEmpty.length > 0 || campQuestion == "")
        {
            swal("Ensure to fill all fields");
            return false;
        } else
        {
            var answerField = "";
            var responseType = [];
            var responseExpected = $('.KeyTexts').val();
            //  if($('.moreOptions').html() != "" )
            //  {
            if (AnswerType == "myRadio" || AnswerType == "mySelect") {
                for (var i = 0; i <= optionNum; i++)
                {

                    if ($('.opt_' + i).val() == "" || $('.opt_' + i).val() == null)
                    {
                        swal("Ensure to fill all fields");
                        return false;
                    } else if ($('.opt_' + i).val().indexOf('__&&__') >= 0)
                    {
                        swal("Remove All __&&__ Characters From All Response Inputs");
                        return false;
                    }
                    responseType.push($('.opt_' + i).val());

                }
                answerField = inputBuilder(AnswerType, optionNum, responseType);
                if (checkRadios.length < 1)
                {
                    swal("Oops! No Expected Response");
                    return false;
                } else
                {
                    responseExpected = $('.' + checkRadios[0]).val();
                }
            } else if (AnswerType == "myTextfield" || AnswerType == "myTextarea")
            {
                if ($('.KeyTexts').val() == "" || $('.KeyTexts').val() == null)
                {
                    swal("Oops! No Key-Word Specified");
                    return false;
                } else if ($('.KeyTexts').val().indexOf('__&&__') >= 0)
                {
                    swal("Remove All __&&__ Characters From All Response Inputs");
                    return false;
                } else
                {
                    responseExpected = $('.KeyTexts').val();
                }
                answerField = inputBuilder(AnswerType, optionNum, responseType);
                responseType = "NIL";
            } else if (AnswerType == "myCheck")
            {
                for (var i = 0; i <= optionNum; i++)
                {

                    if ($('.opt_' + i).val() == "" || $('.opt_' + i).val() == null)
                    {
                        swal("Ensure to fill all fields");
                        return false;
                    } else if ($('.opt_' + i).val().indexOf('__&&__') >= 0)
                    {
                        swal("Remove All __&&__ Characters From All Response Inputs");
                        return false;
                    }
                    responseType.push($('.opt_' + i).val());

                }
                answerField = inputBuilder(AnswerType, optionNum, responseType);
                var logic = "";
                $('input[name=radioName]:checked', '#myForm').val()
                if ($('input[name=logic]:checked').val() == "" || $('input[name=logic]:checked').val() == null)
                {
                   // swal("Oops! No Response Logic Selected");
                   // return false;
                } else
                {
                    logic = $('input[name=logic]:checked').val();
                }
                if (checkBoxes.length < 1)
                {
                    swal("Oops! No Expected Response");
                    return false;
                } else
                {
                    responseExpected = "";
                    $.each(checkBoxes, function (count, val) {
                        if ($('.' + val).val() != null)
                            responseExpected = responseExpected + $('.' + val).val() + "__&&__";
                    });
                    responseExpected = responseExpected + "__&&__" + logic;
                }


            } else if (AnswerType == "myRange")
            {


                var validationArray = [];
                $.each(campAnswers, function (count, val) {
                    if (val.name.indexOf('range') >= 0 || val.name == "leastval") {
                        if (!val.value.match(/^\d+$/)) {

                              //this is included to stop the least expected value from being mandatory, initially i should not include if statement here
                           if(val.name != "leastval")
                                  {
                                validationArray.push("Data Error");
                                   }
                       
                           // validationArray.push("Data Error");
                        }

                    }
                });
                $.each(campAnswers, function (count, val) {
                    if (val.name.indexOf('valdesc') >= 0) {
                        if (val.value.indexOf('__&&__') >= 0) {

                            validationArray.push("Comma Error");
                        }

                    }
                });

                if (validationArray.length > 0)
                {
                    if (validationArray[0] == "Data Error") {
                        swal("Oops! Range values must contain only digits");
                    } else if (validationArray[0] == "Comma Error") {
                        swal("Oops! Remove All __&&__ Characters From Range Descriptions");
                    }
                    return false;
                }

  if(parseInt($('.leastval').val()) > parseInt($('.rangemax').val()) || parseInt($('.leastval').val()) < parseInt($('.rangemin').val()))
               {
                  //this is included to stop the least expected value from being mandatory
                 // swal("Least Value Expected Must Fall Between Max And Min Value");
                // return false;

                } 


  if(parseInt($('.rangemax').val()) <= parseInt($('.rangemin').val()))
               {

                  swal("Maximum Value Must Be Greater Than Minimum Value");
                 return false;

                } 

                responseType.push(campAnswers[3].value + "-" + campAnswers[4].value);
                responseType.push(campAnswers[6].value + "-" + campAnswers[7].value)
                responseExpected = campAnswers[5].value;

            }
            //}
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var CampaignID = $('.campName').val();

            var Question = campQuestion;
            $('#newCamp').val($('.campName_' + CampaignID.split('_')[0]).html());
           var questId = localStorage.getItem('questToEdit');
            $.ajax({
                url: '/updateQuestionData',
                type: 'get',
                data: {'_token': CSRF_TOKEN, 'CampaignID': $('#campName').val().split('_')[0], 'ClientID': $('#campName').val().split('_')[1], 'Question': Question, 'ResponseType': responseType, 'responseExpected': responseExpected, 'myResponse': AnswerType, 'ResponseData': responseType, 'questId': questId},
                success: function (data) {
 
                    swal(data);
                    //location.href = "/CampaignList";
var previewInfo = localStorage.getItem('previewInfo');
showAddQuestion(previewInfo.split('__&&__')[0], previewInfo.split('__&&__')[1], previewInfo.split('__&&__')[2], 'fa-plus');
                     //showFormArea();
clearCamp();
 $('html,body').animate({
        scrollTop: $(".formPreview").offset().top},
           'slow');


                     
                },
                error: function (data) {

                    swal("Oops! Error Occured");
                }
            });

        }

       
    }

}


function addnewcamp(prizeType)
{

showDivLoading();

    if(prizeType == '1'){

        //for campaigns
        var newCamp = $('#newCamp').val();
        var allClients = $('#allClients').val();
        var campEnd = $('#campEnd').val();
        campEnd = campEnd.split('/')[2] + "-" + campEnd.split('/')[0] + "-" + campEnd.split('/')[1];
        var campStart = $('#campStart').val();
        campStart = campStart.split('/')[2] + "-" + campStart.split('/')[0] + "-" + campStart.split('/')[1];
        var campNotes = $('#campNotes').val();
        var ImagePath = $('#fupload').val().split('\\')[2];
        var campType = $('#campType').val();
        var prizeName = "";
        if($('#prizeName').val() == null)
        {
            prizeName = "N/A";
        }
        else{
            prizeName = $('#prizeName').val();
        }
        var responseArray = [newCamp, allClients, campEnd, campStart, campNotes, ImagePath, campType, prizeName];
        var checkEmpty = [];
        $.each(responseArray, function (count, val) {

            if (val == "" || val == null)
            {
                hideDivLoading();
                checkEmpty.push("1");
               
            }

        });

        if (checkEmpty.length > 0)
        {
              hideDivLoading();
swal("All Fields Must Have A Value");
            return false;
        }

        var ppName = $('#fupload').val();
        var ext = ppName.split('.')[1].toLowerCase();

        if (ext != "jpg" && ext != "jpeg" && ext != "png")
        {
              hideDivLoading();
            swal("Oops! File no supported.");
            return;
        }
         else
        {






     
   //check image dimension
       window.URL = window.URL || window.webkitURL;
    var fileInput = $('#upload_form').find("input[type=file]")[0],
        file = fileInput.files && fileInput.files[0];

    if( file ) {
        var img = new Image();

        img.src = window.URL.createObjectURL( file );

        img.onload = function() {
            var width = img.naturalWidth,
                height = img.naturalHeight;

            window.URL.revokeObjectURL( img.src );

            if( width == 380 && height == 380 ) {
                //continues
                  

               

            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url: '/newCampaign',
                type: 'get',
                data: {'_token': CSRF_TOKEN, 'newCampaign': newCamp, 'allClients': allClients, 'campEnd': campEnd, 'campStart': campStart, 'campNotes': campNotes, 'ImagePath': ImagePath, 'campType': campType, 'prizeName': prizeName, 'prizeType': prizeType},
                success: function (dataId) {
                    var myFile = new FormData($("#upload_form")[0]);
                    myFile.append('campId', dataId);
                        //hideDivLoading();
                      if(dataId == 'Already Exists'){

                            swal('Data Already Exist');
                            return false;
                            }
                    $.ajax({
                        url: '/uploadCampaignImage', // point to server-side PHP script 
                        dataType: 'json', // what to expect back from the PHP script, if anything
                        aync: true,
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: myFile,
                        type: 'post',
                        success: function (data) {
                            //$('.toCollapse').slideToggle();
                            //tope
                            //hideDivLoading();
                           
                            var forSpecial = "";
                            if(campType == "1")
                            {
                                campType = "Special";
                                forSpecial = '<td><a onclick="toggleSpecialPrize(\''+data+'\', \''+'fa-hourglass-end'+'\')" class="fa fa-hourglass-end fa-hourglass-end_'+data+'" style="color:green;" title="End Special Prize"></a></td>';
                            }
                            else
                            {
                                campType = "Not Special";
                            }


                            var mytable = $('#sample_3').DataTable();
                            var rowArray = [];
                            rowArray.push(newCamp);
                            rowArray.push(campNotes);
                            rowArray.push(allClients);
                            rowArray.push(campStart);
                            rowArray.push(campEnd);
                            rowArray.push(campType);
                            rowArray.push(0);
                            rowArray.push(prizeName+' <span style="visibility: hidden;" class="ImagePath_'+data+'">'+ImagePath+'</span>');
                            rowArray.push('<table><tr>'+
                                                                                        '<td><a class="fa fa-eye fa-eye_'+data+'" onclick="showQuest(\''+data+'\', \''+'fa-eye'+'\');"></a></td>'+
                                                                                        '<td><a title="Preview Questions" class="fa fa-question fa-question_'+data+'" onclick="showAddQuestion(\''+data+'\', \''+allClients+'\', \''+newCamp+'\', \''+'fa-question'+'\')"></a></td>'+
                                                                                        '<td><a class="fa fa-pencil fa-pencil_'+data+'" onclick="editCamp(\''+data+'\', \''+'fa-pencil'+'\')" ></a></td>'+
                                                                                        '<td><a onclick="delCamp(\''+data+'\', \''+'fa-trash'+'\')" class="fa fa-trash fa-trash_'+data+'" ></a></td>'+
                                                                                         forSpecial+
                                                                                        '<td class="toggleCamp_'+data+'"><a onclick="deactivateCamp(\''+data+'\', \''+'fa-power-off'+'\')" style="color: green;" class="fa fa-power-off fa-power-off_'+data+'" ></a></td>'+
                                                                                    '</tr></table>');
                            mytable.row.add(rowArray);
                            mytable.draw();

                           hideDivLoading();
                            swal("Submit Successful");
                           sendMultipleMail(dataId);
                       $('.reset').click();
                        },
                        error: function (data) {
                        hideDivLoading();
                            swal("Oops! Error Occured.");
                        }
                    });


                    //    $('.campName').append('<option value="' + data + '_' + allClients + '">' + newCamp + '</option>');




                },
                error: function (data) {
                 hideDivLoading();
                    swal("Oops! Error Occured");
                }
            });




            }
            else {
                //stops process
              hideDivLoading();
               swal('Image Dimension Must Be 380 X 380 Pixels');
                return;
            }
        };
    }
    else { //No file was input or browser doesn't support client side reading
        //stops process
             hideDivLoading();
            swal('Image Dimension Must Be 380 X 380 Pixels');
                return;
    }










        }
    }
    else
    {
	
        //for prizes
        var newCamp = $('#newCamp').val();
        var allClients = $('#allClients').val();
        var campEnd = $('#campEnd').val();
        campEnd = campEnd.split('/')[2] + "-" + campEnd.split('/')[0] + "-" + campEnd.split('/')[1];
        var campStart = $('#campStart').val();
        campStart = campStart.split('/')[2] + "-" + campStart.split('/')[0] + "-" + campStart.split('/')[1];
        var campNotes = $('#campNotes').val();
        var ImagePath = $('#fupload').val().split('\\')[2];
        var campType = $('#campType').val();

        if(allClients == null)
        {

            allClients = "N/A";

        }

        if(campType == null)
        {

            campType = "N/A";

        }

        var prizeName = "";
        if($('#prizeName').val() == null)
        {
            prizeName = "N/A";
        }
        else{
            prizeName = $('#prizeName').val();
        }
        var responseArray = [newCamp, allClients, campEnd, campStart, campNotes, ImagePath, campType, prizeName];
        var checkEmpty = [];
        $.each(responseArray, function (count, val) {

            if (val == "" || val == null)
            {
                   hideDivLoading();
                checkEmpty.push("1");
             
            }

        });

        if (checkEmpty.length > 0)
        {
          hideDivLoading();
swal("All Fields Must Have A Value");
            return false;
        }

        var ppName = $('#fupload').val();
        var ext = ppName.split('.')[1].toLowerCase();
        if (ext != "jpg" && ext != "jpeg" && ext != "png")
        {
            hideDivLoading();
            swal("Oops! File no supported.");
        } else
        {


 //check image dimension
       window.URL = window.URL || window.webkitURL;
    var fileInput = $('#upload_form').find("input[type=file]")[0],
        file = fileInput.files && fileInput.files[0];

    if( file ) {
        var img = new Image();

        img.src = window.URL.createObjectURL( file );

        img.onload = function() {
            var width = img.naturalWidth,
                height = img.naturalHeight;

            window.URL.revokeObjectURL( img.src );

            if( width == 380 && height == 380 ) {
                //continues
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url: '/newCampaign',
                type: 'get',
                data: {'_token': CSRF_TOKEN, 'newCampaign': newCamp, 'allClients': allClients, 'campEnd': campEnd, 'campStart': campStart, 'campNotes': campNotes, 'ImagePath': ImagePath, 'campType': campType, 'prizeName': prizeName, 'prizeType': prizeType},
                success: function (data) {
                    var myFile = new FormData($("#upload_form")[0]);
                    myFile.append('campId', data);
                   hideDivLoading();
                      if(data == 'Already Exists'){

                            swal('Data Already Exist');
                            return false;
                            }
                    $.ajax({
                        url: '/uploadCampaignImage', // point to server-side PHP script 
                        dataType: 'json', // what to expect back from the PHP script, if anything
                        aync: true,
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: myFile,
                        type: 'post',
                        success: function (data) {
                              hideDivLoading();
                            //$('.toCollapse').slideToggle();
                            //tope
                             if(data == 'Already Exist'){

                            swal('Data Already Exists');
                            return false;
                            }

                            $('.reset').click();
                            var forSpecial = "";
                            if(campType == "1")
                            {
                                campType = "Special";
                                forSpecial = '<td><a onclick="toggleSpecialPrize(\''+data+'\', \''+'fa-hourglass-end'+'\')" class="fa fa-hourglass-end fa-hourglass-end_'+data+'" style="color:green;" title="End Special Prize"></a></td>';
                            }
                            else
                            {
                                campType = "Not Special";
                            }


                            var mytable = $('#sample_3').DataTable();
                            var rowArray = [];
                            rowArray.push(newCamp);
                            rowArray.push(campNotes);
                            rowArray.push("N/A");
                            rowArray.push(campStart);
                            rowArray.push(campEnd);
                            rowArray.push("N/A");
                            //rowArray.push(prizeName);
                            rowArray.push(ImagePath);
                            rowArray.push('<table><tr>'+
                                                                                        '<td><a class="fa fa-trophy fa-trophy_'+data+'" onclick="selectWinner(\''+data+'\', \''+'fa-trophy'+'\')" title="Select Winner"></a></td>'+
                                                                                        '<td><a class="fa fa-eye fa-eye_'+data+'" onclick="viewWinner(\''+data+'\', \''+'fa-eye'+'\')" title="View Winners"></a></td>'+
                                                            
                                                                                    '</tr></table>');
                            mytable.row.add(rowArray);
                            mytable.draw();
                            swal("Submit Successful");
                             $('.reset').click();
                        },
                        error: function (data) {
                              hideDivLoading();
                            swal("Oops! Error Occured.");
                        }
                    });


                    //    $('.campName').append('<option value="' + data + '_' + allClients + '">' + newCamp + '</option>');

                },
                error: function (data) {
                   hideDivLoading();
                    swal("Oops! Error Occured");
                }
            });
}
            else {
                  hideDivLoading();
                //stops process
               swal('Image Dimension Must Be 380 X 380 Pixels');
                return;
            }
        };
    }
    else { //No file was input or browser doesn't support client side reading
        //stops process
              hideDivLoading();
            swal('Image Dimension Must Be 380 X 380 Pixels');
                return;
    }
        }
	
	
    }
}


function sendMultipleMail(campId)
{

var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/sendMultipleMail',
        type: 'get',
        async: true,
        data: {'_token': CSRF_TOKEN, 'campId': campId},
        success: function (data) {
         swal(data);
        },
        error: function (data) {
            swal("Oops! Error Occured");
        }
    });

}


function showQuest(campId, initialIcon)
{

    //$('.viewQuestBtn2').removeClass('fa-eye');
    //$('.viewQuestBtn2').addClass('fa-spinner fa-spin');

    showLoadingIcon(initialIcon, campId);


    $('.viewQuestBtn').html('Processing...');
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/getallquest',
        type: 'get',
        data: {'_token': CSRF_TOKEN, campId: campId},
        success: function (data) {

            $('#viewCamp').modal();
            $('.QuestionList').html(data);
            
            //$('.viewQuestBtn2').addClass('fa-eye');
            //$('.viewQuestBtn2').removeClass('fa-spinner fa-spin');
            removeLoadingIcon(initialIcon, campId);

            $('.viewQuestBtn').html('View Questions');

        },
        error: function (data) {

            swal("Oops! Error Occured");
        }
    });
}


function delCamp(campId, initialIcon)
{

    showLoadingIcon(initialIcon, campId);
    $('#confirmdelete').modal();
    $('.myId').val(campId);
    removeLoadingIcon(initialIcon, campId);
}

function deleteCamp()
{
    $('#confirmdelete').modal('toggle');
    var campId = $('.myId').val();

    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/delCamp',
        type: 'get',
        data: {'_token': CSRF_TOKEN, campId: campId},
        success: function (data) {
if(data == 'Delete Successful')
{
            $('.row_' + campId).hide();
 }
else
{
swal(data);
}
       },
        error: function (data) {

            swal("Oops! Error Occured");
        }
    });
}

function clientList()
{
    var clients = "";
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/getAllClient',
        type: 'get',
        data: {'_token': CSRF_TOKEN},
        success: function (data) {
            $.each(data, function(count, val){
                clients = clients + "<option value='"+val.id+"'>"+val.CompanyName+"</option>";      
            });
            return clients;
        },
        error: function (data) {

            swal("Oops! Error Occured");
        }
    });

}

function toggleSpecialPrize(campId, initialIcon)
{

    showLoadingIcon(initialIcon, campId);
    var clients = "";
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/toggleSpecialPrize',
        type: 'get',
        data: {'_token': CSRF_TOKEN, 'campId': campId},
        success: function (data) {
            if(data == 1){
                $('.toggleCampSpecial_'+campId).html('<a onclick="toggleSpecialPrize(\''+campId+'\', \''+initialIcon+'\')" class="fa fa-hourglass-end fa-hourglass-end_'+campId+'" style="color:green;" title="End Special Prize" ></a>');
            }
            else if(data == 2)
            {
                $('.toggleCampSpecial_'+campId).html('<a onclick="toggleSpecialPrize(\''+campId+'\', \''+initialIcon+'\')" class="fa fa-hourglass-end fa-hourglass-end_'+campId+'" style="color:red;" title="Resume Special Prize" ></a>');
            }
           
            removeLoadingIcon(initialIcon, campId);      
        },
        error: function (data) {

            swal("Oops! Error Occured");
        }
    });

}

function editCamp(campId, initialIcon, campCategory)
{

    showLoadingIcon(initialIcon, campId);
    var clients = "<option value=''>Select Client</option>";
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/getAllClient',
        type: 'get',
        data: {'_token': CSRF_TOKEN},
        success: function (data) {
            $.each(data, function(count, val){
                clients = clients + "<option value='"+val.id+"'>"+val.CompanyName+"</option>";      
            });

      
            if(campCategory == 1){


                $('.prizeNameBoxModal').remove();
                $('.editCampIdModal').val(campId);
                $('#newCampModal').val($('.campName_' + campId).html());
                $('#campStartModal').val($('.StartDate_' + campId).html());
                $('#campEndModal').val($('.EndDate_' + campId).html());
                $('#campNotesModal').val($('.campDet_' + campId).html());
                

                var f = $('.CampType_'+campId).html();
                if($('.CampType_'+campId).html().trim() == "Special"){
                    $('#campTypeModal').val(1);
                    showPrizeNameModal();
                }
                else
                {
                    $('#campTypeModal').val(0);
                }
                $('.pic').html('<img src="public/images/CampaignImages/' + campId + '/' + $.trim($('.ImagePath_' + campId).html()) + '" width="70%" />');
                $('#allClientsModal').html(clients);
                checkIfWinnerExists();

                $('#editCampModal').modal();
var checker = $('#prizeNameModal').val();
var testing = $('.PrizeName_' + campId).html().split('<span style')[0];
                $('#prizeNameModal').val(testing);
   checker = $('#prizeNameModal').val();
            }
            else
            {





                $('.prizeNameBoxModal').remove();
                $('.editCampIdModal').val(campId);
                $('#newCampModal2').val($('.campName_' + campId).html());
                $('#campStartModal2').val($('.StartDate_' + campId).html());
                $('#campEndModal2').val($('.EndDate_' + campId).html());
                $('#campNotesModal2').val($('.campDet_' + campId).html());
                //$('#prizeNameModal2').val($('.PrizeName_' + campId).html());

                //var f = $('.CampType_'+campId).html();
                //if($('.CampType_'+campId).html().trim() == "Special"){
                //$('#campTypeModal').val(1);
                //showPrizeNameModal();
                //}
                //else
                //{
                // $('#campTypeModal').val(0);
                //}
                $('.pic').html('<img src="public/images/CampaignImages/' + campId + '/' + $.trim($('.ImagePath_' + campId).html()) + '" width="70%" />');
                //$('#allClientsModal').html(clients);
                //checkIfWinnerExists();


                $('#editPrizeModal').modal();
            }

            removeLoadingIcon(initialIcon, campId);

        },
        error: function (data) {

            swal("Oops! Error Occured");
        }
    });
   
}

function checkIfWinnerExists()
{

    var campId = $('.editCampIdModal').val();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/checkIfWinnerExists',
        type: 'get',
        data: {'_token': CSRF_TOKEN, 'campId': campId},
        success: function (data) {
            if(data == "Winner Already Exists")
            {

                $('#prizeNameModal').prop('readonly',true);
                $('#useCampNameModal').hide();
                $('#campTypeModal').prop('disabled',true);

            }

        },
        error: function (data) {

            swal("Oops! Error Occured");
        }
    });
}

function updateCamp(campCategory)
{
    var campId = $('.editCampIdModal').val();
    $('#showMsg').html("");
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    if(campCategory == 1){
        var formData = $('#updateCampForm').serializeArray();
    }
    else
    {
        var formData = $('#updateCampForm2').serializeArray();

    }    

    var checkEmpty = [];
    var formVal = [];


    $.each(formData, function (count, val) {
        if(val.name != "useCampNameModal"){
            if (val.value == "" || val.value == "N/A")
            {
                checkEmpty.push(val.name);
            }


            formVal.push(val.value);


            if(val.name == "newCampModal2")
            {

                //meaning it is just a prize. Not a campaign

                formVal.push('N/A');



            }

            if(val.name == "Campaign End Date2")
            {
                //meaning it is just a prize. Not a campaign

                formVal.push('N/A');

            }

            if(val.name == "Campaign End Date")
            {


                if($('#campTypeModal').is(':disabled'))
                {


                    formVal.push($('#campTypeModal').val());

                }

            }

        
        }

    });
    if ( $('input[name="useCampNameModal"]').is(':checked') ) {
        formVal.push($('#prizeNameModal').val());
    }else
        if($('#campTypeModal').val() == 0){
            formVal.push('N/A');
        }
    if (checkEmpty.length > 0)
    {
        $('#showMsg').html('<span style="color: red;">The ' + checkEmpty[0] + ' Field Is Required</span><div>&emsp;</div>');
        return false;
    }

    $.ajax({
        url: '/updateCamp',
        type: 'get',
        data: {'_token': CSRF_TOKEN, campId: campId, 'allVal': formVal},
        success: function (data) {

            if(campCategory == 1){
                $('#editCampModal').modal('toggle');
                $('.campName_' + campId).html($('#newCampModal').val());
                $('.StartDate_' + campId).html($('#campStartModal').val());
                $('.EndDate_' + campId).html($('#campEndModal').val());
                $('.campDet_' + campId).html($('#campNotesModal').val());
                var t = $('#campTypeModal').val();
                if($('#campTypeModal').val() == "1"){
                    $('.CampType_' + campId).html("Special");
                    $('.PrizeName_'+ campId).html($('#prizeNameModal').val());

                }
                else
                {
                    $('.CampType_' + campId).html("Non Special");
                    $('.PrizeName_'+ campId).html('N/A');
                }
                $('.ClientName_' + campId).html($('#allClientsModal').val());

            }
            else
            {
                $('#editPrizeModal').modal('toggle');
                $('.campName_' + campId).html($('#newCampModal2').val());
                $('.StartDate_' + campId).html($('#campStartModal2').val());
                $('.EndDate_' + campId).html($('#campEndModal2').val());
                $('.campDet_' + campId).html($('#campNotesModal2').val());


            } 

           

        },
        error: function (data) {

            swal("Oops! Error Occured");
        }
    });





}

function getClients(sel)
{
    $('.clientParam').val(sel.options[sel.selectedIndex].text);
}

function addnewclient()
{

  showDivLoading();
    var allData = $('#addClientForm').serializeArray();
    var checkEmpty = [];
    var formVal = [];
    $.each(allData, function (count, val) {
        if (val.value == null || val.value == "")
        {
            checkEmpty.push(val.name);
        }
        formVal.push(val.value);
    });

    if (checkEmpty.length > 0)
    {
       hideDivLoading();
        swal(checkEmpty[0] + "is a required field");
        return false;
    }

    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/addNewClient',
        type: 'get',
        data: {'_token': CSRF_TOKEN, 'allVal': formVal},
        success: function (data) {
       hideDivLoading();
             if(data == 'Already Exists')
              {

                 swal('Data Already Exist');
                 return false;

              }
            if(data == "Error Occured!" || data == "Company Already Exist"){

                swal("Client Successfully Created");
            }else{
              //  $('.toCollapse').slideToggle();
                var mytable = $('#sample_3').DataTable();
                var rowArray = [];
                rowArray.push(formVal[0]);
                rowArray.push(formVal[1]);
                rowArray.push(formVal[2]);
                rowArray.push(formVal[3]);
                rowArray.push('<table style="background-color: none;"><tr>'+
                                                                            '<td><i class="fa fa-pencil" onclick="editClientModal(\''+data+'\')" ></i></td>'+
                                                                            '<td><i onclick="delClient(\''+data+'\')" class="fa fa-trash" ></i></td>'+
                                                                            '<td class="toggleClient_'+data+'"><i onclick="suspendClient(\''+data+'\')" style="cursor: pointer;color: green;" class="fa fa-power-off " ></i></td>'+
                                                                        '</tr></table>');
                mytable.row.add(rowArray);
                mytable.draw();
                //tope2

                swal("Client Successfully Created");
$('.reset').click();
            }
        },
        error: function (data) {
           hideDivLoading();
            swal("Oops! Error Occured");
        }
    });
}



//my search code
function Clear() {    
    $('#sample_3 tr').show();
}

function Search(word) {

    //var initalData = JSON.parse(localStorage.getItem('AllFilterData'));



    // i clear table then hide unwanted rows. I love me.
    Clear();

    //remove all noExl classes from the rows
    $('#sample_3 tr').removeClass('noExl');
    $('#sample_3 tr td:nth-child(8)').each(function () {
        if ($(this).html() != word) {
            $(this).parent().hide();

            //add noExl class to hidden rows so it wont be downloaded with report
            $(this).parent().addClass('noExl');
        }
    });
}



function getResponseType(UserRole)
{
    //alert(UserRole);
    //set user role
    localStorage.setItem('UserRole', UserRole);

    $('#questList').html('');
    var mytable = $('#sample_3').DataTable();
    mytable.clear();
    mytable.draw();
    $('.miniRep').show();
    $('.miniRep').html('<div>&emsp;&emsp;&emsp;&emsp;</div>Loading Reports...<div>&emsp;&emsp;&emsp;&emsp;</div>');
    var campId = $('#camp').val();
    if(campId  == null || campId  == "")
    {
        $('.miniRep').hide();
        return false
    }
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/showMiniRep',
        type: 'get',
        data: {'_token': CSRF_TOKEN, 'campId': campId},
        async: true,
        success: function (data) {
         
            if(data[0] != "" && data[0] != null && data[0] != 0 ){

                thisQuestion("Not Applicable", campId);
                filterProcess();

                var mytable = $('#sample_3').DataTable();
                var tableSize = parseInt(localStorage.getItem('tableSize'));

                var totUserBar = ((data[0]/data[1])*100).toFixed(0);
                var percentageLeadBar =((data[2]/data[0])*100).toFixed(0);
                var allResBar = ((tableSize/data[3])*100).toFixed(0);
                var targetsBar=((data[0]/data[1])*100).toFixed(0);


                //set minireport for client or admin
                var miniReportData = '';

                if(UserRole == "isAdmin")
                {

                    $('.miniRep').html('<div>&emsp;&emsp;&emsp;&emsp;</div>'+
                       '<div class="row">'+             
                    '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">'+
                                                    '<div class="dashboard-stat2 topReport">'+
                                                        '<div class="display">'+
                                                            '<div class="number">'+
                                                                '<h3 class="font-green-sharp">'+
                                                                    '<span style="font-size: 20px;">Total Users</span>'+
                  ' <!--                                                <small class="font-green-sharp">$</small>-->'+
                    '</h3>'+
'<!--                                            <small>TOTAL PROFIT</small>-->'+
                    '</div>'+
                    '<div class="icon">'+
                        '<i class="icon-user"></i>'+
                    '</div>'+
                '</div>'+
                '<div class="progress-info">'+
                    '<div class="progress">'+
                        '<span style="width: '+totUserBar+'%;" class="progress-bar progress-bar-success green-sharp">'+
                            '<span class="sr-only totUsers"></span>'+
                        '</span>'+
                    '</div>'+
                    '<div class="status">'+
                        '<div class="status-title"> progress </div>'+
                        '<div class="status-number totUsers" > </div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
'</div>'+


'<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">'+
            '<div class="dashboard-stat2 topReport">'+
                '<div class="display">'+
                    '<div class="number">'+
                        '<h3 class="font-red-haze">'+
                            '<span style="font-size: 20px;">Percentage Lead</span>'+
  ' <!--                                                <small class="font-green-sharp">$</small>-->'+
                    '</h3>'+
'<!--                                            <small>TOTAL PROFIT</small>-->'+
                    '</div>'+
                    '<div class="icon">'+
                        '<i class="icon-user"></i>'+
                    '</div>'+
                '</div>'+
                '<div class="progress-info">'+
                    '<div class="progress">'+
                        '<span style="width: '+percentageLeadBar+'%;" class="progress-bar progress-bar-success red-haze">'+
                            '<span class="sr-only percentageLead"></span>'+
                        '</span>'+
                    '</div>'+
                    '<div class="status">'+
                        '<div class="status-title"> progress </div>'+
                        '<div class="status-number percentageLead"> </div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
'</div>'+



'<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">'+
            '<div class="dashboard-stat2 topReport">'+
                '<div class="display">'+
                    '<div class="number">'+
                        '<h3 class="font-blue-sharp">'+
                            '<span style="font-size: 20px;">Results</span>'+
  ' <!--                                                <small class="font-green-sharp">$</small>-->'+
                    '</h3>'+
'<!--                                            <small>TOTAL PROFIT</small>-->'+
                    '</div>'+
                    '<div class="icon">'+
                        '<i class="icon-pie-chart"></i>'+
                    '</div>'+
                '</div>'+
                '<div class="progress-info">'+
                    '<div class="progress">'+
                        '<span style="width: '+allResBar+'%;" class="progress-bar progress-bar-success blue-sharp">'+
                            '<span class="sr-only allRes"></span>'+
                        '</span>'+
                    '</div>'+
                    '<div class="status">'+
                        '<div class="status-title"> progress </div>'+
                        '<div class="status-number allRes"> </div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
'</div>'+



'<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">'+
            '<div class="dashboard-stat2 topReport">'+
                '<div class="display">'+
                    '<div class="number">'+
                        '<h3 class="font-purple-soft">'+
                            '<span style="font-size: 20px;">Target</span>'+
  ' <!--                                                <small class="font-green-sharp">$</small>-->'+
                    '</h3>'+
'<!--                                            <small>TOTAL PROFIT</small>-->'+
                    '</div>'+
                    '<div class="icon">'+
                        '<i class="icon-pie-chart"></i>'+
                    '</div>'+
                '</div>'+
                '<div class="progress-info">'+
                    '<div class="progress">'+
                        '<span style="width: '+targetsBar+'%;" class="progress-bar progress-bar-success purple-soft">'+
                            '<span class="sr-only targets"></span>'+
                        '</span>'+
                    '</div>'+
                    '<div class="status">'+
                        '<div class="status-title"> progress </div>'+
                        '<div class="status-number targets"></div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
'</div>'+


        '</div><br><button class="btn btn-xs btn-info pull-left " title="Download to excel" onclick="downloadToExcelBtn();">Download To Excel</button>&emsp;<button class="btn btn-xs btn-danger pull-left" title="Reset Filter" onclick="getResponseType();">Reset Filter</button>&emsp;<button class="btn btn-xs sbold green pull-left" title="Show Leads only" onclick="Search(\''+'Lead'+'\')">Leads</button>&emsp;<button class="btn btn-xs btn-primary pull-left" title="Show only non-Leads" onclick="Search(\''+'----'+'\')">Not Leads</button><button class="btn sbold green pull-right collapser" type="button" onclick="toggleQuestList();" >Add Filter &nbsp;<i class="fa fa-plus"></i></button><br>'+
 



'<br>');

                }
                else
                {
                    $('.miniRep').html('<div>&emsp;&emsp;&emsp;&emsp;</div>'+
                       '<div class="row">'+             
                    '<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">'+
                                                    '<div class="dashboard-stat2 topReport">'+
                                                        '<div class="display">'+
                                                            '<div class="number">'+
                                                                '<h3 class="font-green-sharp">'+
                                                                    '<span style="font-size: 20px;">Total Users</span>'+
                   ' <!--                                                <small class="font-green-sharp">$</small>-->'+
                    '</h3>'+
'<!--                                            <small>TOTAL PROFIT</small>-->'+
                    '</div>'+
                    '<div class="icon">'+
                        '<i class="icon-user"></i>'+
                    '</div>'+
                '</div>'+
                '<div class="progress-info">'+
                    '<div class="progress">'+
                        '<span style="width: '+totUserBar+'%;" class="progress-bar progress-bar-success green-sharp">'+
                            '<span class="sr-only totUsers"></span>'+
                        '</span>'+
                    '</div>'+
                    '<div class="status">'+
                        '<div class="status-title"> progress </div>'+
                        '<div class="status-number totUsers" > </div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
'</div>'+


'<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">'+
            '<div class="dashboard-stat2 topReport">'+
                '<div class="display">'+
                    '<div class="number">'+
                        '<h3 class="font-red-haze">'+
                            '<span style="font-size: 20px;">Percentage Lead</span>'+
  ' <!--                                                <small class="font-green-sharp">$</small>-->'+
                    '</h3>'+
'<!--                                            <small>TOTAL PROFIT</small>-->'+
                    '</div>'+
                    '<div class="icon">'+
                        '<i class="icon-user"></i>'+
                    '</div>'+
                '</div>'+
                '<div class="progress-info">'+
                    '<div class="progress">'+
                        '<span style="width: '+percentageLeadBar+'%;" class="progress-bar progress-bar-success red-haze">'+
                            '<span class="sr-only percentageLead"></span>'+
                        '</span>'+
                    '</div>'+
                    '<div class="status">'+
                        '<div class="status-title"> progress </div>'+
                        '<div class="status-number percentageLead"> </div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
'</div>'+


'<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">'+
            '<div class="dashboard-stat2 topReport">'+
                '<div class="display">'+
                    '<div class="number">'+
                        '<h3 class="font-purple-soft">'+
                            '<span style="font-size: 20px;">Target</span>'+
  ' <!--                                                <small class="font-green-sharp">$</small>-->'+
                    '</h3>'+
'<!--                                            <small>TOTAL PROFIT</small>-->'+
                    '</div>'+
                    '<div class="icon">'+
                        '<i class="icon-pie-chart"></i>'+
                    '</div>'+
                '</div>'+
                '<div class="progress-info">'+
                    '<div class="progress">'+
                        '<span style="width: '+targetsBar+'%;" class="progress-bar progress-bar-success purple-soft">'+
                            '<span class="sr-only targets"></span>'+
                        '</span>'+
                    '</div>'+
                    '<div class="status">'+
                        '<div class="status-title"> progress </div>'+
                        '<div class="status-number targets"></div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
'</div>'+


        '</div><br><button class="btn btn-xs btn-info pull-left " title="Download to excel" onclick="downloadToExcelBtn();">Download To Excel</button>&emsp;<button class="btn btn-xs btn-danger pull-left" title="Reset Filter" onclick="getResponseType();">Reset Filter</button>&emsp;<button class="btn btn-xs sbold green pull-left" title="Show Leads only" onclick="Search(\''+'Lead'+'\')">Leads</button>&emsp;<button class="btn btn-xs btn-primary pull-left" title="Show only non-Leads" onclick="Search(\''+'----'+'\')">Not Leads</button><button class="btn sbold green pull-right collapser" type="button" onclick="toggleQuestList();" >Add Filter &nbsp;<i class="fa fa-plus"></i></button><br>'+
 



'<br>');
                }


                if(UserRole == "isAdmin"){
                    $('.totUsers').html(data[0]+' / '+data[1]);
                    $('.percentageLead').html(((data[2]/data[0])*100).toFixed(2)+'%');
                }else{
                    $('.totUsers').html(data[0]);
                    $('.percentageLead').html(data[2]+' out of  '+data[0]+' '+((data[2]/data[0])*100).toFixed(2)+'%');
                }
                $('.allRes').html(tableSize+'/'+data[3]);

                $('.targets').html(((data[0]/data[1])*100).toFixed(2)+'%');

                $('.miniRep').show();

            }
            else
            {
                var totUserBar = ((data[0]/data[1])*100).toFixed(0);
                var percentageLeadBar =0;
                var allResBar = 0;
                var targetsBar=((data[0]/data[1])*100).toFixed(0);

                if(UserRole == "isAdmin")
                {

                    $('.miniRep').html('<div>&emsp;&emsp;&emsp;&emsp;</div>'+
                       '<div class="row">'+             
                    '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">'+
                                                    '<div class="dashboard-stat2 topReport">'+
                                                        '<div class="display">'+
                                                            '<div class="number">'+
                                                                '<h3 class="font-green-sharp">'+
                                                                    '<span style="font-size: 20px;">Total Users</span>'+
         ' <!--                                                <small class="font-green-sharp">$</small>-->'+
                    '</h3>'+
'<!--                                            <small>TOTAL PROFIT</small>-->'+
                    '</div>'+
                    '<div class="icon">'+
                        '<i class="icon-user"></i>'+
                    '</div>'+
                '</div>'+
                '<div class="progress-info">'+
                    '<div class="progress">'+
                        '<span style="width: '+totUserBar+'%;" class="progress-bar progress-bar-success green-sharp">'+
                            '<span class="sr-only totUsers"></span>'+
                        '</span>'+
                    '</div>'+
                    '<div class="status">'+
                        '<div class="status-title"> progress </div>'+
                        '<div class="status-number totUsers" > </div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
'</div>'+


'<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">'+
            '<div class="dashboard-stat2 topReport">'+
                '<div class="display">'+
                    '<div class="number">'+
                        '<h3 class="font-green-sharp">'+
                            '<span style="font-size: 20px;">Percentage Lead</span>'+
  ' <!--                                                <small class="font-green-sharp">$</small>-->'+
                    '</h3>'+
'<!--                                            <small>TOTAL PROFIT</small>-->'+
                    '</div>'+
                    '<div class="icon">'+
                        '<i class="icon-user"></i>'+
                    '</div>'+
                '</div>'+
                '<div class="progress-info">'+
                    '<div class="progress">'+
                        '<span style="width: '+percentageLeadBar+'%;" class="progress-bar progress-bar-success green-sharp">'+
                            '<span class="sr-only percentageLead"></span>'+
                        '</span>'+
                    '</div>'+
                    '<div class="status">'+
                        '<div class="status-title"> progress </div>'+
                        '<div class="status-number percentageLead"> </div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
'</div>'+



'<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">'+
            '<div class="dashboard-stat2 topReport">'+
                '<div class="display">'+
                    '<div class="number">'+
                        '<h3 class="font-green-sharp">'+
                            '<span style="font-size: 20px;">Results</span>'+
  ' <!--                                                <small class="font-green-sharp">$</small>-->'+
                    '</h3>'+
'<!--                                            <small>TOTAL PROFIT</small>-->'+
                    '</div>'+
                    '<div class="icon">'+
                        '<i class="icon-pie-chart"></i>'+
                    '</div>'+
                '</div>'+
                '<div class="progress-info">'+
                    '<div class="progress">'+
                        '<span style="width: '+0+'%;" class="progress-bar progress-bar-success green-sharp">'+
                            '<span class="sr-only allRes"></span>'+
                        '</span>'+
                    '</div>'+
                    '<div class="status">'+
                        '<div class="status-title"> progress </div>'+
                        '<div class="status-number allRes"> </div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
'</div>'+



'<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">'+
            '<div class="dashboard-stat2 topReport">'+
                '<div class="display">'+
                    '<div class="number">'+
                        '<h3 class="font-green-sharp">'+
                            '<span style="font-size: 20px;">Target</span>'+
  ' <!--                                                <small class="font-green-sharp">$</small>-->'+
                    '</h3>'+
'<!--                                            <small>TOTAL PROFIT</small>-->'+
                    '</div>'+
                    '<div class="icon">'+
                        '<i class="icon-pie-chart"></i>'+
                    '</div>'+
                '</div>'+
                '<div class="progress-info">'+
                    '<div class="progress">'+
                        '<span style="width: '+targetsBar+'%;" class="progress-bar progress-bar-success green-sharp">'+
                            '<span class="sr-only targets"></span>'+
                        '</span>'+
                    '</div>'+
                    '<div class="status">'+
                        '<div class="status-title"> progress </div>'+
                        '<div class="status-number targets"></div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
'</div>'+


        '</div><br><button class="btn btn-xs btn-info pull-left " title="Download to excel" onclick="downloadToExcelBtn();">Download To Excel</button>&emsp;<button class="btn btn-xs btn-danger pull-left" title="Reset Filter" onclick="getResponseType();">Reset Filter</button>&emsp;<button class="btn btn-xs sbold green pull-left" title="Show Leads only" onclick="Search(\''+'Lead'+'\')">Leads</button>&emsp;<button class="btn btn-xs btn-primary pull-left" title="Show only non-Leads" onclick="Search(\''+'----'+'\')">Not Leads</button><button class="btn sbold green pull-right collapser" type="button" onclick="toggleQuestList();">Add Filter &nbsp;<i class="fa fa-plus"></i></button><br>'+











'<br>');


                }
                else
                {

                    $('.miniRep').html('<div>&emsp;&emsp;&emsp;&emsp;</div>'+
                       '<div class="row">'+             
                    '<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">'+
                                                    '<div class="dashboard-stat2 topReport">'+
                                                        '<div class="display">'+
                                                            '<div class="number">'+
                                                                '<h3 class="font-green-sharp">'+
                                                                    '<span style="font-size: 20px;">Total Users</span>'+
  ' <!--                                                <small class="font-green-sharp">$</small>-->'+
                    '</h3>'+
'<!--                                            <small>TOTAL PROFIT</small>-->'+
                    '</div>'+
                    '<div class="icon">'+
                        '<i class="icon-user"></i>'+
                    '</div>'+
                '</div>'+
                '<div class="progress-info">'+
                    '<div class="progress">'+
                        '<span style="width: '+totUserBar+'%;" class="progress-bar progress-bar-success green-sharp">'+
                            '<span class="sr-only totUsers"></span>'+
                        '</span>'+
                    '</div>'+
                    '<div class="status">'+
                        '<div class="status-title"> progress </div>'+
                        '<div class="status-number totUsers" > </div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
'</div>'+


'<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">'+
            '<div class="dashboard-stat2 topReport">'+
                '<div class="display">'+
                    '<div class="number">'+
                        '<h3 class="font-green-sharp">'+
                            '<span style="font-size: 20px;">Percentage Lead</span>'+
  ' <!--                                                <small class="font-green-sharp">$</small>-->'+
                    '</h3>'+
'<!--                                            <small>TOTAL PROFIT</small>-->'+
                    '</div>'+
                    '<div class="icon">'+
                        '<i class="icon-user"></i>'+
                    '</div>'+
                '</div>'+
                '<div class="progress-info">'+
                    '<div class="progress">'+
                        '<span style="width: '+percentageLeadBar+'%;" class="progress-bar progress-bar-success green-sharp">'+
                            '<span class="sr-only percentageLead"></span>'+
                        '</span>'+
                    '</div>'+
                    '<div class="status">'+
                        '<div class="status-title"> progress </div>'+
                        '<div class="status-number percentageLead"> </div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
'</div>'+

'<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">'+
            '<div class="dashboard-stat2 topReport">'+
                '<div class="display">'+
                    '<div class="number">'+
                        '<h3 class="font-green-sharp">'+
                            '<span style="font-size: 20px;">Target</span>'+
  ' <!--                                                <small class="font-green-sharp">$</small>-->'+
                    '</h3>'+
'<!--                                            <small>TOTAL PROFIT</small>-->'+
                    '</div>'+
                    '<div class="icon">'+
                        '<i class="icon-pie-chart"></i>'+
                    '</div>'+
                '</div>'+
                '<div class="progress-info">'+
                    '<div class="progress">'+
                        '<span style="width: '+targetsBar+'%;" class="progress-bar progress-bar-success green-sharp">'+
                            '<span class="sr-only targets"></span>'+
                        '</span>'+
                    '</div>'+
                    '<div class="status">'+
                        '<div class="status-title"> progress </div>'+
                        '<div class="status-number targets"></div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
'</div>'+


        '</div><br><button class="btn btn-xs btn-info pull-left " title="Download to excel" onclick="downloadToExcelBtn();">Download To Excel</button>&emsp;<button class="btn btn-xs btn-danger pull-left" title="Reset Filter" onclick="getResponseType();">Reset Filter</button>&emsp;<button class="btn btn-xs sbold green pull-left" title="Show Leads only" onclick="Search(\''+'Lead'+'\')">Leads</button>&emsp;<button class="btn btn-xs btn-primary pull-left" title="Show only non-Leads" onclick="Search(\''+'----'+'\')">Not Leads</button><button class="btn sbold green pull-right collapser" type="button" onclick="toggleQuestList();">Add Filter &nbsp;<i class="fa fa-plus"></i></button><br>'+











'<br>');


                }


                $('.allRes').html('0/0');


                if(UserRole == "isAdmin"){
                    $('.totUsers').html(data[0]+' / '+data[1]);
                    $('.percentageLead').html('0.00%');
                }else{
                    $('.totUsers').html(data[0]);
                    $('.percentageLead').html('0 out of  0 '+(0).toFixed(2)+'%');
                }

                $('.targets').html(((data[0]/data[1])*100).toFixed(2)+'%');


                $('.miniRep').show();
                thisQuestion("Not Applicable", campId);
            }

        },
        error: function (data) {
            $('.miniRep').hide();
            var mytable = $('#sample_3').DataTable();
            mytable.clear();
            mytable.draw();
            swal("Oops! Error Occured");
        }
    });




}

function filterProcess()
{

    $('#actualQuest').html("");
    localStorage.setItem('saveFilter', null);
    localStorage.setItem('currentLimit', null);
    var mytable = $('#sample_3').DataTable();
    //mytable.clear();
    //mytable.draw();
    $('#filterBody').html("<tr><th colspan='10' align='left'>Fetching Data...</th></tr>");
    var campId = $('#camp').val();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/getFilterQuestions',
        type: 'get',
        data: {'_token': CSRF_TOKEN, 'campId': campId},
        success: function (data) {

            //var response = "<div>&emsp;</div><div>&emsp;</div><b>List Of Questions Selected Campaigh</b><div>&emsp;</div><input type='radio' value='all' name='responseOpt' onclick='thisQuestion(\"" + "Not Applicable" + "\", \"" + campId + "\")'/>View All<div>&emsp;</div>";
            var response = '<div class="panel-group none myAccordion" id="accordion">'+
                '<div class="panel panel-default" id="panel1">'+
                    '<div class="panel-heading">'+
                         '<h4 class="panel-title">'+
                    '<a data-target="#collapseOne" href="#collapseOne" class="collapser" onclick="accordionBtn();">'+
                      'Filter Questions'+
                    '</a>'+
                 ' </h4>'+

                    '</div> <div id="collapseOne" class="collapse questionList"><table class="table table-striped table-bordered table-hover"><thead><tr><td>Select</td><td>Question</td><td>Filter</td></tr></thead><tbody id="questionsHere">';           
            localStorage.setItem('questIds', '');
            localStorage.setItem('inputTypes', '');
            var questIds = [];
            var inputTypes = [];
            $.each(data, function (count, val) {
                questIds.push(val.id);
                inputTypes.push(val.ResponseType);

                response = response + "<tr><td><label class='mt-checkbox mt-checkbox-single mt-checkbox-outline'><input type='checkbox' value='" + val.id + "' name='responseOpt' class='checkboxOption_"+val.id+"' onclick='thisQuestion(\"" + val.id + "\", \"" + campId + "\"); '/><span></span></label></td><td>" + val.Question +"</td><td class='thisQuestion_"+val.id+"'></td></tr>";
            });
            response = response + "<tr><td></td><td></td><td colspan='3'><table width='100%'><tr><td>"+

            "<span class='myReferrer'><strong style='  cursor: pointer;' class='popoverData' href='#' data-content='' rel='tooltip' data-placement='bottom' data-original-title='OR filters all users who gives atleast one selected responses. AND filters all users who gives all the exact selected response' data-trigger='hover'><i class='fa fa-question-circle helpCss'></i></strong></span>&emsp; Filter Logic:"+
 
            "</td><td><select class='filterType pull-right form-control'><option value='OR'>OR</option><option value='AND'>AND</option></select></td></tr><tr><td>&emsp;</td><td>&emsp;</td></tr><tr><th colspan='2'>"+

            "<button class='btn sbold green' type='button' class='filterBtn' style='width: 100%;' onclick='filterAll(\""+campId+"\", \""+"Normal"+"\");'>Filter &nbsp; <i class='fa fa-filter'></i></button>"+

            "</th></tr></table></td></tr>";
            localStorage.setItem('questIds', questIds);
            localStorage.setItem('inputTypes', inputTypes);
            response = response + "</tbody> </table></div></div>";


            $('#questList').html(response);
            $('.popoverData').tooltip();
            $('.popoverOption').tooltip({trigger: "hover"});
            if(mytable.data().count() > 50)
            {
                //show the next and previous button on filter campaign page
                $('.navigationBtns').show();
            }

            //get all data in table into array and keep in local storage
            //localStorage.setItem('AllFilterData', JSON.stringify(mytable.data().toArray()));
        },
        error: function (data) {

            swal("Oops! Error Occured");
        }
    });

}


function thisQuestion(questId, campId)
{
    if (questId == "Not Applicable")
    {
        //  $('#actualQuest').html("<div>&emsp;</div><div align='center'><input type='button' class='btn btn-primary btn-sm filterButton' value='Filter' onclick='filterBy(\"" + campId + "\", \"" + "getAll" + "\", \"" + "Not Applicable" + "\", \"" + "Normal" + "\")'></div>");

        $('#myPagination').html("<button type='button' class='btn sbold green btn-xs' onclick='filterBy(\"" + campId + "\", \"" + "getAll" + "\", \"" + "Not Applicable" + "\", \"" + "Previous" + "\");'> <i class='fa fa-arrow-left'></i>&nbsp;Previous </button>&emsp;<button type='button' class='btn sbold green btn-xs' onclick='filterBy(\"" + campId + "\", \"" + "getAll" + "\", \"" + "Not Applicable" + "\", \"" + "Next" + "\");'>Load More &nbsp; <i class='fa fa-arrow-right'></i></button>");
        filterBy(campId, "getAll", "Not Applicable", "Normal");
    } else {
        if(!$('input.checkboxOption_'+questId).is(':checked')){
            $('.thisQuestion_'+questId).html("");
            return false;
        }

        localStorage.setItem('saveFilter', null);
        localStorage.setItem('currentLimit', null);
        var mytable = $('#sample_3').DataTable();
        mytable.clear();
        mytable.draw();

        $('.thisQuestion_'+questId).html("Loading");
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: '/getQuestOpt',
            type: 'get',
            async: true,
            data: {'_token': CSRF_TOKEN, 'questId': questId},
            success: function (data) {

                if (data != "" && $('input.checkboxOption_'+questId).is(':checked')) {
                    //   $('.thisQuestion_'+questId).html('<form method="post" id="filterForm'+questId+'"><div align="center">'+data.split('__')[0] + "<div>&emsp;</div><div align='center'><input type='button' class='btn btn-primary btn-sm filterButton' value='Filter' onclick='filterBy(\"" + campId + "\", \"" + questId + "\", \"" + data.split('__')[1] + "\", \"" + "Normal" + "\")'></div></div></form>");
                 
                    $('.thisQuestion_'+questId).html('<form method="post" id="filterForm'+questId+'"><div align="center">'+data.split('__')[0] + "<div>&emsp;</div></div></form>");

   
                    $('#myPagination').html("<button type='button' class='btn sbold green btn-xs' onclick='filterBy(\"" + campId + "\", \"" + questId + "\", \"" + data.split('__')[1] + "\", \"" + "Previous" + "\")'><i class='fa fa-arrow-left'></i>&nbsp;Previous</button>&emsp;<button type='button' class='btn sbold green btn-xs' onclick='filterBy(\"" + campId + "\", \"" + questId + "\", \"" + data.split('__')[1] + "\", \"" + "Next" + "\")'>Load More &nbsp; <i class='fa fa-arrow-right'></i></button>");
                    //filterBy(campId, questId, data.split('__')[1], "Normal");

                } 
                else if(!$('input.checkboxOption_'+questId).is(':checked')){
                    $('.thisQuestion_'+questId).html("");
                }else
                {
                    $('.thisQuestion_'+questId).html("Campaign Has No Questions");
                }

                if (data.split('__')[1] == "myRange") {
                    $('#rangeSelect' + questId).ionRangeSlider({
                        hide_min_max: false,
                        keyboard: true,
                        min: data.split('__')[2],
                        max: data.split('__')[3],
                        type: 'single',
                        step: 1,
                        grid: true
                    });
                }
            },
            error: function (data) {
                $('.thisQuestion_'+questId).html("Error Occured");
                swal("Oops! Error Occured");
            }
        });
    }
}



function viewUserAnswers(userId, campId)
{

    $('.eyeToggle_'+userId).removeClass('fa fa-eye');
    $('.eyeToggle_'+userId).addClass('fa fa-spinner fa-spin');
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/getUserResponse',
        type: 'get',
        data: {'_token': CSRF_TOKEN, campId: campId, userId: userId},
        success: function (data) {



            $('#viewResponse').modal();
            $('.ResponseList').html(data);
            $('.eyeToggle_'+userId).removeClass('fa fa-spinner fa-spin');
            $('.eyeToggle_'+userId).addClass('fa fa-eye');


        },
        error: function (data) {

            swal("Oops! Error Occured");
        }
    });

}


function checkAll()
{
    var getCampaign = localStorage.getItem('currentCampaignFiltered');
    var allIds = localStorage.getItem('keppTableIds');
    var getCounts = localStorage.getItem('keepTableCounts');
    if($('input.toggleAll').is(':checked')){
        $.each(allIds.split(','), function(count, val){
            $('.checks_'+count).html('<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">'+
                                                                        '<input type="checkbox" class="userChecks_'+val+'" checked />'+
                                                                        '<span></span>'+
                                                                    '</label>');
        });
    }
    else
    {
        $.each(allIds.split(','), function(count, val){
            $('.checks_'+count).html('<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">'+
                                                                        '<input type="checkbox" class="userChecks_'+val+'"/>'+
                                                                        '<span></span>'+
                                                                    '</label>');
        });
    }
}


function toggleAllLead(option)
{
    var getCampaign = localStorage.getItem('currentCampaignFiltered');
    var allIds = localStorage.getItem('keppTableIds');
    var getCounts = localStorage.getItem('keepTableCounts');
    var allIdArray = [];
    if(option == 'add'){

        $.each(allIds.split(','), function(count, val){
            if($('input.userChecks_'+val).is(':checked')){
                allIdArray.push(val);
                $('.toggleLead_'+getCounts.split(',')[count]).html('<button type="button" class="btn btn-danger btn-xs " onclick="suspendLead(\''+val+'\', \''+getCampaign+'\', \''+getCounts.split(',')[count]+'\')"><i style="cursor: pointer; color: white;" class="fa fa-times toggler_'+count+'" ></i></button>');

                $('.LeadStatus_'+getCounts.split(',')[count]).html('Lead');
            }
        });

    }
    else
    {


        $.each(allIds.split(','), function(count, val){

            if($('input.userChecks_'+val).is(':checked')){
                allIdArray.push(val);
                $('.toggleLead_'+getCounts.split(',')[count]).html('<button type="button" class="btn btn-success btn-xs " onclick="suspendLead(\''+val+'\', \''+getCampaign+'\', \''+getCounts.split(',')[count]+'\')"><i onclick="suspendLead(\''+val+'\', \''+getCampaign+'\', \''+getCounts.split(',')[count]+'\')" style="cursor: pointer; color: white;" class="fa fa-check toggler_'+count+'" ></i></button>');

                $('.LeadStatus_'+getCounts.split(',')[count]).html('-');
            }
        });
    }

   if(allIdArray.length < 1)
   {

   swal('No User Selected For Multiple Lead Toggle');
   return false;

   }

    //update at backend
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/updateMultipleLead',
        type: 'get',
        data: {'_token': CSRF_TOKEN, allIdArray: allIdArray, campId: getCampaign, toggleOption: option},
        async: true,
        success: function (data) {
               
        },
        error: function (data) {
            swal("Oops! Error Occured");
        }
    });

}

function filterAll(campId, actionType)
{
    var UserRole = localStorage.getItem('UserRole');
    localStorage.setItem('currentCampaignFiltered', campId);
    var mytable = $('#sample_3').DataTable();
    mytable.clear();
    mytable.draw();


    $('#filterBody').html("<tr><th colspan='10'>Fetching Data...</th></tr>");
    var filterType = $('.filterType').val();
    var arrayPack = [];

    localStorage.getItem('questIds');
    localStorage.getItem('inputTypes');

    var tableIdsArray = [];
    var tableCountsArray = [];


    var questIdArray = localStorage.getItem('questIds');
    var inputTypeArray = localStorage.getItem('inputTypes');
      
    //mytable.destroy();


    var newQuestArray = [];
    var newInputTypeArray = [];
    $.each(questIdArray.split(',') , function(counter, values){

        if($('input.checkboxOption_'+values).is(':checked')){
            newQuestArray.push(questIdArray.split(',')[counter]);
            newInputTypeArray.push(inputTypeArray.split(',')[counter]);
        }

    });
    var filterBy = [];
    var validateEmpty = [];
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.each(newQuestArray, function(countFilterBy, filterByVal){
        var formResult = $('#filterForm'+filterByVal).serializeArray();
        if(formResult.length < 1)
        {
            validateEmpty.push(1);
        }
        var filterOptionArray = [];
        $.each(formResult, function (count, val) {

            filterOptionArray.push(val.value);
        });
        filterBy.push(filterOptionArray[0]);
    });

    if(validateEmpty.length > 0 || newQuestArray.length < 1)
    {
        swal('Filter options must have a value');
        var mytable = $('#sample_3').DataTable();
        mytable.clear();
        mytable.draw();
        return false;
    }

    //check if 'and' was selected. If yes, then filterby array length must be more than one
    if(filterBy.length < 2 && filterType == "AND") 
    {
        swal('"AND" filters must have atleast two filter options');
        return false;
    }

    //$('.collapser').click();
    $('.collapse').slideToggle();
    //$('.myAccordion').addClass('none');



    var rowArays = [];
    $.ajax({
        url: '/filterAndAnswers',
        type: 'get',
        data: {'_token': CSRF_TOKEN, 'questId': newQuestArray, 'campId': campId, 'questType': newInputTypeArray, 'filterBy': filterBy, 'actionType': actionType, filterType: filterType },
        success: function (data) {
            //fill top report
            var totUserBar = ((data[2]/data[6])*100).toFixed(0);

            if(data[2] == 0){
                var percentageLeadBar = 0;

            }
            else{
                var percentageLeadBar =((data[7]/data[2])*100).toFixed(0);
            }



            var targetsBar=((data[2]/data[6])*100).toFixed(0);
            //mini report binbing was formerly here. Temitope
            $('.totUsersBar').html(((data[2]/data[6])*100).toFixed(0));

            $('.targetsBar').html(((data[2]/data[6])*100).toFixed(0));

                   
            if(data[0].length > 0 && data[0][0].length > 0){
                var mytable = $('#sample_3').DataTable();
                    
                // mytable.clear();
                // mytable.draw();
                mytable.destroy();
                var countAll = 0;
                $.each(data[0], function (count1, val1) {
                      
                    $.each(val1, function (count, val) {
                        rowArays = [];
                        rowArays.push('<button type="button" class="btn btn-primary btn-xs " onclick="viewUserAnswers(\''+val.id+'\', \''+campId+'\')"><i class="fa fa-eye eyeToggle_'+val.id+'" style="color: white;"></i></button>');
                        rowArays.push(val.Surname + " " + val.Other_Name);
                        rowArays.push(val.State + " " + val.City);
                        rowArays.push(val.Phone_Number);
                        rowArays.push(val.Email);
                        rowArays.push(val.Gender);
                        rowArays.push(val.Age_Range);
                        
                        if(data[4][countAll] == 1)
                        {
                            rowArays.push("Lead");
                            rowArays.push('<button type="button" class="btn btn-danger btn-xs " onclick="suspendLead(\''+val.id+'\', \''+campId+'\', \''+countAll+'\')"><i style="cursor: pointer; color: white;" class="fa fa-times toggler_'+countAll+'" ></i></button>');
                            //key this for toggle lead checkbox ontop
                            tableIdsArray.push(val.id);
                            tableCountsArray.push(countAll);
                            localStorage.setItem('keppTableIds', tableIdsArray);
                            localStorage.setItem('keepTableCounts', tableCountsArray);
                        }
                        else
                        {
                            rowArays.push("----");
                            rowArays.push('<button type="button" class="btn btn-success btn-xs " onclick="suspendLead(\''+val.id+'\', \''+campId+'\', \''+countAll+'\')"><i style="cursor: pointer; color: white;" class="fa fa-check toggler_'+countAll+'" ></i></button>');
                            //key this for toggle lead checkbox ontop
                            tableIdsArray.push(val.id);
                            tableCountsArray.push(countAll);
                            localStorage.setItem('keppTableIds', tableIdsArray);
                            localStorage.setItem('keepTableCounts', tableCountsArray);
                        }

                        //fill the checks column on the list
                        //rowArays.push('<input type="checkbox" class="userChecks_'+val.id+'" value="'+val.id+'">');
                        rowArays.push('<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">'+
                                                                                    '<input type="checkbox" class="userChecks_'+val.id+'" value="'+val.id+'" />'+
                                                                                    '<span></span>'+
                                                                                '</label>');

                        if (rowArays.length > 0)
                        {
                            //  var mytable = $('#sample_3').DataTable();
                                   
                            var mytable = $('#sample_3').DataTable({
                                'paging':   false,
                                'columnDefs': [
{
    //destroy: true,
    'targets': 0,
    'createdCell':  function (td, cellData, rowData, row, col) {
        $(td).attr('class', 'noExl'); 
 
        mytable.draw();
                                   
    }
},
{
    //destroy: true,
    'targets': 7,
    'createdCell':  function (td, cellData, rowData, row, col) {
        $(td).attr('class', 'LeadStatus_'+countAll); 
 
        mytable.draw();
                                   
    }
},
{
    'targets': 8,
    'createdCell':  function (td, cellData, rowData, row, col) {
        $(td).attr('class', 'noExl toggleLead_'+countAll); 
                
        mytable.draw();
                                  
    }
},
{
    'targets': 9,
    'createdCell':  function (td, cellData, rowData, row, col) {
        $(td).attr('class', 'noExl checks_'+countAll); 
        mytable.draw();

                                  
    }
}

                                ],
 
                            });
                            //for union
                            mytable.row.add(rowArays);
                            countAll++;
                        } else {
                            // $('#filterBody').html("<tr><td colspan='6'>No Result Available</td></tr>");
                            var mytable = $('#sample_3').DataTable();
                            // mytable.row.remove();
                            mytable.clear();
                            mytable.draw();
                        }

                    });


                });
                //  $('#currentPage').html('Showing '+ data[1] + ' to ' +(parseInt(data[3])) + ' of '+data[2]+' entries');

            }
            else {
                // $('#filterBody').html("<tr><td colspan='6'>No Result Available</td></tr>");
                var mytable = $('#sample_3').DataTable();
                // mytable.row.remove();
                mytable.clear();
                mytable.draw();
            }

            if(data[2] == 0){

                var allResBar = 0;
            }
            else{
                var allResBar = (((countAll)/(data[8]))*100).toFixed(0);
                $('.allResBar').html(((countAll/data[8])*100).toFixed(0));

            }


            //mini report transfered here

            if(UserRole == "isAdmin")
            {

                $('.miniRep').html('<div>&emsp;&emsp;&emsp;&emsp;</div>'+
                   '<div class="row">'+             
                '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">'+
                                                '<div class="dashboard-stat2 topReport">'+
                                                    '<div class="display">'+
                                                        '<div class="number">'+
                                                            '<h3 class="font-green-sharp">'+
                                                                '<span style="font-size: 20px;">Total Users</span>'+
        ' <!--                                                <small class="font-green-sharp">$</small>-->'+
                    '</h3>'+
'<!--                                            <small>TOTAL PROFIT</small>-->'+
                '</div>'+
                '<div class="icon">'+
                    '<i class="icon-user"></i>'+
                '</div>'+
            '</div>'+
            '<div class="progress-info">'+
                '<div class="progress">'+
                    '<span style="width: '+totUserBar+'%;" class="progress-bar progress-bar-success green-sharp">'+
                        '<span class="sr-only totUsers"></span>'+
                    '</span>'+
                '</div>'+
                '<div class="status">'+
                    '<div class="status-title"> progress </div>'+
                    '<div class="status-number totUsers" > </div>'+
                '</div>'+
            '</div>'+
        '</div>'+
'</div>'+


'<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">'+
        '<div class="dashboard-stat2 topReport">'+
            '<div class="display">'+
                '<div class="number">'+
                    '<h3 class="font-red-haze">'+
                        '<span style="font-size: 20px;">Percentage Lead</span>'+
  ' <!--                                                <small class="font-green-sharp">$</small>-->'+
                    '</h3>'+
'<!--                                            <small>TOTAL PROFIT</small>-->'+
                '</div>'+
                '<div class="icon">'+
                    '<i class="icon-user"></i>'+
                '</div>'+
            '</div>'+
            '<div class="progress-info">'+
                '<div class="progress">'+
                    '<span style="width: '+percentageLeadBar+'%;" class="progress-bar progress-bar-success red-haze">'+
                        '<span class="sr-only percentageLead"></span>'+
                    '</span>'+
                '</div>'+
                '<div class="status">'+
                    '<div class="status-title"> progress </div>'+
                    '<div class="status-number percentageLead"> </div>'+
                '</div>'+
            '</div>'+
        '</div>'+
'</div>'+



'<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">'+
        '<div class="dashboard-stat2 topReport">'+
            '<div class="display">'+
                '<div class="number">'+
                    '<h3 class="font-blue-sharp">'+
                        '<span style="font-size: 20px;">Results</span>'+
  ' <!--                                                <small class="font-green-sharp">$</small>-->'+
                    '</h3>'+
'<!--                                            <small>TOTAL PROFIT</small>-->'+
                '</div>'+
                '<div class="icon">'+
                    '<i class="icon-pie-chart"></i>'+
                '</div>'+
            '</div>'+
            '<div class="progress-info">'+
                '<div class="progress">'+
                    '<span style="width: '+allResBar+'%;" class="progress-bar progress-bar-success blue-sharp">'+
                        '<span class="sr-only allRes"></span>'+
                    '</span>'+
                '</div>'+
                '<div class="status">'+
                    '<div class="status-title"> progress </div>'+
                    '<div class="status-number allRes"> </div>'+
                '</div>'+
            '</div>'+
        '</div>'+
'</div>'+



'<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">'+
        '<div class="dashboard-stat2 topReport">'+
            '<div class="display">'+
                '<div class="number">'+
                    '<h3 class="font-purple-soft">'+
                        '<span style="font-size: 20px;">Target</span>'+
  ' <!--                                                <small class="font-green-sharp">$</small>-->'+
                    '</h3>'+
'<!--                                            <small>TOTAL PROFIT</small>-->'+
                '</div>'+
                '<div class="icon">'+
                    '<i class="icon-pie-chart"></i>'+
                '</div>'+
            '</div>'+
            '<div class="progress-info">'+
                '<div class="progress">'+
                    '<span style="width: '+targetsBar+'%;" class="progress-bar progress-bar-success purple-soft">'+
                        '<span class="sr-only targets"></span>'+
                    '</span>'+
                '</div>'+
                '<div class="status">'+
                    '<div class="status-title"> progress </div>'+
                    '<div class="status-number targets"></div>'+
                '</div>'+
            '</div>'+
        '</div>'+
'</div>'+


    '</div><br>&emsp;<button class="btn btn-xs btn-info pull-left " title="Download to excel" onclick="downloadToExcelBtn();">Download To Excel</button>&emsp;<button class="btn btn-xs btn-danger pull-left" title="Reset Filter" onclick="getResponseType();">Reset Filter</button>&emsp;<button class="btn btn-xs sbold green pull-left" title="Show Leads only" onclick="Search(\''+'Lead'+'\')">Leads</button>&emsp;<button class="btn btn-xs btn-primary pull-left" title="Show only non-Leads" onclick="Search(\''+'----'+'\')">Not Leads</button><button class="btn sbold green pull-right collapser" type="button" onclick="toggleQuestList();">Add Filter &nbsp;<i class="fa fa-plus"></i></button><br>'+










'<br>');


            }
            else
            {

                $('.miniRep').html('<div>&emsp;&emsp;&emsp;&emsp;</div>'+
                   '<div class="row">'+             
                '<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">'+
                                                '<div class="dashboard-stat2 topReport">'+
                                                    '<div class="display">'+
                                                        '<div class="number">'+
                                                            '<h3 class="font-green-sharp">'+
                                                                '<span style="font-size: 20px;">Total Users</span>'+
        ' <!--                                                <small class="font-green-sharp">$</small>-->'+
                    '</h3>'+
'<!--                                            <small>TOTAL PROFIT</small>-->'+
                '</div>'+
                '<div class="icon">'+
                    '<i class="icon-user"></i>'+
                '</div>'+
            '</div>'+
            '<div class="progress-info">'+
                '<div class="progress">'+
                    '<span style="width: '+totUserBar+'%;" class="progress-bar progress-bar-success green-sharp">'+
                        '<span class="sr-only totUsers"></span>'+
                    '</span>'+
                '</div>'+
                '<div class="status">'+
                    '<div class="status-title"> progress </div>'+
                    '<div class="status-number totUsers" > </div>'+
                '</div>'+
            '</div>'+
        '</div>'+
'</div>'+


'<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">'+
        '<div class="dashboard-stat2 topReport">'+
            '<div class="display">'+
                '<div class="number">'+
                    '<h3 class="font-red-haze">'+
                        '<span style="font-size: 20px;">Percentage Lead</span>'+
  ' <!--                                                <small class="font-green-sharp">$</small>-->'+
                    '</h3>'+
'<!--                                            <small>TOTAL PROFIT</small>-->'+
                '</div>'+
                '<div class="icon">'+
                    '<i class="icon-user"></i>'+
                '</div>'+
            '</div>'+
            '<div class="progress-info">'+
                '<div class="progress">'+
                    '<span style="width: '+percentageLeadBar+'%;" class="progress-bar progress-bar-success red-haze">'+
                        '<span class="sr-only percentageLead"></span>'+
                    '</span>'+
                '</div>'+
                '<div class="status">'+
                    '<div class="status-title"> progress </div>'+
                    '<div class="status-number percentageLead"> </div>'+
                '</div>'+
            '</div>'+
        '</div>'+
'</div>'+

'<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">'+
        '<div class="dashboard-stat2 topReport">'+
            '<div class="display">'+
                '<div class="number">'+
                    '<h3 class="font-purple-soft">'+
                        '<span style="font-size: 20px;">Target</span>'+
  ' <!--                                                <small class="font-green-sharp">$</small>-->' +
                    '</h3>' +
'<!--                                            <small>TOTAL PROFIT</small>-->' +
                '</div>'+
                '<div class="icon">'+
                    '<i class="icon-pie-chart"></i>'+
                '</div>'+
            '</div>'+
            '<div class="progress-info">'+
                '<div class="progress">'+
                    '<span style="width: '+targetsBar+'%;" class="progress-bar progress-bar-success purple-soft">'+
                        '<span class="sr-only targets"></span>'+
                    '</span>'+
                '</div>'+
                '<div class="status">'+
                    '<div class="status-title"> progress </div>'+
                    '<div class="status-number targets"></div>'+
                '</div>'+
            '</div>'+
        '</div>'+
'</div>'+


    '</div><br>&emsp;<button class="btn btn-xs btn-info pull-left " title="Download to excel" onclick="downloadToExcelBtn();">Download To Excel</button>&emsp;<button class="btn btn-xs btn-danger pull-left" title="Reset Filter" onclick="getResponseType();">Reset Filter</button>&emsp;<button class="btn btn-xs sbold green pull-left" title="Show Leads only" onclick="Search(\''+'Lead'+'\')">Leads</button>&emsp;<button class="btn btn-xs btn-primary pull-left" title="Show only non-Leads" onclick="Search(\''+'----'+'\')">Not Leads</button><button class="btn sbold green pull-right collapser" type="button" onclick="toggleQuestList();">Add Filter &nbsp;<i class="fa fa-plus"></i></button><br>'+










'<br>');


            }

            if(UserRole == "isAdmin"){
                $('.totUsers').html(data[2]+' / '+data[6]);
            }else{
                $('.totUsers').html(data[2]);
            }




            if(data[2] == 0){

                if(UserRole == "isAdmin"){
                    $('.percentageLead').html('0.00%');
                }else{
                    $('.percentageLead').html('0 out of  0 0.00%');
                }

                $('.percentageLeadBar').html('0');
            }
            else{


                if(UserRole == "isAdmin"){
                    $('.percentageLead').html(((data[7]/data[2])*100).toFixed(2)+'%');
                }else{
                    $('.percentageLead').html(data[7]+' out of  '+data[2]+' '+((data[7]/data[2])*100).toFixed(2)+'%');
                }

                $('.percentageLeadBar').html(((data[7]/data[2])*100).toFixed(0));
            }

            $('.allResBar').html(((countAll/data[8])*100).toFixed(0));
            $('.allRes').html(countAll+'/'+data[8]);
            $('.targets').html(((data[2]/data[6])*100).toFixed(2)+'%');

            if(mytable.data().count() > 50)
            {
                //show the next and previous button on filter campaign page
                $('.navigationBtns').show();
            }

            //get all data in table into array and keep in local storage
            //localStorage.setItem('AllFilterData', JSON.stringify(mytable.data().toArray()));

        },
        error: function (data) {

            swal("Oops! Error Occured");
        }
    });
       


}


function toggleQuestList()
{
    $('.myAccordion').removeClass('none');
    $('.questionList').slideToggle();
}

function accordionBtn()
{
    $('.questionList').slideToggle()
}

function filterBy(campId, questId, questType, actionType)
{
    localStorage.setItem('currentCampaignFiltered', campId);
    var tableIdsArray = [];
    var tableCountsArray = [];
    $('#filterBody').html("<tr><th colspan='10'>Fetching Data...</th></tr>");
    if ((actionType == "Next" || actionType == "Normal") && questId != "getAll") {
        //        var mytable = $('#sample_3').DataTable();
        //        mytable.clear();
        //        mytable.draw();
        if (questType == "mySelect" || questType == "myRadio")
        {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var formResult = $('#filterForm'+questId).serializeArray();
            var filterOptionArray = [];
            $.each(formResult, function (count, val) {
                filterOptionArray.push(val.value);
            });
            var filterBy = filterOptionArray[0];
            var rowArays = [];
            $.ajax({
                url: '/filterAnswers',
                type: 'get',
                data: {'_token': CSRF_TOKEN, 'questId': questId, 'campId': campId, 'questType': questType, 'filterBy': filterBy, 'actionType': actionType},
                success: function (data) {
                   
                    if(data[0].length > 0 && data[0][0].length > 0){
                        var mytable = $('#sample_3').DataTable();
                    
                        mytable.clear();
                        mytable.draw();
                        mytable.destroy();
                        var countAll = 0;
                        $.each(data[0], function (count1, val1) {
                      
                            $.each(val1, function (count, val) {
                                rowArays = [];
                                rowArays.push('<button type="button" class="btn btn-primary btn-xs" onclick="viewUserAnswers(\''+val.id+'\', \''+campId+'\')"><i class="fa fa-eye eyeToggle_'+val.id+'" style="color: white;"></i></button>'); 
                                rowArays.push(val.Surname + " " + val.Other_Name);
                                rowArays.push(val.State + " " + val.City);
                                rowArays.push(val.Phone_Number);
                                rowArays.push(val.Email);
                                rowArays.push(val.Gender);
                                rowArays.push(val.Age_Range);
                        
                                if(data[4][countAll] == 1)
                                {
                                    rowArays.push("Lead");
                                    rowArays.push('<button type="button" class="btn btn-danger btn-xs " onclick="suspendLead(\''+val.id+'\', \''+campId+'\', \''+countAll+'\')"><i style="cursor: pointer; color: white;" class="fa fa-times toggler_'+countAll+'" ></i></button>');

                                    //to keep each id used to toggle leads
                                    tableIdsArray.push(val.id);
                                    tableCountsArray.push(countAll);
                                    localStorage.setItem('keppTableIds', tableIdsArray);
                                    localStorage.setItem('keepTableCounts', tableCountsArray);
                                }
                                else
                                {
                                    rowArays.push("----");
                                    rowArays.push('<button type="button" class="btn btn-success btn-xs " onclick="suspendLead(\''+val.id+'\', \''+campId+'\', \''+countAll+'\')"><i style="cursor: pointer; color: white;" class="fa fa-check toggler_'+countAll+'" ></i></button>');
                                    //to keep each id used to toggle leads
                                    tableIdsArray.push(val.id);
                                    tableCountsArray.push(countAll);
                                    localStorage.setItem('keppTableIds', tableIdsArray);
                                    localStorage.setItem('keepTableCounts', tableCountsArray);
                                }



                                //fill the checks column on the list
                                rowArays.push('<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">'+
                                                                                            '<input type="checkbox" class="userChecks_'+val.id+'" value="'+val.id+'" />'+
                                                                                            '<span></span>'+
                                                                                        '</label>');


                                if (rowArays.length > 0)
                                {
                                    //  var mytable = $('#sample_3').DataTable();
                                   
                                    var mytable = $('#sample_3').DataTable({
                                        'paging':   false,
                                        'columnDefs': [
      {
          //destroy: true,
          'targets': 0,
          'createdCell':  function (td, cellData, rowData, row, col) {
              $(td).attr('class', 'noExl'); 
 
              mytable.draw();
                                   
          }
      },
  {
      //destroy: true,
      'targets': 7,
      'createdCell':  function (td, cellData, rowData, row, col) {
          $(td).attr('class', 'LeadStatus_'+countAll); 
 
          mytable.draw();
                                   
      }
  },
  {
      'targets': 8,
      'createdCell':  function (td, cellData, rowData, row, col) {
          $(td).attr('class', ' noExl toggleLead_'+countAll); 
          mytable.draw();
                                  
      }
  },
  {
      'targets': 9,
      'createdCell':  function (td, cellData, rowData, row, col) {
          $(td).attr('class', 'noExl checks_'+countAll); 
          mytable.draw();

                                  
      }
  }

                                        ],
 
                                    });
                                    mytable.row.add(rowArays);
                                    countAll++;

                                } else {
                                    // $('#filterBody').html("<tr><td colspan='6'>No Result Available</td></tr>");
                                    var mytable = $('#sample_3').DataTable();
                                    // mytable.row.remove();
                                    mytable.clear();
                                    mytable.draw();
                                }

                            });


                        });
                  

                        //   $('#currentPage').html('Showing '+ data[1] + ' to ' +(parseInt(data[3])) + ' of '+data[2]+' entries');

                    }
                    else {
                        // $('#filterBody').html("<tr><td colspan='6'>No Result Available</td></tr>");
                        var mytable = $('#sample_3').DataTable();
                        // mytable.row.remove();
                        mytable.clear();
                        mytable.draw();
                    }

                    if(mytable.data().count() > 50)
                    {
                        //show the next and previous button on filter campaign page
                        $('.navigationBtns').show();
                    }

                    //get all data in table into array and keep in local storage
                    //localStorage.setItem('AllFilterData', JSON.stringify(mytable.data().toArray()));
            
                },
                error: function (data) {

                    swal("Oops! Error Occured");
                }
            });
        } else if (questType == "myRange")
        {

            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var formResult = $('#filterForm'+questId).serializeArray();
            var filterOptionArray = [];
            $.each(formResult, function (count, val) {
                filterOptionArray.push(val.value);
            });
            var filterBy = filterOptionArray[0];
            $.ajax({
                url: '/filterRangeAnswers',
                type: 'get',
                data: {'_token': CSRF_TOKEN, 'questId': questId, 'campId': campId, 'questType': questType, 'filterBy': filterBy, 'actionType': actionType},
                success: function (data) {
                  
                    if(data[0].length > 0 && data[0][0].length > 0){
                        var mytable = $('#sample_3').DataTable();
                        // mytable.row.remove();
                        mytable.clear();
                        mytable.draw();
                        mytable.destroy();
                        var countAll = 0;
                        $.each(data[0], function (count1, val1) {

                            $.each(val1, function (count, val) {
                                rowArays = [];
                                rowArays.push('<button type="button" class="btn btn-primary btn-xs" onclick="viewUserAnswers(\''+val.id+'\', \''+campId+'\')"><i class="fa fa-eye eyeToggle_'+val.id+'" style="color: white;"></i></button>');
                                rowArays.push(val.Surname + " " + val.Other_Name);
                                rowArays.push(val.State + " " + val.City);
                                rowArays.push(val.Phone_Number);
                                rowArays.push(val.Email);
                                rowArays.push(val.Gender);
                                rowArays.push(val.Age_Range);
                                if(data[4][countAll] == 1)
                                {
                                    rowArays.push("Lead");
                                    rowArays.push('<button type="button" class="btn btn-danger btn-xs " onclick="suspendLead(\''+val.id+'\', \''+campId+'\', \''+countAll+'\')"><i style="cursor: pointer; color: white;" class="fa fa-times toggler_'+countAll+'" ></i></button>');

                                    //to keep each id used to toggle leads
                                    tableIdsArray.push(val.id);
                                    tableCountsArray.push(countAll);
                                    localStorage.setItem('keppTableIds', tableIdsArray);
                                    localStorage.setItem('keepTableCounts', tableCountsArray);
                                }
                                else
                                {
                                    rowArays.push("----");
                                    rowArays.push('<button type="button" class="btn btn-success btn-xs " onclick="suspendLead(\''+val.id+'\', \''+campId+'\', \''+countAll+'\')"><i style="cursor: pointer; color: white;" class="fa fa-check toggler_'+countAll+'" ></i></button>');

                                    //to keep each id used to toggle leads
                                    tableIdsArray.push(val.id);
                                    tableCountsArray.push(countAll);
                                    localStorage.setItem('keppTableIds', tableIdsArray);
                                    localStorage.setItem('keepTableCounts', tableCountsArray);
                                }

                                //fill the checks column on the list
                                rowArays.push('<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">'+
                                                                                            '<input type="checkbox" class="userChecks_'+val.id+'" value="'+val.id+'" />'+
                                                                                            '<span></span>'+
                                                                                        '</label>');


                                if (rowArays.length > 0)
                                {
                                    //  var mytable = $('#sample_3').DataTable();
                                   
                                    var mytable = $('#sample_3').DataTable({
                                        'paging':   false,
                                        'columnDefs': [
      {
          //destroy: true,
          'targets': 0,
          'createdCell':  function (td, cellData, rowData, row, col) {
              $(td).attr('class', 'noExl'); 
 
              mytable.draw();
                                   
          }
      },
  {
      //destroy: true,
      'targets': 7,
      'createdCell':  function (td, cellData, rowData, row, col) {
          $(td).attr('class', 'LeadStatus_'+countAll); 
 
          mytable.draw();
                                   
      }
  },
  {
      'targets': 8,
      'createdCell':  function (td, cellData, rowData, row, col) {
          $(td).attr('class', 'noExl toggleLead_'+countAll); 
              
          mytable.draw();
                                  
      }
  },
  {
      'targets': 9,
      'createdCell':  function (td, cellData, rowData, row, col) {
          $(td).attr('class', 'noExl checks_'+countAll); 
             
          mytable.draw();

                                  
      }
  }

                                        ],


                                    });
                                    mytable.row.add(rowArays);
                                    countAll++;
                                } else {
                                    // $('#filterBody').html("<tr><td colspan='6'>No Result Available</td></tr>");
                                    var mytable = $('#sample_3').DataTable();
                                    // mytable.row.remove();
                                    mytable.clear();
                                    mytable.draw();
                                }

                            });


                        });
                        //   $('#currentPage').html('Showing '+ data[1] + ' to ' +(parseInt(data[3])) + ' of '+data[2]+' entries');
                    
                    } else {
                        // $('#filterBody').html("<tr><td colspan='6'>No Result Available</td></tr>");
                        var mytable = $('#sample_3').DataTable();
                        // mytable.row.remove();
                        mytable.clear();
                        mytable.draw();
                    }
                    if(mytable.data().count() > 50)
                    {
                        //show the next and previous button on filter campaign page
                        $('.navigationBtns').show();
                    }

                    //get all data in table into array and keep in local storage
                    //localStorage.setItem('AllFilterData', JSON.stringify(mytable.data().toArray()));
                        
                },
                error: function (data) {

                    swal("Oops! Error Occured");
                }
            });
        } else if (questType.indexOf("myCheck") >= 0)
        {
            //            var mytable = $('#sample_3').DataTable();
            //            //   mytable.remove();
            //            mytable.clear();
            //            mytable.draw();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var formResult = $('#filterForm'+questId).serializeArray();
            var filterOptionArray = [];
            $.each(formResult, function (count, val) {
                filterOptionArray.push(val.value);
            });

            $.ajax({
                url: '/filterSelectAnswers',
                type: 'get',
                data: {'_token': CSRF_TOKEN, 'questId': questId, 'campId': campId, 'questType': questType, 'filterBy': filterOptionArray, 'actionType' : actionType},
                success: function (data) {

                  
                    if(data[0].length > 0 && data[0][0].length > 0){
                        var mytable = $('#sample_3').DataTable();
                        mytable.clear();
                        mytable.draw();
                        mytable.destroy();
                        var countAll = 0;
                        $.each(data[0], function (count1, val1) {

                            $.each(val1, function (count, val) {
                                rowArays = [];
                                rowArays.push('<button type="button" class="btn btn-primary btn-xs" onclick="viewUserAnswers(\''+val.id+'\', \''+campId+'\')"><i class="fa fa-eye eyeToggle_'+val.id+'" style="color: white;"></i></button>');
                                rowArays.push(val.Surname + " " + val.Other_Name);
                                rowArays.push(val.State + " " + val.City);
                                rowArays.push(val.Phone_Number);
                                rowArays.push(val.Email);
                                rowArays.push(val.Gender);
                                rowArays.push(val.Age_Range);
                                if(data[4][countAll] == 1)
                                {
                                    rowArays.push("Lead");
                                    rowArays.push('<button type="button" class="btn btn-danger btn-xs " onclick="suspendLead(\''+val.id+'\', \''+campId+'\', \''+countAll+'\')"><i style="cursor: pointer; color: white;" class="fa fa-times toggler_'+countAll+'" ></i></button>');
                                    //to keep each id used to toggle leads
                                    tableIdsArray.push(val.id);
                                    tableCountsArray.push(countAll);
                                    localStorage.setItem('keppTableIds', tableIdsArray);
                                    localStorage.setItem('keepTableCounts', tableCountsArray);

                                }
                                else
                                {
                                    rowArays.push("----");
                                    rowArays.push('<button type="button" class="btn btn-success btn-xs " onclick="suspendLead(\''+val.id+'\', \''+campId+'\', \''+countAll+'\')"><i style="cursor: pointer; color: white;" class="fa fa-check toggler_'+countAll+'" ></i></button>');

                                    //to keep each id used to toggle leads
                                    tableIdsArray.push(val.id);
                                    tableCountsArray.push(countAll);
                                    localStorage.setItem('keppTableIds', tableIdsArray);
                                    localStorage.setItem('keepTableCounts', tableCountsArray);
                                }

                                //fill the checks column on the list
                                rowArays.push('<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">'+
                                                                                            '<input type="checkbox" class="userChecks_'+val.id+'" value="'+val.id+'" />'+
                                                                                            '<span></span>'+
                                                                                        '</label>');


                                if (rowArays.length > 0)
                                {
                                    //  var mytable = $('#sample_3').DataTable();
                                   
                                    var mytable = $('#sample_3').DataTable({
                                        'paging':   false,
                                        'columnDefs': [
      {
          //destroy: true,
          'targets': 0,
          'createdCell':  function (td, cellData, rowData, row, col) {
              $(td).attr('class', 'noExl'); 
 
              mytable.draw();
                                   
          }
      },
  {
      //destroy: true,
      'targets': 7,
      'createdCell':  function (td, cellData, rowData, row, col) {
          $(td).attr('class', 'LeadStatus_'+countAll); 
 
          mytable.draw();
                                   
      }
  },
  {
      'targets': 8,
      'createdCell':  function (td, cellData, rowData, row, col) {
          $(td).attr('class', 'noExl toggleLead_'+countAll); 
             
          mytable.draw();
                                  
      }
  },
  {
      'targets': 9,
      'createdCell':  function (td, cellData, rowData, row, col) {
          $(td).attr('class', 'noExl checks_'+countAll); 
        
          mytable.draw();

                                  
      }
  }

                                        ],
 

                                    });
                                    mytable.row.add(rowArays);
                                    countAll++;
                                } else {
                                    // $('#filterBody').html("<tr><td colspan='6'>No Result Available</td></tr>");
                                    var mytable = $('#sample_3').DataTable();
                                    // mytable.row.remove();
                                    mytable.clear();
                                    mytable.draw();
                                }

                            });


                        });
                        // $('#currentPage').html('Showing '+ data[1] + ' to ' +(parseInt(data[3])) + ' of '+data[2]+' entries');
                    }else {
                        // $('#filterBody').html("<tr><td colspan='6'>No Result Available</td></tr>");
                        var mytable = $('#sample_3').DataTable();
                        // mytable.row.remove();
                        mytable.clear();
                        mytable.draw();
                    }
               
                    if(mytable.data().count() > 50)
                    {
                        //show the next and previous button on filter campaign page
                        $('.navigationBtns').show();
                    }

                    //get all data in table into array and keep in local storage
                    //localStorage.setItem('AllFilterData', JSON.stringify(mytable.data().toArray()));
                },
                error: function (data) {

                    swal("Oops! Error Occured");
                }
            });
        }
    } else if (actionType == "Previous" && questId != "getAll") {

        if (questType == "mySelect" || questType == "myRadio")
        {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var formResult = $('#filterForm'+questId).serializeArray();
            var filterOptionArray = [];
            $.each(formResult, function (count, val) {
                filterOptionArray.push(val.value);
            });
            var filterBy = filterOptionArray[0];
            var rowArays = [];
            $.ajax({
                url: '/filterAnswers',
                type: 'get',
                data: {'_token': CSRF_TOKEN, 'questId': questId, 'campId': campId, 'questType': questType, 'filterBy': filterBy, 'actionType': actionType},
                success: function (data) {

                   
                    if(data[0].length > 0 && data[0][0].length > 0){
                        var mytable = $('#sample_3').DataTable();
                        mytable.clear();
                        mytable.draw();
                        mytable.destroy();
                        var countAll = 0;
                        $.each(data[0], function (count1, val1) {

                            $.each(val1, function (count, val) {
                                rowArays = [];
                                rowArays.push('<button type="button" class="btn btn-primary btn-xs" onclick="viewUserAnswers(\''+val.id+'\', \''+campId+'\')"><i class="fa fa-eye eyeToggle_'+val.id+'" style="color: white;"></i></button>');
                                rowArays.push(val.Surname + " " + val.Other_Name);
                                rowArays.push(val.State + " " + val.City);
                                rowArays.push(val.Phone_Number);
                                rowArays.push(val.Email);
                                rowArays.push(val.Gender);
                                rowArays.push(val.Age_Range);
                                if(data[4][countAll] == 1)
                                {
                                    rowArays.push("Lead");
                                    rowArays.push('<button type="button" class="btn btn-danger btn-xs " onclick="suspendLead(\''+val.id+'\', \''+campId+'\', \''+countAll+'\')"><i style="cursor: pointer; color: white;" class="fa fa-times toggler_'+countAll+'" ></i></button>');


                                    //to keep each id used to toggle leads
                                    tableIdsArray.push(val.id);
                                    tableCountsArray.push(countAll);
                                    localStorage.setItem('keppTableIds', tableIdsArray);
                                    localStorage.setItem('keepTableCounts', tableCountsArray);}
                                else
                                {
                                    rowArays.push("----");
                                    rowArays.push('<button type="button" class="btn btn-success btn-xs " onclick="suspendLead(\''+val.id+'\', \''+campId+'\', \''+countAll+'\')"><i style="cursor: pointer; color: white;" class="fa fa-check toggler_'+countAll+'" ></i></button>');

                                    //to keep each id used to toggle leads
                                    tableIdsArray.push(val.id);
                                    tableCountsArray.push(countAll);
                                    localStorage.setItem('keppTableIds', tableIdsArray);
                                    localStorage.setItem('keepTableCounts', tableCountsArray);
                                }


                                //fill the checks column on the list
                                rowArays.push('<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">'+
                                                                                            '<input type="checkbox" class="userChecks_'+val.id+'" value="'+val.id+'" />'+
                                                                                            '<span></span>'+
                                                                                        '</label>');


                                if (rowArays.length > 0)
                                {
                                    //  var mytable = $('#sample_3').DataTable();
                                   
                                    var mytable = $('#sample_3').DataTable({
                                        'paging':   false,
                                        'columnDefs': [
       {
           //destroy: true,
           'targets': 0,
           'createdCell':  function (td, cellData, rowData, row, col) {
               $(td).attr('class', 'noExl'); 
 
               mytable.draw();
                                   
           }
       },
  {
      //destroy: true,
      'targets': 7,
      'createdCell':  function (td, cellData, rowData, row, col) {
          $(td).attr('class', 'LeadStatus_'+countAll); 
 
          mytable.draw();
                                   
      }
  },
  {
      'targets': 8,
      'createdCell':  function (td, cellData, rowData, row, col) {
          $(td).attr('class', 'noExl toggleLead_'+countAll); 
              
          mytable.draw();
                                  
      }
  },
  {
      'targets': 9,
      'createdCell':  function (td, cellData, rowData, row, col) {
          $(td).attr('class', 'noExl checks_'+countAll); 
          
          mytable.draw();

                                  
      }
  }

                                        ],
 

                                    });
                                    mytable.row.add(rowArays);
                                    countAll++;      
                                } else {
                                    // $('#filterBody').html("<tr><td colspan='6'>No Result Available</td></tr>");
                                    var mytable = $('#sample_3').DataTable();
                                    // mytable.row.remove();
                                    mytable.clear();
                                    mytable.draw();
                                }

                            });


                        });
                        //$('#currentPage').html('Showing '+ data[1] + ' to ' +(parseInt(data[3])) + ' of '+data[2]+' entries');
                    }
                    else {
                        // $('#filterBody').html("<tr><td colspan='6'>No Result Available</td></tr>");
                        var mytable = $('#sample_3').DataTable();
                        // mytable.row.remove();
                        mytable.clear();
                        mytable.draw();
                    }
                    if(mytable.data().count() > 50)
                    {
                        //show the next and previous button on filter campaign page
                        $('.navigationBtns').show();
                    }

                    //get all data in table into array and keep in local storage
                    //localStorage.setItem('AllFilterData', JSON.stringify(mytable.data().toArray()));
                },
                error: function (data) {

                    swal("Oops! Error Occured");
                }
            });
        } else if (questType == "myRange")
        {
        
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var formResult = $('#filterForm'+questId).serializeArray();
            var filterOptionArray = [];
            $.each(formResult, function (count, val) {
                filterOptionArray.push(val.value);
            });
            var filterBy = filterOptionArray[0];
            $.ajax({
                url: '/filterRangeAnswers',
                type: 'get',
                data: {'_token': CSRF_TOKEN, 'questId': questId, 'campId': campId, 'questType': questType, 'filterBy': filterBy, 'actionType': actionType},
                success: function (data) {
                      
                    if(data[0].length > 0 && data[0][0].length > 0){
                        var mytable = $('#sample_3').DataTable();
                        mytable.clear();
                        mytable.draw();
                        mytable.destroy();
                        var countAll = 0;
                        $.each(data[0], function (count1, val1) {
                            $.each(val1, function (count, val) {
                                rowArays = [];
                                rowArays.push('<button type="button" class="btn btn-primary btn-xs" onclick="viewUserAnswers(\''+val.id+'\', \''+campId+'\')"><i class="fa fa-eye eyeToggle_'+val.id+'" style="color: white;"></i></button>');
                                rowArays.push(val.Surname + " " + val.Other_Name);
                                rowArays.push(val.State + " " + val.City);
                                rowArays.push(val.Phone_Number);
                                rowArays.push(val.Email);
                                rowArays.push(val.Gender);
                                rowArays.push(val.Age_Range);
                                if(data[4][countAll] == 1)
                                {
                                    rowArays.push("Lead");
                                    rowArays.push('<button type="button" class="btn btn-danger btn-xs " onclick="suspendLead(\''+val.id+'\', \''+campId+'\', \''+countAll+'\')"><i style="cursor: pointer; color: white;" class="fa fa-times toggler_'+countAll+'" ></i></button>');
                                    //to keep each id used to toggle leads
                                    tableIdsArray.push(val.id);
                                    tableCountsArray.push(countAll);
                                    localStorage.setItem('keppTableIds', tableIdsArray);
                                    localStorage.setItem('keepTableCounts', tableCountsArray);

                                }
                                else
                                {
                                    rowArays.push("----");
                                    rowArays.push('<button type="button" class="btn btn-success btn-xs " onclick="suspendLead(\''+val.id+'\', \''+campId+'\', \''+countAll+'\')"><i style="cursor: pointer; color: white;" class="fa fa-check toggler_'+countAll+'" ></i></button>');

                                    //to keep each id used to toggle leads
                                    tableIdsArray.push(val.id);
                                    tableCountsArray.push(countAll);
                                    localStorage.setItem('keppTableIds', tableIdsArray);
                                    localStorage.setItem('keepTableCounts', tableCountsArray);
                                }

                                //fill the checks column on the list
                                rowArays.push('<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">'+
                                                                                            '<input type="checkbox" class="userChecks_'+val.id+'" value="'+val.id+'" />'+
                                                                                            '<span></span>'+
                                                                                        '</label>');


                                if (rowArays.length > 0)
                                {
                                    //  var mytable = $('#sample_3').DataTable();
                                   
                                    var mytable = $('#sample_3').DataTable({
                                        'paging':   false,
                                        'columnDefs': [
      {
          //destroy: true,
          'targets': 0,
          'createdCell':  function (td, cellData, rowData, row, col) {
              $(td).attr('class', 'noExl'); 
 
              mytable.draw();
                                   
          }
      },
  {
      //destroy: true,
      'targets': 7,
      'createdCell':  function (td, cellData, rowData, row, col) {
          $(td).attr('class', 'LeadStatus_'+countAll); 
 
          mytable.draw();
                                   
      }
  },
  {
      'targets': 8,
      'createdCell':  function (td, cellData, rowData, row, col) {
          $(td).attr('class', 'noExl toggleLead_'+countAll); 
              
          mytable.draw();
                                  
      }
  },
  {
      'targets': 9,
      'createdCell':  function (td, cellData, rowData, row, col) {
          $(td).attr('class', 'noExl checks_'+countAll); 
            
          mytable.draw();

                                  
      }
  }

                                        ],
                                    });
                                    mytable.row.add(rowArays);
                                    countAll++;
                                } else {
                                    // $('#filterBody').html("<tr><td colspan='6'>No Result Available</td></tr>");
                                    var mytable = $('#sample_3').DataTable();
                                    // mytable.row.remove();
                                    mytable.clear();
                                    mytable.draw();
                                }

                            });


                        });
                        // $('#currentPage').html('Showing '+ data[1] + ' to ' +(parseInt(data[3])) + ' of '+data[2]+' entries');
                    }else {
                        // $('#filterBody').html("<tr><td colspan='6'>No Result Available</td></tr>");
                        var mytable = $('#sample_3').DataTable();
                        // mytable.row.remove();
                        mytable.clear();
                        mytable.draw();
                    }
                    if(mytable.data().count() > 50)
                    {
                        //show the next and previous button on filter campaign page
                        $('.navigationBtns').show();
                    }

                    //get all data in table into array and keep in local storage
                    //localStorage.setItem('AllFilterData', JSON.stringify(mytable.data().toArray()));
                },
                error: function (data) {

                    swal("Oops! Error Occured");
                }
            });
        } else if (questType.indexOf("myCheck") >= 0)
        {
            //            var mytable = $('#sample_3').DataTable();
            //            //   mytable.remove();
            //            mytable.clear();
            //            mytable.draw();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var formResult = $('#filterForm'+questId).serializeArray();
            var filterOptionArray = [];
            $.each(formResult, function (count, val) {
                filterOptionArray.push(val.value);
            });

            $.ajax({
                url: '/filterSelectAnswers',
                type: 'get',
                data: {'_token': CSRF_TOKEN, 'questId': questId, 'campId': campId, 'questType': questType, 'filterBy': filterOptionArray, 'actionType': actionType},
                success: function (data) {

                    
                     
                    if(data[0].length > 0 && data[0][0].length > 0){
                        var mytable = $('#sample_3').DataTable();
                        mytable.clear();
                        mytable.draw();
                        mytable.destroy();
                        var countAll = 0;
                        $.each(data[0], function (count1, val1) {

                            $.each(val1, function (count, val) {
                                rowArays = [];
                                rowArays.push('<button type="button" class="btn btn-primary btn-xs" onclick="viewUserAnswers(\''+val.id+'\', \''+campId+'\')"><i class="fa fa-eye eyeToggle_'+val.id+'" style="color: white;"></i></button>'); 
                                rowArays.push(val.Surname + " " + val.Other_Name);
                                rowArays.push(val.State + " " + val.City);
                                rowArays.push(val.Phone_Number);
                                rowArays.push(val.Email);
                                rowArays.push(val.Gender);
                                rowArays.push(val.Age_Range);
                                if(data[4][countAll] == 1)
                                {
                                    rowArays.push("Lead");
                                    rowArays.push('<button type="button" class="btn btn-danger btn-xs " onclick="suspendLead(\''+val.id+'\', \''+campId+'\', \''+countAll+'\')"><i style="cursor: pointer; color: white;" class="fa fa-times toggler_'+countAll+'" ></i></button>');

                                    //to keep each id used to toggle leads
                                    tableIdsArray.push(val.id);
                                    tableCountsArray.push(countAll);
                                    localStorage.setItem('keppTableIds', tableIdsArray);
                                    localStorage.setItem('keepTableCounts', tableCountsArray);
                                }
                                else
                                {
                                    rowArays.push("----");
                                    rowArays.push('<button type="button" class="btn btn-success btn-xs " onclick="suspendLead(\''+val.id+'\', \''+campId+'\', \''+countAll+'\')"><i style="cursor: pointer; color: white;" class="fa fa-check toggler_'+countAll+'" ></i></button>');

                                    //to keep each id used to toggle leads
                                    tableIdsArray.push(val.id);
                                    tableCountsArray.push(countAll);
                                    localStorage.setItem('keppTableIds', tableIdsArray);
                                    localStorage.setItem('keepTableCounts', tableCountsArray);
                                }


                                //fill the checks column on the list
                                rowArays.push('<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">'+
                                                                                            '<input type="checkbox" class="userChecks_'+val.id+'" value="'+val.id+'" />'+
                                                                                            '<span></span>'+
                                                                                        '</label>');


                                if (rowArays.length > 0)
                                {
                                    //  var mytable = $('#sample_3').DataTable();
                                   
                                    var mytable = $('#sample_3').DataTable({
                                        'paging':   false,
                                        'columnDefs': [
      {
          //destroy: true,
          'targets': 0,
          'createdCell':  function (td, cellData, rowData, row, col) {
              $(td).attr('class', 'noExl'); 
 
              mytable.draw();
                                   
          }
      },
  {
      //destroy: true,
      'targets': 7,
      'createdCell':  function (td, cellData, rowData, row, col) {
          $(td).attr('class', 'LeadStatus_'+countAll); 
 
          mytable.draw();
                                   
      }
  },
  {
      'targets': 8,
      'createdCell':  function (td, cellData, rowData, row, col) {
          $(td).attr('class', 'noExl toggleLead_'+countAll); 
          
          mytable.draw();
                                  
      }
  },
  {
      'targets': 9,
      'createdCell':  function (td, cellData, rowData, row, col) {
          $(td).attr('class', 'noExl checks_'+countAll); 
     
          mytable.draw();

                                  
      }
  }

                                        ],
 

                                    });
                                    mytable.row.add(rowArays);
                                    countAll++;
                                } else {
                                    // $('#filterBody').html("<tr><td colspan='6'>No Result Available</td></tr>");
                                    var mytable = $('#sample_3').DataTable();
                                    // mytable.row.remove();
                                    mytable.clear();
                                    mytable.draw();
                                }

                            });


                        });
                        //  $('#currentPage').html('Showing '+ data[1] + ' to ' +(parseInt(data[3])) + ' of '+data[2]+' entries');
                    }
                    else {
                        // $('#filterBody').html("<tr><td colspan='6'>No Result Available</td></tr>");
                        var mytable = $('#sample_3').DataTable();
                        // mytable.row.remove();
                        mytable.clear();
                        mytable.draw();
                    }
                    if(mytable.data().count() > 50)
                    {
                        //show the next and previous button on filter campaign page
                        $('.navigationBtns').show();
                    }

                    //get all data in table into array and keep in local storage
                    //localStorage.setItem('AllFilterData', JSON.stringify(mytable.data().toArray()));

                },
                error: function (data) {

                    swal("Oops! Error Occured");
                }
            });
        }
    } else if (questId == "getAll")
    {
        if (actionType == "Normal" || actionType == "Next") {
            //  $('#actualQuest').html("Loading");
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var rowArays = [];
            //            var mytable = $('#sample_3').DataTable();
            //            mytable.clear();
            //            mytable.draw();
            $.ajax({
                url: '/filterAll',
                type: 'get',
                data: {'_token': CSRF_TOKEN, 'campId': campId, 'actionType': actionType},
                success: function (data) {

                     
                    if(data[0].length > 0 && data[0][0].length > 0){
                        var mytable = $('#sample_3').DataTable();
                        mytable.clear();
                        mytable.draw();
                        mytable.destroy();
                        var countAll = 0;
                        $.each(data[0], function (count1, val1) {

                            $.each(val1, function (count, val) {
                                rowArays = [];
                                rowArays.push('<button type="button" class="btn btn-primary btn-xs" onclick="viewUserAnswers(\''+val.id+'\', \''+campId+'\')"><i class="fa fa-eye eyeToggle_'+val.id+'" style="color: white;"></i></button>');
                                rowArays.push(val.Surname + " " + val.Other_Name);
                                rowArays.push(val.State + " " + val.City);
                                rowArays.push(val.Phone_Number);
                                rowArays.push(val.Email);
                                rowArays.push(val.Gender);
                                rowArays.push(val.Age_Range);
                                if(data[4][countAll] == 1)
                                {
                                    rowArays.push("Lead");
                                    rowArays.push('<button type="button" class="btn btn-danger btn-xs " onclick="suspendLead(\''+val.id+'\', \''+campId+'\', \''+countAll+'\')"><i style="cursor: pointer; color: white;" class="fa fa-times toggler_'+countAll+'" ></i></button>');

                                    //to keep each id used to toggle leads
                                    tableIdsArray.push(val.id);
                                    tableCountsArray.push(countAll);
                                    localStorage.setItem('keppTableIds', tableIdsArray);
                                    localStorage.setItem('keepTableCounts', tableCountsArray);
                                }
                                else
                                {
                                    rowArays.push("----");
                                    rowArays.push('<button type="button" class="btn btn-success btn-xs " onclick="suspendLead(\''+val.id+'\', \''+campId+'\', \''+countAll+'\')"><i style="cursor: pointer; color: white;" class="fa fa-check toggler_'+countAll+'" ></i></button>');

                                    //to keep each id used to toggle leads
                                    tableIdsArray.push(val.id);
                                    tableCountsArray.push(countAll);
                                    localStorage.setItem('keppTableIds', tableIdsArray);
                                    localStorage.setItem('keepTableCounts', tableCountsArray);
                                }

                                //fill the checks column on the list
                                rowArays.push('<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">'+
                                                                                            '<input type="checkbox" class="userChecks_'+val.id+'" value="'+val.id+'" />'+
                                                                                            '<span></span>'+
                                                                                        '</label>');


                                if (rowArays.length > 0)
                                {
                                    //  var mytable = $('#sample_3').DataTable();
                                   
                                    var mytable = $('#sample_3').DataTable({
                                        'paging':   false,
                                        'columnDefs': [
       {
           //destroy: true,
           'targets': 0,
           'createdCell':  function (td, cellData, rowData, row, col) {
               $(td).attr('class', 'noExl'); 
 
               mytable.draw();
                                   
           }
       },
  {
      //destroy: true,
      'targets': 7,
      'createdCell':  function (td, cellData, rowData, row, col) {
          $(td).attr('class', 'LeadStatus_'+countAll); 
 
          mytable.draw();
                                   
      }
  },
  {
      'targets': 8,
      'createdCell':  function (td, cellData, rowData, row, col) {
          $(td).attr('class', 'noExl toggleLead_'+countAll); 
              
          mytable.draw();
                                  
      }
  },
  {
      'targets': 9,
      'createdCell':  function (td, cellData, rowData, row, col) {
          $(td).attr('class', 'noExl checks_'+countAll); 
          
          mytable.draw();

                                  
      }
  }

                                        ],
 

                                    });
                                    mytable.row.add(rowArays);
                                    countAll++;
                                } else {
                                    // $('#filterBody').html("<tr><td colspan='6'>No Result Available</td></tr>");
                                    var mytable = $('#sample_3').DataTable();
                                    // mytable.row.remove();
                                    mytable.clear();
                                    mytable.draw();
                                }

                            });


                        });
                        localStorage.setItem('tableSize', countAll);
                        //   $('#currentPage').html('Showing '+ data[1] + ' to ' +(parseInt(data[3])) + ' of '+data[2]+' entries');
                    }
                    else {
                        // $('#filterBody').html("<tr><td colspan='6'>No Result Available</td></tr>");
                        var mytable = $('#sample_3').DataTable();
                        // mytable.row.remove();
                        mytable.clear();
                        mytable.draw();
                    }
                    if(mytable.data().count() > 50)
                    {
                        //show the next and previous button on filter campaign page
                        $('.navigationBtns').show();
                    }

                    //get all data in table into array and keep in local storage
                    //localStorage.setItem('AllFilterData', JSON.stringify(mytable.data().toArray()));
                },
                error: function (data) {
                    $('.thisQuestion_'+questId).html("Error Occured");
                    swal("Oops! Error Occured");
                }
            });
        } else if (actionType == "Previous")
        {
            //            $('#actualQuest').html("Loading");
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var rowArays = [];
            //            var mytable = $('#sample_3').DataTable();
            //            mytable.clear();
            //            mytable.draw();
            $.ajax({
                url: '/filterAll',
                type: 'get',
                data: {'_token': CSRF_TOKEN, 'campId': campId, 'actionType': actionType},
                success: function (data) {


                   
                    if(data[0].length > 0 && data[0][0].length > 0){
                        var mytable = $('#sample_3').DataTable();
                        mytable.clear();
                        mytable.draw();
                        mytable.destroy();
                        var countAll = 0;
                        $.each(data[0], function (count1, val1) {

                            $.each(val1, function (count, val) {
                                rowArays = [];
                                rowArays.push('<button type="button" class="btn btn-primary btn-xs" onclick="viewUserAnswers(\''+val.id+'\', \''+campId+'\')"><i class="fa fa-eye eyeToggle_'+val.id+'" style="color: white;"></i></button>');
                                rowArays.push(val.Surname + " " + val.Other_Name);
                                rowArays.push(val.State + " " + val.City);
                                rowArays.push(val.Phone_Number);
                                rowArays.push(val.Email);
                                rowArays.push(val.Gender);
                                rowArays.push(val.Age_Range);
                                if(data[4][countAll] == 1)
                                {
                                    rowArays.push("Lead");
                                    rowArays.push('<button type="button" class="btn btn-danger btn-xs " onclick="suspendLead(\''+val.id+'\', \''+campId+'\', \''+countAll+'\')"><i style="cursor: pointer; color: white;" class="fa fa-times toggler_'+countAll+'" ></i></button>');
                                    //to keep each id used to toggle leads
                                    tableIdsArray.push(val.id);
                                    tableCountsArray.push(countAll);
                                    localStorage.setItem('keppTableIds', tableIdsArray);
                                    localStorage.setItem('keepTableCounts', tableCountsArray);
                                }
                                else
                                {
                                    rowArays.push("----");
                                    rowArays.push('<button type="button" class="btn btn-success btn-xs " onclick="suspendLead(\''+val.id+'\', \''+campId+'\', \''+countAll+'\')"><i style="cursor: pointer; color: white;" class="fa fa-check toggler_'+countAll+'" ></i></button>');
                                    //to keep each id used to toggle leads
                                    tableIdsArray.push(val.id);
                                    tableCountsArray.push(countAll);
                                    localStorage.setItem('keppTableIds', tableIdsArray);
                                    localStorage.setItem('keepTableCounts', tableCountsArray);
                                }

                                //fill the checks column on the list
                                rowArays.push('<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">'+
                                                                                            '<input type="checkbox" class="userChecks_'+val.id+'" value="'+val.id+'" />'+
                                                                                            '<span></span>'+
                                                                                        '</label>');


                                if (rowArays.length > 0)
                                {
                                    //  var mytable = $('#sample_3').DataTable();
                                   
                                    var mytable = $('#sample_3').DataTable({
                                        'paging':   false,
                                        'columnDefs': [
       {
           //destroy: true,
           'targets': 0,
           'createdCell':  function (td, cellData, rowData, row, col) {
               $(td).attr('class', 'noExl'); 
 
               mytable.draw();
                                   
           }
       },
  {
      //destroy: true,
      'targets': 7,
      'createdCell':  function (td, cellData, rowData, row, col) {
          $(td).attr('class', 'LeadStatus_'+countAll); 
 
          mytable.draw();
                                   
      }
  },
  {
      'targets': 8,
      'createdCell':  function (td, cellData, rowData, row, col) {
          $(td).attr('class', 'noExl toggleLead_'+countAll); 
              
          mytable.draw();
                                  
      }
  },
  {
      'targets': 9,
      'createdCell':  function (td, cellData, rowData, row, col) {
          $(td).attr('class', 'noExl checks_'+countAll); 
            
          mytable.draw();

                                  
      }
  }

                                        ],
 

                                    });
                                    mytable.row.add(rowArays);
                                    countAll++;
                                } else {
                                    // $('#filterBody').html("<tr><td colspan='6'>No Result Available</td></tr>");
                                    var mytable = $('#sample_3').DataTable();
                                    // mytable.row.remove();
                                    mytable.clear();
                                    mytable.draw();
                                }

                            });


                        });
                        //  $('#currentPage').html('Showing '+ data[1] + ' to ' +(parseInt(data[3])) + ' of '+data[2]+' entries');
                    }else {
                        // $('#filterBody').html("<tr><td colspan='6'>No Result Available</td></tr>");
                        var mytable = $('#sample_3').DataTable();
                        // mytable.row.remove();
                        mytable.clear();
                        mytable.draw();
                    }
              
                    if(mytable.data().count() > 50)
                    {
                        //show the next and previous button on filter campaign page
                        $('.navigationBtns').show();
                    }

                    //get all data in table into array and keep in local storage
                    //localStorage.setItem('AllFilterData', JSON.stringify(mytable.data().toArray()));
                },
                error: function (data) {
                    $('.thisQuestion_'+questId).html("Error Occured");
                    swal("Oops! Error Occured");
                }
            });
        }
    }
}


function deactivateCamp(campId, initialIcon)
{

    showLoadingIcon(initialIcon, campId);
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/suspendCapaign',
        type: 'get',
        data: {'_token': CSRF_TOKEN, 'campId': campId},
        success: function (data) {
            var Status = "";
            if(data == 1)
            {
                Status = "green";
            }
            else
            {
                Status = "red";
            }
            $('.toggleCamp_'+campId).html('<a onclick="deactivateCamp('+campId+', \''+initialIcon+'\')" style="color: '+Status+';" class="fa fa-power-off fa-power-off_'+campId+'" ></a>');

            removeLoadingIcon(initialIcon, campId);
        },
        error: function (data) {
            $('#actualQuest').html("Error Occured");
            swal("Oops! Error Occured");
        }
    });
}

function editClientModal(clientID)
{
    $('#editClient').modal();
    $('.clientID').val(clientID);
    $('#newClient').val($('.CompanyName_' + clientID).html());
    $('#ClientAdd').val($('.CompanyAddress_' + clientID).html());
    $('#clientNameModal').val($('.UserName_' + clientID).html());
   // $('#clientPassword').val($('.Password_' + clientID).html());
}

function editClient()
{
    $('#clientError').html('');
    var serialize = $('#updateClientForm').serializeArray();
    var checkEmpty = [];
    $.each(serialize, function(count, val)
    {
        if(val.value == "" || val.value == null)
        {
            checkEmpty.push(val.name);
        }
    });
    
    if(checkEmpty.length > 0)
    {
        $('#clientError').html('<span style="red">The '+checkEmpty[0]+' Field is Required</span>');
        return false;
    }
    var client = $('#newClient').val();
    var clientAdd = $('#ClientAdd').val();
    var username = $('#clientNameModal').val();
  //  var password = $('#clientPassword').val();
    var clientID = $('.clientID').val();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/editClient',
        type: 'get',
        data: {'_token': CSRF_TOKEN, 'clientID': clientID, 'client': client, 'clientAdd': clientAdd, 'username': username },
        success: function (data) {
            $('.CompanyName_' + clientID).html(client);
            $('.CompanyAddress_' + clientID).html(clientAdd);
            $('.UserName_' + clientID).html(username);
          //  $('.Password_' + clientID).html(password);
            $('#editClient').modal('toggle');
                
        },
        error: function (data) {
            $('#actualQuest').html("Error Occured");
            swal("Oops! Error Occured");
        }
    });
}

function suspendClient(clientId)
{
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/suspendClient',
        type: 'get',
        data: {'_token': CSRF_TOKEN, 'clientId': clientId},
        success: function (data) {
            var Status = "";
            if(data == 1)
            {
                Status = "green";
            }
            else
            {
                Status = "red";
            }
            $('.toggleClient_'+clientId).html('<a onclick="suspendClient('+clientId+')" style="color: '+Status+';" class="fa fa-power-off" ></a>');
        },
        error: function (data) {
            $('#actualQuest').html("Error Occured");
            swal("Oops! Error Occured");
        }
    });
}

function delClient(clientId)
{

    $('#confirmdelete').modal();
    $('.myId').val(clientId);
}

function deleteClient()
{
    $('#confirmdelete').modal('toggle');
    var clientId = $('.myId').val();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/delClient',
        type: 'get',
        data: {'_token': CSRF_TOKEN, clientId: clientId},
        success: function (data) {
   
            $('.row_' + clientId).hide();
        },
        error: function (data) {

            swal("Oops! Error Occured");
        }
    });
}

function delUser(userID)
{
    $('#confirmdelete').modal();
    $('.myId').val(userID);
}


function deleteUser()
{
    $('#confirmdelete').modal('toggle');
    var userId = $('.myId').val();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/delUser',
        type: 'get',
        data: {'_token': CSRF_TOKEN, userId: userId},
        success: function (data) {
   
            $('.row_' + userId).hide();
        },
        error: function (data) {

            swal("Oops! Error Occured");
        }
    });
}

function suspendUser(userId)
{
showLoadingIcon('fa-power-off', userId)
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/suspendUser',
        type: 'get',
        data: {'_token': CSRF_TOKEN, 'userId': userId},
        success: function (data) {
            var Status = "";
            if(data == 1)
            {
                Status = "green";
            }
            else
            {
                Status = "red";
            }
            $('.toggleUser_'+userId).html('<a onclick="suspendUser('+userId+')" style="color: '+Status+';" class="fa fa-power-off fa-power-off_'+userId+'" ></a>');
        },
        error: function (data) {
removeLoadingIcon('fa-power-off', userId)
            $('#actualQuest').html("Error Occured");
            swal("Oops! Error Occured");
        }
    });
}

function suspendLead(userId, campId, count)
{
    $('.toggler_'+count).removeClass('fa-check');
    $('.toggler_'+count).removeClass('fa-times');
    $('.toggler_'+count).addClass('fa-spinner fa-spin');
    $('.LeadStatus_'+count).html('----');
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/suspendLead',
        type: 'get',
        data: {'_token': CSRF_TOKEN, 'userId': userId, campId: campId},
        success: function (data) {
            var Level = "";
            if(data == 1)
            {
                Level = "danger";
            }
            else
            {
                Level = "success";
            }
            // <i onclick="suspendLead('<?php echo $eachUser->id; ?>')" style="cursor: pointer;" class="<?php if($eachUser->Level == 1){ echo 'fa fa-times'; }else{echo 'fa fa-check';} ?>" ></i>
            if(Level == "success")
            {
                $('.toggleLead_'+count).html('<button type="button" class="btn btn-'+Level+' btn-xs " onclick="suspendLead('+userId+', '+campId+', '+count+')"><a style="cursor: pointer; color: white;" class="fa fa-check toggler_'+count+'" ></a></button>');
                $('.LeadStatus_'+count).html('----');
            }
            else
            {
                $('.toggleLead_'+count).html('<button type="button" class="btn btn-'+Level+' btn-xs " onclick="suspendLead('+userId+', '+campId+', '+count+')"><a style="cursor: pointer; color: white;" class="fa fa-times toggler_'+count+'" ></a></button>');
                $('.LeadStatus_'+count).html('Lead');
            }
               
        },
        error: function (data) {
            $('#actualQuest').html("Error Occured");
            swal("Oops! Error Occured");
        }
    });
}



function selectWinner(campId, initialIcon)
{

    showLoadingIcon(initialIcon, campId);
    var over = '<div id="overlay">' +
               '<div  id="loading"style="color: blue; font-size: 20px;"><i class="fa fa-trophy fa-spin" style="color: blue; font-size: 150px;"></i><div style="margin-top: 40px;">Processing...</div></div>' +
               '</div>';
    $(over).appendTo('body');
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/goGetWinner',
        type: 'get',
        data: {'_token': CSRF_TOKEN, campId: campId},
        success: function (data) {
            $('#overlay').remove();
            if(data == "Nomore")
            {
                swal('No more user available for selection');
                removeLoadingIcon(initialIcon, campId);
                return false;
            }


            if(data == "Ended")
            {
                swal('The special prize for this campaign has ended');
                removeLoadingIcon(initialIcon, campId);
                return false;
            }

            if(data != "nil"){
                // swal('The winner is '+ data[0] +'\n\n Email: '+data[1]+'\n\n Phone Number: '+data[2]+' \n\n From: '+data[3]+' City in '+data[4]+' State');
                swal({
                    title: 'The winner is '+ data[0] +'\n\n Email: '+data[1]+'\n\n Phone Number: '+data[2]+' \n\n From: '+data[3]+' City in '+data[4]+' State',
                    text: "Would you like to continue?",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Accept!',
                    cancelButtonText: 'Reject!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false

                }, function (dismiss) {
 
                    if (dismiss == false) {
                        swal(
                          'Cancelled!',
                          'You have rejected this winner :)',
                          'error'
                        );

                    }
                    else if(dismiss == true)
                    {
                        $(over).appendTo('body');
                        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                        $.ajax({
                            url: '/regWinner',
                            type: 'get',
                            data: {'_token': CSRF_TOKEN, 'userId': data[5], 'campId': campId},
                            success: function (data) {
                                $('#overlay').remove();
                      if(data == "Success"){
                                swal(
                   'Great!',
                   'You now have a new Winner!',
                   'success'
                 );
}
else{
swal('Oops! Something went wrong.');
}
                            },
                            error: function (data) {
                                //$('#actualQuest').html("Error Occured");
                                $('#overlay').remove();
                                swal("Oops! Error Occured");
                            }
                        });



                    }
                });
            }
            else{
                swal('No user took this this survey yet');
            }
            removeLoadingIcon(initialIcon, campId);
        },
        error: function (data) {
            $('#overlay').remove();
            removeLoadingIcon(initialIcon, campId);
            swal("Oops! Error Occured");
        }
    });


    //$('#randomWinner').modal('show');

}

function viewWinner(campId, initialIcon)
{

    showLoadingIcon(initialIcon, campId);
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var mytable = $('#ourWinners').DataTable();
    mytable.clear();
    mytable.draw();
    $.ajax({
        url: '/viewPastWinner',
        type: 'get',
        data: {'_token': CSRF_TOKEN, 'campId': campId},
        success: function (data) {
            removeLoadingIcon(initialIcon, campId);
            $('#fetchingRow').html('<tr><td colspan="6" align="center">Fetching Data</td></tr>');

            var mytable = $('#ourWinners').DataTable();
            $.each(data.reverse(), function(count, value){
                var rowArray = [];
                var otherName ="";
                if(value.Other_Name != null && value.Other_Name != "")
                {
                    otherName = value.Other_Name;
                }
                rowArray.push(value.Surname+' '+otherName);
                rowArray.push(value.Email);
                rowArray.push(value.Phone_Number);
                rowArray.push(value.City);
                rowArray.push(value.State);
                //rowArray.push('<table><tr><td><button class="btn btn-danger btn-xs" onclick="removeWinner(\''+value.id+'\', \''+campId+'\')">Remove</button></td><td><button class="btn btn-success btn-xs" onclick="sendUserMail(\''+value.id+'\', \''+campId+'\')">Send Mail</button></td></tr></table>');
                mytable.row.add(rowArray);
            
            });
           

            $('#fetchingRow').html('');
            mytable.draw();
            $('#viewWinners').modal('show');



        },
        error: function (data) {
            //$('#actualQuest').html("Error Occured");
            removeLoadingIcon(initialIcon, campId);
            swal("Oops! Error Occured");
        }
    });

}

function removeWinner(userId, campId)
{

    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
        url: '/removeWinner',
        type: 'get',
        data: {'_token': CSRF_TOKEN, 'campId': campId, 'userId': userId},
        success: function (data) {

            if(data == "Delete Successful")
            {

                viewWinner(campId, 'fa-eye');

            }
        },
        error: function (data) {
            
            swal("Oops! Error Occured");
        }
    });


}

function showPrizeName()
{
    var campType = $('#campType').val();

    if(campType == "1")
    {

        $('.myCampType').after('<div class="prizeNameBox"><div class="form-group">'+
                                                    '<label class="col-md-3 control-label">Prize Name</label>'+
                                                    '<div class="col-md-4">'+
                         '<input id="prizeName" name="prizeName" class="form-control form-control-inline " type="text" placeholder="Enter Prize Name" />'+
                      
                                                    '</div>'+
                                                '</div>' +
        '<div class="form-group">'+
                                                    '<label class="col-md-3 control-label"></label>'+
                                                    '<div class="col-md-4">'+
                         ' <input id="useCampName" name="useCampName" class=" " type="checkbox" onclick="useMyCampName();"  /> &emsp; Use Campaign Name'+
                      
                                                    '</div>'+
                                                '</div></div>'
        );

    }
    else if(campType == "0")
    {

        $('.prizeNameBox').remove();

    }


}

function updatePrizeName()
{

    if ( $('input[name="useCampName"]').is(':checked') ) {
        $('#prizeName').prop('disabled', true);
        $('#prizeName').val($('#newCamp').val());
    } 

}


function useMyCampName()
{

    if ( $('input[name="useCampName"]').is(':checked') ) {
        $('#prizeName').prop('disabled', true);
        $('#prizeName').val($('#newCamp').val());
    } 
    else {
        $('#prizeName').prop('disabled', false);
        $('#prizeName').val('');
    }


}

function showPrizeNameModal()
{
    var campType = $('#campTypeModal').val();
    var campId = $('.editCampIdModal').val();
    if(campType == "1")
    {


        $('.myCampTypeModal').after('<div class="prizeNameBoxModal"><div class="form-group">'+
                                                    '<label class="col-md-4 control-label">Prize Name</label>'+
                                                    '<div class="col-md-8">'+
                         '<input id="prizeNameModal" name="Prize Name" class="form-control form-control-inline " type="text" placeholder="Enter Prize Name"  />'+
                      
                                                    '</div>'+
                                                '</div>' +
        '<div class="form-group">'+
                                                    '<label class="col-md-4 control-label"></label>'+
                                                    '<div class="col-md-8">'+
                         ' <input id="useCampNameModal" name="useCampNameModal" class=" " type="checkbox" onclick="useMyCampNameModal();"  /> &emsp; Use Campaign Name'+
                      
                                                    '</div>'+
                                                '</div></div>'
        );
        $('#prizeNameModal').val($('.PrizeName_' + campId).html());

    }
    else if(campType == "0")
    {

        $('.prizeNameBoxModal').remove();
        $('#prizeNameModal').val($('.PrizeName_' + campId).html());
    }


}

function updatePrizeNameModal()
{

    if ( $('input[name="useCampNameModal"]').is(':checked') ) {
        $('#prizeNameModal').prop('disabled', true);
        $('#prizeNameModal').val($('#newCampModal').val());
    } 

}


function useMyCampNameModal()
{
    var campId = $('.editCampIdModal').val();
    if ( $('input[name="useCampNameModal"]').is(':checked') ) {
        $('#prizeNameModal').prop('disabled', true);
        $('#prizeNameModal').val($('#newCampModal').val());
    } 
    else {
        $('#prizeNameModal').prop('disabled', false);
        $('#prizeNameModal').val('');
        $('#prizeNameModal').val($('.PrizeName_' + campId).html());
    }


}



function AmountChange()
{

    var newAmount = $('.perSurvey').val();
    if(newAmount == null || newAmount == ""){
        $('.amountChangeBtn').prop('disabled', true);
    }
    else
    {
        $('.amountChangeBtn').prop('disabled', false);
    }

}

 

function changeAmount()
{

    var newAmount = $('.perSurvey').val();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
        url: '/updateAmountToWin',
        type: 'get',
        data: {'_token': CSRF_TOKEN, 'newAmount': newAmount},
        success: function (data) {
            $('.amountChangeBtn').prop('disabled', true);
            swal('Update Successful ');
          
        },
        error: function (data) {
            
            swal("Oops! Error Occured");
        }
    });



}


function goToSpecialPrize()
{

    $('.pagePart').hide();
    $('.pageMid').show();
    $('.howtowinInfo').hide();
    $('.wholebody').show();
    $('.question_container').addClass('closed');
    $('.boxes').hide();
    $('.nomore').hide();
    // $('.myloadmore').html("");
    $('.loadingCamp').show();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
        url: '/getSpecialPrize',
        type: 'get',
        data: {'_token': CSRF_TOKEN },
        success: function (data) {
            $('.loadingCamp').hide();
            
            if (data[0].length < 1)
            {
                $('.nomore').html('<div class="column medium-3 " ><article class="ucard-graph-state text-center col-sm-4" style=" margin-top: 130px; margin-left: 30%;">Reward list is empty</article></div>');
                $('.nomore').show();
               

            } else {

                var allSPecials = "";
                $.each(data[0], function(count, value){

                    allSPecials = allSPecials + '<div class="col-sm-5 smallBox"><div>'+
'<img src="public/images/CampaignImages/'+value.id+'/'+value.ImagePath+'" width="80%"></div>'+
'<div style="padding-top: 55px;font-weight: bold;">test reward1</div>'+
'<div style="padding-top: 11px;"><u>How to win</u></div>'+
'<div style="padding-top: 11px;">'+
'Keep taking surveys and we hold a periodical raffle where you can win rewards weekly.'+
'</div></div>'+
                    '<div class="col-sm-2 "></div>';

                });

                var prizeWon = "";
                $.each(data[1], function(count, value){

                    prizeWon = prizeWon + '<tr>'+value.PrizeWon+'</tr><tr>'+value.created_at+'</tr>';

                });


                $('.boxes').html('<div class="col-sm-8" style="padding-top: 20px;"><div class="col-sm-12 pull-left" style="padding-bottom: 25px;"><b>Rewards Available</b></div>'+allSPecials+
                '</div><div class="col-sm-4" style="padding-top: 20px;">'+
                '<div class="col-sm-12 pull-left" style="padding-bottom: 25px;"><b>Prizes won</b></div>'+
                '<div class="col-sm-12 " style="padding-bottom: 25px;">'+
                '<table class="col-sm-12 table-responsive table-striped table-bordered table-hover datatable table-datatable" id="forPrizes"><thead><tr><th>Prize</th><th>Time</th></tr></thead><tbody>'+
                prizeWon+
                '</tbody></table></div></div>');
               
                $('.boxes').show();
                
            }
            $('.BalancePanel').hide();
            $('.BalancePanel').removeClass('balancePanelDesign');
         
            $('.ProfileNav-item').removeClass('is-active');
            $('.available').addClass('is-active');
            $('.popoverData').tooltip();
            $('.popoverOption').tooltip({trigger: "hover"});
             var dT = $('#forPrizes').DataTable( {
                    "searching": false
                } );

        },
        error: function (data) {
            // swal(data);
            swal("Oops! Error Occured");
        }
    });



}



function closeAddQuestion()
{

    $('.previewArea').slideUp();

    $('.formArea').slideUp();

    $('html,body').animate({
        scrollTop: $("#previewArea").offset().top},
           'slow');

}

function showAddQuestion(campId, clientId, campName, initialIcon)
{
  var campId = $('.selectCamp').val();
if(campId == "")
{
$('.formPreview').html('');
return false;
}
    //swal('let go');
   // showLoadingIcon(initialIcon, campId);

    $('.previewArea').slideDown();
 $('.formArea').slideUp();
$('.AddFormBtn').html('Add Question');

    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $('html,body').animate({
        scrollTop: $("#previewArea").offset().top},
           'slow');
    $('.formPreview').html('<div align="center" class="fadedDiv">Loading...</div>');
    $.ajax({
        url: '/getQuestionPreview',
        type: 'get',
        data: {'_token': CSRF_TOKEN, 'campId': campId},
        success: function (data) {

  localStorage.setItem('previewInfo', campId+'__&&__'+data[2]+'__&&__'+data[1]);

            $('.fadedDiv').fadeOut(300);
      
            $('.formPreview').html(data[0]);




            //removeLoadingIcon(initialIcon, campId);
        },
        error: function (data) {
            
            //removeLoadingIcon(initialIcon, campId);
            $('.fadedDiv').fadeOut(300);

            if(campId == "")
{
$('.formPreview').html('<div style="font-weight: bolder;padding: 30px;" align="center">No Campaign Selected</div>');
}
            $('.formPreview').html('<div style="font-weight: bolder;padding: 30px;" align="center">Oops! Error Occured</div>');
        }
    });


}


function UpdateCampQuest(campId, questId)
{

localStorage.setItem('questToEdit', questId);
    var prevInfo = localStorage.getItem('previewInfo');
    $('.actionType').val('update');
$('.AddFormBtn').html('Close Form');
    var getInitial = $('.formArea').html();
    $('.formArea').html('<div align="center" class="fadedDiv">Loading...</div>');
    $('.formArea').slideDown();

    
    $('html,body').animate({
        scrollTop: $(".previewArea").offset().top},
           'slow');
       var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
   
    $.ajax({
        url: '/getCampAttribute',
        type: 'get',
        data: {'_token': CSRF_TOKEN, 'campId': campId, 'questId': questId },
        success: function (data) {
          
          $('.fadedDiv').fadeOut(300);
              $('.formArea').html(getInitial);
          $('.moreOptions').html('');
         
    $('.campName').html('<option value="' + prevInfo.split('__&&__')[0] + '_' + prevInfo.split('__&&__')[1] + '">' + prevInfo.split('__&&__')[2] + '</option>');

  $('.nicEdit-main').remove();

 $('.nicEdit-panelContain').remove();
    var myWysiwyg = new nicEditor({fullPanel : true}).panelInstance('campQuest');

          $('.nicEdit-main').html(data[0]);
           if(data[2] == "mySelect" || data[2] == "myRadio")
           {
           
           $('#myResponse').val('mySelect');
           getType();
           
           $.each(data[1].split('__&&__'), function(count, val){

           if(val != ""){
            
           $('.opt_'+count).val(val);
           if(val == data[3])
            {
           $("#Expected_"+count).prop("checked", true)
           
            }

           if(count < data[1].split('__&&__').length-2)
           {

           addto('mySelect');

           }
           
            }

            });
           

           }
   else          
           if(data[2] == "myRange")
           {
           
           $('#myResponse').val('myRange');
           getType();
           
           var max = data[1].split('__&&__')[0].split('-')[0];
          var min = data[1].split('__&&__')[0].split('-')[1];
           var maxLabel = data[1].split('__&&__')[1].split('-')[1];
          var minLabel = data[1].split('__&&__')[1].split('-')[0];

         
 
        $('.rangemax').val(max);
         $('.rangemin').val(min);
 $('.leastval').val(data[3]);
 $('.minvaldesc ').val(minLabel);
 $('.maxvaldesc').val(maxLabel);

           }
  else          
           if(data[2] == "myCheck")
           {
           
           $('#myResponse').val('myCheck');
           getType();

             $.each(data[1].split('__&&__'), function(count, val){

           if(val != ""){
            
           $('.opt_'+count).val(val);

    if($.inArray(val, data[3].split('__&&__')) > -1){
      
           $("#ExpectedBox_"+count).prop("checked", true)
           
            }

           if(count < data[1].split('__&&__').length-2)
           {

           addto('myCheck');

           }
           
            }

            });
           
          var logic = data[3].split('__&&__')[data[3].split('__&&__').length-1];
          if(logic == "AND"){
            $('.andLogic').prop("checked", true);
          }
          else if(logic == "OR"){
           {

               $('.orLogic').prop("checked", true);
            }



}

               }


        },
        error: function (data) {
            
            $('.fadedDiv').fadeOut(300);
            $('.formPreview').html('<div style="font-weight: bolder;padding: 30px;" align="center">Oops! Error Occured</div>');
        }
    });

  

}

function showFormArea()
{
 
//if($('.actionType').val() == 'update')
//{
clearCamp();
$('.actionType').val('Add');

    $('.formArea').slideToggle();
    var prevInfo = localStorage.getItem('previewInfo');
    $('.campName').html('<option value="' + prevInfo.split('__&&__')[0] + '_' + prevInfo.split('__&&__')[1] + '">' + prevInfo.split('__&&__')[2] + '</option>');

    var AddFormBtnState = $('.AddFormBtn').html();
    if(AddFormBtnState == "Add Question")
    {
        $('.AddFormBtn').html('Close Form');
        var myWysiwyg = new nicEditor({fullPanel : true}).panelInstance('campQuest');
    }
    else
    {
        $('.AddFormBtn').html('Add Question');
    }
/*
}else if($('.actionType').val() == 'Add'){ 

    $('.actionType').val('Add');

    $('.formArea').slideToggle();
    var prevInfo = localStorage.getItem('previewInfo');
    $('.campName').html('<option value="' + prevInfo.split('__&&__')[0] + '_' + prevInfo.split('__&&__')[1] + '">' + prevInfo.split('__&&__')[2] + '</option>');

    var AddFormBtnState = $('.AddFormBtn').html();
    if(AddFormBtnState == "Add Question")
    {
        $('.AddFormBtn').html('Close Form');
        var myWysiwyg = new nicEditor({fullPanel : true}).panelInstance('campQuest');
    }
    else
    {
        $('.AddFormBtn').html('Add Question');
    }
}*/

}

function confirnCampDel(campId, questId)
{

$('#confirmCampDelete').modal();
$('.campId').val(campId);
$('.questId').val(questId);

}

function deleteCampQuest()
{
var campId = $('.campId').val();
var questId = $('.questId').val();
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    //swal('campId = '+campId+' and QuestId = '+questId);
    $.ajax({
        url: '/deleteCampQuest',
        type: 'get',
        data: {'_token': CSRF_TOKEN, 'questId': questId, 'campId': campId},
        success: function (data) {
$('#confirmCampDelete').toggle();
          if(data == "Delete Successful"){
$('#confirmCampDelete').modal('toggle');
            $('.quest_'+questId).slideUp();

//decrease quest count column
               $('.QuestCount_'+campId).html(($('.QuestCount_'+campId).html())-1);
            }
else
{
swal(data);
}
        },
        error: function (data) {
            $('#confirmCampDelete').toggle();
            swal('error occured');
        }
    });


}


function resendMail(clientId)
{
showLoadingIcon('fa-paper-plane', clientId);
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/clientResendActivationMail',
        type: 'get',
        data: {'_token': CSRF_TOKEN, 'clientId': clientId },
        success: function (data) {
       removeLoadingIcon('fa-paper-plane', clientId);

         swal(data);
        },
        error: function (data) {
       removeLoadingIcon('fa-paper-plane', clientId);
            swal('Error occured');
        }
    });

}