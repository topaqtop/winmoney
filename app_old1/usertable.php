<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class usertable extends Model
{
    //
     protected  $fillable = ['facebook_id', 'Surname', 'Other_Name', 'Email', 'State', 'City', 'Gender', 'Age_Range', 'Phone_Number', 'Password', 'Confirm_Password', 'ReferrerCode', 'ReferrerBy', 'UserImg', 'SentCode'];
}
