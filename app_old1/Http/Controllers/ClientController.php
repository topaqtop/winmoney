<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\AnswerRecord;
use App\usertable;
use App\adminList;
use App\availableprizes;
use App\OurWinner;
use App\OurCampaign;
use App\SpecialPrize;
use App\CampaignQuestions;
use App\Clients;
use Session;
use DB;
use Carbon\Carbon;
use App\SessionKeeper;
use App\FormBuilder;

class ClientController extends Controller {

    //


  //  public function ClientPage() {
    //    return view('ClientPage');
  // 

    public function RegisterPage() {

        return view('RegisterPage');
    }


public function newmemberReg(Request $request)
{


  $getUser = usertable::where('Email', '=', $request->Email)->first();
            if ($getUser) {
                return response()->json('Email Already Exists.');
            }
            $user = new usertable();
          
            $user->Surname = $request->Surname;
            $user->Other_Name = $request->OtherName;
            $user->Email = $request->Email;
            $user->State = $request->State;
            $user->City = $request->City;
            $user->Phone_Number = $request->Phone;
            $user->facebook_id = "Direct Reg.";

            $user->save();

            $checkEmail = usertable::where('Email', '=', $request->Email)->first();
          //  session()->put('logedIn', $checkEmail->id);
            Session::put('logedIn', $checkEmail->id);
            //Session::set('logedIn', 1);


            $getUser = usertable::where('Email', '=', $request->Email)->first();
            if (!$getUser) {
                return response()->json('Unsuccessful! Error Occured.');
            }

            $linkPassword = rand(100000000, 999999999);
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < 30; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            ;

            $getUser->SentCode = $randomString . $linkPassword;
            $getUser->CodeTimeStart = Carbon::now();
            $getUser->save();

            @$headers = 'From: ' . "no-reply@winmoney.ng" . "\r\n";

           mail($request->Email, "Winmoney Account Confirmation", "Clink the link below to confirm your account \r\n http://winmoney.ng/confirmAccount/" . $randomString.$linkPassword, @$headers);


            return response()->json('Success');
         

}

     public function RegisterUser(Request $request) {

        //lets validate
        $rules = array(
            'Surname' => 'required', // just a normal required validation
            'Other_Name' => 'required',
            'Email' => 'required|email', // required and must be unique in the ducks table
            'State' => 'required',
            'City' => 'required',
//            'Gender' => 'required',
//            'Age_Range' => 'required',
           'Phone_Number' => 'required|min:11|numeric', // required and has to match the password field
           // 'Password' => 'required|min:6',
          //  'Confirm_Password' => 'required|same:Password',
            'g-recaptcha-response' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        // check if the validator failed -----------------------
        if ($validator->fails()) {

            return Redirect::to('')
                            ->withErrors($validator)
                            ->withInput(Input::except('password1', 'password2'));
        } else {
            session_start();
            //get random referred code


           // $referrerCode = rand(100000000, 999999999);
//            $uniqueCode = "0";
//            while($checkIfCodeExist = usertable::where('ReferrerCode', '=', $referrerCode)->first())
//            {
//                $uniqueCode = (string)$referrerCode;
//            }
           $input = Input::only('Surname', 'Other_Name', 'Email', 'State', 'City', 'Phone_Number', 'Gender', 'Age_Range', 'ReferrerCode', 'Password', 'Confirm_Password');
           
           $getUser = usertable::where('Email', '=', $input['Email'])->first();
            if ($getUser) {
                return Redirect::to('')->with('message', 'Email Already Exists.');
            }
            $user = new usertable();
          
            $user->Surname = $input['Surname'];
            $user->Other_Name = $input['Other_Name'];
            $user->Email = $input['Email'];
            $user->State = $input['State'];
            $user->City = $input['City'];
//            $user->Gender = $input['Gender'];
//            $user->Age_Range = $input['Age_Range'];
            $user->Phone_Number = $input['Phone_Number'];
           // $user->Password = $input['Password'];
           // $user->Confirm_Password = $input['Confirm_Password'];
            $user->facebook_id = "Direct Reg.";
           // $user->ReferrerCode = $referrerCode;

            $user->save();

          // $checkEmail = usertable::where('Email', '=', $input['Email'])->where('Password', '=', md5($input['Password']))->first();
            $checkEmail = usertable::where('Email', '=', $input['Email'])->first();
            session()->put('logedIn', $checkEmail->id);
            Session::put('logedIn', $checkEmail->id);
            //Session::set('logedIn', 1);


            $getUser = usertable::where('Email', '=', $input['Email'])->first();
            if (!$getUser) {
                return Redirect::to('')->with('message', 'Unsuccessful! Error Occured.');
            }

            $linkPassword = rand(100000000, 999999999);
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < 30; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            ;

            $getUser->SentCode = $randomString . $linkPassword;
            $getUser->CodeTimeStart = Carbon::now();
            $getUser->save();

            @$headers = 'From: ' . "no-reply@winmoney.ng" . "\r\n";

           mail($input['Email'], "Winmoney Account Confirmation", "Clink the link below to confirm your account \r\n http://winmoney.ng/confirmAccount/" . $randomString.$linkPassword, @$headers);


            return Redirect::to('')->with('message', 'Registeration Successful! \n We Have Sent A Link To Your Email For Account Confirmation.');
            // return redirect()->back()->with('message', 'SignUp Successful');
        }
    }

    public function confirmAccount($response) {
        $getUser = usertable::where('SentCode', '=', $response)->first();
        if (!$getUser) {
            return view("confirmpassword")->with('response', 'Link either expired or never existed');
        } else {
            $dateFromDatabase = strtotime((string) $getUser->CodeTimeStart);
            $dateTwelveHoursAgo = strtotime("-1440 minutes");

           if ($dateFromDatabase >= $dateTwelveHoursAgo) {
                // less than 5minutes ago
                //$getUser->UserStatus = 1;
               // $getUser->save();
                Session::put('logedIn', $getUser->id);
                  return Redirect::to('/completeRegistration');
             //   return Redirect::to('/successful')->with('regStatus', 'Complete');
          } else {
                // more than 5minutes ago
               return view("confirmpassword")->with('response', 'Link either expired or never existed');
           }
        }
    }

    public function completeRegistration()
    {
        
        return view("completeRegistration");
    }
    
    public function userComplete(Request $request)
    {
         $rules = array(
          
            'Gender' => 'required',
           'Age_Range' => 'required',
             'Password' => 'required|min:6',
            'Confirm_Password' => 'required|same:Password',
           // 'Phone_Number' => 'required|min:11|numeric', // required and has to match the password field
           
        );
        $validator = Validator::make(Input::all(), $rules);

        // check if the validator failed -----------------------
        if ($validator->fails()) {

            return Redirect::to('completeRegistration')
                            ->withErrors($validator);
                            //->withInput(Input::except('password1', 'password2'));
        } else {
            
          // $referrerCode = rand(100000000, 999999999);
             
          //  $input = Input::only('Gender', 'Age_Range', 'ReferrerCode', 'Password', 'Confirm_Password');
           $input = Input::only('Gender', 'Age_Range', 'Password', 'Confirm_Password');
            /* if (@$input['ReferrerCode'] != null && @$input['ReferrerCode'] != "") {
                $checkReferrerCode = usertable::where('ReferrerCode', '=', @$input['ReferrerCode'])->first();
                if (!$checkReferrerCode) {
                    return Redirect::to('/completeRegistration')->with('message', 'Incorrect Referrer Code');
                } else {
                    $user->ReferrerBy = @$input['ReferrerCode'];
                }
            }*/
            $user = usertable::find(Session::get('logedIn'));
         
             $user->Gender = $input['Gender'];
            $user->Age_Range = $input['Age_Range'];
            //$user->Phone_Number = $input['Phone_Number'];
            $user->Password = md5($input['Password']);
            $user->Confirm_Password = md5($input['Confirm_Password']);
           // $user->ReferrerCode = $referrerCode;
            $user->UserStatus = 1;
           
            $user->save();

            Session::put('logedIn', Session::get('logedIn'));
            Session::put('regStatus', 'Complete');
    
         return Redirect::to('/successful'); 
        }

    }

    public function CompleteRegPage() {
        $NigerianStates = array(
            'Abuja',
            'Abia',
            'Adamawa',
            'Akwa Ibom',
            'Anambra',
            'Bauchi',
            'Bayelsa',
            'Benue',
            'Borno',
            'Cross River',
            'Delta',
            'Ebonyi',
            'Edo',
            'Ekiti',
            'Enugu',
            'Gombe',
            'Imo',
            'Jigawa',
            'Kaduna',
            'Kano',
            'Katsina',
            'Kebbi',
            'Kogi',
            'Kwara',
            'Lagos',
            'Nassarawa',
            'Niger',
            'Ogun',
            'Ondo',
            'Osun',
            'Oyo',
            'Plateau',
            'Rivers',
            'Sokoto',
            'Taraba',
            'Yobe',
            'Zamfara',
        );
        $stateArray = "<option value=''>Your state Of residence</option>";
        foreach ($NigerianStates as $k) {
            $stateArray = $stateArray . "<option value='" . $k . "'>" . $k . "</option>";
        }
        return view('CompleteRegPage')->with('allStates', $stateArray);
    }

    public function UpdateUserInfo(Request $request) {
        try {
           
            
            $id = Session::get('logedIn');
            //$user = user::findOrFail($id);
            $user = usertable::where('facebook_id', $id)->first();
            if($user  == null)
            {
            
            $user = usertable::where('id', $id)->first(); 
            
            }
            //$request contain your post data sent from your edit from
            //$user is an object which contains the column names of your table
            //Set user object attributes
            $referrerCode = rand(100000000, 999999999);

            $user->Phone_number = $request->Phone;
           //  return response()->json($user->Phone_number);
            $user->Password = md5($request->Password);
            $user->Confirm_Password = md5($request->Password);
            $user->State = $request->State;
            $user->City = $request->City;
            $user->Age_Range = $request->Age;
           // $user->ReferrerCode = $referrerCode;
             $user->UserStatus= 1;

            session()->put('logedIn', $id);
            Session::put('logedIn', $id);
            if (@$request->ref != null && @$request->ref != "") {
                $checkReferrerCode = usertable::where('ReferrerCode', '=', @$request->ref)->first();
                if (!$checkReferrerCode) {
                    Session::put('regStatus', 'Incomplete');
                    return response()->json('Incorrect Referrer Code, Please Try Again.');
                } else {
                    $user->ReferrerBy = @$request->ref;
                }
            }
            Session::put('regStatus', 'Complete');
            // Save/update user. 
            // This will will update your the row in ur db.
            $user->save();

            // Session::set('logedIn', 1);w
            return response()->json("Success");

            // return Redirect::to('/successful')->with('UserName', $id);
        } catch (ModelNotFoundException $err) {
            //Show error page
             return response()->json('An Error Has Occured');

        }
    }

      public function checkRegComplete()
      {
      
      $checkInfo = usertable::find(Session::get('logedIn'));
      if($checkInfo == null)
      {
      
       $checkInfo = usertable::where('facebook_id', '=', Session::get('logedIn'))->first();
      }
      
      if($checkInfo != null)
      {
      
      if($checkInfo->Password == null)
      {
      
      return response()->json(false);
      
      }
      
      return response()->json(true);
      
      }
      
      return response()->json(false);
      
      }




    public function nowLoggedIn() {
        //   $logedInStatus =  session()->pull('logedIn');
        Session::put('camLimit', 20);
        if (Session::get('logedIn') != "" && Session::get('logedIn') != null) {
            // $id = session()->pull('logedIn');
            //save it to the session again so it wont forget
            Session::get('logedIn');

            $checkInfo = usertable::where('facebook_id', '=', Session::get('logedIn'))->first();
            if ($checkInfo == null) {
                $checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
            }




            if ($checkInfo->UserImg != null && $checkInfo->UserImg != "") {
                Session::put('DP', $checkInfo->UserImg);
            } else if ($checkInfo->facebook_id != null && $checkInfo->facebook_id != "" && $checkInfo->facebook_id != "Direct Reg.") {
                Session::put('DP', $checkInfo->facebook_id);
            } else {
                Session::put('DP', 'noDP');
            }



            // return view("Campaigns")->with('UserName', $checkInfo->Surname." ".$checkInfo->Other_Name);
            // $newQuest = DB::table('clientcampaigns')->whereNotIn('id', function($q){
            //$q->select('CampaignID')->from('answer_records');
//})->get();
            // $newQuest = DB::select("SELECT * FROM clientcampaigns WHERE id NOT IN ( SELECT CampaignID FROM answer_records )");
            // foreach ($newQuest as $eachRow) {
            //$getQuest = CampaignQuestions::all()->where('CampaignID', '=', $eachRow->id); 
            //DB::select("SELECT * FROM campaign_questions WHERE CampaignID = $eachRow->id");
            // }
            //  $latest = DB::table('campaign_questions')
            // ->select('*')->where('CampaignID', '=', DB::table('clientcampaigns')->select('id')->whereNotIn('CampaignID', '=', DB::table('answer_records')->select('CampaignID')))->get();
//return view("IndexPage")->with('regStatus', 'Incomplete')->with('allStates', $stateArray);
            $regStatus = Session::get('regStatus');
            $stateArray = Session::get('allStates');
            // $prizeTotal = DB::raw("SELECT sum(NumberAvailable) FROM availableprizes");

            $prizeTotal = DB::table('availableprizes')
                    ->sum('NumberAvailable');
            if ($regStatus == "Complete") {
                
                $forCompleted = DB::select("SELECT pm.* FROM campaign_questions pm Natural left join answer_records WHERE pm.CampaignID in (SELECT pd.CampaignID FROM answer_records pd where UserID = " . $checkInfo->id . ")");
                $latest = DB::select("SELECT pm.* FROM campaign_questions pm Natural left join answer_records WHERE pm.CampaignID NOT IN (SELECT pd.CampaignID FROM answer_records pd where UserID = " . $checkInfo->id . ") order by pm.ID");
                $winners = DB::select("SELECT * FROM our_winners");
                $allPrizes = DB::select("SELECT * FROM availableprizes limit 7");
                if ($checkInfo->ReferrerCode != null && $checkInfo->ReferrerCode != "") {
                    $referredUsers = DB::select("SELECT * FROM usertables where ReferrerBy = " . $checkInfo->ReferrerCode . "  limit 7");
                } else {
                    $referredUsers = array();
                }
                $countCamp = DB::select("SELECT count(*) from our_campaigns");
                $allQuestions = DB::select("SELECT * from our_campaigns where EndDate > " . Carbon::now()->format('Y-m-d')." and StartDate >= " . Carbon::now()->format('Y-m-d')." and Status = 1 ");
                $countQuest = 0;
                $arrayCountCampaign = array();
                foreach ($allQuestions as $eachQuestion) {
                    $confirmQuest = AnswerRecord::where('CampaignID', '=', $eachQuestion->id)->where('UserID', '=', Session::get('logedIn'))->first();
                  // $confirmQuest = DB::select("SELECT Distinct UserID from answer_records where UserID = ".Session::get('logedIn')." and ");
                   
                    if ($confirmQuest != null && !in_array($confirmQuest->id, $arrayCountCampaign)) {
                        $countQuest++;
                         $arrayCountCampaign[] = $confirmQuest->id;
                    }
                }
                 foreach ($allQuestions as $eachQuestion) {
                    $confirmQuest = CampaignQuestions::where('CampaignID', '=', $eachQuestion->id)->first();
                    if ($confirmQuest == null) {
                        $countQuest++;
                    }
                }
                $allanswered = DB::select("SELECT DISTINCT CampaignID FROM answer_records where UserID = " . $checkInfo->id);
                return view("IndexPage")->with('UserName', @$checkInfo)
                                ->with(@compact('latest', 'latest'))
                                ->with(@compact('winners', 'winners'))
                                ->with(@compact('allPrizes', 'allPrizes'))
                                ->with(@compact('referredUsers', 'referredUsers'))
                                ->with(@compact('prizeTotal', $prizeTotal))
                                ->with('regStatus', 'Complete')
                                ->with('answered', @(count($allanswered)))
                                ->with('DP', @Session::get('DP'))
                                ->with(@compact('forCompleted', 'forCompleted'))
                                ->with('available', @(count($allQuestions)-$countQuest));
            } else if ($regStatus == "Incomplete") {
                $NigerianStates = array(
                    'Abuja',
                    'Abia',
                    'Adamawa',
                    'Akwa Ibom',
                    'Anambra',
                    'Bauchi',
                    'Bayelsa',
                    'Benue',
                    'Borno',
                    'Cross River',
                    'Delta',
                    'Ebonyi',
                    'Edo',
                    'Ekiti',
                    'Enugu',
                    'Gombe',
                    'Imo',
                    'Jigawa',
                    'Kaduna',
                    'Kano',
                    'Katsina',
                    'Kebbi',
                    'Kogi',
                    'Kwara',
                    'Lagos',
                    'Nassarawa',
                    'Niger',
                    'Ogun',
                    'Ondo',
                    'Osun',
                    'Oyo',
                    'Plateau',
                    'Rivers',
                    'Sokoto',
                    'Taraba',
                    'Yobe',
                    'Zamfara',
                );
                $stateArray = "<option value=''>Your state of residence</option>";
                foreach ($NigerianStates as $k) {
                    $stateArray = $stateArray . "<option value='" . $k . "'>" . $k . "</option>";
                }
                 $forCompleted = DB::select("SELECT pm.* FROM campaign_questions pm Natural left join answer_records WHERE pm.CampaignID in (SELECT pd.CampaignID FROM answer_records pd where UserID = " . $checkInfo->id . ")");
                $latest = DB::select("SELECT pm.* FROM campaign_questions pm Natural left join answer_records WHERE pm.CampaignID NOT IN (SELECT pd.CampaignID FROM answer_records pd where UserID = " . $checkInfo->id . ") order by pm.ID");
                $winners = DB::select("SELECT * FROM our_winners");
                $allPrizes = DB::select("SELECT * FROM availableprizes limit 7");
                if ($checkInfo->ReferrerCode != null && $checkInfo->ReferrerCode != "") {
                    $referredUsers = DB::select("SELECT * FROM usertables where ReferrerBy = " . $checkInfo->ReferrerCode . "  limit 7");
                } else {
                    $referredUsers = array();
                }
                $countCamp = DB::select("SELECT count(*) from our_campaigns");
                $allQuestions = DB::select("SELECT * from our_campaigns where EndDate > " . Carbon::now()->format('Y-m-d')." and StartDate >= " . Carbon::now()->format('Y-m-d')." and Status = 1 ");
                $countQuest = 0;
                $arrayCountCampaign = array();
                foreach ($allQuestions as $eachQuestion) {
                    $confirmQuest = AnswerRecord::where('CampaignID', '=', $eachQuestion->id)->where('UserID', '=', Session::get('logedIn'))->first();
                    if ($confirmQuest != null && !in_array($confirmQuest->id, $arrayCountCampaign)) {
                        $countQuest++;
                         $arrayCountCampaign[] = $confirmQuest->id;
                    }
                }
                 foreach ($allQuestions as $eachQuestion) {
                    $confirmQuest = CampaignQuestions::where('CampaignID', '=', $eachQuestion->id)->first();
                    if ($confirmQuest == null) {
                        $countQuest++;
                    }
                }
                $allanswered = DB::select("SELECT DISTINCT CampaignID FROM answer_records where UserID = " . $checkInfo->id);
                return view("IndexPage")->with('UserName', @$checkInfo)
                                ->with(@compact('latest', 'latest'))
                                ->with(@compact('winners', 'winners'))
                                ->with(@compact('allPrizes', 'allPrizes'))
                                ->with(@compact('referredUsers', 'referredUsers'))
                                ->with(@compact('prizeTotal', $prizeTotal))
                                ->with('regStatus', 'Incomplete')
                                ->with('answered', @(count($allanswered)))
                                ->with('DP', @Session::get('DP'))
                                 ->with(@compact('forCompleted', 'forCompleted'))
                                ->with('available',  @(count($allQuestions)-$countQuest))
                                ->with('allStates', @$stateArray);
            } else {
                $NigerianStates = array(
                    'Abuja',
                    'Abia',
                    'Adamawa',
                    'Akwa Ibom',
                    'Anambra',
                    'Bauchi',
                    'Bayelsa',
                    'Benue',
                    'Borno',
                    'Cross River',
                    'Delta',
                    'Ebonyi',
                    'Edo',
                    'Ekiti',
                    'Enugu',
                    'Gombe',
                    'Imo',
                    'Jigawa',
                    'Kaduna',
                    'Kano',
                    'Katsina',
                    'Kebbi',
                    'Kogi',
                    'Kwara',
                    'Lagos',
                    'Nassarawa',
                    'Niger',
                    'Ogun',
                    'Ondo',
                    'Osun',
                    'Oyo',
                    'Plateau',
                    'Rivers',
                    'Sokoto',
                    'Taraba',
                    'Yobe',
                    'Zamfara',
                );
                $stateArray = "<option value=''>Your state of residence</option>";
                foreach ($NigerianStates as $k) {
                    $stateArray = $stateArray . "<option value='" . $k . "'>" . $k . "</option>";
                }
                $campignId = 0;
                 $forCompleted = DB::select("SELECT pm.* FROM campaign_questions pm Natural left join answer_records WHERE pm.CampaignID in (SELECT pd.CampaignID FROM answer_records pd where UserID = " . $checkInfo->id . ")");
                $latest = DB::select("SELECT pm.* FROM campaign_questions pm Natural left join answer_records WHERE pm.CampaignID NOT IN (SELECT pd.CampaignID FROM answer_records pd where UserID = " . $checkInfo->id . ") order by pm.ID");
                foreach ($latest as $d) {
                    $campignId = $d->CampaignID;
                }
                $winners = DB::select("SELECT * FROM our_winners");
                $allPrizes = DB::select("SELECT * FROM availableprizes limit 7");
                if ($checkInfo->ReferrerCode != null && $checkInfo->ReferrerCode != "") {
                    $referredUsers = DB::select("SELECT * FROM usertables where ReferrerBy = " . $checkInfo->ReferrerCode . "  limit 7");
                } else {
                    $referredUsers = array();
                }
                  $countCamp = DB::select("SELECT count(*) from our_campaigns");
                $allQuestions = DB::select("SELECT * from our_campaigns where EndDate > " . Carbon::now()->format('Y-m-d')." and StartDate >= " . Carbon::now()->format('Y-m-d')." and Status = 1 ");
                $countQuest = 0;
                $arrayCountCampaign = array();
                foreach ($allQuestions as $eachQuestion) {
                    $confirmQuest = AnswerRecord::where('CampaignID', '=', $eachQuestion->id)->where('UserID', '=', Session::get('logedIn'))->first();
                    if ($confirmQuest != null && !in_array($confirmQuest->id, $arrayCountCampaign)) {
                        $countQuest++;
                         $arrayCountCampaign[] = $confirmQuest->id;
                    }
                }
                foreach ($allQuestions as $eachQuestion) {
                    $confirmQuest = CampaignQuestions::where('CampaignID', '=', $eachQuestion->id)->first();
                    if ($confirmQuest == null) {
                        $countQuest++;
                    }
                }
                $allanswered = DB::select("SELECT DISTINCT CampaignID FROM answer_records where UserID = " . $checkInfo->id);

                return view("IndexPage")->with('UserName', @$checkInfo)
                                ->with(@compact('latest', 'latest'))
                                ->with(@compact('winners', 'winners'))
                                ->with(@compact('allPrizes', 'allPrizes'))
                                ->with(@compact('referredUsers', 'referredUsers'))
                                ->with('regStatus', 'unknown')
                                ->with('answered', @(count($allanswered)))
                                ->with(@compact('prizeTotal', $prizeTotal))
                                ->with('DP', @Session::get('DP'))
                                ->with('available',  @(count($allQuestions)-$countQuest))
                                ->with('answered', @(count($allanswered)))
                                ->with(@compact('forCompleted', 'forCompleted'))
                                ->with('allStates', @$stateArray);
            }
        } else {
            return Redirect::to('');
        }
//        if ($request->session()->exists('logedIn')) {
//            return Redirect::to('/successful');
//        } else {
//            return Redirect::to('');
//        }
    }

    public function WinMoneyWebsite() {
        $NigerianStates = array(
            'Abuja',
            'Abia',
            'Adamawa',
            'Akwa Ibom',
            'Anambra',
            'Bauchi',
            'Bayelsa',
            'Benue',
            'Borno',
            'Cross River',
            'Delta',
            'Ebonyi',
            'Edo',
            'Ekiti',
            'Enugu',
            'Gombe',
            'Imo',
            'Jigawa',
            'Kaduna',
            'Kano',
            'Katsina',
            'Kebbi',
            'Kogi',
            'Kwara',
            'Lagos',
            'Nassarawa',
            'Niger',
            'Ogun',
            'Ondo',
            'Osun',
            'Oyo',
            'Plateau',
            'Rivers',
            'Sokoto',
            'Taraba',
            'Yobe',
            'Zamfara',
        );
        $stateArray = "<option value=''>Your state of residence</option>";
        foreach ($NigerianStates as $k) {
            $stateArray = $stateArray . "<option value='" . $k . "'>" . $k . "</option>";
        }

        $allPrize = availableprizes::select('PrizeName')->get();
        $allWInners = OurWinner::all();
        return view('website')->with('allStates', $stateArray)->with(compact('allPrize', 'allPrize'))
                        ->with(compact('allWInners', 'allWInners'));
    }

    public function userSignin(Request $request) {
        //lets validate

        session_start();
        $checkInfo = usertable::where('Email', '=', $request->username)->where('Password', '=', md5($request->password))->first();

        if ($checkInfo != null) {

            if ($checkInfo->UserStatus == 0) {
                return response()->json("Invalid Profile");
            }

            session()->put('logedIn', $checkInfo->id);
            Session::put('logedIn', $checkInfo->id);
            // Session::set('logedIn', 1);
            // $campaign_questions = new campaign_questions();
            $getQuest = "";
            $latest = DB::select("SELECT pm.* FROM campaign_questions pm Natural left join answer_records WHERE pm.CampaignID not in (SELECT pd.CampaignID FROM answer_records pd where UserID = " . $checkInfo->id . ")");
            //return view('Campaigns');
            Session::put('regStatus', 'Complete');
            $winners = DB::select("SELECT * FROM our_winners limit 5");
            return response()->json("Success");
//            return Redirect::to('/successful')->with('UserName', $checkInfo)->with(compact('latest', 'latest'))
//                            ->with(compact('winners', 'winners'))
//                            ->with('regStatus', 'Complete');
        } else {

            return response()->json("Incorrect Details");
        }
    }

    public function userSignout() {
        Session::flush();
        return Redirect::to('');
    }

    public function prevCamp() {
        if (Session::get('logedIn') != "" && Session::get('logedIn') != null) {
            // $id = session()->pull('logedIn');
            //save it to the session again so it wont forget
            Session::get('logedIn');
            //$checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
            $checkInfo = usertable::where('facebook_id', '=', Session::get('logedIn'))->first();
            if ($checkInfo == null) {
                $checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
            }
            $latest = DB::select("SELECT pm.* FROM campaign_questions pm Natural left join answer_records WHERE pm.CampaignID in (SELECT pd.CampaignID FROM answer_records pd where UserID = " . $checkInfo->id . ")");

            $winners = DB::select("SELECT * FROM our_winners limit 5");

            return view("previousCampaign")->with('UserName', $checkInfo)
                            ->with(compact('latest', 'latest'))
                            ->with(compact('winners', 'winners'));
        } else {
            return Redirect::to('');
        }
    }

    public function saveAnswer(Request $request) {

        $count = 0;
        foreach (@$request->answer as $answer) {
            $answer_record = new AnswerRecord();
            $answer_record->UserID = Session::get('logedIn');
            $answer_record->QuestionID = $request->questionId[$count];
            $answer_record->Answer = $answer;
            $answer_record->CampaignID = $request->campaignID;
            $answer_record->save();

            //scoring
            //get input type, questionID and expected response from question table
            $questionInfo = CampaignQuestions::where('id', '=', $request->questionId[$count])->first();

            //get questionID and answer selected from answer records based on userid
            $answerInfo = AnswerRecord::where('QuestionID', '=', $request->questionId[$count])
                    ->where('UserID', '=', Session::get('logedIn'))
                    ->first();

            //select option
            if ($questionInfo->ResponseType == "mySelect" || $questionInfo->ResponseType == "myRadio") {
                if (str_replace('__&&__', '', $answerInfo->Answer) == $questionInfo->ExpectedResponse) {
                    $answerInfo->Score = 1;
                } else {
                    $answerInfo->Score = 0;
                }
            } else if ($questionInfo->ResponseType == "myRange") {
                if (str_replace('__&&__', '', $answerInfo->Answer) >= $questionInfo->ExpectedResponse) {
                    $answerInfo->Score = 1;
                } else {
                    $answerInfo->Score = 0;
                }
            } else if ($questionInfo->ResponseType == "myCheck") {
                //get answer logic
                $answerArray = explode('__&&__', $answerInfo->Answer);
                $questionArray = explode('__&&__', $questionInfo->ExpectedResponse);
                $logic = explode('__&&__', $questionInfo->ExpectedResponse)[count(explode('__&&__', $questionInfo->ExpectedResponse)) - 1];
                if ($logic == "AND") {
                    if (count(array_diff($answerArray, $questionArray)) >= 1) {
                        $answerInfo->Score = 0;
                    } else {
                        $answerInfo->Score = 1;
                    }
                } else if ($logic == "OR") {
                    if (count(array_intersect($answerArray, $questionArray)) >= 1) {
                        $answerInfo->Score = 1;
                    } else {
                        $answerInfo->Score = 0;
                    }
                }
            }

            $answerInfo->save();
            $count++;
        }
        return response()->json($answer_record);
    }

    public function alreadyAnswered(Request $request) {
        $answer_record = AnswerRecord::where('UserID', '=', Session::get('logedIn'))->where('CampaignID', '=', $request->campaignID)->get();
        if ($answer_record) {
            return response()->json(var_dump($answer_record));
        }
    }

    public function getCampaign(Request $request) {
        $frames = "";
        if ($request->action == "done") {
            
            //for Not done campaign up
            $special = "";
            $test = "";
            //  $checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
            $checkInfo = usertable::where('facebook_id', '=', Session::get('logedIn'))->first();
            if ($checkInfo == null) {
                $checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
            }
            $latest = DB::select("SELECT pm.* FROM campaign_questions pm Natural left join answer_records WHERE pm.CampaignID in (SELECT pd.CampaignID FROM answer_records pd where UserID = " . $checkInfo->id . ")");
            $count = 0;
            $allId = array();
            $campCount = 0;
            foreach ($latest as $myCampaign) {

                $tillEnd = (Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->diffInDays(Carbon::now()) + 1) == 1 ? 'Campaign Ends today' : (Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->diffInDays(Carbon::now()) + 1) . ' Day(s) to end';
                $specialPrize = SpecialPrize::where('CampignID', '=', $myCampaign->CampaignID)->first();
                if ($specialPrize) {
                    $special = '<h5>Special Prize: ' . $specialPrize->SpecialPrize . '</h5>';
                } else {
                    $special = '<h5>&emsp;</h5>';
                }
                if (!in_array($myCampaign->CampaignID, $allId)) {

                    $allId[] = $myCampaign->CampaignID;
                    $count++;

                    $eachQuest = "<ul class='questions'>";
                    csrf_field();
                    $countForm = 0;
                    foreach ($latest as $getQuest) {
                        if ($getQuest->CampaignID == $myCampaign->CampaignID) {

                            $countForm++;
                            //  $eachQuest = $eachQuest . "<label>" . $getQuest->Question . "</label><br>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $getQuest->ResponseData)) . "<br><br><input type='hidden' name='QuestID' class='Quest_" . $getQuest->id . "' value='" . $getQuest->id . "' />";
                            if ($getQuest->ResponseType == "mySelect") {
                                $inputPart = "";
                                // $getInput = DB::select("SELECT FormContent FROM form_builders where FormType = '" . $getQuest->ResponseType . "'");
                                $getInput = FormBuilder::where('FormType', '=', $getQuest->ResponseType)->first();
                                foreach (explode(',', $getQuest->ResponseData) as $eachOpt) {
                                    if ($eachOpt != "" && $eachOpt != null)
                                        $inputPart = $inputPart . "<option value='" . $eachOpt . "'>" . $eachOpt . "</option>";
                                }
                                $finalInput = str_replace('eachopt', $inputPart, $getInput->FormContent);
                                //  $eachQuest = $eachQuest . "<label>" . $getQuest->Question . "</label><br>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $finalInput)) . "<br><br><input type='hidden' name='QuestID' class='Quest_" . $getQuest->id . "' value='" . $getQuest->id . "' />";
                                $eachQuest = $eachQuest . " <li class='question-list'>" .
                                        "<div class='wrapper'>" .
                                        "<div class='item'>" .
                                        "<span><i class='fa fa-arrow-right' style='color: gold;'></i></span>" .
                                        "</div>" .
                                        " <div class='question'><span>" . $getQuest->Question . "</span></div>" .
                                        "<div class='response'>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $finalInput)) . "</div><input type='hidden' name='QuestID' class='Quest_" . $getQuest->id . "' value='" . $getQuest->id . "' />" .
                                        "</div>" .
                                        " </li>";
                            }
                            else if ($getQuest->ResponseType == "myCheck") {
                                $inputPart = "<span class='checkBoxClass_" . $getQuest->id . "'><input type='hidden' name='checkBoxClass_" . $getQuest->id . "' value='checkBoxClass_" . $getQuest->id . "'>";

                                // $getInput = DB::select("SELECT FormContent FROM form_builders where FormType = '" . $getQuest->ResponseType . "'");
                                $getInput = FormBuilder::where('FormType', '=', $getQuest->ResponseType)->first();
                                foreach (explode(',', $getQuest->ResponseData) as $eachOpt) {
                                    if ($eachOpt != "" && $eachOpt != null) {

                                        // $f = str_replace('eachopt', $eachOpt, $getInput->FormContent);
                                        $inputPart = $inputPart . "<input type='checkbox' name='checkbox' class='toDisable_subforcampid largeChecks' id='val_subforcount' value='" . $eachOpt . "' >" . $eachOpt . " &emsp;";
                                    }
                                }
                                $inputPart = $inputPart . "</span>";
                                // $eachQuest = $eachQuest . "<label>" . $getQuest->Question . "</label><br>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $inputPart)) . "<br><br>";
                                $eachQuest = $eachQuest . " <li class='question-list'>" .
                                        "<div class='wrapper'>" .
                                        "<div class='item'>" .
                                        "<span><i class='fa fa-arrow-right' style='color: gold;'></i></span>" .
                                        "</div>" .
                                        " <div class='question'><span>" . $getQuest->Question . "</span></div>" .
                                        "<div class='response'>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $inputPart)) . "</div>" .
                                        "</div>" .
                                        " </li>";
                            } else if ($getQuest->ResponseType == "myRadio") {
                                $inputPart = "<span class='radioClass_" . $getQuest->id . "'><input type='hidden' name='radioClass_" . $getQuest->id . "' value='radioClass_" . $getQuest->id . "'>";
                                // $getInput = DB::select("SELECT FormContent FROM form_builders where FormType = '" . $getQuest->ResponseType . "'");
                                $getInput = FormBuilder::where('FormType', '=', $getQuest->ResponseType)->first();
                                foreach (explode(',', $getQuest->ResponseData) as $eachOpt) {
                                    if ($eachOpt != "" && $eachOpt != null) {
                                        // $f = str_replace('eachopt', $eachOpt, $getInput->FormContent);
                                        $inputPart = $inputPart . "<input type='radio' name='radioClass_" . $getQuest->id . "' class='toDisable_subforcampid largeChecks' id='val_subforcount' value='" . $eachOpt . "' >" . $eachOpt . " &emsp;";
                                    }
                                }
                                $inputPart = $inputPart . "</span>";
                                $eachQuest = $eachQuest . "<label>" . $getQuest->Question . "</label><br>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $inputPart)) . "<br><br>";
                            } else if ($getQuest->ResponseType == "myRange") {
                                $max = explode('-', explode(',', $getQuest->ResponseData)[0])[0];
                                $min = explode('-', explode(',', $getQuest->ResponseData)[0])[1];
                                //   $inputPart = "<input type='text' id='' name='rangeClass_' class='rangeSelect_".$getQuest->id."' value='' />";
                                $inputPart = "<input type='text' id='rangeSelect" . $getQuest->id . "' name='rangeClass_" . $getQuest->id . "' class='form-control rangeClass_" . $getQuest->id . "'  value='' /><div><span class='pull-left leastDesc rangeInfo' style='color: #333;   font-size: 12px; line-height: 1.333;text-shadow: none;top: 0;padding: 1px 5px; background: rgba(0,0,0,0.1); border-radius: 3px;'>" . explode('-', explode(',', $getQuest->ResponseData)[1])[0] . "</span><span class='pull-right maxDesc rangeInfo' style='color: #333;   font-size: 12px; line-height: 1.333;text-shadow: none;top: 0;padding: 1px 5px; background: rgba(0,0,0,0.1); border-radius: 3px;'>" . explode('-', explode(',', $getQuest->ResponseData)[1])[1] . "</span></div>";

                                $eachQuest = $eachQuest . " <li class='question-list'>" .
                                        "<div class='wrapper'>" .
                                        "<div class='item'>" .
                                        "<span><i class='fa fa-arrow-right' style='color: gold;'></i></span>" .
                                        "</div>" .
                                        " <div class='question'><span>" . $getQuest->Question . "</span></div>" .
                                        "<div class='response'>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $inputPart)) . "</div>" .
                                        "</div>" .
                                        " </li>" .
                                        "<script>" .
                                        "$('#rangeSelect" . $getQuest->id . "').ionRangeSlider({" .
                                        "hide_min_max: false," .
                                        " keyboard: true," .
                                        "min: '" . ($min) . "'," .
                                        "max: '" . ($max) . "'," .
                                        "type: 'single'," .
                                        "step: 1," .
                                        "grid: true" .
                                        "});" .
                                        " </script>";
                            }
                        }
                    }
                    $eachQuest = $eachQuest . "<input type='hidden' name='CampaigInfo' value='" . $myCampaign->CampaignID . "' /> </div>";
                    if (strlen(OurCampaign::find($myCampaign->CampaignID)->CampaignNotes) > 70) {
                        $det = '<span style=" display: inline;" class="popoverData" href="#" data-content="" rel="tooltip" data-placement="bottom" data-original-title="' . OurCampaign::find($myCampaign->CampaignID)->CampaignNotes . '" data-trigger="hover">' . substr(OurCampaign::find($myCampaign->CampaignID)->CampaignNotes, 0, 70) . '...</span>';
                    } else {
                        $det = OurCampaign::find($myCampaign->CampaignID)->CampaignNotes . '<br><br>';
                    }
                    if ($count > 0 && Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->addDay() > Carbon::now() && Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->StartDate)->addDay() < Carbon::now() && OurCampaign::find($myCampaign->CampaignID)->Status == 1) {
                        $quotedRegStatus = "'" . @$request->regStatus . "'";
                        $quotedCampID = "'" . @$myCampaign->CampaignID . "'";
                        $img = "public/images/CampaignImages/" . @$myCampaign->CampaignID . "/" . OurCampaign::find($myCampaign->CampaignID)->ImagePath;
                        $frames = $frames . ' <div class=" col-lg-4 campBoxes camp_' . $myCampaign->CampaignID . ' myClass" align="center"><div class=" ">' .
                                ' <div class="box--sml text-center flush-pd-trailer col-lg-12">' .
                                '<a class="block " href="#">' .
                                '<img src="' . $img . '" style="width: 100%;"><h5 class="box-title elltext">' . OurCampaign::find($myCampaign->CampaignID)->CampaignName . '</h5>' .
                                '<h5 class="box-title elltext" style="font-weight: 700;">' .
                                $special .
                                '</h5>' .
                                '<h5>Cash Value: #30,000</h5>' .
                                '<h5 class="box-title elltext">' . $tillEnd . '</h5>' .
                                // '<h6><p>Details:' . $det . '</p></h6>' .
                                //'<p class="push-sbit"><button class="btn btn-prize btn-xs campaign_count_' . $count . ' QuestClass" onclick="checkReg(' . $quotedRegStatus . ', ' . $quotedCampID . ')">ENTER NOW <i class="fa fa-arrow-right"></i></button></p>' .
                                // '</h6><p class="push-sbit"><button class="btn btn-primary btn-xs campaign_count_' . $count . ' QuestClass" onclick="checkReg(' . $quotedRegStatus . ', ' . $quotedCampID . ')">TAKE QUIZ</button></p>' .
                                '</a>' .
                                '<input type="hidden" value="' . $eachQuest . '" class="getQuest_' . $myCampaign->CampaignID . '">' .
                                ' <input type="hidden" value="' . $count . '" class="getCount_' . $myCampaign->CampaignID . '">' .
                                ' </div>' .
                                '</div>' .
                                '</div>';
                    }
                }

                $campCount++;
            }
            
            //   return view("previousCampaign")->with(compact('latest', 'latest'));
        } else if ($request->action == "notdone") {

            $special = "";
            $test = "";
            //   $checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
            $checkInfo = usertable::where('facebook_id', '=', Session::get('logedIn'))->first();
            if ($checkInfo == null) {
                $checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
            }
            $latest = DB::select("SELECT pm.* FROM campaign_questions pm Natural left join answer_records WHERE pm.CampaignID not in (SELECT pd.CampaignID FROM answer_records pd where UserID = " . $checkInfo->id . ")");
            $count = 0;
            $allId = array();
            $campCount = 0;
            foreach ($latest as $myCampaign) {

                $test = $test . "entered";
                //  $tillEnd = (Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->diffInDays(Carbon::now()->format('Y-m-d')) + 1) == 1 ? 'Campaign Ends today' : (Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->diffInDays(Carbon::now()->format('Y-m-d')) + 1) . ' Day(s) to end';
                @$tillEnd = (Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->diffInDays(Carbon::now()) + 1) == 1 ? 'Campaign Ends today' : (Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->diffInDays(Carbon::now()) + 1) . ' Day(s) to end';
                $specialPrize = SpecialPrize::where('CampignID', '=', $myCampaign->CampaignID)->first();
                if ($specialPrize) {
                    $special = '<h5>Special Prize: ' . $specialPrize->SpecialPrize . '</h5>';
                } else {
                    $special = '<h5>&emsp;</h5>';
                }
                if (!in_array($myCampaign->CampaignID, $allId)) {

                    $allId[] = $myCampaign->CampaignID;
                    $count++;

                    $eachQuest = "<div class='questionWrapper'>";
                    csrf_field();
                    $countForm = 0;
                    foreach ($latest as $getQuest) {
                        if ($getQuest->CampaignID == $myCampaign->CampaignID) {

                            $countForm++;
                            // $eachQuest = $eachQuest . "<label>" . $getQuest->Question . "</label><br>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $getQuest->ResponseData)) . "<br><br><input type='hidden' name='QuestID' class='Quest_" . $getQuest->id . "' value='" . $getQuest->id . "' />";
                            if ($getQuest->ResponseType == "mySelect") {
                                $inputPart = "";
                                // $getInput = DB::select("SELECT FormContent FROM form_builders where FormType = '" . $getQuest->ResponseType . "'");
                                $getInput = FormBuilder::where('FormType', '=', $getQuest->ResponseType)->first();
                                foreach (explode(',', $getQuest->ResponseData) as $eachOpt) {
                                    if ($eachOpt != "" && $eachOpt != null)
                                        $inputPart = $inputPart . "<option value='" . $eachOpt . "'>" . $eachOpt . "</option>";
                                }
                                $finalInput = str_replace('eachopt', $inputPart, $getInput->FormContent);
                                //   $eachQuest = $eachQuest . "<label>" . $getQuest->Question . "</label><br>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $finalInput)) . "<br><br><input type='hidden' name='QuestID' class='Quest_" . $getQuest->id . "' value='" . $getQuest->id . "' />";
                                $eachQuest = $eachQuest . " <li class='question-list'>" .
                                        "<div class='wrapper'>" .
                                        "<div class='item'>" .
                                        "<span><i class='fa fa-arrow-right' style='color: gold;'></i></span>" .
                                        "</div>" .
                                        " <div class='question'><span>" . $getQuest->Question . "</span></div>" .
                                        "<div class='response'>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $finalInput)) . "</div><input type='hidden' name='QuestID' class='Quest_" . $getQuest->id . "' value='" . $getQuest->id . "' />" .
                                        "</div>" .
                                        " </li>";
                            }
                            else if ($getQuest->ResponseType == "myCheck") {
                                $inputPart = "<span class='checkBoxClass_" . $getQuest->id . "'><input type='hidden' name='checkBoxClass_" . $getQuest->id . "' value='checkBoxClass_" . $getQuest->id . "'>";

                                // $getInput = DB::select("SELECT FormContent FROM form_builders where FormType = '" . $getQuest->ResponseType . "'");
                                $getInput = FormBuilder::where('FormType', '=', $getQuest->ResponseType)->first();
                                foreach (explode(',', $getQuest->ResponseData) as $eachOpt) {
                                    if ($eachOpt != "" && $eachOpt != null) {

                                        // $f = str_replace('eachopt', $eachOpt, $getInput->FormContent);
                                        $inputPart = $inputPart . "<input type='checkbox' name='checkbox' class='toDisable_subforcampid largeChecks' id='val_subforcount' value='" . $eachOpt . "' >" . $eachOpt . " &emsp;";
                                    }
                                }
                                $inputPart = $inputPart . "</span>";
                                //  $eachQuest = $eachQuest . "<label>" . $getQuest->Question . "</label><br>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $inputPart)) . "<br><br>";
                                $eachQuest = $eachQuest . " <li class='question-list'>" .
                                        "<div class='wrapper'>" .
                                        "<div class='item'>" .
                                        "<span><i class='fa fa-arrow-right' style='color: gold;'></i></span>" .
                                        "</div>" .
                                        " <div class='question'><span>" . $getQuest->Question . "</span></div>" .
                                        "<div class='response'>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $inputPart)) . "</div>" .
                                        "</div>" .
                                        " </li>";
                            } else if ($getQuest->ResponseType == "myRadio") {
                                $inputPart = "<span class='radioClass_" . $getQuest->id . "'><input type='hidden' name='radioClass_" . $getQuest->id . "' value='radioClass_" . $getQuest->id . "'>";
                                // $getInput = DB::select("SELECT FormContent FROM form_builders where FormType = '" . $getQuest->ResponseType . "'");
                                $getInput = FormBuilder::where('FormType', '=', $getQuest->ResponseType)->first();
                                foreach (explode(',', $getQuest->ResponseData) as $eachOpt) {
                                    if ($eachOpt != "" && $eachOpt != null) {
                                        // $f = str_replace('eachopt', $eachOpt, $getInput->FormContent);
                                        $inputPart = $inputPart . "<input type='radio' name='radioClass_" . $getQuest->id . "' class='toDisable_subforcampid largeChecks' id='val_subforcount' value='" . $eachOpt . "' >" . $eachOpt . " &emsp;";
                                    }
                                }
                                $inputPart = $inputPart . "</span>";
                                //   $eachQuest = $eachQuest . "<label>" . $getQuest->Question . "</label><br>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $inputPart)) . "<br><br>";

                                $eachQuest = $eachQuest . " <li class='question-list'>" .
                                        "<div class='wrapper'>" .
                                        "<div class='item'>" .
                                        "<span><i class='fa fa-arrow-right' style='color: gold;'></i></span>" .
                                        "</div>" .
                                        " <div class='question'><span>" . $getQuest->Question . "</span></div>" .
                                        "<div class='response'>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $inputPart)) . "</div>" .
                                        "</div>" .
                                        " </li>";
                            } else if ($getQuest->ResponseType == "myRange") {
                                $max = explode('-', explode(',', $getQuest->ResponseData)[0])[0];
                                $min = explode('-', explode(',', $getQuest->ResponseData)[0])[1];
                                //   $inputPart = "<input type='text' id='' name='rangeClass_' class='rangeSelect_".$getQuest->id."' value='' />";
                                $inputPart = "<input type='text' id='rangeSelect" . $getQuest->id . "' name='rangeClass_" . $getQuest->id . "' class='form-control rangeClass_" . $getQuest->id . "'  value='' /><div><span class='pull-left leastDesc rangeInfo' style='color: #333;   font-size: 12px; line-height: 1.333;text-shadow: none;top: 0;padding: 1px 5px; background: rgba(0,0,0,0.1); border-radius: 3px;'>" . explode('-', explode(',', $getQuest->ResponseData)[1])[0] . "</span><span class='pull-right maxDesc rangeInfo' style='color: #333;   font-size: 12px; line-height: 1.333;text-shadow: none;top: 0;padding: 1px 5px; background: rgba(0,0,0,0.1); border-radius: 3px;'>" . explode('-', explode(',', $getQuest->ResponseData)[1])[1] . "</span></div>";

                                // $eachQuest = $eachQuest . "<label>" . $getQuest->Question . "</label><br>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $inputPart)) . "<br><br>" .
                                $eachQuest = $eachQuest . " <li class='question-list'>" .
                                        "<div class='wrapper'>" .
                                        "<div class='item'>" .
                                        "<span><i class='fa fa-arrow-right' style='color: gold;'></i></span>" .
                                        "</div>" .
                                        " <div class='question'><span>" . $getQuest->Question . "</span></div>" .
                                        "<div class='response'>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $inputPart)) . "</div>" .
                                        "</div>" .
                                        " </li>" .
                                        "<script>" .
                                        "$('#rangeSelect" . $getQuest->id . "').ionRangeSlider({" .
                                        "hide_min_max: false," .
                                        " keyboard: true," .
                                        "min: '" . ($min) . "'," .
                                        "max: '" . ($max) . "'," .
                                        "type: 'single'," .
                                        "step: 1," .
                                        "grid: true" .
                                        "});" .
                                        " </script>";
                            }
                        }
                    }
                    @$eachQuest = $eachQuest . "<input type='hidden' name='CampaigInfo' value='" . $myCampaign->CampaignID . "' /> </div>";
                    if (strlen(@OurCampaign::find(@$myCampaign->CampaignID)->CampaignNotes) > 70) {
                        @$det = '<span style=" display: inline;" class="popoverData" href="#" data-content="" rel="tooltip" data-placement="bottom" data-original-title="' . @OurCampaign::find(@$myCampaign->CampaignID)->CampaignNotes . '" data-trigger="hover">' . substr(@OurCampaign::find(@$myCampaign->CampaignID)->CampaignNotes, 0, 70) . '...</span>';
                    } else {
                        @$det = OurCampaign::find(@$myCampaign->CampaignID)->CampaignNotes . '<br><br>';
                    }
                    if ($count > 0 && @Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->addDay() > @Carbon::now() && @Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->StartDate)->addDay() < @Carbon::now() && OurCampaign::find($myCampaign->CampaignID)->Status == 1) {
                        $quotedRegStatus = "'" . @$request->regStatus . "'";
                        $quotedCampID = "'" . @$myCampaign->CampaignID . "'";
                        $img = "public/images/CampaignImages/" . @$myCampaign->CampaignID . "/" . OurCampaign::find($myCampaign->CampaignID)->ImagePath;
                        $frames = $frames . ' <div class=" col-lg-4 campBoxes camp_' . $myCampaign->CampaignID . ' myClass" align="center"><div class=" ">' .
                                ' <div class="box--sml text-center flush-pd-trailer col-lg-12">' .
                                '<a class="block " href="#">' .
                                '<img src="' . $img . '" style="width: 100%; "><h5 class="box-title elltext">' . OurCampaign::find($myCampaign->CampaignID)->CampaignName . '</h5>' .
                                '<h5 class="box-title elltext" style="font-weight: 700;">' .
                                $special .
                                '</h5>' .
                                '<h5>Cash Value: #30,000</h5>' .
                                '<h5 class="box-title elltext">' . $tillEnd . '</h5>' .
                                // '<h6><p>Details:' . $det . '</p></h6>' .
                                '<p class="push-sbit"><button class="btn btn-prize btn-xs campaign_count_' . $count . ' QuestClass" onclick="checkReg(' . $quotedRegStatus . ', ' . $quotedCampID . ')">ENTER NOW <i class="fa fa-arrow-right"></i></button></p>' .
                                // '</h6><p class="push-sbit"><button class="btn btn-primary btn-xs campaign_count_' . $count . ' QuestClass" onclick="checkReg(' . $quotedRegStatus . ', ' . $quotedCampID . ')">TAKE QUIZ</button></p>' .
                                '</a>' .
                                '<input type="hidden" value="' . $eachQuest . '" class="getQuest_' . $myCampaign->CampaignID . '">' .
                                ' <input type="hidden" value="' . $count . '" class="getCount_' . $myCampaign->CampaignID . '">' .
                                ' </div>' .
                                '</div>' .
                                '</div>';
                    }
                }

                $campCount++;
            }
            
                    $special = "";
            $test = "";
            //  $checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
            $checkInfo = usertable::where('facebook_id', '=', Session::get('logedIn'))->first();
            if ($checkInfo == null) {
                $checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
            }
            $latest = DB::select("SELECT pm.* FROM campaign_questions pm Natural left join answer_records WHERE pm.CampaignID in (SELECT pd.CampaignID FROM answer_records pd where UserID = " . $checkInfo->id . ")");
            $count = 0;
            $allId = array();
            $campCount = 0;
            foreach ($latest as $myCampaign) {

                $tillEnd = (Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->diffInDays(Carbon::now()) + 1) == 1 ? 'Campaign Ends today' : (Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->diffInDays(Carbon::now()) + 1) . ' Day(s) to end';
                $specialPrize = SpecialPrize::where('CampignID', '=', $myCampaign->CampaignID)->first();
                if ($specialPrize) {
                    $special = '<h5>Special Prize: ' . $specialPrize->SpecialPrize . '</h5>';
                } else {
                    $special = '<h5>&emsp;</h5>';
                }
                if (!in_array($myCampaign->CampaignID, $allId)) {

                    $allId[] = $myCampaign->CampaignID;
                    $count++;

                    $eachQuest = "<ul class='questions'>";
                    csrf_field();
                    $countForm = 0;
                    foreach ($latest as $getQuest) {
                        if ($getQuest->CampaignID == $myCampaign->CampaignID) {

                            $countForm++;
                            //  $eachQuest = $eachQuest . "<label>" . $getQuest->Question . "</label><br>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $getQuest->ResponseData)) . "<br><br><input type='hidden' name='QuestID' class='Quest_" . $getQuest->id . "' value='" . $getQuest->id . "' />";
                            if ($getQuest->ResponseType == "mySelect") {
                                $inputPart = "";
                                // $getInput = DB::select("SELECT FormContent FROM form_builders where FormType = '" . $getQuest->ResponseType . "'");
                                $getInput = FormBuilder::where('FormType', '=', $getQuest->ResponseType)->first();
                                foreach (explode(',', $getQuest->ResponseData) as $eachOpt) {
                                    if ($eachOpt != "" && $eachOpt != null)
                                        $inputPart = $inputPart . "<option value='" . $eachOpt . "'>" . $eachOpt . "</option>";
                                }
                                $finalInput = str_replace('eachopt', $inputPart, $getInput->FormContent);
                                //  $eachQuest = $eachQuest . "<label>" . $getQuest->Question . "</label><br>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $finalInput)) . "<br><br><input type='hidden' name='QuestID' class='Quest_" . $getQuest->id . "' value='" . $getQuest->id . "' />";
                                $eachQuest = $eachQuest . " <li class='question-list'>" .
                                        "<div class='wrapper'>" .
                                        "<div class='item'>" .
                                        "<span><i class='fa fa-arrow-right' style='color: gold;'></i></span>" .
                                        "</div>" .
                                        " <div class='question'><span>" . $getQuest->Question . "</span></div>" .
                                        "<div class='response'>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $finalInput)) . "</div><input type='hidden' name='QuestID' class='Quest_" . $getQuest->id . "' value='" . $getQuest->id . "' />" .
                                        "</div>" .
                                        " </li>";
                            }
                            else if ($getQuest->ResponseType == "myCheck") {
                                $inputPart = "<span class='checkBoxClass_" . $getQuest->id . "'><input type='hidden' name='checkBoxClass_" . $getQuest->id . "' value='checkBoxClass_" . $getQuest->id . "'>";

                                // $getInput = DB::select("SELECT FormContent FROM form_builders where FormType = '" . $getQuest->ResponseType . "'");
                                $getInput = FormBuilder::where('FormType', '=', $getQuest->ResponseType)->first();
                                foreach (explode(',', $getQuest->ResponseData) as $eachOpt) {
                                    if ($eachOpt != "" && $eachOpt != null) {

                                        // $f = str_replace('eachopt', $eachOpt, $getInput->FormContent);
                                        $inputPart = $inputPart . "<input type='checkbox' name='checkbox' class='toDisable_subforcampid largeChecks' id='val_subforcount' value='" . $eachOpt . "' >" . $eachOpt . " &emsp;";
                                    }
                                }
                                $inputPart = $inputPart . "</span>";
                                // $eachQuest = $eachQuest . "<label>" . $getQuest->Question . "</label><br>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $inputPart)) . "<br><br>";
                                $eachQuest = $eachQuest . " <li class='question-list'>" .
                                        "<div class='wrapper'>" .
                                        "<div class='item'>" .
                                        "<span><i class='fa fa-arrow-right' style='color: gold;'></i></span>" .
                                        "</div>" .
                                        " <div class='question'><span>" . $getQuest->Question . "</span></div>" .
                                        "<div class='response'>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $inputPart)) . "</div>" .
                                        "</div>" .
                                        " </li>";
                            } else if ($getQuest->ResponseType == "myRadio") {
                                $inputPart = "<span class='radioClass_" . $getQuest->id . "'><input type='hidden' name='radioClass_" . $getQuest->id . "' value='radioClass_" . $getQuest->id . "'>";
                                // $getInput = DB::select("SELECT FormContent FROM form_builders where FormType = '" . $getQuest->ResponseType . "'");
                                $getInput = FormBuilder::where('FormType', '=', $getQuest->ResponseType)->first();
                                foreach (explode(',', $getQuest->ResponseData) as $eachOpt) {
                                    if ($eachOpt != "" && $eachOpt != null) {
                                        // $f = str_replace('eachopt', $eachOpt, $getInput->FormContent);
                                        $inputPart = $inputPart . "<input type='radio' name='radioClass_" . $getQuest->id . "' class='toDisable_subforcampid largeChecks' id='val_subforcount' value='" . $eachOpt . "' >" . $eachOpt . " &emsp;";
                                    }
                                }
                                $inputPart = $inputPart . "</span>";
                                $eachQuest = $eachQuest . "<label>" . $getQuest->Question . "</label><br>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $inputPart)) . "<br><br>";
                            } else if ($getQuest->ResponseType == "myRange") {
                                $max = explode('-', explode(',', $getQuest->ResponseData)[0])[0];
                                $min = explode('-', explode(',', $getQuest->ResponseData)[0])[1];
                                //   $inputPart = "<input type='text' id='' name='rangeClass_' class='rangeSelect_".$getQuest->id."' value='' />";
                                $inputPart = "<input type='text' id='rangeSelect" . $getQuest->id . "' name='rangeClass_" . $getQuest->id . "' class='form-control rangeClass_" . $getQuest->id . "'  value='' /><div><span class='pull-left leastDesc rangeInfo' style='color: #333;   font-size: 12px; line-height: 1.333;text-shadow: none;top: 0;padding: 1px 5px; background: rgba(0,0,0,0.1); border-radius: 3px;'>" . explode('-', explode(',', $getQuest->ResponseData)[1])[0] . "</span><span class='pull-right maxDesc rangeInfo' style='color: #333;   font-size: 12px; line-height: 1.333;text-shadow: none;top: 0;padding: 1px 5px; background: rgba(0,0,0,0.1); border-radius: 3px;'>" . explode('-', explode(',', $getQuest->ResponseData)[1])[1] . "</span></div>";

                                $eachQuest = $eachQuest . " <li class='question-list'>" .
                                        "<div class='wrapper'>" .
                                        "<div class='item'>" .
                                        "<span><i class='fa fa-arrow-right' style='color: gold;'></i></span>" .
                                        "</div>" .
                                        " <div class='question'><span>" . $getQuest->Question . "</span></div>" .
                                        "<div class='response'>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $inputPart)) . "</div>" .
                                        "</div>" .
                                        " </li>" .
                                        "<script>" .
                                        "$('#rangeSelect" . $getQuest->id . "').ionRangeSlider({" .
                                        "hide_min_max: false," .
                                        " keyboard: true," .
                                        "min: '" . ($min) . "'," .
                                        "max: '" . ($max) . "'," .
                                        "type: 'single'," .
                                        "step: 1," .
                                        "grid: true" .
                                        "});" .
                                        " </script>";
                            }
                        }
                    }
                    $eachQuest = $eachQuest . "<input type='hidden' name='CampaigInfo' value='" . $myCampaign->CampaignID . "' /> </div>";
                    if (strlen(OurCampaign::find($myCampaign->CampaignID)->CampaignNotes) > 70) {
                        $det = '<span style=" display: inline;" class="popoverData" href="#" data-content="" rel="tooltip" data-placement="bottom" data-original-title="' . OurCampaign::find($myCampaign->CampaignID)->CampaignNotes . '" data-trigger="hover">' . substr(OurCampaign::find($myCampaign->CampaignID)->CampaignNotes, 0, 70) . '...</span>';
                    } else {
                        $det = OurCampaign::find($myCampaign->CampaignID)->CampaignNotes . '<br><br>';
                    }
                    if ($count > 0 && Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->addDay() > Carbon::now() && Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->StartDate)->addDay() < Carbon::now() && OurCampaign::find($myCampaign->CampaignID)->Status == 1) {
                        $quotedRegStatus = "'" . @$request->regStatus . "'";
                        $quotedCampID = "'" . @$myCampaign->CampaignID . "'";
                        $img = "public/images/CampaignImages/" . @$myCampaign->CampaignID . "/" . OurCampaign::find($myCampaign->CampaignID)->ImagePath;
                        $frames = $frames . ' <div class="grayout col-lg-4 campBoxes camp_' . $myCampaign->CampaignID . ' myClass" align="center"><div class=" ">' .
                                ' <div class="box--sml text-center flush-pd-trailer col-lg-12">' .
                                '<a class="block " href="#">' .
                                '<img src="' . $img . '" style="width: 100%;"><h5 class="box-title elltext">' . OurCampaign::find($myCampaign->CampaignID)->CampaignName . '</h5>' .
                                '<h5 class="box-title elltext" style="font-weight: 700;">' .
                                $special .
                                '</h5>' .
                                '<h5>Cash Value: #30,000</h5>' .
                                '<h5 class="box-title elltext">' . $tillEnd . '</h5>' .
                                // '<h6><p>Details:' . $det . '</p></h6>' .
                                '<p class="push-sbit"><button class="btn btn-prize btn-xs campaign_count_' . $count . ' QuestClass" disabled>COMPLETED <i class="fa fa-arrow-right"></i></button></p>' .
                                // '</h6><p class="push-sbit"><button class="btn btn-primary btn-xs campaign_count_' . $count . ' QuestClass" onclick="checkReg(' . $quotedRegStatus . ', ' . $quotedCampID . ')">TAKE QUIZ</button></p>' .
                                '</a>' .
                                '<input type="hidden" value="' . $eachQuest . '" class="getQuest_' . $myCampaign->CampaignID . '">' .
                                ' <input type="hidden" value="' . $count . '" class="getCount_' . $myCampaign->CampaignID . '">' .
                                ' </div>' .
                                '</div>' .
                                '</div>';
                    }
                }

                $campCount++;
            }
            
            //   return view("previousCampaign")->with(compact('latest', 'latest'));
        }

        return response()->json(($frames));
    }

    public function getMore(Request $request) {
        //$campLimit = (int)Session::get('campLimit') + 1;

        Session::put('campLimit', (Session::get('campLimit') + 20));
        $frames = "";
        $test = "";

        if ($request->action == "done") {

            $special = "";

            // $checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
            $checkInfo = usertable::where('facebook_id', '=', Session::get('logedIn'))->first();
            if ($checkInfo == null) {
                $checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
            }
//            $latest = DB::select("SELECT pm.* FROM campaign_questions pm Natural left join answer_records WHERE pm.CampaignID in (SELECT pd.CampaignID FROM answer_records pd where UserID = " . $checkInfo->id . ") limit ". Session::get('campLimit'));
            $latest = DB::select("SELECT pm.* FROM campaign_questions pm Natural left join answer_records WHERE pm.CampaignID in (SELECT pd.CampaignID FROM answer_records pd where UserID = " . $checkInfo->id . ")");
            $count = 0;
            $allId = array();
            $campCount = 0;

            foreach ($latest as $myCampaign) {
                $test = $test . "entered";
                $tillEnd = (Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->diffInDays(Carbon::now()) + 1) == 1 ? 'Campaign Ends today' : (Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->diffInDays(Carbon::now()) + 1) . ' Day(s) to end';
                $specialPrize = SpecialPrize::where('CampignID', '=', $myCampaign->CampaignID)->first();
                if ($specialPrize) {
                    $special = '<h6>Special Prize: ' . $specialPrize->SpecialPrize . '</h6>';
                } else {
                   $special = '<h5>&emsp;</h5>';
                }
                if (!in_array($myCampaign->CampaignID, $allId)) {

                    $allId[] = $myCampaign->CampaignID;
                    $count++;

                    $eachQuest = "<div class='questionWrapper'>";
                    csrf_field();
                    $countForm = 0;
                    foreach ($latest as $getQuest) {
                        if ($getQuest->CampaignID == $myCampaign->CampaignID) {

                            $countForm++;
                            // $eachQuest = $eachQuest . "<label>" . $getQuest->Question . "</label><br>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $getQuest->ResponseData)) . "<br><br><input type='hidden' name='QuestID' class='Quest_" . $getQuest->id . "' value='" . $getQuest->id . "' />";
                            if ($getQuest->ResponseType == "mySelect") {
                                $inputPart = "";
                                // $getInput = DB::select("SELECT FormContent FROM form_builders where FormType = '" . $getQuest->ResponseType . "'");
                                $getInput = FormBuilder::where('FormType', '=', $getQuest->ResponseType)->first();
                                foreach (explode(',', $getQuest->ResponseData) as $eachOpt) {
                                    if ($eachOpt != "" && $eachOpt != null)
                                        $inputPart = $inputPart . "<option value='" . $eachOpt . "'>" . $eachOpt . "</option>";
                                }
                                $finalInput = str_replace('eachopt', $inputPart, $getInput->FormContent);
                                $eachQuest = $eachQuest . "<label>" . $getQuest->Question . "</label><br>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $finalInput)) . "<br><br><input type='hidden' name='QuestID' class='Quest_" . $getQuest->id . "' value='" . $getQuest->id . "' />";
                            }
                            else if ($getQuest->ResponseType == "myCheck") {
                                $inputPart = "<span class='checkBoxClass_" . $getQuest->id . "'><input type='hidden' name='checkBoxClass_" . $getQuest->id . "' value='checkBoxClass_" . $getQuest->id . "'>";

                                // $getInput = DB::select("SELECT FormContent FROM form_builders where FormType = '" . $getQuest->ResponseType . "'");
                                $getInput = FormBuilder::where('FormType', '=', $getQuest->ResponseType)->first();
                                foreach (explode(',', $getQuest->ResponseData) as $eachOpt) {
                                    if ($eachOpt != "" && $eachOpt != null) {

                                        // $f = str_replace('eachopt', $eachOpt, $getInput->FormContent);
                                        $inputPart = $inputPart . "<input type='checkbox' name='checkbox' class='toDisable_subforcampid largeChecks' id='val_subforcount' value='" . $eachOpt . "' >" . $eachOpt . " &emsp;";
                                    }
                                }
                                $inputPart = $inputPart . "</span>";
                                $eachQuest = $eachQuest . "<label>" . $getQuest->Question . "</label><br>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $inputPart)) . "<br><br>";
                            } else if ($getQuest->ResponseType == "myRadio") {
                                $inputPart = "<span class='radioClass_" . $getQuest->id . "'><input type='hidden' name='radioClass_" . $getQuest->id . "' value='radioClass_" . $getQuest->id . "'>";
                                // $getInput = DB::select("SELECT FormContent FROM form_builders where FormType = '" . $getQuest->ResponseType . "'");
                                $getInput = FormBuilder::where('FormType', '=', $getQuest->ResponseType)->first();
                                foreach (explode(',', $getQuest->ResponseData) as $eachOpt) {
                                    if ($eachOpt != "" && $eachOpt != null) {
                                        // $f = str_replace('eachopt', $eachOpt, $getInput->FormContent);
                                        $inputPart = $inputPart . "<input type='radio' name='radioClass_" . $getQuest->id . "' class='toDisable_subforcampid largeChecks' id='val_subforcount' value='" . $eachOpt . "' >" . $eachOpt . " &emsp;";
                                    }
                                }
                                $inputPart = $inputPart . "</span>";
                                $eachQuest = $eachQuest . "<label>" . $getQuest->Question . "</label><br>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $inputPart)) . "<br><br>";
                            } else if ($getQuest->ResponseType == "myRange") {
                                $max = explode('-', explode(',', $getQuest->ResponseData)[0])[0];
                                $min = explode('-', explode(',', $getQuest->ResponseData)[0])[1];
                                //   $inputPart = "<input type='text' id='' name='rangeClass_' class='rangeSelect_".$getQuest->id."' value='' />";
                                $inputPart = "<input type='text' id='rangeSelect" . $getQuest->id . "' name='rangeClass_" . $getQuest->id . "' class='form-control rangeClass_" . $getQuest->id . "'  value='' /><div><span class='pull-left leastDesc rangeInfo' style='color: #333;   font-size: 12px; line-height: 1.333;text-shadow: none;top: 0;padding: 1px 5px; background: rgba(0,0,0,0.1); border-radius: 3px;'>" . explode('-', explode(',', $getQuest->ResponseData)[1])[0] . "</span><span class='pull-right maxDesc rangeInfo' style='color: #333;   font-size: 12px; line-height: 1.333;text-shadow: none;top: 0;padding: 1px 5px; background: rgba(0,0,0,0.1); border-radius: 3px;'>" . explode('-', explode(',', $getQuest->ResponseData)[1])[1] . "</span></div>";

                                $eachQuest = $eachQuest . "<label>" . $getQuest->Question . "</label><br>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $inputPart)) . "<br><br>" .
                                        "<script>" .
                                        "$('#rangeSelect" . $getQuest->id . "').ionRangeSlider({" .
                                        "hide_min_max: false," .
                                        " keyboard: true," .
                                        "min: '" . ($min) . "'," .
                                        "max: '" . ($max) . "'," .
                                        "type: 'single'," .
                                        "step: 1," .
                                        "grid: true" .
                                        "});" .
                                        " </script>";
                            }
                        }
                    }
                    $eachQuest = $eachQuest . "<input type='hidden' name='CampaigInfo' value='" . $myCampaign->CampaignID . "' /> </div>";
                    if ($count > 0 && Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->addDay() > Carbon::now()) {
                        $quotedRegStatus = "'" . @$request->regStatus . "'";
                        $quotedCampID = "'" . @$myCampaign->CampaignID . "'";
                        $img = "public/images/CampaignImages/" . @$myCampaign->CampaignID . "/" . OurCampaign::find($myCampaign->CampaignID)->ImagePath;
                        $frames = $frames . ' <div class=" col-lg-4 campBoxes camp_' . $myCampaign->CampaignID . ' myClass" align="center"><div class=" ">' .
                                ' <div class="box--sml text-center flush-pd-trailer col-lg-12">' .
                                '<a class="block " href="#">' .
                                '<img src="' . $img . '" width="100%"><h6 class="box-title elltext">' . OurCampaign::find($myCampaign->CampaignID)->CampaignName . '</h6>' .
                                '<h6 class="box-title elltext">' . $tillEnd . '</h6>' .
                                '<h6 class="box-title elltext" style="font-weight: 700;">' .
                                $special .
                                '<h6><p>Details:' . OurCampaign::find($myCampaign->CampaignID)->CampaignNotes . '</p></h6>' .
                                '</h6><p class="push-sbit"><button class="btn btn-primary btn-xs campaign_count_' . $count . ' QuestClass" onclick="checkReg(' . $quotedRegStatus . ', ' . $quotedCampID . ')">TAKE QUIZ</button></p>' .
                                '</a>' .
                                '<input type="hidden" value="' . $eachQuest . '" class="getQuest_' . $myCampaign->CampaignID . '">' .
                                ' <input type="hidden" value="' . $count . '" class="getCount_' . $myCampaign->CampaignID . '">' .
                                ' </div>' .
                                '</div>' .
                                '</div>';
                    }
                }

                $campCount++;
            }
            //   return view("previousCampaign")->with(compact('latest', 'latest'));
        } else if ($request->action == "notdone") {

            $special = "";
            $test = "";
            // $checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
            $checkInfo = usertable::where('facebook_id', '=', Session::get('logedIn'))->first();
            if ($checkInfo == null) {
                $checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
            }
            //  $latest = DB::select("SELECT pm.* FROM campaign_questions pm Natural left join answer_records WHERE pm.CampaignID not in (SELECT pd.CampaignID FROM answer_records pd where UserID = " . $checkInfo->id . ") limit ".Session::get('campLimit'));
            $latest = DB::select("SELECT pm.* FROM campaign_questions pm Natural left join answer_records WHERE pm.CampaignID not in (SELECT pd.CampaignID FROM answer_records pd where UserID = " . $checkInfo->id . ")");
            $count = 0;
            $allId = array();
            $campCount = 0;
            foreach ($latest as $myCampaign) {
                $test = $test . "entered";
                //  $tillEnd = (Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->diffInDays(Carbon::now()->format('Y-m-d')) + 1) == 1 ? 'Campaign Ends today' : (Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->diffInDays(Carbon::now()->format('Y-m-d')) + 1) . ' Day(s) to end';
                $tillEnd = (Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->diffInDays(Carbon::now()) + 1) == 1 ? 'Campaign Ends today' : (Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->diffInDays(Carbon::now()) + 1) . ' Day(s) to end';
                $specialPrize = SpecialPrize::where('CampignID', '=', $myCampaign->CampaignID)->first();
                if ($specialPrize) {
                    $special = '<h5>Special Prize: ' . $specialPrize->SpecialPrize . '</h5>';
                } else {
                    $special = '<h5>&emsp;</h5>';
                }
                if (!in_array($myCampaign->CampaignID, $allId)) {

                    $allId[] = $myCampaign->CampaignID;
                    $count++;

                    $eachQuest = "<div class='questionWrapper'>";
                    csrf_field();
                    $countForm = 0;
                    foreach ($latest as $getQuest) {
                        if ($getQuest->CampaignID == $myCampaign->CampaignID) {

                            $countForm++;
                            $eachQuest = $eachQuest . "<label>" . $getQuest->Question . "</label><br>" . str_replace('subforcampid', $myCampaign->CampaignID, str_replace('subforcount', $countForm, $getQuest->ResponseData)) . "<br><br><input type='hidden' name='QuestID' class='Quest_" . $getQuest->id . "' value='" . $getQuest->id . "' />";
                        }
                    }
                    $eachQuest = $eachQuest . "<input type='hidden' name='CampaigInfo' value='" . $myCampaign->CampaignID . "' /> </div>";

                    if ($count > 0 && Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->addDay() > Carbon::now()) {
                        $quotedRegStatus = "'" . @$request->regStatus . "'";
                        $quotedCampID = "'" . @$myCampaign->CampaignID . "'";
                        $img = "public/images/CampaignImages/" . @$myCampaign->CampaignID . "/" . OurCampaign::find($myCampaign->CampaignID)->ImagePath;
                        $frames = $frames . ' <div class=" col-lg-4 campBoxes camp_' . $myCampaign->CampaignID . ' myClass" align="center"><div class=" ">' .
                                ' <div class="box--sml text-center flush-pd-trailer col-lg-12">' .
                                '<a class="block " href="#">' .
                                '<img src="' . $img . '" width="100%"><h6 class="box-title elltext">' . OurCampaign::find($myCampaign->CampaignID)->CampaignName . '</h6>' .
                                '<h6 class="box-title elltext">' . $tillEnd . '</h6>' .
                                '<h6 class="box-title elltext" style="font-weight: 700;">' .
                                $special .
                                '<h6><p>Details:' . OurCampaign::find($myCampaign->CampaignID)->CampaignNotes . '</p></h6>' .
                                '</h6><p class="push-sbit"><button class="btn btn-primary btn-xs campaign_count_' . $count . ' QuestClass" onclick="checkReg(' . $quotedRegStatus . ', ' . $quotedCampID . ')">TAKE QUIZ</button></p>' .
                                '</a>' .
                                '<input type="hidden" value="' . $eachQuest . '" class="getQuest_' . $myCampaign->CampaignID . '">' .
                                ' <input type="hidden" value="' . $count . '" class="getCount_' . $myCampaign->CampaignID . '">' .
                                ' </div>' .
                                '</div>' .
                                '</div>';
                    }
                }

                $campCount++;
            }
            //   return view("previousCampaign")->with(compact('latest', 'latest'));
        }
        //  return response()->json($latest);
        return response()->json($frames);
    }

    public function uploadDP() {

       $input = Input::only('fupload');
      
     $ext = explode('.', $input['fupload']->getClientOriginalName())[1];

     $input['fupload']->move(public_path('images'), $input['fupload']->getClientOriginalName());
 
    return response()->json($input['fupload']->getClientOriginalName());
//return response()->json('let start');
    }

    public function upDateDBColumn(Request $request) {
       $getUser = usertable::where('id', '=', Session::get('logedIn'))->first();
if(!$getUser)
{
  $getUser = usertable::where('facebook_id', '=', Session::get('logedIn'))->first();
}
        $ext = explode('.', $request->imgName)[1];
        $getUser->UserImg = Session::get('logedIn') . '.' . $ext;
        $getUser->save();

       copy('public/images/' . $request->imgName, 'public/images/' . Session::get('logedIn') . '.' . $ext);
        return response()->json(Session::get('logedIn') . '.' . $ext); 
      //  return response()->json(Session::get('logedIn'));
    }

    public function forgotPassword(Request $request) {
        try {
            //Find the user object from model if it exists
            //$user = user::findOrFail($id);
            $user = usertable::where('Email', '=', $request->confirmMail)->first();
            if (!$user) {
                return response()->json('Invalid Email');
            }

            $linkPassword = rand(100000000, 999999999);
            $user->SentCode = $linkPassword;
            $user->CodeTimeStart = Carbon::now();
            $user->save();

            @$headers = 'From: ' . "no-reply@winmoney.cg" . "\r\n";

            mail($request->confirmMail, "Winmoney Profile Reset Password", "Clink the link below to change your password \r\n http://winmoney.ng/confirmation/" . $linkPassword, @$headers);

            return response()->json('Check Your Mail To Complete Process');

            // return Redirect::to('/successful')->with('UserName', $id);
        } catch (ModelNotFoundException $err) {
            //Show error page
            return response()->json('Error');
        }
    }

    public function confirmpassword($response) {
        $getUser = usertable::where('SentCode', '=', $response)->first();
        if (!$getUser) {
            return view("confirmpassword")->with('response', 'Link either expired or never existed');
        } else {
            $dateFromDatabase = strtotime((string) $getUser->CodeTimeStart);
            $dateTwelveHoursAgo = strtotime("-5 minutes");

            if ($dateFromDatabase >= $dateTwelveHoursAgo) {
                // less than 5minutes ago
                $changePassword = '<div class="changePass"><input type="password" id="password" class="form-control" palaceholder="Enter Password"/><br><br><input type="password" id="password1" class="form-control" palaceholder="Retype Password"/><br><br><button type="button"class="btn btn-primary btn-xs">Save Changes</button></div>';
                $myContent = '<input type="hidden" value="' . $changePassword . '" class="myPasswordChange" />';
                return view("confirmpassword")->with('response', $myContent);
            } else {
                // more than 5minutes ago
                return view("confirmpassword")->with('response', 'Link either expired or never existed');
            }
        }
    }

    public function createCampaign() {
        $allCampaigns = DB::select("SELECT * FROM our_campaigns");
        $allClients = DB::select("SELECT * FROM clients where Status = 1");
        return view("CreateCampaign")
                        ->with(compact('allCampaigns', $allCampaigns))
                        ->with(compact('allClients', $allClients));
    }

    public function saveQuestion(Request $request) {
        //concatenate responsedata

        $checkCamp = OurCampaign::find($request->CampaignID);
        if ($checkCamp) {
            $allOpts = "";
            foreach ($request->ResponseType as $responseType) {
                $allOpts = $allOpts . $responseType . "__&&__";
            }

            $question = new CampaignQuestions();
            $question->Question = $request->Question;
            $question->ClientID = $request->ClientID;
            $question->CampaignID = $request->CampaignID;
            $question->ResponseData = $allOpts;
            $question->ResponseType = $request->myResponse;
            $question->ExpectedResponse = $request->responseExpected;
            $question->save();
            return response()->json("Successful");
        }
        return response()->json("Campaign Not Found");
    }

    public function newCampaign(Request $request) {

        $newCamp = new OurCampaign();
        $newCamp->ClientID = $request->allClients;
        $newCamp->CampaignName = $request->newCampaign;
        $newCamp->CampaignNotes = $request->campNotes;
        $newCamp->EndDate = $request->campEnd;
        $newCamp->StartDate = $request->campStart;
        $newCamp->Status = 1;
        $newCamp->ImagePath = $request->ImagePath;
        $newCamp->save();

        $getId = OurCampaign::where('ClientID', '=', $request->allClients)
                ->where('CampaignName', '=', $request->newCampaign)
                ->where('CampaignNotes', '=', $request->campNotes)
                ->where('EndDate', '=', $request->campEnd)
                ->first();
        if (!file_exists('public/images/CampaignImages/' . $getId->id)) {
            mkdir('public/images/CampaignImages/' . $getId->id, 0777, true);
        }
        return response()->json($getId->id);
    }

    public function AdminPage() {
    if(Session::get('adminLogin') == null)
    {
    
      return Redirect::to('adminLogin');
    }
        return view("AdminPage");
    }

	public function clientPage()
	{
	if(Session::get('clientLogin') == null)
    {
    
      return Redirect::to('clientLogin');
    }
	return view("clientPage");
	
	}

    public function adminLogin() {
        return view("adminLogin");
    }

    public function adminAuth(Request $request) {

        $checkAdmin = adminList::where('Username', '=', $request->username)
                ->where('Password', '=', md5($request->password))
                ->first();
        if ($checkAdmin) {
            Session::put('adminLogin', $checkAdmin->id);
            return Redirect::to('/CampaignList');
        }

        return Redirect::to('/adminLogin')->with('message', 'Error');
    }

public function clientAuth(Request $request)
{
  $checkClient = Clients::where('Username', '=', $request->username)
                ->where('Password', '=', md5($request->password))
                ->first();
        if ($checkClient ) {
            Session::put('clientLogin', $checkClient ->id);
            return Redirect::to('/clientPage');
        }

        return Redirect::to('/clientLogin')->with('message', 'Error');
}



    public function CampaignCreate() {
    if(Session::get('adminLogin') == null)
    {
    
       return Redirect::to('adminLogin');
    }
        $allCampaigns = DB::select("SELECT * FROM our_campaigns");
        $allClients = DB::select("SELECT * FROM clients where Status = 1");
        return view("CampaignCreate")
                        ->with(compact('allCampaigns', $allCampaigns))
                        ->with(compact('allClients', $allClients));
    }

    public function AddQuestion(Request $request) {
    if(Session::get('adminLogin') == null)
    {
    
      return Redirect::to('adminLogin');
    }
        $campaignName = DB::select("SELECT * FROM our_campaigns where id = '" . $request . "'");
        $allCampaigns = DB::select("SELECT * FROM our_campaigns");
        $allClients = DB::select("SELECT * FROM clients where Status = 1");
        return view("AddQuestion")
                        ->with(compact('allCampaigns', $allCampaigns))
                        ->with(compact('allClients', $allClients))
                        ->with(compact('campaignName', $campaignName));
    }

    public function Report() {
    if(Session::get('adminLogin') == null)
    {
    
       return Redirect::to('adminLogin');
    }
        return view("Report");
    }

    public function CampaignList() {
    if(Session::get('adminLogin') == null)
    {
    
      return Redirect::to('adminLogin');
    }
        $campaigns = OurCampaign::all();
        $clients = Clients::where('Status', '=', 1);
        return view("CampaignList")
                        ->with('campaigns', $campaigns)
                        ->with('clients', $clients);
    }

    public function getallquest(Request $request) {
        $getAllQuest = DB::select("SELECT * FROM campaign_questions WHERE CampaignID = '" . $request->campId . "'");
        $allQuest = " <table class='table table-striped table-bordered table-hover' id=''><thead><tr><th>Count</th><th colspan='2'>Question</th><tr></thead><tbody>";
        $count = 0;
        foreach ($getAllQuest as $eachQuest) {
            $count++;
            $allQuest = $allQuest . '<tr><td>' . $count . '</td><td colspan="2">' . $eachQuest->Question . '<td></tr>';
        }
        return response()->json($allQuest . '</tbody></table>');
    }

    public function delCamp(Request $request) {
        $checkId = OurCampaign::find($request->campId);
        if ($checkId) {
            DB::table('our_campaigns')->delete($request->campId);
            return response()->json("Delete Successful");
        }
        return response()->json("Campaign Does Not Exist");
    }
    
    public function delClient(Request $request) {
        $checkId = Clients::find($request->clientId);
        if ($checkId) {
            DB::table('clients')->delete($request->clientId);
            return response()->json("Delete Successful");
        }
        return response()->json("Client Does Not Exist");
    }
    

    function getCamp(Request $request) {
        $disCamp = OurCampaign::find($request->campId);

        return response()->json($disCamp);
    }

    public function uploadCampaignImage() {

        $input = Input::only('fupload', 'campId');

        $ext = explode('.', $input['fupload']->getClientOriginalName())[1];

        $input['fupload']->move(public_path('images/CampaignImages/' . $input['campId']), $input['fupload']->getClientOriginalName());
        //  
        return response()->json($input['fupload']->getClientOriginalName());
        // return response()->json($dummyName.'.'.$ext);
    }

    public function updateCampaignImg() {
        $input = Input::only('updateUpload', 'campId');

        $ext = explode('.', $input['updateUpload']->getClientOriginalName())[1];
        $files = glob('images/CampaignImages/' . $input['campId'] . '/*'); // get all file names
        foreach ($files as $file) { // iterate files
            if (is_file($file))
                unlink($file); // delete file
        }
        $input['updateUpload']->move(public_path('images/CampaignImages/' . $input['campId']), $input['updateUpload']->getClientOriginalName());

        //now update image name on db
        $getCamp = OurCampaign::find($input['campId']);
        $getCamp->ImagePath = $input['updateUpload']->getClientOriginalName();
        $getCamp->save();

        return response()->json($input['updateUpload']->getClientOriginalName());
        // return response()->json($dummyName.'.'.$ext);
    }

    function updateCamp(Request $request) {
        $getCamp = OurCampaign::find($request->campId);
        $getCamp->CampaignName = $request->allVal[0];
        $getCamp->ClientID = $request->allVal[1];
        $getCamp->CampaignNotes = $request->allVal[2];
        $startDate = $request->allVal[3];
        if (strpos($startDate, '/') !== false) {
            $startDate = explode('/', $request->allVal[3])[2] . explode('/', $request->allVal[3])[0] . explode('/', $request->allVal[3])[1];
        }
        $getCamp->StartDate = $startDate;

        $endDate = $request->allVal[4];
        if (strpos($endDate, '/') !== false) {
            $endDate = explode('/', $request->allVal[4])[2] . explode('/', $request->allVal[4])[0] . explode('/', $request->allVal[4])[1];
        }
        $getCamp->EndDate = $endDate;
        $getCamp->save();
        return response()->json("Update Successful");
    }

    public function CreateClient() {
    if(Session::get('adminLogin') == null)
    {
    
      return Redirect::to('adminLogin');
    }
        return view("CreateClient");
    }

    public function addNewClient(Request $request) {
        $clientTable = new Clients();
        $clientTable->CompanyName = $request->allVal[0];
        $clientTable->CompanyAddress = $request->allVal[1];
        $clientTable->Username = $request->allVal[2];
        $clientTable->Password = md5($request->allVal[3]);
        $clientTable->save();
        return response()->json("Success");
    }

    public function ViewClient() {
    if(Session::get('adminLogin') == null)
    {
    
       return Redirect::to('adminLogin');
    }
        $allClients = Clients::all();
        return view("ViewClient")
                        ->with('allClients', $allClients);
    }

    public function FilterCampaign() {
    if(Session::get('adminLogin') == null)
    {
    
   return Redirect::to('adminLogin');
    }
        $allCampaign = OurCampaign::all();
        Session::put('adminLogin', 1);
        //update session keeper
        $sessionKeeper = SessionKeeper::where('ForUser', '=', Session::get('adminLogin'))->first();
        if ($sessionKeeper != null) {
            $sessionKeeper->Status = 0;
            $sessionKeeper->save();
        } else {
            $sessionKeeper = new SessionKeeper();
            $sessionKeeper->Status = 0;
            $sessionKeeper->ForUser = Session::get('adminLogin');
            $sessionKeeper->Entity = "CamppaignFilter Table Record";
            $sessionKeeper->save();
        }

        return view("FilterCampaign")
                        ->with('allCampaign', $allCampaign);
    }

    public function getFilterQuestions(Request $request) {
        $allCampaign = CampaignQuestions::where('CampaignID', '=', $request->campId)->get();
        return response()
                        ->json($allCampaign);
    }

    public function getQuestOpt(Request $request) {
        $updateSessionKeeper = SessionKeeper::where('ForUser', '=', Session::get('adminLogin'))->first();
        $updateSessionKeeper->Status = 0;
        $updateSessionKeeper->save();

        $quest = CampaignQuestions::find($request->questId);
        $getResponse = $quest->ResponseType;
        $inputType = "";
        $countSteps = 0;
        if ($getResponse == "myCheck") {
            foreach (explode('__&&__', $quest->ResponseData) as $responseData) {
                if ($responseData != "") {
                    $countSteps++;
                    $inputType = $inputType . "<input type='checkbox' name='inputType' value='" . $responseData . "' id='checkFilter'>" . $responseData . "&emsp;";
                }
            }
            $inputType = $inputType . "__myCheck_" . $countSteps;
        } else if ($getResponse == "myRadio") {
            foreach (explode('__&&__', $quest->ResponseData) as $responseData) {
                if ($responseData != "")
                    $inputType = $inputType . "<input type='radio' name='inputType' value='" . $responseData . "'>" . $responseData . "&emsp;";
            }
            $inputType = $inputType . "__myRadio";
        } else if ($getResponse == "mySelect") {
            $inputType = "<select id='selectOpt' name='selectOpt'>";
            foreach (explode('__&&__', $quest->ResponseData) as $responseData) {
                if ($responseData != "")
                    $inputType = $inputType . "<option value='" . $responseData . "'>" . $responseData . "</option>";
            }

            $inputType = $inputType . "</select>__mySelect";
        } else if ($getResponse == "myRange") {

            $max = explode('-', explode('__&&__', $quest->ResponseData)[0])[0];
            $min = explode('-', explode('__&&__', $quest->ResponseData)[0])[1];
            //   $inputPart = "<input type='text' id='' name='rangeClass_' class='rangeSelect_".$getQuest->id."' value='' />";
            $inputType = "<input type='text' id='rangeSelect" . $quest->id . "' name='rangeClass_" . $quest->id . "' class='form-control rangeClass_" . $quest->id . "'  value='' /><div><span class='pull-left leastDesc rangeInfo' style='color: #333;   font-size: 12px; line-height: 1.333;text-shadow: none;top: 0;padding: 1px 5px; background: rgba(0,0,0,0.1); border-radius: 3px;'>" . explode('-', explode('__&&__', $quest->ResponseData)[1])[0] . "</span><span class='pull-right maxDesc rangeInfo' style='color: #333;   font-size: 12px; line-height: 1.333;text-shadow: none;top: 0;padding: 1px 5px; background: rgba(0,0,0,0.1); border-radius: 3px;'>" . explode('-', explode('__&&__', $quest->ResponseData)[1])[1] . "</span></div>__myRange__" . $min . "__" . $max;
        }
        return response()
                        ->json($inputType);
    }

public function filterAndAnswers(Request $request)
{

//get inputTypes
$countQuest = 0;
$questType = array();
foreach($request->questType as $eachQuestType)
{

$questType[] = $eachQuestType;
}

$filters = array();
foreach($request->filterBy as $eachFilter)
{

$filters[] = $eachFilter;
}

$aggregateReport = [];
foreach($request->questId as $eachQuest)
{
$getUserId = [];
 $allUsers = [];
 $leadArray = [];
if($questType[$countQuest] == "mySelect" || $questType[$countQuest] == "myRadio"){

 $getCurrentPage = SessionKeeper::where('ForUser', '=', Session::get('adminLogin'))->first();
        $filterRecord = DB::select("SELECT distinct UserID from answer_records where CampaignID = " . $request->campId . " and QuestionID = " . $eachQuest . " and Answer = '" . $filters[$countQuest] . "__&&__'");
       
       
        foreach ($filterRecord as $eachUserID) {
        $getUserId[] = $eachUserID->UserID;
            $allUsers[] = DB::select("SELECT * from usertables where id = " . $eachUserID->UserID);
       $getLeadStatus = AnswerRecord::where('CampaignID', '=', $request->campId)->where('UserID', '=', $eachUserID->UserID)->first();
         $leadArray[] = $getLeadStatus->LeadStatus;
        }
        
}
else if($questType[$countQuest] == "myRange")
{


$getCurrentPage = SessionKeeper::where('ForUser', '=', Session::get('adminLogin'))->first();

        $filterRecord = DB::select("SELECT distinct UserID from answer_records where CampaignID = " . $request->campId . " and QuestionID = " . $eachQuest  . " and Answer >= " . $filters[$countQuest]);
        
        
        foreach ($filterRecord as $eachUserID) {
$getUserId[] = $eachUserID->UserID;
            $allUsers[] = DB::select("SELECT * from usertables where id = " . $eachUserID->UserID);
       $getLeadStatus = AnswerRecord::where('CampaignID', '=', $request->campId)->where('UserID', '=', $eachUserID->UserID)->first();
         $leadArray[] = $getLeadStatus->LeadStatus;
        }

   

}
else if($questType[$countQuest] == "myCheck")
{

  $getCurrentPage = SessionKeeper::where('ForUser', '=', Session::get('adminLogin'))->first();
        
        $queryPart = "";
        
        foreach ($filters[$countQuest] as $eachCheck) {
        
        $queryPart  = $queryPart."and Answer like '%" . $eachCheck . "%'";
        
        }
            $filterRecord = DB::select("SELECT distinct UserID from answer_records where CampaignID = " . $request->campId . " and QuestionID = " . $eachQuest ." ".$queryPart);

         
        foreach ($filterRecord as $eachUserID) {
$getUserId[] = $eachUserID->UserID;
            $allUsers[] = DB::select("SELECT * from usertables where id = " . $eachUserID->UserID);
       $getLeadStatus = AnswerRecord::where('CampaignID', '=', $request->campId)->where('UserID', '=', $eachUserID->UserID)->first();
         $leadArray[] = $getLeadStatus->LeadStatus;
        }
        


}

$aggregateReport[] = $getUserId;
$countQuest++;

}



//now find intersection of users

$increament = 1;
$innerArrayLevel = [];
$allArrays  = [];
$allUsersArray = [];
$countLeads = 0;

if($request->filterType == 'AND'){
foreach($aggregateReport as $eachOfAggregate){
if($increament == 1)
{
$allArrays[] = array_intersect($aggregateReport[0], $aggregateReport[$increament]);
}else{
if(isset($allArrays[$increament-2]))
$allArrays[] = array_intersect($allArrays[$increament-2], $aggregateReport[$increament-1]);

}
$increament++;
}
}
else if($request->filterType == 'OR')
{

foreach($aggregateReport as $eachOfAggregate){
if($increament == 1)
{
if(!isset($aggregateReport[1]))
{
$allArrays[] = $aggregateReport[0];
break;
}
else{
$allArrays[] = array_merge($aggregateReport[0], $aggregateReport[$increament]);
}
}else{
if(isset($allArrays[$increament-2]))
$allArrays[] = array_merge($allArrays[$increament-2], $aggregateReport[$increament-1]);

}
$increament++;
}
$allArrays[] = array_unique($allArrays[sizeof($allArrays)-1]);
}

//now get all users by id
$countUsers = 0;
foreach($allArrays[sizeof($allArrays)-1] as $usersHere)
{
    $allUsersArray[] = DB::select("SELECT * from usertables where id = " . $usersHere);
   $checkLeadStatus = DB::select("SELECT * from answer_records where UserID = ". $usersHere ." and CampaignID = ".$request->campId ." and LeadStatus = 1");
    if($checkLeadStatus)
    {
    $countLeads++;
    }
    $countUsers++;
}


$pageArray = [];
        $countByPage = 0;
        $countActual = 0;
        $beginiFrom = 0;
        $endAt = 0;
        foreach ($allUsersArray as $eachUser) {
         if($request->actionType == "Normal")
        {
        	 $pageArray[] = $eachUser;
        }
           else if ($request->actionType == "Next") {
                if ($countByPage >= $getCurrentPage->Status && $countActual < 50) {
                    $pageArray[] = $eachUser;
                    $countActual++;
                    if ($countActual == 1) {
                        $beginiFrom = $countByPage + 1;
                    }
                }
                $countByPage++;
            } else if ($request->actionType == "Previous") {
                if ($countByPage >= $getCurrentPage->Status - 50 && $countActual < 50) {
                    $pageArray[] = $eachUser;

                    $countActual++;
                    if ($countActual == 1) {
                        $beginiFrom = $countByPage + 1;
                    }
                }
                $countByPage++;
            }
        }
        $endAt = $countActual + ($beginiFrom - 1);
        if (sizeof($pageArray) > 0) {
            $currentStatus = $getCurrentPage->Status;
            if ($request->actionType == "Next") {
                if ($currentStatus + $countActual >= 0)
                    $getCurrentPage->Status = $currentStatus + $countActual;
            } else if ($request->actionType == "Previous") {
                if ($currentStatus - $countActual >= 0)
                    $getCurrentPage->Status = $currentStatus - $countActual;
            } else if ($request->actionType == "Normal") {
                if ($countActual >= 0)
                    $getCurrentPage->Status = $countActual;
            }
            $getCurrentPage->save();
        }
        
        $getNumTotal = DB::select("SELECT * from usertables where UserStatus = 1 and Password != '' ");
        
        $FrontEndData = [];
        $FrontEndData[] = $pageArray;
        $FrontEndData[] = $beginiFrom;
        $FrontEndData[] = sizeof($allUsersArray);
        $FrontEndData[] = $endAt;
        $FrontEndData[] = $leadArray;
        
        //sends number of peaple in arrray
        $FrontEndData[] = $increament-1;
        
        //send total number of people
        $FrontEndData[] = sizeof($getNumTotal);
        
        //send number of leads
        $FrontEndData[] = $countLeads;
        
return response()->json($FrontEndData);
//return response()->json($allArrays);
//return response()->json($aggregateReport );

//return response()->json($allArrays[sizeof($allArrays)-1]);


}


    public function filterAnswers(Request $request) {
        // $filterRecord = AnswerRecord::where('CampaignID', '=', $request->campId)
        // ->where('QuestionID', '=', $request->questId)
        // ->where('Answer', '=', $request->filterBy)->get();
        $getCurrentPage = SessionKeeper::where('ForUser', '=', Session::get('adminLogin'))->first();
        $filterRecord = DB::select("SELECT distinct UserID from answer_records where CampaignID = " . $request->campId . " and QuestionID = " . $request->questId . " and Answer = '" . $request->filterBy . "__&&__'");
        $allUsers = [];
       $leadArray = [];
        foreach ($filterRecord as $eachUserID) {

            $allUsers[] = DB::select("SELECT * from usertables where id = " . $eachUserID->UserID);
       $getLeadStatus = AnswerRecord::where('CampaignID', '=', $request->campId)->where('UserID', '=', $eachUserID->UserID)->first();
         $leadArray[] = $getLeadStatus->LeadStatus;
        }
        //now pick by page
        $pageArray = [];
        $countByPage = 0;
        $countActual = 0;
        $beginiFrom = 0;
        $endAt = 0;
        foreach ($allUsers as $eachUser) {
        if($request->actionType == "Normal")
        {
        	 $pageArray[] = $eachUser;
        	
        }
           else if ($request->actionType == "Next" ) {
           
                if ($countByPage >= $getCurrentPage->Status && $countActual < 50) {
                    $pageArray[] = $eachUser;
                    $countActual++;
                    if ($countActual == 1) {
                        $beginiFrom = $countByPage + 1;
                    }
                }
                $countByPage++;
            } else if ($request->actionType == "Previous") {
                if ($countByPage >= $getCurrentPage->Status - 50 && $countActual < 50) {
                    $pageArray[] = $eachUser;

                    $countActual++;
                    if ($countActual == 1) {
                        $beginiFrom = $countByPage + 1;
                    }
                }
                $countByPage++;
            }
        }
        $endAt = $countActual + ($beginiFrom - 1);
        if (sizeof($pageArray) > 0) {
            $currentStatus = $getCurrentPage->Status;
            if ($request->actionType == "Next") {
                if ($currentStatus + $countActual >= 0)
                    $getCurrentPage->Status = $currentStatus + $countActual;
            } else if ($request->actionType == "Previous") {
                if ($currentStatus - $countActual >= 0)
                    $getCurrentPage->Status = $currentStatus - $countActual;
            } else if ($request->actionType == "Normal") {
                if ($countActual >= 0)
                    $getCurrentPage->Status = $countActual;
            }
            $getCurrentPage->save();
        }
        $FrontEndData = [];
        $FrontEndData[] = $pageArray;
        $FrontEndData[] = $beginiFrom;
        $FrontEndData[] = sizeof($allUsers);
        $FrontEndData[] = $endAt;
        $FrontEndData[] = $leadArray;
        return response()->json($FrontEndData);
    }

    public function filterRangeAnswers(Request $request) {

        $getCurrentPage = SessionKeeper::where('ForUser', '=', Session::get('adminLogin'))->first();

        $filterRecord = DB::select("SELECT distinct UserID from answer_records where CampaignID = " . $request->campId . " and QuestionID = " . $request->questId . " and Answer >= " . $request->filterBy);
        $allUsers = [];
        
        $leadArray = [];
        foreach ($filterRecord as $eachUserID) {

            $allUsers[] = DB::select("SELECT * from usertables where id = " . $eachUserID->UserID);
       $getLeadStatus = AnswerRecord::where('CampaignID', '=', $request->campId)->where('UserID', '=', $eachUserID->UserID)->first();
         $leadArray[] = $getLeadStatus->LeadStatus;
        }

        //now pick by page
        $pageArray = [];
        $countByPage = 0;
        $countActual = 0;
        $beginiFrom = 0;
        $endAt = 0;
        foreach ($allUsers as $eachUser) {
   
        if($request->actionType == "Normal")
        {
      //  if ($countByPage >= $getCurrentPage->Status && $countActual < 50) {
                    $pageArray[] = $eachUser;
                    //$countActual++;
                   // if ($countActual == 1) {
                    //    $beginiFrom = $countByPage + 1;
                   // }
             //   }
        }
                  else  if ($request->actionType == "Next") {
                if ($countByPage >= $getCurrentPage->Status && $countActual < 50) {
                    $pageArray[] = $eachUser;
                    $countActual++;
                    if ($countActual == 1) {
                        $beginiFrom = $countByPage + 1;
                    }
                }
                $countByPage++;
            } else if ($request->actionType == "Previous") {
                if ($countByPage >= $getCurrentPage->Status - 50 && $countActual < 50) {
                    $pageArray[] = $eachUser;

                    $countActual++;
                    if ($countActual == 1) {
                        $beginiFrom = $countByPage + 1;
                    }
                }
                $countByPage++;
            }
        }
        $endAt = $countActual + ($beginiFrom - 1);
        if (sizeof($pageArray) > 0) {
            $currentStatus = $getCurrentPage->Status;
            if ($request->actionType == "Next") {
                if ($currentStatus + $countActual >= 0)
                    $getCurrentPage->Status = $currentStatus + $countActual;
            } else if ($request->actionType == "Previous") {
                if ($currentStatus - $countActual >= 0)
                    $getCurrentPage->Status = $currentStatus - $countActual;
            } else if ($request->actionType == "Normal") {
                if ($countActual >= 0)
                    $getCurrentPage->Status = $countActual;
            }
            $getCurrentPage->save();
        }
        $FrontEndData = [];
        $FrontEndData[] = $pageArray;
        $FrontEndData[] = $beginiFrom;
        $FrontEndData[] = sizeof($allUsers);
        $FrontEndData[] = $endAt;
         $FrontEndData[] = $leadArray;

        return response()->json($FrontEndData);
    }

    public function filterSelectAnswers(Request $request) {
        $getCurrentPage = SessionKeeper::where('ForUser', '=', Session::get('adminLogin'))->first();
        $allUsers = [];
        $queryPart = "";
        
        foreach ($request->filterBy as $eachCheck) {
        
        $queryPart  = $queryPart."and Answer like '%" . $eachCheck . "%'";
        
        }
            $filterRecord = DB::select("SELECT distinct UserID from answer_records where CampaignID = " . $request->campId . " and QuestionID = " . $request->questId ." ".$queryPart);

          $leadArray = [];
        foreach ($filterRecord as $eachUserID) {

            $allUsers[] = DB::select("SELECT * from usertables where id = " . $eachUserID->UserID);
       $getLeadStatus = AnswerRecord::where('CampaignID', '=', $request->campId)->where('UserID', '=', $eachUserID->UserID)->first();
         $leadArray[] = $getLeadStatus->LeadStatus;
        }
        

        $pageArray = [];
        $countByPage = 0;
        $countActual = 0;
        $beginiFrom = 0;
        $endAt = 0;
        foreach ($allUsers as $eachUser) {
         if($request->actionType == "Normal")
        {
        	 $pageArray[] = $eachUser;
        }
           else if ($request->actionType == "Next") {
                if ($countByPage >= $getCurrentPage->Status && $countActual < 50) {
                    $pageArray[] = $eachUser;
                    $countActual++;
                    if ($countActual == 1) {
                        $beginiFrom = $countByPage + 1;
                    }
                }
                $countByPage++;
            } else if ($request->actionType == "Previous") {
                if ($countByPage >= $getCurrentPage->Status - 50 && $countActual < 50) {
                    $pageArray[] = $eachUser;

                    $countActual++;
                    if ($countActual == 1) {
                        $beginiFrom = $countByPage + 1;
                    }
                }
                $countByPage++;
            }
        }
        $endAt = $countActual + ($beginiFrom - 1);
        if (sizeof($pageArray) > 0) {
            $currentStatus = $getCurrentPage->Status;
            if ($request->actionType == "Next") {
                if ($currentStatus + $countActual >= 0)
                    $getCurrentPage->Status = $currentStatus + $countActual;
            } else if ($request->actionType == "Previous") {
                if ($currentStatus - $countActual >= 0)
                    $getCurrentPage->Status = $currentStatus - $countActual;
            } else if ($request->actionType == "Normal") {
                if ($countActual >= 0)
                    $getCurrentPage->Status = $countActual;
            }
            $getCurrentPage->save();
        }
        $FrontEndData = [];
        $FrontEndData[] = $pageArray;
        $FrontEndData[] = $beginiFrom;
        $FrontEndData[] = sizeof($allUsers);
        $FrontEndData[] = $endAt;
        $FrontEndData[] = $leadArray;
        return response()->json($FrontEndData);
    }




    public function filterAll(Request $request) {
        $getCurrentPage = SessionKeeper::where('ForUser', '=', Session::get('adminLogin'))->first();
        $filterRecord = DB::select("SELECT distinct UserID from answer_records where CampaignID = " . $request->campId);
        $allUsers = [];
        $leadArray = [];
        foreach ($filterRecord as $eachUserID) {

            $allUsers[] = DB::select("SELECT * from usertables where id = " . $eachUserID->UserID);
       $getLeadStatus = AnswerRecord::where('CampaignID', '=', $request->campId)->where('UserID', '=', $eachUserID->UserID)->first();
         $leadArray[] = $getLeadStatus->LeadStatus;
        }
        $pageArray = [];
        $countByPage = 0;
        $countActual = 0;
        $beginiFrom = 0;
        $endAt = 0;
        foreach ($allUsers as $eachUser) {
         if($request->actionType == "Normal")
                 {
        	 $pageArray[] = $eachUser;
        }
          else if ($request->actionType == "Next") {
                if ($countByPage >= $getCurrentPage->Status && $countActual < 50) {
                    $pageArray[] = $eachUser;
                    $countActual++;
                    if ($countActual == 1) {
                        $beginiFrom = $countByPage + 1;
                    }
                }
                $countByPage++;
            } else if ($request->actionType == "Previous") {
                if ($countByPage >= $getCurrentPage->Status - 50 && $countActual < 50) {
                    $pageArray[] = $eachUser;

                    $countActual++;
                    if ($countActual == 1) {
                        $beginiFrom = $countByPage + 1;
                    }
                }
                $countByPage++;
            }
        }
        $endAt = $countActual + ($beginiFrom - 1);
        if (sizeof($pageArray) > 0) {
            $currentStatus = $getCurrentPage->Status;
            if ($request->actionType == "Next") {
                if ($currentStatus + $countActual >= 0)
                    $getCurrentPage->Status = $currentStatus + $countActual;
            } else if ($request->actionType == "Previous") {
                if ($currentStatus - $countActual >= 0)
                    $getCurrentPage->Status = $currentStatus - $countActual;
            } else if ($request->actionType == "Normal") {
                if ($countActual >= 0)
                    $getCurrentPage->Status = $countActual;
            }
            $getCurrentPage->save();
        }
        $FrontEndData = [];
        $FrontEndData[] = $pageArray;
        $FrontEndData[] = $beginiFrom;
        $FrontEndData[] = sizeof($allUsers);
        $FrontEndData[] = $endAt;
        $FrontEndData[] = $leadArray;
        return response()->json($FrontEndData);
    }

    public function suspendCapaign(Request $request) {
        $thisCamp = OurCampaign::find($request->campId);
        $getStatus = 0;
        if ($thisCamp->Status == 1) {
            $thisCamp->Status = 0;
        } else {
            $thisCamp->Status = 1;
        }
        $thisCamp->save();

        return response()->json($thisCamp->Status);
    }
      public function suspendClient(Request $request) {
        $thisClient = Clients::find($request->clientId);
        $getStatus = 0;
        if ($thisClient->Status == 1) {
            $thisClient->Status = 0;
        } else {
            $thisClient->Status = 1;
        }
        $thisClient->save();

        return response()->json($thisClient->Status);
    }

    public function adminLogout() {
        Session::flush();
        return Redirect::to('adminLogin');
    }
    public function clientLogout() {
        Session::flush();
        return Redirect::to('clientLogin');
    }

    public function editClient(Request $request) {
        $findClient = Clients::find($request->clientID);
        $findClient->CompanyName = $request->client;
        $findClient->CompanyAddress = $request->clientAdd;
        $findClient->Username = $request->username;
        $findClient->Password = md5($request->password);
        $findClient->save();
        
    }
    
    public function viewusers()
    {
     if(Session::get('adminLogin') == null)
    {
    
    return Redirect::to('adminLogin');
    }
        $allUsers = usertable::all();
        return view("viewusers")
        ->with('allUsers', $allUsers);
    }

     public function ViewLeads()
    {
    if(Session::get('clientLogin') == null)
    {
      return Redirect::to('clientLogin');
    }
    $campaigns = DB::select("SELECT * from our_campaigns where ClientID = '".Session::get('clientLogin')."' and Status = 1 order by EndDate desc");
      //  $campaigns = OurCampaign::where('ClientID', '=',  Session::get('clientLogin'))->where('Status', '=', 1)->get();
        $clients = Clients::where('Status', '=', 1);
        return view("ViewLeads")
                        ->with('campaigns', $campaigns)
                        ->with('clients', $clients);
    }     

    
    public function delUser(Request $request){
          $userId = usertable::find($request->userId);
        if ($userId) {
            DB::table('usertables')->delete($request->userId);
            return response()->json("Delete Successful");
        }
        return response()->json("User Does Not Exist");
    }
    
    public function suspendUser(Request $request)
    {
         $thisClient = usertable::find($request->userId);
        $getStatus = 0;
        if ($thisClient->UserStatus == 1) {
            $thisClient->UserStatus = 0;
        } else {
            $thisClient->UserStatus = 1;
        }
        $thisClient->save();

        return response()->json($thisClient->UserStatus);
    }
    
     public function suspendLead(Request $request)
     {
     $getStatus = 0;
     $thisUser = AnswerRecord::where('CampaignID', '=', $request->campId)->where('UserID', '=', $request->userId)->get();
         foreach($thisUser as $eachUser){
        
        if ($eachUser->LeadStatus == 1) {
            $eachUser->LeadStatus = 0;
        } else {
            $eachUser->LeadStatus = 1;
        }
        $eachUser->save();
       }
       $getStatus = $eachUser->LeadStatus;
        return response()->json($getStatus);
     }
     
     public function HowToWin()
     {
         return view("HowToWin");
     }

 public function homeOption()
     {
 $NigerianStates = array(
            'Abuja',
            'Abia',
            'Adamawa',
            'Akwa Ibom',
            'Anambra',
            'Bauchi',
            'Bayelsa',
            'Benue',
            'Borno',
            'Cross River',
            'Delta',
            'Ebonyi',
            'Edo',
            'Ekiti',
            'Enugu',
            'Gombe',
            'Imo',
            'Jigawa',
            'Kaduna',
            'Kano',
            'Katsina',
            'Kebbi',
            'Kogi',
            'Kwara',
            'Lagos',
            'Nassarawa',
            'Niger',
            'Ogun',
            'Ondo',
            'Osun',
            'Oyo',
            'Plateau',
            'Rivers',
            'Sokoto',
            'Taraba',
            'Yobe',
            'Zamfara',
        );
        $stateArray = "<option value=''>Your state of residence</option>";
        foreach ($NigerianStates as $k) {
            $stateArray = $stateArray . "<option value='" . $k . "'>" . $k . "</option>";
        }

        $allPrize = availableprizes::select('PrizeName')->get();
        $allWInners = OurWinner::all();
        return view('homeOption')->with('allStates', $stateArray)->with(compact('allPrize', 'allPrize'))
                        ->with(compact('allWInners', 'allWInners'));
        
     }
     
     
     public function HowDoIWin()
     {
  /*   if(Session::get('logedIn') == null){
 return Redirect::to('');

}
            $checkInfo = usertable::where('facebook_id', '=', Session::get('logedIn'))->first();
            if ($checkInfo == null) {
                $checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
            }

            if ($checkInfo->UserImg != null && $checkInfo->UserImg != "") {
                Session::put('DP', $checkInfo->UserImg);
            } else if ($checkInfo->facebook_id != null && $checkInfo->facebook_id != "" && $checkInfo->facebook_id != "Direct Reg.") {
                Session::put('DP', $checkInfo->facebook_id);
            } else {
                Session::put('DP', 'noDP');
            }*/
       return view("HowDoIWin");
// ->with('DP', @Session::get('DP'));
     }
     
     public function PrivacyPolicy()
     {
/* if(Session::get('logedIn') == null){
 return Redirect::to('');

}
            $checkInfo = usertable::where('facebook_id', '=', Session::get('logedIn'))->first();
            if ($checkInfo == null) {
                $checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
            }

            if ($checkInfo->UserImg != null && $checkInfo->UserImg != "") {
                Session::put('DP', $checkInfo->UserImg);
            } else if ($checkInfo->facebook_id != null && $checkInfo->facebook_id != "" && $checkInfo->facebook_id != "Direct Reg.") {
                Session::put('DP', $checkInfo->facebook_id);
            } else {
                Session::put('DP', 'noDP');
            }*/
       return view("PrivacyPolicy");
      //  ->with('DP', @Session::get('DP'));
     }
     
     public function TermsAndCondition()
     {
      /*if(Session::get('logedIn') == null){
 return Redirect::to('');

}
            $checkInfo = usertable::where('facebook_id', '=', Session::get('logedIn'))->first();
            if ($checkInfo == null) {
                $checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
            }

            if ($checkInfo->UserImg != null && $checkInfo->UserImg != "") {
                Session::put('DP', $checkInfo->UserImg);
            } else if ($checkInfo->facebook_id != null && $checkInfo->facebook_id != "" && $checkInfo->facebook_id != "Direct Reg.") {
                Session::put('DP', $checkInfo->facebook_id);
            } else {
                Session::put('DP', 'noDP');
            }*/
       return view("TermsAndCondition");
       // ->with('DP', @Session::get('DP'));
     
     }
     
     public function GeneralRules()
     {
     /* if(Session::get('logedIn') == null){
 return Redirect::to('');

}
        $checkInfo = usertable::where('facebook_id', '=', Session::get('logedIn'))->first();
            if ($checkInfo == null) {
                $checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
            }

            if ($checkInfo->UserImg != null && $checkInfo->UserImg != "") {
                Session::put('DP', $checkInfo->UserImg);
            } else if ($checkInfo->facebook_id != null && $checkInfo->facebook_id != "" && $checkInfo->facebook_id != "Direct Reg.") {
                Session::put('DP', $checkInfo->facebook_id);
            } else {
                Session::put('DP', 'noDP');
            }*/
       return view("GeneralRules");
       // ->with('DP', @Session::get('DP'));
     
     }
     
     public function clientLogin()
     {
     
     return view("clientLogin");
     }


    public function getParticipant(Request $request)
    {
    
 $filterRecord = DB::select("SELECT distinct UserID from answer_records where CampaignID = ".$request->campId." and LeadStatus = 1");
 $allUsers = array();
     foreach ($filterRecord as $eachUserID) {

            $allUsers[] = DB::select("SELECT * from usertables where id = ".$eachUserID->UserID);
        }
    return response()->json($allUsers);
    
    }
    
    public function getAllClient(Request $request)
    {
    $allClient = DB::select("SELECT * from clients where Status = 1");
    
    return response()->json($allClient );
    }
    
    public function WinMoney_PrivacyPolicy()
    {
      return view('WinMoney_PrivacyPolicy');
    }
    
    
    
     public function Winmoney_GeneralRules()
    {
      return view('Winmoney_GeneralRules');
    }
    
     public function Winmoney_HowToWin()
    {
      return view('Winmoney_HowToWin');
    }
    
    
     public function Winmoney_TermsAndCondition()
    {
      return view('Winmoney_TermsAndCondition');
    }
    
    public function sendMail(Request $request)
    {
     @$headers = 'From: ' . $request->Email . "\r\n";
    mail('info@winmoney.ng', $request->Subject, $request->Message, @$headers);
    return response()->json('Success');
    }

  public function showMiniRep(Request $request)
  {
    $getTotalUsers = DB::select("SELECT distinct UserID from answer_records where CampaignID = ".$request->campId);
    $getUserCount = DB::select("SELECT * from usertables where Password != '' and UserStatus = 1");
    
    $responseArray = array();
    $responseArray[] = count($getTotalUsers);
    $responseArray[] = count($getUserCount);
    return response()->json($responseArray);
  }

}
