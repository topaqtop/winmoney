<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBalanceHistorysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('balance_historys', function (Blueprint $table) {
            $table->increments('id');
             $table->integer('UserID');
             $table->datetime('DayTime');
              $table->string('ActionPerformed');
$table->integer('CurrentBalance');
$table->integer('Amount');
              $table->integer('CampaignID');
$table->string('Response');
             
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('balance_historys');
    }
}
