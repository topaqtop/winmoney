<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\AnswerRecord;
use App\usertable;
use App\adminList;
use App\availableprizes;
use App\OurWinner;
use App\OurCampaign;
use App\SpecialPrize;
use App\CampaignQuestions;
use App\Clients;
use Session;
use DB;
use Carbon\Carbon;
use App\dummyDP;
use App\FormBuilder;


class ClientController extends Controller {

    //
    
  
    public function ClientPage() {
        return view('ClientPage');
    }

    public function RegisterPage() {

        return view('RegisterPage');
    }

    public function RegisterUser(Request $request) {

        //lets validate
        $rules = array(
            'Surname' => 'required', // just a normal required validation
            'Other_Name' => 'required',
            'Email' => 'required|email', // required and must be unique in the ducks table
            'State' => 'required',
            'City' => 'required',
            'Gender' => 'required',
            'Age_Range' => 'required',
            'Phone_Number' => 'required|min:11|numeric', // required and has to match the password field
            'Password' => 'required|min:6',
            'Confirm_Password' => 'required|same:Password',
            'g-recaptcha-response' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        // check if the validator failed -----------------------
        if ($validator->fails()) {

            return Redirect::to('WinMoneyIndex')
                            ->withErrors($validator)
                            ->withInput(Input::except('password1', 'password2'));
        } else {
            session_start();
            //get random referred code


            $referrerCode = rand(100000000, 999999999);
//            $uniqueCode = "0";
//            while($checkIfCodeExist = usertable::where('ReferrerCode', '=', $referrerCode)->first())
//            {
//                $uniqueCode = (string)$referrerCode;
//            }
            $input = Input::only('Surname', 'Other_Name', 'Email', 'State', 'City', 'Gender', 'Age_Range', 'Phone_Number', 'Password', 'Confirm_Password', 'ReferrerCode', 'ReferrerBy');
            $user = new usertable();
            if (@$input['ReferrerCode'] != null && @$input['ReferrerCode'] != "") {
                $checkReferrerCode = usertable::where('ReferrerCode', '=', @$input['ReferrerCode'])->first();
                if (!$checkReferrerCode) {
                    return Redirect::to('/WinMoneyIndex')->with('message', 'Incorrect Referrer Code');
                } else {
                    $user->ReferrerBy = @$input['ReferrerCode'];
                }
            }
            $user->Surname = $input['Surname'];
            $user->Other_Name = $input['Other_Name'];
            $user->Email = $input['Email'];
            $user->State = $input['State'];
            $user->City = $input['City'];
            $user->Gender = $input['Gender'];
            $user->Age_Range = $input['Age_Range'];
            $user->Phone_Number = $input['Phone_Number'];
            $user->Password = $input['Password'];
            $user->Confirm_Password = $input['Confirm_Password'];
            $user->facebook_id = "Direct Reg.";
            $user->ReferrerCode = $referrerCode;

            $user->save();

            $checkEmail = usertable::where('Email', '=', $input['Email'])->where('Password', '=', $input['Password'])->first();
            session()->put('logedIn', $checkEmail->id);
            Session::put('logedIn', $checkEmail->id);
            //Session::set('logedIn', 1);

            return Redirect::to('/WinMoneyIndex')->with('message', 'Registeration Successful');
            // return redirect()->back()->with('message', 'SignUp Successful');
        }
    }

    public function CompleteRegPage() {
        $NigerianStates = array(
            'Abuja',
            'Abia',
            'Adamawa',
            'Akwa Ibom',
            'Anambra',
            'Bauchi',
            'Bayelsa',
            'Benue',
            'Borno',
            'Cross River',
            'Delta',
            'Ebonyi',
            'Edo',
            'Ekiti',
            'Enugu',
            'Gombe',
            'Imo',
            'Jigawa',
            'Kaduna',
            'Kano',
            'Katsina',
            'Kebbi',
            'Kogi',
            'Kwara',
            'Lagos',
            'Nassarawa',
            'Niger',
            'Ogun',
            'Ondo',
            'Osun',
            'Oyo',
            'Plateau',
            'Rivers',
            'Sokoto',
            'Taraba',
            'Yobe',
            'Zamfara',
        );
        $stateArray = "<option value=''>Your State Of Residence</option>";
        foreach ($NigerianStates as $k) {
            $stateArray = $stateArray . "<option value='" . $k . "'>" . $k . "</option>";
        }
        return view('CompleteRegPage')->with('allStates', $stateArray);
    }

    public function UpdateUserInfo(Request $request) {
        try {
            //Find the user object from model if it exists
            $rules = array(
                'Phone_Number' => 'required|min:11|numeric', // required and has to match the password field
                'Password' => 'required|min:6',
                'Confirm_Password' => 'required|same:Password',
                'State' => 'required',
                'City' => 'required',
                'Age_Range' => 'required',
            );
            $validator = Validator::make(Input::all(), $rules);

            // check if the validator failed -----------------------
            if ($validator->fails()) {

                return Redirect::to('/successful')
                                ->withErrors($validator)
                                ->withInput(Input::except('password1', 'password2'));
            }

            $id = Session::get('id');
            //$user = user::findOrFail($id);
            $user = usertable::where('facebook_id', $id)->first();
            //$request contain your post data sent from your edit from
            //$user is an object which contains the column names of your table
            //Set user object attributes
            $referrerCode = rand(100000000, 999999999);

            $user->Phone_number = $request['Phone_Number'];
            $user->Password = $request['Password'];
            $user->Confirm_Password = $request['Confirm_Password'];
            $user->State = $request['State'];
            $user->City = $request['City'];
            $user->Age_Range = $request['Age_Range'];
            $user->ReferrerCode = $referrerCode;

            session()->put('logedIn', $id);
            Session::put('logedIn', $id);
            if (@$request['ReferrerCode'] != null && @$request['ReferrerCode'] != "") {
                $checkReferrerCode = usertable::where('ReferrerCode', '=', @$request['ReferrerCode'])->first();
                if (!$checkReferrerCode) {
                    Session::put('regStatus', 'Incomplete');
                    return Redirect::to('/successful')->with('message', 'Incorrect Referrer Code, Please Try Again.');
                } else {
                    $user->ReferrerBy = @$request['ReferrerCode'];
                }
            }
            Session::put('regStatus', 'Complete');
            // Save/update user. 
            // This will will update your the row in ur db.
            $user->save();

            // Session::set('logedIn', 1);w
            return Redirect::to('/successful')->with('message', 'Registration Complete. You May Now Login');

            // return Redirect::to('/successful')->with('UserName', $id);
        } catch (ModelNotFoundException $err) {
            //Show error page
        }
    }

    public function nowLoggedIn() {
        //   $logedInStatus =  session()->pull('logedIn');
        Session::put('campLimit', 20);
        if (Session::get('logedIn') != "" && Session::get('logedIn') != null) {
            // $id = session()->pull('logedIn');
            //save it to the session again so it wont forget
            Session::get('logedIn');
            $checkInfo = usertable::where('facebook_id', '=', Session::get('logedIn'))->first();
            if ($checkInfo == null) {
                $checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
            }

           
            if($checkInfo->UserImg != null && $checkInfo->UserImg != "")
            {
                 Session::put('DP', $checkInfo->UserImg);
            }
            else if($checkInfo->facebook_id != null && $checkInfo->facebook_id != "" && $checkInfo->facebook_id != "Direct Reg.")
            {
                Session::put('DP', $checkInfo->facebook_id);
            }
            else
            {
                 Session::put('DP', 'noDP');
            }
           
           

            // return view("Campaigns")->with('UserName', $checkInfo->Surname." ".$checkInfo->Other_Name);
            // $newQuest = DB::table('clientcampaigns')->whereNotIn('id', function($q){
            //$q->select('CampaignID')->from('answer_records');
//})->get();
            // $newQuest = DB::select("SELECT * FROM clientcampaigns WHERE id NOT IN ( SELECT CampaignID FROM answer_records )");
            // foreach ($newQuest as $eachRow) {
            //$getQuest = CampaignQuestions::all()->where('CampaignID', '=', $eachRow->id); 
            //DB::select("SELECT * FROM campaign_questions WHERE CampaignID = $eachRow->id");
            // }
            //  $latest = DB::table('campaign_questions')
            // ->select('*')->where('CampaignID', '=', DB::table('clientcampaigns')->select('id')->whereNotIn('CampaignID', '=', DB::table('answer_records')->select('CampaignID')))->get();
//return view("IndexPage")->with('regStatus', 'Incomplete')->with('allStates', $stateArray);
            $regStatus = Session::get('regStatus');
            $stateArray = Session::get('allStates');
           // $prizeTotal = DB::raw("SELECT sum(NumberAvailable) FROM availableprizes");
          
           $prizeTotal = DB::table('availableprizes')
            ->sum('NumberAvailable');
            if ($regStatus == "Complete") {

                $latest = DB::select("SELECT pm.* FROM campaign_questions pm Natural left join answer_records WHERE pm.CampaignID NOT IN (SELECT pd.CampaignID FROM answer_records pd where UserID = " . $checkInfo->id . ")");
                $winners = DB::select("SELECT * FROM our_winners");
                $allPrizes = DB::select("SELECT * FROM availableprizes limit 7");
               if($checkInfo->ReferrerCode != null && $checkInfo->ReferrerCode != "")
                {
                     $referredUsers = DB::select("SELECT * FROM usertables where ReferrerBy = " . $checkInfo->ReferrerCode . "  limit 7");
                }
               else
               {
                   $referredUsers = array();
               }
               $allQuestions = DB::select("SELECT * from our_campaigns where EndDate > ". Carbon::now()->format('Y-m-d'));
                $allanswered = DB::select("SELECT DISTINCT CampaignID FROM answer_records where UserID = ".$checkInfo->id);
                return view("IndexPage")->with('UserName', $checkInfo)
                                ->with(compact('latest', 'latest'))
                                ->with(compact('winners', 'winners'))
                                ->with(compact('allPrizes', 'allPrizes'))
                                ->with(compact('referredUsers', 'referredUsers'))
                                 ->with(compact('prizeTotal', $prizeTotal))
                                ->with('regStatus', 'Complete')
  ->with('DP', Session::get('DP'))
                         ->with('answered', (count($allanswered)))
                                ->with('available', (count($allQuestions)-count($allanswered)));
            } else if ($regStatus == "Incomplete") {
                $NigerianStates = array(
                    'Abuja',
                    'Abia',
                    'Adamawa',
                    'Akwa Ibom',
                    'Anambra',
                    'Bauchi',
                    'Bayelsa',
                    'Benue',
                    'Borno',
                    'Cross River',
                    'Delta',
                    'Ebonyi',
                    'Edo',
                    'Ekiti',
                    'Enugu',
                    'Gombe',
                    'Imo',
                    'Jigawa',
                    'Kaduna',
                    'Kano',
                    'Katsina',
                    'Kebbi',
                    'Kogi',
                    'Kwara',
                    'Lagos',
                    'Nassarawa',
                    'Niger',
                    'Ogun',
                    'Ondo',
                    'Osun',
                    'Oyo',
                    'Plateau',
                    'Rivers',
                    'Sokoto',
                    'Taraba',
                    'Yobe',
                    'Zamfara',
                );
                $stateArray = "<option value=''>Your State Of Residence</option>";
                foreach ($NigerianStates as $k) {
                    $stateArray = $stateArray . "<option value='" . $k . "'>" . $k . "</option>";
                }

                $latest = DB::select("SELECT pm.* FROM campaign_questions pm Natural left join answer_records WHERE pm.CampaignID NOT IN (SELECT pd.CampaignID FROM answer_records pd where UserID = " . $checkInfo->id . ")");
                $winners = DB::select("SELECT * FROM our_winners");
                $allPrizes = DB::select("SELECT * FROM availableprizes limit 7");
               if($checkInfo->ReferrerCode != null && $checkInfo->ReferrerCode != "")
                {
                     $referredUsers = DB::select("SELECT * FROM usertables where ReferrerBy = " . $checkInfo->ReferrerCode . "  limit 7");
                }
               else
               {
                   $referredUsers = array();
               }
                $allQuestions = DB::select("SELECT * from our_campaigns where EndDate > ". Carbon::now()->format('Y-m-d'));
                $allanswered = DB::select("SELECT DISTINCT CampaignID FROM answer_records where UserID = ".$checkInfo->id);
                return view("IndexPage")->with('UserName', $checkInfo)
                                ->with(compact('latest', 'latest'))
                                ->with(compact('winners', 'winners'))
                                ->with(compact('allPrizes', 'allPrizes'))
                                ->with(compact('referredUsers', 'referredUsers'))
                         ->with(compact('prizeTotal', $prizeTotal))
                                ->with('regStatus', 'Incomplete')
                         ->with('answered', (count($allanswered)))
  ->with('DP', Session::get('DP'))
                                 ->with('available', (count($allQuestions)-count($allanswered)))
                                ->with('allStates', $stateArray);
            } else {
                $NigerianStates = array(
                    'Abuja',
                    'Abia',
                    'Adamawa',
                    'Akwa Ibom',
                    'Anambra',
                    'Bauchi',
                    'Bayelsa',
                    'Benue',
                    'Borno',
                    'Cross River',
                    'Delta',
                    'Ebonyi',
                    'Edo',
                    'Ekiti',
                    'Enugu',
                    'Gombe',
                    'Imo',
                    'Jigawa',
                    'Kaduna',
                    'Kano',
                    'Katsina',
                    'Kebbi',
                    'Kogi',
                    'Kwara',
                    'Lagos',
                    'Nassarawa',
                    'Niger',
                    'Ogun',
                    'Ondo',
                    'Osun',
                    'Oyo',
                    'Plateau',
                    'Rivers',
                    'Sokoto',
                    'Taraba',
                    'Yobe',
                    'Zamfara',
                );
                $stateArray = "<option value=''>Your State Of Residence</option>";
                foreach ($NigerianStates as $k) {
                    $stateArray = $stateArray . "<option value='" . $k . "'>" . $k . "</option>";
                }
                $campignId = 0;
                $latest = DB::select("SELECT pm.* FROM campaign_questions pm Natural left join answer_records WHERE pm.CampaignID NOT IN (SELECT pd.CampaignID FROM answer_records pd where UserID = " . $checkInfo->id . ")");
                foreach ($latest as $d)
                {
                   $campignId = $d->CampaignID;
                }
                $winners = DB::select("SELECT * FROM our_winners");
                $allPrizes = DB::select("SELECT * FROM availableprizes limit 7");
              if($checkInfo->ReferrerCode != null && $checkInfo->ReferrerCode != "")
                {
                     $referredUsers = DB::select("SELECT * FROM usertables where ReferrerBy = " . $checkInfo->ReferrerCode . "  limit 7");
                }
               else
               {
                   $referredUsers = array();
               }
                $allQuestions = DB::select("SELECT * from our_campaigns where EndDate > ". Carbon::now()->format('Y-m-d'));
                $allanswered = DB::select("SELECT DISTINCT CampaignID FROM answer_records where UserID = ".$checkInfo->id);
               
                return view("IndexPage")->with('UserName', $checkInfo)
                                ->with(compact('latest', 'latest'))
                                ->with(compact('winners', 'winners'))
                                ->with(compact('allPrizes', 'allPrizes'))
                                ->with(compact('referredUsers', 'referredUsers'))
                                ->with('regStatus', 'unknown')
                         ->with('answered', (count($allanswered)))
                         ->with(compact('prizeTotal', $prizeTotal))
  ->with('DP', Session::get('DP'))
                                 ->with('available', (count($allQuestions)-count($allanswered)))
                         ->with('answered', (count($allanswered)))
                                ->with('allStates', $stateArray);
            }
        } else {
            return Redirect::to('/WinMoneyIndex');
        }
//        if ($request->session()->exists('logedIn')) {
//            return Redirect::to('/successful');
//        } else {
//            return Redirect::to('/WinMoneyIndex');
//        }
    }

    public function WinMoneyWebsite() {
        $NigerianStates = array(
            'Abuja',
            'Abia',
            'Adamawa',
            'Akwa Ibom',
            'Anambra',
            'Bauchi',
            'Bayelsa',
            'Benue',
            'Borno',
            'Cross River',
            'Delta',
            'Ebonyi',
            'Edo',
            'Ekiti',
            'Enugu',
            'Gombe',
            'Imo',
            'Jigawa',
            'Kaduna',
            'Kano',
            'Katsina',
            'Kebbi',
            'Kogi',
            'Kwara',
            'Lagos',
            'Nassarawa',
            'Niger',
            'Ogun',
            'Ondo',
            'Osun',
            'Oyo',
            'Plateau',
            'Rivers',
            'Sokoto',
            'Taraba',
            'Yobe',
            'Zamfara',
        );
        $stateArray = "<option value=''>Your State Of Residence</option>";
        foreach ($NigerianStates as $k) {
            $stateArray = $stateArray . "<option value='" . $k . "'>" . $k . "</option>";
        }

        $allPrize = availableprizes::select('PrizeName')->get();
        $allWInners = OurWinner::all();
        return view('website')->with('allStates', $stateArray)->with(compact('allPrize', 'allPrize'))
                        ->with(compact('allWInners', 'allWInners'));
    }

    public function userSignin(Request $request) {
        //lets validate

        session_start();
        $checkInfo = usertable::where('Email', '=', Input::get('LoginEmail'))->where('Password', '=', Input::get('loginpassword'))->first();

        if ($checkInfo != null) {

            session()->put('logedIn', $checkInfo->id);
            Session::put('logedIn', $checkInfo->id);
            // Session::set('logedIn', 1);
            // $campaign_questions = new campaign_questions();
            $getQuest = "";
            $latest = DB::select("SELECT pm.* FROM campaign_questions pm Natural left join answer_records WHERE pm.CampaignID not in (SELECT pd.CampaignID FROM answer_records pd where UserID = " . $checkInfo->id . ")");
            //return view('Campaigns');
            Session::put('regStatus', 'Complete');
            $winners = DB::select("SELECT * FROM our_winners limit 5");
            return Redirect::to('/successful')->with('UserName', $checkInfo)->with(compact('latest', 'latest'))
                            ->with(compact('winners', 'winners'))
                            ->with('regStatus', 'Complete');
        } else {

            return redirect()->back()->with('message', 'Incorrect Username or Password');
        }
    }

    public function userSignout() {
        Session::flush();
        return Redirect::to('WinMoneyIndex');
    }

    public function prevCamp() {
        if (Session::get('logedIn') != "" && Session::get('logedIn') != null) {
            // $id = session()->pull('logedIn');
            //save it to the session again so it wont forget
            Session::get('logedIn');
            //$checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
$checkInfo = usertable::where('facebook_id', '=', Session::get('logedIn'))->first();
            if ($checkInfo == null) {
                $checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
            }

            $latest = DB::select("SELECT pm.* FROM campaign_questions pm Natural left join answer_records WHERE pm.CampaignID in (SELECT pd.CampaignID FROM answer_records pd where UserID = " . $checkInfo->id . ") limit ".Session::get('campLimit'));

            $winners = DB::select("SELECT * FROM our_winners limit 5");

            return view("previousCampaign")->with('UserName', $checkInfo)
                            ->with(compact('latest', 'latest'))
                            ->with(compact('winners', 'winners'));
        } else {
            return Redirect::to('/WinMoneyIndex');
        }
    }

    public function saveAnswer(Request $request) {


        $answer_record = new AnswerRecord();
        $answer_record->UserID = Session::get('logedIn');
        $answer_record->QuestionID = $request->questionId;
        $answer_record->Answer = $request->answer;
        $answer_record->CampaignID = $request->campaignID;
        $answer_record->save();

        return response()->json($answer_record);
    }

    public function alreadyAnswered(Request $request) {
        $answer_record = AnswerRecord::where('UserID', '=', Session::get('logedIn'))->where('CampaignID', '=', $request->campaignID)->get();
        if ($answer_record) {
            return response()->json(var_dump($answer_record));
        }
    }

    public function getCampaign(Request $request) {
        $frames = "";
        if ($request->action == "done") {
            
            $special = "";
            $test = "";
           // $checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
$checkInfo = usertable::where('facebook_id', '=', Session::get('logedIn'))->first();
            if ($checkInfo == null) {
                $checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
            }

            $latest = DB::select("SELECT pm.* FROM campaign_questions pm Natural left join answer_records WHERE pm.CampaignID in (SELECT pd.CampaignID FROM answer_records pd where UserID = " . $checkInfo->id . ")");
          $count = 0;
           $allId = array();
           $campCount = 0;
            foreach ($latest as $myCampaign) {
              
               if($campCount <=  Session::get('campLimit')){
               $tillEnd = (Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->diffInDays(Carbon::now()) + 1) == 1 ? 'Campaign Ends today' : (Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->diffInDays(Carbon::now()) + 1) . ' Day(s) to end';
                $specialPrize = SpecialPrize::where('CampignID', '=', $myCampaign->CampaignID)->first();
                if ($specialPrize) {
                    $special = '<h6>Special Prize: ' . $specialPrize->SpecialPrize . '</h6>';
                } else {
                    $special = '<h5>Special Prize: NIL</h5>';
                }
                            if(!in_array($myCampaign -> CampaignID, $allId)){
                           
                            $allId[] = $myCampaign->CampaignID;
                            $count++;
                           
                            $eachQuest = "<form method='post' id='form_" . $count . "' align='left'>";
                            csrf_field(); 
 $countForm = 0; 
                            foreach($latest as $getQuest){
                            if($getQuest -> CampaignID == $myCampaign -> CampaignID){

 $countForm++;
$eachQuest = $eachQuest . "<label>" . $getQuest->Question . "</label><br><input type='text' name='val_" . $countForm . "' class='form-control toDisable_" . $myCampaign->CampaignID . "' id='val_" . $countForm . "' /><br><br><input type='hidden' name='QuestID' class='Quest_" . $getQuest->id . "' value='" . $getQuest->id . "' />"; 

                            }
                           }
 $eachQuest = $eachQuest . "<input type='hidden' name='CampaigInfo' value='" . $myCampaign->CampaignID . "' /> </form>"; 
                           if($count > 0 && Carbon::parse(OurCampaign::find($myCampaign->CampaignID) -> EndDate)->addDay() > Carbon::now()){
                           $quotedRegStatus = "'".@$request->regStatus."'";
                               $quotedCampID = "'".@$myCampaign->CampaignID."'";
                $frames = $frames . ' <div class=" col-lg-4 campBoxes camp_'.$myCampaign -> CampaignID.' myClass" align="center"><div class=" ">' .
                        ' <div class="box--sml text-center flush-pd-trailer col-lg-12">' .
                        '<a class="block " href="#">' .
                        '<img src="winner.jpg" width="100%"><h6 class="box-title elltext">'.OurCampaign::find($myCampaign->CampaignID) -> CampaignName.'</h6>' .
                        '<h6 class="box-title elltext">' . $tillEnd . '</h6>' .
                        '<h6 class="box-title elltext" style="font-weight: 700;">' .
                        $special .
                        '<h6><p>Details:'.OurCampaign::find($myCampaign->CampaignID) -> CampaignNotes.'</p></h6>' .
                        
                        '</a>' .
                        '<input type="hidden" value="' . $eachQuest . '" class="getQuest_'.$myCampaign -> CampaignID.'">' .
                        ' <input type="hidden" value="' . $count . '" class="getCount_'.$myCampaign -> CampaignID.'">' .
                        ' </div>' .
                        '</div>' .
                        '</div>';
            }
        }
            }
             $campCount++;
            }
        //   return view("previousCampaign")->with(compact('latest', 'latest'));
    }
    else  if ($request->action == "notdone") {
           
            $special = "";
            $test = "";
           // $checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
$checkInfo = usertable::where('facebook_id', '=', Session::get('logedIn'))->first();
            if ($checkInfo == null) {
                $checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
            }

            $latest = DB::select("SELECT pm.* FROM campaign_questions pm Natural left join answer_records WHERE pm.CampaignID not in (SELECT pd.CampaignID FROM answer_records pd where UserID = " . $checkInfo->id . ")");
          $count = 0;
           $allId = array();
           $campCount = 0;
            foreach ($latest as $myCampaign) {
                
               if($campCount <=  Session::get('campLimit')){
                 $test = $test. "entered";
              //  $tillEnd = (Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->diffInDays(Carbon::now()->format('Y-m-d')) + 1) == 1 ? 'Campaign Ends today' : (Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->diffInDays(Carbon::now()->format('Y-m-d')) + 1) . ' Day(s) to end';
               $tillEnd = (Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->diffInDays(Carbon::now()) + 1) == 1 ? 'Campaign Ends today' : (Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->diffInDays(Carbon::now()) + 1) . ' Day(s) to end';
                $specialPrize = SpecialPrize::where('CampignID', '=', $myCampaign->CampaignID)->first();
                if ($specialPrize) {
                    $special = '<h6>Special Prize: ' . $specialPrize->SpecialPrize . '</h6>';
                } else {
                    $special = '<h5>Special Prize: NIL</h5>';
                }
                            if(!in_array($myCampaign -> CampaignID, $allId)){
                           
                            $allId[] = $myCampaign->CampaignID;
                            $count++;
                           
                            $eachQuest = "<form method='post' id='form_" . $count . "' align='left'>";
                            csrf_field(); 
 $countForm = 0; 
                            foreach($latest as $getQuest){
                            if($getQuest -> CampaignID == $myCampaign -> CampaignID){

 $countForm++;
$eachQuest = $eachQuest . "<label>" . $getQuest->Question . "</label><br><input type='text' name='val_" . $countForm . "' class='form-control toDisable_" . $myCampaign->CampaignID . "' id='val_" . $countForm . "' /><br><br><input type='hidden' name='QuestID' class='Quest_" . $getQuest->id . "' value='" . $getQuest->id . "' />"; 

                            }
                           }
 $eachQuest = $eachQuest . "<input type='hidden' name='CampaigInfo' value='" . $myCampaign->CampaignID . "' /> </form>"; 
                           
                             if($count > 0 && Carbon::parse(OurCampaign::find($myCampaign->CampaignID) -> EndDate)->addDay() > Carbon::now()){
                 $quotedRegStatus = "'".@$request->regStatus."'";
                               $quotedCampID = "'".@$myCampaign->CampaignID."'";
$frames = $frames . ' <div class=" col-lg-4 campBoxes camp_'.$myCampaign -> CampaignID.' myClass" align="center"><div class=" ">' .
                        ' <div class="box--sml text-center flush-pd-trailer col-lg-12">' .
                        '<a class="block " href="#">' .
                        '<img src="winner.jpg" width="100%"><h6 class="box-title elltext">'.OurCampaign::find($myCampaign->CampaignID) -> CampaignName.'</h6>' .
                        '<h6 class="box-title elltext">' . $tillEnd . '</h6>' .
                        '<h6 class="box-title elltext" style="font-weight: 700;">' .
                        $special .
                        '<h6><p>Details:'.OurCampaign::find($myCampaign->CampaignID) -> CampaignNotes.'</p></h6>' .
                        '</h6><p class="push-sbit"><button class="btn btn-primary btn-xs campaign_count_'.$count.' QuestClass" onclick="checkReg(' . $quotedRegStatus . ', ' . $quotedCampID . ')">TAKE QUIZ</button></p>' .
                        '</a>' .
                        '<input type="hidden" value="' . $eachQuest . '" class="getQuest_'.$myCampaign -> CampaignID.'">' .
                        ' <input type="hidden" value="' . $count . '" class="getCount_'.$myCampaign -> CampaignID.'">' .
                        ' </div>' .
                        '</div>' .
                        '</div>';
            }
        }
            }
             $campCount++;
            }
        //   return view("previousCampaign")->with(compact('latest', 'latest'));
    }
        
 return response()->json(($frames));
}

public function getMore(Request $request)
{
    //$campLimit = (int)Session::get('campLimit') + 20;
    
    Session::put('campLimit', (Session::get('campLimit')+1));
       $frames = "";
       $test = "";
       
        if ($request->action == "done") {
            
            $special = "";
           
          //  $checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
$checkInfo = usertable::where('facebook_id', '=', Session::get('logedIn'))->first();
            if ($checkInfo == null) {
                $checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
            }

//            $latest = DB::select("SELECT pm.* FROM campaign_questions pm Natural left join answer_records WHERE pm.CampaignID in (SELECT pd.CampaignID FROM answer_records pd where UserID = " . $checkInfo->id . ") limit ". Session::get('campLimit'));
         $latest = DB::select("SELECT pm.* FROM campaign_questions pm Natural left join answer_records WHERE pm.CampaignID in (SELECT pd.CampaignID FROM answer_records pd where UserID = " . $checkInfo->id .")");
            $count = 0;
           $allId = array();
           $campCount = 0;
         
            foreach ($latest as $myCampaign) {
            $test = $test."entered";
               if($campCount <=  Session::get('campLimit')){
               $tillEnd = (Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->diffInDays(Carbon::now()) + 1) == 1 ? 'Campaign Ends today' : (Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->diffInDays(Carbon::now()) + 1) . ' Day(s) to end';
                $specialPrize = SpecialPrize::where('CampignID', '=', $myCampaign->CampaignID)->first();
                if ($specialPrize) {
                    $special = '<h6>Special Prize: ' . $specialPrize->SpecialPrize . '</h6>';
                } else {
                    $special = '<h5>Special Prize: NIL</h5>';
                }
                            if(!in_array($myCampaign -> CampaignID, $allId)){
                           
                            $allId[] = $myCampaign->CampaignID;
                            $count++;
                           
                            $eachQuest = "<form method='post' id='form_" . $count . "' align='left'>";
                            csrf_field(); 
 $countForm = 0; 
                            foreach($latest as $getQuest){
                            if($getQuest -> CampaignID == $myCampaign -> CampaignID){

 $countForm++;
$eachQuest = $eachQuest . "<label>" . $getQuest->Question . "</label><br><input type='text' name='val_" . $countForm . "' class='form-control toDisable_" . $myCampaign->CampaignID . "' id='val_" . $countForm . "' /><br><br><input type='hidden' name='QuestID' class='Quest_" . $getQuest->id . "' value='" . $getQuest->id . "' />"; 

                            }
                           }
 $eachQuest = $eachQuest . "<input type='hidden' name='CampaigInfo' value='" . $myCampaign->CampaignID . "' /> </form>"; 
                           if($count > 0 && Carbon::parse(OurCampaign::find($myCampaign->CampaignID) -> EndDate)->addDay() > Carbon::now()){
               $quotedRegStatus = "'".@$request->regStatus."'";
                               $quotedCampID = "'".@$myCampaign->CampaignID."'";
                $frames = $frames . ' <div class=" col-lg-4 campBoxes camp_'.$myCampaign -> CampaignID.' myClass" align="center"><div class=" ">' .
                        ' <div class="box--sml text-center flush-pd-trailer col-lg-12">' .
                        '<a class="block " href="#">' .
                        '<img src="winner.jpg" width="100%"><h6 class="box-title elltext">'.OurCampaign::find($myCampaign->CampaignID) -> CampaignName.'</h6>' .
                        '<h6 class="box-title elltext">' . $tillEnd . '</h6>' .
                        '<h6 class="box-title elltext" style="font-weight: 700;">' .
                        $special .
                        '<h6><p>Details:'.OurCampaign::find($myCampaign->CampaignID) -> CampaignNotes.'</p></h6>' .
                        
                        '</a>' .
                        '<input type="hidden" value="' . $eachQuest . '" class="getQuest_'.$myCampaign -> CampaignID.'">' .
                        ' <input type="hidden" value="' . $count . '" class="getCount_'.$myCampaign -> CampaignID.'">' .
                        ' </div>' .
                        '</div>' .
                        '</div>';
            }
        }
            }
            $campCount++;
            }
        //   return view("previousCampaign")->with(compact('latest', 'latest'));
    }
    else  if ($request->action == "notdone") {
           
            $special = "";
            $test = "";
          //  $checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
$checkInfo = usertable::where('facebook_id', '=', Session::get('logedIn'))->first();
            if ($checkInfo == null) {
                $checkInfo = usertable::where('id', '=', Session::get('logedIn'))->first();
            }

          //  $latest = DB::select("SELECT pm.* FROM campaign_questions pm Natural left join answer_records WHERE pm.CampaignID not in (SELECT pd.CampaignID FROM answer_records pd where UserID = " . $checkInfo->id . ") limit ".Session::get('campLimit'));
            $latest = DB::select("SELECT pm.* FROM campaign_questions pm Natural left join answer_records WHERE pm.CampaignID not in (SELECT pd.CampaignID FROM answer_records pd where UserID = " . $checkInfo->id.")");
          $count = 0;
           $allId = array();
           $campCount = 0;
            foreach ($latest as $myCampaign) {
                    $test = $test."entered";
               if($campCount <=  Session::get('campLimit')){
              //  $tillEnd = (Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->diffInDays(Carbon::now()->format('Y-m-d')) + 1) == 1 ? 'Campaign Ends today' : (Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->diffInDays(Carbon::now()->format('Y-m-d')) + 1) . ' Day(s) to end';
               $tillEnd = (Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->diffInDays(Carbon::now()) + 1) == 1 ? 'Campaign Ends today' : (Carbon::parse(OurCampaign::find($myCampaign->CampaignID)->EndDate)->diffInDays(Carbon::now()) + 1) . ' Day(s) to end';
                $specialPrize = SpecialPrize::where('CampignID', '=', $myCampaign->CampaignID)->first();
                if ($specialPrize) {
                    $special = '<h6>Special Prize: ' . $specialPrize->SpecialPrize . '</h6>';
                } else {
                    $special = '<h5>Special Prize: NIL</h5>';
                }
                            if(!in_array($myCampaign -> CampaignID, $allId)){
                           
                            $allId[] = $myCampaign->CampaignID;
                            $count++;
                           
                            $eachQuest = "<form method='post' id='form_" . $count . "' align='left'>";
                            csrf_field(); 
 $countForm = 0; 
                            foreach($latest as $getQuest){
                            if($getQuest -> CampaignID == $myCampaign -> CampaignID){

 $countForm++;
$eachQuest = $eachQuest . "<label>" . $getQuest->Question . "</label><br><input type='text' name='val_" . $countForm . "' class='form-control toDisable_" . $myCampaign->CampaignID . "' id='val_" . $countForm . "' /><br><br><input type='hidden' name='QuestID' class='Quest_" . $getQuest->id . "' value='" . $getQuest->id . "' />"; 

                            }
                           }
 $eachQuest = $eachQuest . "<input type='hidden' name='CampaigInfo' value='" . $myCampaign->CampaignID . "' /> </form>"; 
                           
                             if($count > 0 && Carbon::parse(OurCampaign::find($myCampaign->CampaignID) -> EndDate)->addDay() > Carbon::now()){
 $quotedRegStatus = "'".@$request->regStatus."'";
                               $quotedCampID = "'".@$myCampaign->CampaignID."'";
                $frames = $frames . ' <div class=" col-lg-4 campBoxes camp_'.$myCampaign -> CampaignID.' myClass" align="center"><div class=" ">' .
                        ' <div class="box--sml text-center flush-pd-trailer col-lg-12">' .
                        '<a class="block " href="#">' .
                        '<img src="winner.jpg" width="100%"><h6 class="box-title elltext">'.OurCampaign::find($myCampaign->CampaignID) -> CampaignName.'</h6>' .
                        '<h6 class="box-title elltext">' . $tillEnd . '</h6>' .
                        '<h6 class="box-title elltext" style="font-weight: 700;">' .
                        $special .
                        '<h6><p>Details:'.OurCampaign::find($myCampaign->CampaignID) -> CampaignNotes.'</p></h6>' .
                        '</h6><p class="push-sbit"><button class="btn btn-primary btn-xs campaign_count_'.$count.' QuestClass" onclick="checkReg(' . $quotedRegStatus . ', ' . $quotedCampID . ')">TAKE QUIZ</button></p>' .
                        '</a>' .
                        '<input type="hidden" value="' . $eachQuest . '" class="getQuest_'.$myCampaign -> CampaignID.'">' .
                        ' <input type="hidden" value="' . $count . '" class="getCount_'.$myCampaign -> CampaignID.'">' .
                        ' </div>' .
                        '</div>' .
                        '</div>';
            }
        }
            }
            $campCount++;
            }
        //   return view("previousCampaign")->with(compact('latest', 'latest'));
    }
    // return response()->json($latest);
    return response()->json($frames);
}


public function uploadDP()
{
  
     $input = Input::only('fupload');
      
     $ext = explode('.', $input['fupload']->getClientOriginalName())[1];
  // $dummyName =  updateDummyDP($input['fupload'], $ext);
   //  $input['fupload']->move(public_path('images'), Session::get('logedIn').'.'.$ext);



     $input['fupload']->move(public_path('images'), $input['fupload']->getClientOriginalName());
 //  
    return response()->json($input['fupload']->getClientOriginalName());
    // return response()->json($dummyName.'.'.$ext);
}


public function upDateDBColumn(Request $request)
{
     $getUser = usertable::where('id', '=', Session::get('logedIn'))->first();
     $ext = explode('.', $request->imgName)[1];
    $getUser->UserImg = Session::get('logedIn').'.'.$ext;
     $getUser->save();
     
    // $input['fupload']->move(public_path('images'), Session::get('logedIn').'.'.$ext);
   //  $request->imgName->move(public_path('images'), Session::get('logedIn').'.'.$ext);
     copy('public/images/'.$request->imgName, 'public/images/'.Session::get('logedIn').'.'.$ext);
    return response()->json(Session::get('logedIn').'.'.$ext);
}



  public function forgotPassword(Request $request) {
        try {
            //Find the user object from model if it exists
            //$user = user::findOrFail($id);
            $user = usertable::where('Email', '=', $request->confirmMail)->first();
            if (!$user) {
                return response()->json('Invalid Email');
            }
            
            $characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
             $string = '';
 $max = strlen($characters) - 1;
 for ($i = 0; $i < ($max*3); $i++) {
      $string .= $characters[mt_rand(0, $max)];
 }
            $linkPassword = rand(100000000, 999999999);
            $user->SentCode = $linkPassword.$string;
            $user->CodeTimeStart = Carbon::now();
            $user->save();
            
        @$headers = 'From: ' . "no-reply@winmoney.ng" . "\r\n";
        
          mail($request->confirmMail,"Winmoney Profile Reset Password","Clink the link below to change your password \r\n http://test.winmoney.ng/confirmation/".$linkPassword.$string, @$headers);
            
            return response()->json('Check Your Mail To Complete Process');

            // return Redirect::to('/successful')->with('UserName', $id);
        } catch (ModelNotFoundException $err) {
            //Show error page
            return response()->json('Error');
        }
    }

    public function confirmpassword($response) {
         
        $getUser = usertable::where('SentCode', '=', $response)->first();
        if (!$getUser) {
            return view("confirmpassword")->with('response', 'Link either expired or never existed');
        } else {
            $dateFromDatabase = strtotime((string) $getUser->CodeTimeStart);
            $dateFiveMinutesAgo = strtotime("-5 minutes");

            if ($dateFromDatabase >= $dateFiveMinutesAgo) {
                // less than 5minutes ago
               
                 return view("confirmpassword")->with('response', $response);
            } else {
                // more than 5minutes ago
                  return view("confirmpassword")->with('response', 'Link either expired or never existed');
            }
        }
    }
    
    public function savePassword(Request $request)
    {
    
     $user = usertable::where('SentCode', '=', $request->sentCode)->first();
     $user->Password = $request->first;
     $user->Confirm_Password = $request->second;
      $dateFromDatabase = strtotime((string)  $user->CodeTimeStart);
       $dateFiveMinutesAgo = strtotime("-5 minutes");
     $user->CodeTimeStart = $dateFiveMinutesAgo;
     $user->save();
     return response()->json('Success');
    }
    
    
    
    
    public function createCampaign()
    {
        $allCampaigns = DB::select("SELECT * FROM our_campaigns");
        $allClients = DB::select("SELECT * FROM clients");
        return view("CreateCampaign")
                ->with(compact('allCampaigns', $allCampaigns))
                 ->with(compact('allClients', $allClients));
    }
    
    public function saveQuestion(Request $request)
    {
        //concatenate responsedata
        $allOpts = "";
        foreach($request->ResponseType as $responseType)
        {
            $allOpts = $allOpts.$responseType.",";
        }
        
        $question = new CampaignQuestions();
        $question->Question = $request->Question;
        $question->ClientID = $request->ClientID;
        $question->CampaignID = $request->CampaignID;
        $question->ResponseData = $allOpts;
        $question->ResponseType = $request->myResponse;
        $question->ExpectedResponse = $request->responseExpected;
        $question->save();
        return response()->json("Successful");
    }

    
    public function newCampaign(Request $request)
    {
        
        $newCamp = new OurCampaign();
        $newCamp->ClientID = $request->allClients;
        $newCamp->CampaignName = $request->newCampaign;
        $newCamp->CampaignNotes = $request->campNotes;
        $newCamp->EndDate = $request->campEnd;
        $newCamp->StartDate = $request->campStart;
        $newCamp->save();
        
        $getId = OurCampaign::where('ClientID', '=', $request->allClients)
                ->where('CampaignName', '=', $request->newCampaign)
                ->where('CampaignNotes', '=', $request->campNotes)
                 ->where('EndDate', '=', $request->campEnd)
                ->first();
        
        return response()->json($getId->id);
        
    }
    
    public function AdminPage()
    {
        return view("CampaignCreate");
    }
    
    public function adminLogin()
    {
        return view("adminLogin");
    }
    
    public function adminAuth(Request $request)
    {
    
    $checkAdmin = adminList::where('Username','=',$request->username)
            ->where('Password','=',$request->password)
            ->first(); 
    if($checkAdmin)
    {
        return Redirect::to('/CampaignCreate');
    }
    
    return Redirect::to('/adminLogin')->with('message', 'Error');
    }
    
    public function CampaignCreate()
    {
          $allCampaigns = DB::select("SELECT * FROM our_campaigns");
        $allClients = DB::select("SELECT * FROM clients");
        return view("CampaignCreate")
                ->with(compact('allCampaigns', $allCampaigns))
                 ->with(compact('allClients', $allClients));
    }
    
    public function AddQuestion()
    {
         $allCampaigns = DB::select("SELECT * FROM our_campaigns");
        $allClients = DB::select("SELECT * FROM clients");
        return view("AddQuestion")
                ->with(compact('allCampaigns', $allCampaigns))
                 ->with(compact('allClients', $allClients));
       
    }
    
    public function Report()
    {
        return view("Report");
    }


}