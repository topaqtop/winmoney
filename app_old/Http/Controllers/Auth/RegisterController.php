<?php

namespace App\Http\Controllers\Auth;

use App\usertable;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Redirect;
//use Laravel\Socialite
use Session;
use Socialite;



class RegisterController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Register Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles the registration of new users as well as their
      | validation and creation. By default this controller uses a trait to
      | provide this functionality without requiring any additional code.
      |
     */

use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data) {
        return Validator::make($data, [
                    'name' => 'required|string|max:255',
                    'email' => 'required|string|email|max:255|unique:users',
                    'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data) {
        return User::create([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'password' => bcrypt($data['password']),
        ]);
    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider() {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback() {

        try {
          //  $socialUser = Socialite::driver('facebook')->user();
             /*$myUser = $socialUser->fields([
                    'name', 
                    'first_name', 
                    'last_name', 
                    'email', 
                    'gender', 
                    'verified'
                ]);*/
// retrieve the user
//$user = $driver->user();
           $socialUser = Socialite::driver('facebook')->user();
        } catch (\Exception $e) {
            return redirect('/');
        }

        $user = usertable::where('facebook_id', $socialUser->getId())->first();
        $getFbId = "";
        if (!$user){
          
            usertable::create([
                // 'facebook_id' => $socialUser->getId(),
                // 'name' => $socialUser->getName(),
                // 'email' => $socialUser->getEmail(),
                'facebook_id' => $socialUser->user['id'],
                'Surname' => $socialUser->user['name'],
                'Email' => $socialUser->user['email'],
                'Gender' => $socialUser->user['gender'],
               // 'Age_Range' => $socialUser->user['age']
            ]);
            
            
              $NigerianStates = array(
            'Abuja',
            'Abia',
            'Adamawa',
            'Akwa Ibom',
            'Anambra',
            'Bauchi',
            'Bayelsa',
            'Benue',
            'Borno',
            'Cross River',
            'Delta',
            'Ebonyi',
            'Edo',
            'Ekiti',
            'Enugu',
            'Gombe',
            'Imo',
            'Jigawa',
            'Kaduna',
            'Kano',
            'Katsina',
            'Kebbi',
            'Kogi',
            'Kwara',
            'Lagos',
            'Nassarawa',
            'Niger',
            'Ogun',
            'Ondo',
            'Osun',
            'Oyo',
            'Plateau',
            'Rivers',
            'Sokoto',
            'Taraba',
            'Yobe',
            'Zamfara',
        );
        $stateArray = "<option value=''>Your State Of Residence</option>";
        foreach ($NigerianStates as $k) {
            $stateArray = $stateArray . "<option value='" . $k . "'>" . $k . "</option>";
        }
            
             session()->put('id', $socialUser->getId());
       // return redirect()->to('/completeReg');
              session()->put('logedIn', $socialUser->getId());
            Session::put('logedIn', $socialUser->getId()); 
             Session::put('regStatus', 'Incomplete'); 
              Session::put('allStates', $stateArray); 
            return Redirect::to('/successful')->with('regStatus', 'Incomplete')->with('allStates', $stateArray);
            // return view("IndexPage")->with('regStatus', 'Incomplete')->with('allStates', $stateArray);
        }

       $confirmComplete = usertable::where('facebook_id', '=',$socialUser->user['id'])->where('Password', '!=', null)->where('Password', '!=', "")->first();
        if($confirmComplete){
             Session::put('regStatus', 'Complete'); 
            session()->put('logedIn', $confirmComplete->id);
            Session::put('logedIn', $confirmComplete->id); 
      return Redirect::to('/successful')->with('regStatus', 'Complete');
        }
        else
        {
             $NigerianStates = array(
            'Abuja',
            'Abia',
            'Adamawa',
            'Akwa Ibom',
            'Anambra',
            'Bauchi',
            'Bayelsa',
            'Benue',
            'Borno',
            'Cross River',
            'Delta',
            'Ebonyi',
            'Edo',
            'Ekiti',
            'Enugu',
            'Gombe',
            'Imo',
            'Jigawa',
            'Kaduna',
            'Kano',
            'Katsina',
            'Kebbi',
            'Kogi',
            'Kwara',
            'Lagos',
            'Nassarawa',
            'Niger',
            'Ogun',
            'Ondo',
            'Osun',
            'Oyo',
            'Plateau',
            'Rivers',
            'Sokoto',
            'Taraba',
            'Yobe',
            'Zamfara',
        );
        $stateArray = "<option value=''>Your State Of Residence</option>";
        foreach ($NigerianStates as $k) {
            $stateArray = $stateArray . "<option value='" . $k . "'>" . $k . "</option>";
        }
        
             session()->put('id', $socialUser->getId());
             session()->put('logedIn', $socialUser->getId());
            Session::put('logedIn', $socialUser->getId()); 
             Session::put('regStatus', 'Incomplete'); 
              Session::put('allStates', $stateArray); 
     // return view("IndexPage")->with('regStatus', 'Incomplete')->with('allStates', $stateArray);
             return Redirect::to('/successful')->with('regStatus', 'Incomplete')->with('allStates', $stateArray);
             //session()->put('id', $socialUser->getId());
             
             //return redirect()->to('/completeReg');
        }
        // auth() -> login($user);
       
        // $user->token;
    }

}
