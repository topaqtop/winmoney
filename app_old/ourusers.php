<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ourusers extends Model
{
    //
    protected  $fillable = ['facebook_id', 'Surname', 'Other_Name', 'Email', 'State', 'City', 'Gender', 'Age_Range', 'Phone_Number', 'Password', 'Confirm_Password', 'ReferredCode'];
}
