<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['web']], function(){
    
    //Route::get('/', function () {
   // return view('welcome');
//});
		
});

Route::get('/', 'ClientController@WinMoneyWebsite');	
			

Route::get('auth/facebook', 'Auth\RegisterController@redirectToProvider');
Route::get('auth/facebook/callback', 'Auth\RegisterController@handleProviderCallback');

Route::get('about', function(){
    $bitfumes = ['This', 'Is', 'Bitfumes'];
    return view('about', ['bitfumes' => $bitfumes]);
});

Route::get('UserSignUp', 'ClientController@ClientPage');
Route::get('Register', 'ClientController@RegisterPage');
Route::post('/user', 'ClientController@RegisterUser');
//Route::get('facebookGet', 'ClientController@fbPermission');
Route::get('facebookGet', 'ClientController@fbPermission');


Route::get('successful', 'ClientController@nowLoggedIn');

Route::get('completeReg', 'ClientController@CompleteRegPage');

Route::post('updateuser', 'ClientController@UpdateUserInfo');

Route::get('WinMoneyIndex', 'ClientController@WinMoneyWebsite');

Route::post('SignIn', 'ClientController@userSignin');

Route::get('logout', 'ClientController@userSignout');

Route::get('previousCampaign', 'ClientController@prevCamp');

Route::post('saveAnswer', 'ClientController@saveAnswer');

Route::get('alreadyAnswered', 'ClientController@alreadyAnswered');

Route::get('getCampaign', 'ClientController@getCampaign');

Route::get('getMore', 'ClientController@getMore');

Route::post('uploadDP', 'ClientController@uploadDP');

Route::get('upDateDBColumn', 'ClientController@upDateDBColumn');


Route::get('confirmation/{confirmcode}', 'ClientController@confirmpassword');

Route::get('forgotPassword', 'ClientController@forgotPassword');


Route::get('savePassword', 'ClientController@savePassword');


Route::get('createCampaign', 'ClientController@createCampaign');

Route::get('saveQuestion', 'ClientController@saveQuestion');

Route::get('newCampaign', 'ClientController@newCampaign');

Route::get('AdminPage', 'ClientController@AdminPage');

Route::get('adminLogin', 'ClientController@adminLogin');

Route::get('CampaignCreate', 'ClientController@CampaignCreate');

Route::get('AddQuestion', 'ClientController@AddQuestion');

Route::get('Report', 'ClientController@Report');

Route::post('adminAuth', 'ClientController@adminAuth');



Route::resource('myCRUD', 'resourceController');



Auth::routes();

Route::get('/home', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index');

